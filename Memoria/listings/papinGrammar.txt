Papin:
  `domainModelNSURI' + domainModelNSURI=EString
  `domainModelInstance' + domainModelInstance=EString
  datasets+=Dataset*;

Dataset:
  `dataset' name=ID
  `using' using=[dof::Effect|QualifiedName] `{'
     includedCategories+=IncludedCategory 
     (includedCategories+=IncludedCategory)*`}';

IncludedCategory:
  `include' cat=[dof::Category|QualifiedName] (`{'
  includedCauses+=IncludedCause`}')*;

IncludedCause:
  `include' causes+=[dof::Cause|QualifiedName] (`{'
  includedCauses+=IncludedCause`}')*;
	

