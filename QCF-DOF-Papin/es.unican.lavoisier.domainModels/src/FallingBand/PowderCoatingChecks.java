/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Powder Coating Checks</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.PowderCoatingChecks#getZone <em>Zone</em>}</li>
 *   <li>{@link FallingBand.PowderCoatingChecks#getThickness <em>Thickness</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getPowderCoatingChecks()
 * @model
 * @generated
 */
public interface PowderCoatingChecks extends EObject {
	/**
	 * Returns the value of the '<em><b>Zone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zone</em>' attribute.
	 * @see #setZone(String)
	 * @see FallingBand.FallingPackage#getPowderCoatingChecks_Zone()
	 * @model
	 * @generated
	 */
	String getZone();

	/**
	 * Sets the value of the '{@link FallingBand.PowderCoatingChecks#getZone <em>Zone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zone</em>' attribute.
	 * @see #getZone()
	 * @generated
	 */
	void setZone(String value);

	/**
	 * Returns the value of the '<em><b>Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thickness</em>' attribute.
	 * @see #setThickness(Double)
	 * @see FallingBand.FallingPackage#getPowderCoatingChecks_Thickness()
	 * @model
	 * @generated
	 */
	Double getThickness();

	/**
	 * Sets the value of the '{@link FallingBand.PowderCoatingChecks#getThickness <em>Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Thickness</em>' attribute.
	 * @see #getThickness()
	 * @generated
	 */
	void setThickness(Double value);

} // PowderCoatingChecks
