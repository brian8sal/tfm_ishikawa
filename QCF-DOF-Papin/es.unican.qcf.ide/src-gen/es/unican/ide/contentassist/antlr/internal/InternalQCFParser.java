package es.unican.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import es.unican.services.QCFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQCFParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'qcf'", "'effect'", "'description'", "'category'", "'cause'", "'valueOfInterest'", "'contains'", "'{'", "'}'", "'.'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalQCFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQCFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQCFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalQCF.g"; }


    	private QCFGrammarAccess grammarAccess;

    	public void setGrammarAccess(QCFGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleQCF"
    // InternalQCF.g:53:1: entryRuleQCF : ruleQCF EOF ;
    public final void entryRuleQCF() throws RecognitionException {
        try {
            // InternalQCF.g:54:1: ( ruleQCF EOF )
            // InternalQCF.g:55:1: ruleQCF EOF
            {
             before(grammarAccess.getQCFRule()); 
            pushFollow(FOLLOW_1);
            ruleQCF();

            state._fsp--;

             after(grammarAccess.getQCFRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQCF"


    // $ANTLR start "ruleQCF"
    // InternalQCF.g:62:1: ruleQCF : ( ( rule__QCF__Group__0 ) ) ;
    public final void ruleQCF() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:66:2: ( ( ( rule__QCF__Group__0 ) ) )
            // InternalQCF.g:67:2: ( ( rule__QCF__Group__0 ) )
            {
            // InternalQCF.g:67:2: ( ( rule__QCF__Group__0 ) )
            // InternalQCF.g:68:3: ( rule__QCF__Group__0 )
            {
             before(grammarAccess.getQCFAccess().getGroup()); 
            // InternalQCF.g:69:3: ( rule__QCF__Group__0 )
            // InternalQCF.g:69:4: rule__QCF__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QCF__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQCFAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQCF"


    // $ANTLR start "entryRuleEffect"
    // InternalQCF.g:78:1: entryRuleEffect : ruleEffect EOF ;
    public final void entryRuleEffect() throws RecognitionException {
        try {
            // InternalQCF.g:79:1: ( ruleEffect EOF )
            // InternalQCF.g:80:1: ruleEffect EOF
            {
             before(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getEffectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalQCF.g:87:1: ruleEffect : ( ( rule__Effect__Group__0 ) ) ;
    public final void ruleEffect() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:91:2: ( ( ( rule__Effect__Group__0 ) ) )
            // InternalQCF.g:92:2: ( ( rule__Effect__Group__0 ) )
            {
            // InternalQCF.g:92:2: ( ( rule__Effect__Group__0 ) )
            // InternalQCF.g:93:3: ( rule__Effect__Group__0 )
            {
             before(grammarAccess.getEffectAccess().getGroup()); 
            // InternalQCF.g:94:3: ( rule__Effect__Group__0 )
            // InternalQCF.g:94:4: rule__Effect__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalQCF.g:103:1: entryRuleCategory : ruleCategory EOF ;
    public final void entryRuleCategory() throws RecognitionException {
        try {
            // InternalQCF.g:104:1: ( ruleCategory EOF )
            // InternalQCF.g:105:1: ruleCategory EOF
            {
             before(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getCategoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalQCF.g:112:1: ruleCategory : ( ( rule__Category__Group__0 ) ) ;
    public final void ruleCategory() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:116:2: ( ( ( rule__Category__Group__0 ) ) )
            // InternalQCF.g:117:2: ( ( rule__Category__Group__0 ) )
            {
            // InternalQCF.g:117:2: ( ( rule__Category__Group__0 ) )
            // InternalQCF.g:118:3: ( rule__Category__Group__0 )
            {
             before(grammarAccess.getCategoryAccess().getGroup()); 
            // InternalQCF.g:119:3: ( rule__Category__Group__0 )
            // InternalQCF.g:119:4: rule__Category__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleCause"
    // InternalQCF.g:128:1: entryRuleCause : ruleCause EOF ;
    public final void entryRuleCause() throws RecognitionException {
        try {
            // InternalQCF.g:129:1: ( ruleCause EOF )
            // InternalQCF.g:130:1: ruleCause EOF
            {
             before(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalQCF.g:137:1: ruleCause : ( ( rule__Cause__Group__0 ) ) ;
    public final void ruleCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:141:2: ( ( ( rule__Cause__Group__0 ) ) )
            // InternalQCF.g:142:2: ( ( rule__Cause__Group__0 ) )
            {
            // InternalQCF.g:142:2: ( ( rule__Cause__Group__0 ) )
            // InternalQCF.g:143:3: ( rule__Cause__Group__0 )
            {
             before(grammarAccess.getCauseAccess().getGroup()); 
            // InternalQCF.g:144:3: ( rule__Cause__Group__0 )
            // InternalQCF.g:144:4: rule__Cause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleEString"
    // InternalQCF.g:153:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalQCF.g:154:1: ( ruleEString EOF )
            // InternalQCF.g:155:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalQCF.g:162:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:166:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalQCF.g:167:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalQCF.g:167:2: ( ( rule__EString__Alternatives ) )
            // InternalQCF.g:168:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalQCF.g:169:3: ( rule__EString__Alternatives )
            // InternalQCF.g:169:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalQCF.g:178:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalQCF.g:179:1: ( ruleQualifiedName EOF )
            // InternalQCF.g:180:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalQCF.g:187:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:191:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalQCF.g:192:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalQCF.g:192:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalQCF.g:193:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalQCF.g:194:3: ( rule__QualifiedName__Group__0 )
            // InternalQCF.g:194:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalQCF.g:202:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:206:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalQCF.g:207:2: ( RULE_STRING )
                    {
                    // InternalQCF.g:207:2: ( RULE_STRING )
                    // InternalQCF.g:208:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalQCF.g:213:2: ( RULE_ID )
                    {
                    // InternalQCF.g:213:2: ( RULE_ID )
                    // InternalQCF.g:214:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__QCF__Group__0"
    // InternalQCF.g:223:1: rule__QCF__Group__0 : rule__QCF__Group__0__Impl rule__QCF__Group__1 ;
    public final void rule__QCF__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:227:1: ( rule__QCF__Group__0__Impl rule__QCF__Group__1 )
            // InternalQCF.g:228:2: rule__QCF__Group__0__Impl rule__QCF__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__QCF__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QCF__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__0"


    // $ANTLR start "rule__QCF__Group__0__Impl"
    // InternalQCF.g:235:1: rule__QCF__Group__0__Impl : ( 'qcf' ) ;
    public final void rule__QCF__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:239:1: ( ( 'qcf' ) )
            // InternalQCF.g:240:1: ( 'qcf' )
            {
            // InternalQCF.g:240:1: ( 'qcf' )
            // InternalQCF.g:241:2: 'qcf'
            {
             before(grammarAccess.getQCFAccess().getQcfKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getQCFAccess().getQcfKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__0__Impl"


    // $ANTLR start "rule__QCF__Group__1"
    // InternalQCF.g:250:1: rule__QCF__Group__1 : rule__QCF__Group__1__Impl rule__QCF__Group__2 ;
    public final void rule__QCF__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:254:1: ( rule__QCF__Group__1__Impl rule__QCF__Group__2 )
            // InternalQCF.g:255:2: rule__QCF__Group__1__Impl rule__QCF__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__QCF__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QCF__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__1"


    // $ANTLR start "rule__QCF__Group__1__Impl"
    // InternalQCF.g:262:1: rule__QCF__Group__1__Impl : ( ( rule__QCF__NameAssignment_1 ) ) ;
    public final void rule__QCF__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:266:1: ( ( ( rule__QCF__NameAssignment_1 ) ) )
            // InternalQCF.g:267:1: ( ( rule__QCF__NameAssignment_1 ) )
            {
            // InternalQCF.g:267:1: ( ( rule__QCF__NameAssignment_1 ) )
            // InternalQCF.g:268:2: ( rule__QCF__NameAssignment_1 )
            {
             before(grammarAccess.getQCFAccess().getNameAssignment_1()); 
            // InternalQCF.g:269:2: ( rule__QCF__NameAssignment_1 )
            // InternalQCF.g:269:3: rule__QCF__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QCF__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQCFAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__1__Impl"


    // $ANTLR start "rule__QCF__Group__2"
    // InternalQCF.g:277:1: rule__QCF__Group__2 : rule__QCF__Group__2__Impl ;
    public final void rule__QCF__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:281:1: ( rule__QCF__Group__2__Impl )
            // InternalQCF.g:282:2: rule__QCF__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QCF__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__2"


    // $ANTLR start "rule__QCF__Group__2__Impl"
    // InternalQCF.g:288:1: rule__QCF__Group__2__Impl : ( ( rule__QCF__EffectsAssignment_2 ) ) ;
    public final void rule__QCF__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:292:1: ( ( ( rule__QCF__EffectsAssignment_2 ) ) )
            // InternalQCF.g:293:1: ( ( rule__QCF__EffectsAssignment_2 ) )
            {
            // InternalQCF.g:293:1: ( ( rule__QCF__EffectsAssignment_2 ) )
            // InternalQCF.g:294:2: ( rule__QCF__EffectsAssignment_2 )
            {
             before(grammarAccess.getQCFAccess().getEffectsAssignment_2()); 
            // InternalQCF.g:295:2: ( rule__QCF__EffectsAssignment_2 )
            // InternalQCF.g:295:3: rule__QCF__EffectsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QCF__EffectsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQCFAccess().getEffectsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__Group__2__Impl"


    // $ANTLR start "rule__Effect__Group__0"
    // InternalQCF.g:304:1: rule__Effect__Group__0 : rule__Effect__Group__0__Impl rule__Effect__Group__1 ;
    public final void rule__Effect__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:308:1: ( rule__Effect__Group__0__Impl rule__Effect__Group__1 )
            // InternalQCF.g:309:2: rule__Effect__Group__0__Impl rule__Effect__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Effect__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0"


    // $ANTLR start "rule__Effect__Group__0__Impl"
    // InternalQCF.g:316:1: rule__Effect__Group__0__Impl : ( () ) ;
    public final void rule__Effect__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:320:1: ( ( () ) )
            // InternalQCF.g:321:1: ( () )
            {
            // InternalQCF.g:321:1: ( () )
            // InternalQCF.g:322:2: ()
            {
             before(grammarAccess.getEffectAccess().getEffectAction_0()); 
            // InternalQCF.g:323:2: ()
            // InternalQCF.g:323:3: 
            {
            }

             after(grammarAccess.getEffectAccess().getEffectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0__Impl"


    // $ANTLR start "rule__Effect__Group__1"
    // InternalQCF.g:331:1: rule__Effect__Group__1 : rule__Effect__Group__1__Impl rule__Effect__Group__2 ;
    public final void rule__Effect__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:335:1: ( rule__Effect__Group__1__Impl rule__Effect__Group__2 )
            // InternalQCF.g:336:2: rule__Effect__Group__1__Impl rule__Effect__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Effect__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1"


    // $ANTLR start "rule__Effect__Group__1__Impl"
    // InternalQCF.g:343:1: rule__Effect__Group__1__Impl : ( 'effect' ) ;
    public final void rule__Effect__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:347:1: ( ( 'effect' ) )
            // InternalQCF.g:348:1: ( 'effect' )
            {
            // InternalQCF.g:348:1: ( 'effect' )
            // InternalQCF.g:349:2: 'effect'
            {
             before(grammarAccess.getEffectAccess().getEffectKeyword_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getEffectKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__2"
    // InternalQCF.g:358:1: rule__Effect__Group__2 : rule__Effect__Group__2__Impl rule__Effect__Group__3 ;
    public final void rule__Effect__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:362:1: ( rule__Effect__Group__2__Impl rule__Effect__Group__3 )
            // InternalQCF.g:363:2: rule__Effect__Group__2__Impl rule__Effect__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Effect__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2"


    // $ANTLR start "rule__Effect__Group__2__Impl"
    // InternalQCF.g:370:1: rule__Effect__Group__2__Impl : ( ( rule__Effect__NameAssignment_2 ) ) ;
    public final void rule__Effect__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:374:1: ( ( ( rule__Effect__NameAssignment_2 ) ) )
            // InternalQCF.g:375:1: ( ( rule__Effect__NameAssignment_2 ) )
            {
            // InternalQCF.g:375:1: ( ( rule__Effect__NameAssignment_2 ) )
            // InternalQCF.g:376:2: ( rule__Effect__NameAssignment_2 )
            {
             before(grammarAccess.getEffectAccess().getNameAssignment_2()); 
            // InternalQCF.g:377:2: ( rule__Effect__NameAssignment_2 )
            // InternalQCF.g:377:3: rule__Effect__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Effect__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2__Impl"


    // $ANTLR start "rule__Effect__Group__3"
    // InternalQCF.g:385:1: rule__Effect__Group__3 : rule__Effect__Group__3__Impl rule__Effect__Group__4 ;
    public final void rule__Effect__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:389:1: ( rule__Effect__Group__3__Impl rule__Effect__Group__4 )
            // InternalQCF.g:390:2: rule__Effect__Group__3__Impl rule__Effect__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Effect__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3"


    // $ANTLR start "rule__Effect__Group__3__Impl"
    // InternalQCF.g:397:1: rule__Effect__Group__3__Impl : ( ( rule__Effect__Group_3__0 )? ) ;
    public final void rule__Effect__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:401:1: ( ( ( rule__Effect__Group_3__0 )? ) )
            // InternalQCF.g:402:1: ( ( rule__Effect__Group_3__0 )? )
            {
            // InternalQCF.g:402:1: ( ( rule__Effect__Group_3__0 )? )
            // InternalQCF.g:403:2: ( rule__Effect__Group_3__0 )?
            {
             before(grammarAccess.getEffectAccess().getGroup_3()); 
            // InternalQCF.g:404:2: ( rule__Effect__Group_3__0 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalQCF.g:404:3: rule__Effect__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Effect__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEffectAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3__Impl"


    // $ANTLR start "rule__Effect__Group__4"
    // InternalQCF.g:412:1: rule__Effect__Group__4 : rule__Effect__Group__4__Impl ;
    public final void rule__Effect__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:416:1: ( rule__Effect__Group__4__Impl )
            // InternalQCF.g:417:2: rule__Effect__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4"


    // $ANTLR start "rule__Effect__Group__4__Impl"
    // InternalQCF.g:423:1: rule__Effect__Group__4__Impl : ( ( ( rule__Effect__CategoriesAssignment_4 ) ) ( ( rule__Effect__CategoriesAssignment_4 )* ) ) ;
    public final void rule__Effect__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:427:1: ( ( ( ( rule__Effect__CategoriesAssignment_4 ) ) ( ( rule__Effect__CategoriesAssignment_4 )* ) ) )
            // InternalQCF.g:428:1: ( ( ( rule__Effect__CategoriesAssignment_4 ) ) ( ( rule__Effect__CategoriesAssignment_4 )* ) )
            {
            // InternalQCF.g:428:1: ( ( ( rule__Effect__CategoriesAssignment_4 ) ) ( ( rule__Effect__CategoriesAssignment_4 )* ) )
            // InternalQCF.g:429:2: ( ( rule__Effect__CategoriesAssignment_4 ) ) ( ( rule__Effect__CategoriesAssignment_4 )* )
            {
            // InternalQCF.g:429:2: ( ( rule__Effect__CategoriesAssignment_4 ) )
            // InternalQCF.g:430:3: ( rule__Effect__CategoriesAssignment_4 )
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_4()); 
            // InternalQCF.g:431:3: ( rule__Effect__CategoriesAssignment_4 )
            // InternalQCF.g:431:4: rule__Effect__CategoriesAssignment_4
            {
            pushFollow(FOLLOW_7);
            rule__Effect__CategoriesAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_4()); 

            }

            // InternalQCF.g:434:2: ( ( rule__Effect__CategoriesAssignment_4 )* )
            // InternalQCF.g:435:3: ( rule__Effect__CategoriesAssignment_4 )*
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_4()); 
            // InternalQCF.g:436:3: ( rule__Effect__CategoriesAssignment_4 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==14) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalQCF.g:436:4: rule__Effect__CategoriesAssignment_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Effect__CategoriesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4__Impl"


    // $ANTLR start "rule__Effect__Group_3__0"
    // InternalQCF.g:446:1: rule__Effect__Group_3__0 : rule__Effect__Group_3__0__Impl rule__Effect__Group_3__1 ;
    public final void rule__Effect__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:450:1: ( rule__Effect__Group_3__0__Impl rule__Effect__Group_3__1 )
            // InternalQCF.g:451:2: rule__Effect__Group_3__0__Impl rule__Effect__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Effect__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__0"


    // $ANTLR start "rule__Effect__Group_3__0__Impl"
    // InternalQCF.g:458:1: rule__Effect__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Effect__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:462:1: ( ( 'description' ) )
            // InternalQCF.g:463:1: ( 'description' )
            {
            // InternalQCF.g:463:1: ( 'description' )
            // InternalQCF.g:464:2: 'description'
            {
             before(grammarAccess.getEffectAccess().getDescriptionKeyword_3_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__0__Impl"


    // $ANTLR start "rule__Effect__Group_3__1"
    // InternalQCF.g:473:1: rule__Effect__Group_3__1 : rule__Effect__Group_3__1__Impl ;
    public final void rule__Effect__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:477:1: ( rule__Effect__Group_3__1__Impl )
            // InternalQCF.g:478:2: rule__Effect__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__1"


    // $ANTLR start "rule__Effect__Group_3__1__Impl"
    // InternalQCF.g:484:1: rule__Effect__Group_3__1__Impl : ( ( rule__Effect__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Effect__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:488:1: ( ( ( rule__Effect__DescriptionAssignment_3_1 ) ) )
            // InternalQCF.g:489:1: ( ( rule__Effect__DescriptionAssignment_3_1 ) )
            {
            // InternalQCF.g:489:1: ( ( rule__Effect__DescriptionAssignment_3_1 ) )
            // InternalQCF.g:490:2: ( rule__Effect__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getEffectAccess().getDescriptionAssignment_3_1()); 
            // InternalQCF.g:491:2: ( rule__Effect__DescriptionAssignment_3_1 )
            // InternalQCF.g:491:3: rule__Effect__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Effect__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group_3__1__Impl"


    // $ANTLR start "rule__Category__Group__0"
    // InternalQCF.g:500:1: rule__Category__Group__0 : rule__Category__Group__0__Impl rule__Category__Group__1 ;
    public final void rule__Category__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:504:1: ( rule__Category__Group__0__Impl rule__Category__Group__1 )
            // InternalQCF.g:505:2: rule__Category__Group__0__Impl rule__Category__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Category__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0"


    // $ANTLR start "rule__Category__Group__0__Impl"
    // InternalQCF.g:512:1: rule__Category__Group__0__Impl : ( () ) ;
    public final void rule__Category__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:516:1: ( ( () ) )
            // InternalQCF.g:517:1: ( () )
            {
            // InternalQCF.g:517:1: ( () )
            // InternalQCF.g:518:2: ()
            {
             before(grammarAccess.getCategoryAccess().getCategoryAction_0()); 
            // InternalQCF.g:519:2: ()
            // InternalQCF.g:519:3: 
            {
            }

             after(grammarAccess.getCategoryAccess().getCategoryAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0__Impl"


    // $ANTLR start "rule__Category__Group__1"
    // InternalQCF.g:527:1: rule__Category__Group__1 : rule__Category__Group__1__Impl rule__Category__Group__2 ;
    public final void rule__Category__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:531:1: ( rule__Category__Group__1__Impl rule__Category__Group__2 )
            // InternalQCF.g:532:2: rule__Category__Group__1__Impl rule__Category__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Category__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1"


    // $ANTLR start "rule__Category__Group__1__Impl"
    // InternalQCF.g:539:1: rule__Category__Group__1__Impl : ( 'category' ) ;
    public final void rule__Category__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:543:1: ( ( 'category' ) )
            // InternalQCF.g:544:1: ( 'category' )
            {
            // InternalQCF.g:544:1: ( 'category' )
            // InternalQCF.g:545:2: 'category'
            {
             before(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1__Impl"


    // $ANTLR start "rule__Category__Group__2"
    // InternalQCF.g:554:1: rule__Category__Group__2 : rule__Category__Group__2__Impl rule__Category__Group__3 ;
    public final void rule__Category__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:558:1: ( rule__Category__Group__2__Impl rule__Category__Group__3 )
            // InternalQCF.g:559:2: rule__Category__Group__2__Impl rule__Category__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Category__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2"


    // $ANTLR start "rule__Category__Group__2__Impl"
    // InternalQCF.g:566:1: rule__Category__Group__2__Impl : ( ( rule__Category__NameAssignment_2 ) ) ;
    public final void rule__Category__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:570:1: ( ( ( rule__Category__NameAssignment_2 ) ) )
            // InternalQCF.g:571:1: ( ( rule__Category__NameAssignment_2 ) )
            {
            // InternalQCF.g:571:1: ( ( rule__Category__NameAssignment_2 ) )
            // InternalQCF.g:572:2: ( rule__Category__NameAssignment_2 )
            {
             before(grammarAccess.getCategoryAccess().getNameAssignment_2()); 
            // InternalQCF.g:573:2: ( rule__Category__NameAssignment_2 )
            // InternalQCF.g:573:3: rule__Category__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Category__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2__Impl"


    // $ANTLR start "rule__Category__Group__3"
    // InternalQCF.g:581:1: rule__Category__Group__3 : rule__Category__Group__3__Impl rule__Category__Group__4 ;
    public final void rule__Category__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:585:1: ( rule__Category__Group__3__Impl rule__Category__Group__4 )
            // InternalQCF.g:586:2: rule__Category__Group__3__Impl rule__Category__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Category__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3"


    // $ANTLR start "rule__Category__Group__3__Impl"
    // InternalQCF.g:593:1: rule__Category__Group__3__Impl : ( ( rule__Category__Group_3__0 )? ) ;
    public final void rule__Category__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:597:1: ( ( ( rule__Category__Group_3__0 )? ) )
            // InternalQCF.g:598:1: ( ( rule__Category__Group_3__0 )? )
            {
            // InternalQCF.g:598:1: ( ( rule__Category__Group_3__0 )? )
            // InternalQCF.g:599:2: ( rule__Category__Group_3__0 )?
            {
             before(grammarAccess.getCategoryAccess().getGroup_3()); 
            // InternalQCF.g:600:2: ( rule__Category__Group_3__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==13) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalQCF.g:600:3: rule__Category__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Category__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCategoryAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3__Impl"


    // $ANTLR start "rule__Category__Group__4"
    // InternalQCF.g:608:1: rule__Category__Group__4 : rule__Category__Group__4__Impl ;
    public final void rule__Category__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:612:1: ( rule__Category__Group__4__Impl )
            // InternalQCF.g:613:2: rule__Category__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4"


    // $ANTLR start "rule__Category__Group__4__Impl"
    // InternalQCF.g:619:1: rule__Category__Group__4__Impl : ( ( ( rule__Category__CausesAssignment_4 ) ) ( ( rule__Category__CausesAssignment_4 )* ) ) ;
    public final void rule__Category__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:623:1: ( ( ( ( rule__Category__CausesAssignment_4 ) ) ( ( rule__Category__CausesAssignment_4 )* ) ) )
            // InternalQCF.g:624:1: ( ( ( rule__Category__CausesAssignment_4 ) ) ( ( rule__Category__CausesAssignment_4 )* ) )
            {
            // InternalQCF.g:624:1: ( ( ( rule__Category__CausesAssignment_4 ) ) ( ( rule__Category__CausesAssignment_4 )* ) )
            // InternalQCF.g:625:2: ( ( rule__Category__CausesAssignment_4 ) ) ( ( rule__Category__CausesAssignment_4 )* )
            {
            // InternalQCF.g:625:2: ( ( rule__Category__CausesAssignment_4 ) )
            // InternalQCF.g:626:3: ( rule__Category__CausesAssignment_4 )
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 
            // InternalQCF.g:627:3: ( rule__Category__CausesAssignment_4 )
            // InternalQCF.g:627:4: rule__Category__CausesAssignment_4
            {
            pushFollow(FOLLOW_9);
            rule__Category__CausesAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 

            }

            // InternalQCF.g:630:2: ( ( rule__Category__CausesAssignment_4 )* )
            // InternalQCF.g:631:3: ( rule__Category__CausesAssignment_4 )*
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 
            // InternalQCF.g:632:3: ( rule__Category__CausesAssignment_4 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalQCF.g:632:4: rule__Category__CausesAssignment_4
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Category__CausesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4__Impl"


    // $ANTLR start "rule__Category__Group_3__0"
    // InternalQCF.g:642:1: rule__Category__Group_3__0 : rule__Category__Group_3__0__Impl rule__Category__Group_3__1 ;
    public final void rule__Category__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:646:1: ( rule__Category__Group_3__0__Impl rule__Category__Group_3__1 )
            // InternalQCF.g:647:2: rule__Category__Group_3__0__Impl rule__Category__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Category__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__0"


    // $ANTLR start "rule__Category__Group_3__0__Impl"
    // InternalQCF.g:654:1: rule__Category__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Category__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:658:1: ( ( 'description' ) )
            // InternalQCF.g:659:1: ( 'description' )
            {
            // InternalQCF.g:659:1: ( 'description' )
            // InternalQCF.g:660:2: 'description'
            {
             before(grammarAccess.getCategoryAccess().getDescriptionKeyword_3_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__0__Impl"


    // $ANTLR start "rule__Category__Group_3__1"
    // InternalQCF.g:669:1: rule__Category__Group_3__1 : rule__Category__Group_3__1__Impl ;
    public final void rule__Category__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:673:1: ( rule__Category__Group_3__1__Impl )
            // InternalQCF.g:674:2: rule__Category__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__1"


    // $ANTLR start "rule__Category__Group_3__1__Impl"
    // InternalQCF.g:680:1: rule__Category__Group_3__1__Impl : ( ( rule__Category__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Category__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:684:1: ( ( ( rule__Category__DescriptionAssignment_3_1 ) ) )
            // InternalQCF.g:685:1: ( ( rule__Category__DescriptionAssignment_3_1 ) )
            {
            // InternalQCF.g:685:1: ( ( rule__Category__DescriptionAssignment_3_1 ) )
            // InternalQCF.g:686:2: ( rule__Category__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getCategoryAccess().getDescriptionAssignment_3_1()); 
            // InternalQCF.g:687:2: ( rule__Category__DescriptionAssignment_3_1 )
            // InternalQCF.g:687:3: rule__Category__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Category__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_3__1__Impl"


    // $ANTLR start "rule__Cause__Group__0"
    // InternalQCF.g:696:1: rule__Cause__Group__0 : rule__Cause__Group__0__Impl rule__Cause__Group__1 ;
    public final void rule__Cause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:700:1: ( rule__Cause__Group__0__Impl rule__Cause__Group__1 )
            // InternalQCF.g:701:2: rule__Cause__Group__0__Impl rule__Cause__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Cause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__0"


    // $ANTLR start "rule__Cause__Group__0__Impl"
    // InternalQCF.g:708:1: rule__Cause__Group__0__Impl : ( () ) ;
    public final void rule__Cause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:712:1: ( ( () ) )
            // InternalQCF.g:713:1: ( () )
            {
            // InternalQCF.g:713:1: ( () )
            // InternalQCF.g:714:2: ()
            {
             before(grammarAccess.getCauseAccess().getCauseAction_0()); 
            // InternalQCF.g:715:2: ()
            // InternalQCF.g:715:3: 
            {
            }

             after(grammarAccess.getCauseAccess().getCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__0__Impl"


    // $ANTLR start "rule__Cause__Group__1"
    // InternalQCF.g:723:1: rule__Cause__Group__1 : rule__Cause__Group__1__Impl rule__Cause__Group__2 ;
    public final void rule__Cause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:727:1: ( rule__Cause__Group__1__Impl rule__Cause__Group__2 )
            // InternalQCF.g:728:2: rule__Cause__Group__1__Impl rule__Cause__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Cause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__1"


    // $ANTLR start "rule__Cause__Group__1__Impl"
    // InternalQCF.g:735:1: rule__Cause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__Cause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:739:1: ( ( 'cause' ) )
            // InternalQCF.g:740:1: ( 'cause' )
            {
            // InternalQCF.g:740:1: ( 'cause' )
            // InternalQCF.g:741:2: 'cause'
            {
             before(grammarAccess.getCauseAccess().getCauseKeyword_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__1__Impl"


    // $ANTLR start "rule__Cause__Group__2"
    // InternalQCF.g:750:1: rule__Cause__Group__2 : rule__Cause__Group__2__Impl rule__Cause__Group__3 ;
    public final void rule__Cause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:754:1: ( rule__Cause__Group__2__Impl rule__Cause__Group__3 )
            // InternalQCF.g:755:2: rule__Cause__Group__2__Impl rule__Cause__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Cause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__2"


    // $ANTLR start "rule__Cause__Group__2__Impl"
    // InternalQCF.g:762:1: rule__Cause__Group__2__Impl : ( ( rule__Cause__NameAssignment_2 ) ) ;
    public final void rule__Cause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:766:1: ( ( ( rule__Cause__NameAssignment_2 ) ) )
            // InternalQCF.g:767:1: ( ( rule__Cause__NameAssignment_2 ) )
            {
            // InternalQCF.g:767:1: ( ( rule__Cause__NameAssignment_2 ) )
            // InternalQCF.g:768:2: ( rule__Cause__NameAssignment_2 )
            {
             before(grammarAccess.getCauseAccess().getNameAssignment_2()); 
            // InternalQCF.g:769:2: ( rule__Cause__NameAssignment_2 )
            // InternalQCF.g:769:3: rule__Cause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Cause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__2__Impl"


    // $ANTLR start "rule__Cause__Group__3"
    // InternalQCF.g:777:1: rule__Cause__Group__3 : rule__Cause__Group__3__Impl rule__Cause__Group__4 ;
    public final void rule__Cause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:781:1: ( rule__Cause__Group__3__Impl rule__Cause__Group__4 )
            // InternalQCF.g:782:2: rule__Cause__Group__3__Impl rule__Cause__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__Cause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__3"


    // $ANTLR start "rule__Cause__Group__3__Impl"
    // InternalQCF.g:789:1: rule__Cause__Group__3__Impl : ( ( rule__Cause__Group_3__0 )? ) ;
    public final void rule__Cause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:793:1: ( ( ( rule__Cause__Group_3__0 )? ) )
            // InternalQCF.g:794:1: ( ( rule__Cause__Group_3__0 )? )
            {
            // InternalQCF.g:794:1: ( ( rule__Cause__Group_3__0 )? )
            // InternalQCF.g:795:2: ( rule__Cause__Group_3__0 )?
            {
             before(grammarAccess.getCauseAccess().getGroup_3()); 
            // InternalQCF.g:796:2: ( rule__Cause__Group_3__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==13) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalQCF.g:796:3: rule__Cause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__3__Impl"


    // $ANTLR start "rule__Cause__Group__4"
    // InternalQCF.g:804:1: rule__Cause__Group__4 : rule__Cause__Group__4__Impl rule__Cause__Group__5 ;
    public final void rule__Cause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:808:1: ( rule__Cause__Group__4__Impl rule__Cause__Group__5 )
            // InternalQCF.g:809:2: rule__Cause__Group__4__Impl rule__Cause__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__Cause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__4"


    // $ANTLR start "rule__Cause__Group__4__Impl"
    // InternalQCF.g:816:1: rule__Cause__Group__4__Impl : ( ( rule__Cause__Group_4__0 )? ) ;
    public final void rule__Cause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:820:1: ( ( ( rule__Cause__Group_4__0 )? ) )
            // InternalQCF.g:821:1: ( ( rule__Cause__Group_4__0 )? )
            {
            // InternalQCF.g:821:1: ( ( rule__Cause__Group_4__0 )? )
            // InternalQCF.g:822:2: ( rule__Cause__Group_4__0 )?
            {
             before(grammarAccess.getCauseAccess().getGroup_4()); 
            // InternalQCF.g:823:2: ( rule__Cause__Group_4__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==16) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalQCF.g:823:3: rule__Cause__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cause__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCauseAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__4__Impl"


    // $ANTLR start "rule__Cause__Group__5"
    // InternalQCF.g:831:1: rule__Cause__Group__5 : rule__Cause__Group__5__Impl ;
    public final void rule__Cause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:835:1: ( rule__Cause__Group__5__Impl )
            // InternalQCF.g:836:2: rule__Cause__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__5"


    // $ANTLR start "rule__Cause__Group__5__Impl"
    // InternalQCF.g:842:1: rule__Cause__Group__5__Impl : ( ( rule__Cause__Group_5__0 )? ) ;
    public final void rule__Cause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:846:1: ( ( ( rule__Cause__Group_5__0 )? ) )
            // InternalQCF.g:847:1: ( ( rule__Cause__Group_5__0 )? )
            {
            // InternalQCF.g:847:1: ( ( rule__Cause__Group_5__0 )? )
            // InternalQCF.g:848:2: ( rule__Cause__Group_5__0 )?
            {
             before(grammarAccess.getCauseAccess().getGroup_5()); 
            // InternalQCF.g:849:2: ( rule__Cause__Group_5__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==17) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalQCF.g:849:3: rule__Cause__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Cause__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCauseAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group__5__Impl"


    // $ANTLR start "rule__Cause__Group_3__0"
    // InternalQCF.g:858:1: rule__Cause__Group_3__0 : rule__Cause__Group_3__0__Impl rule__Cause__Group_3__1 ;
    public final void rule__Cause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:862:1: ( rule__Cause__Group_3__0__Impl rule__Cause__Group_3__1 )
            // InternalQCF.g:863:2: rule__Cause__Group_3__0__Impl rule__Cause__Group_3__1
            {
            pushFollow(FOLLOW_5);
            rule__Cause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__0"


    // $ANTLR start "rule__Cause__Group_3__0__Impl"
    // InternalQCF.g:870:1: rule__Cause__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Cause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:874:1: ( ( 'description' ) )
            // InternalQCF.g:875:1: ( 'description' )
            {
            // InternalQCF.g:875:1: ( 'description' )
            // InternalQCF.g:876:2: 'description'
            {
             before(grammarAccess.getCauseAccess().getDescriptionKeyword_3_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__0__Impl"


    // $ANTLR start "rule__Cause__Group_3__1"
    // InternalQCF.g:885:1: rule__Cause__Group_3__1 : rule__Cause__Group_3__1__Impl ;
    public final void rule__Cause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:889:1: ( rule__Cause__Group_3__1__Impl )
            // InternalQCF.g:890:2: rule__Cause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__1"


    // $ANTLR start "rule__Cause__Group_3__1__Impl"
    // InternalQCF.g:896:1: rule__Cause__Group_3__1__Impl : ( ( rule__Cause__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Cause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:900:1: ( ( ( rule__Cause__DescriptionAssignment_3_1 ) ) )
            // InternalQCF.g:901:1: ( ( rule__Cause__DescriptionAssignment_3_1 ) )
            {
            // InternalQCF.g:901:1: ( ( rule__Cause__DescriptionAssignment_3_1 ) )
            // InternalQCF.g:902:2: ( rule__Cause__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getCauseAccess().getDescriptionAssignment_3_1()); 
            // InternalQCF.g:903:2: ( rule__Cause__DescriptionAssignment_3_1 )
            // InternalQCF.g:903:3: rule__Cause__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Cause__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_3__1__Impl"


    // $ANTLR start "rule__Cause__Group_4__0"
    // InternalQCF.g:912:1: rule__Cause__Group_4__0 : rule__Cause__Group_4__0__Impl rule__Cause__Group_4__1 ;
    public final void rule__Cause__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:916:1: ( rule__Cause__Group_4__0__Impl rule__Cause__Group_4__1 )
            // InternalQCF.g:917:2: rule__Cause__Group_4__0__Impl rule__Cause__Group_4__1
            {
            pushFollow(FOLLOW_5);
            rule__Cause__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__0"


    // $ANTLR start "rule__Cause__Group_4__0__Impl"
    // InternalQCF.g:924:1: rule__Cause__Group_4__0__Impl : ( 'valueOfInterest' ) ;
    public final void rule__Cause__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:928:1: ( ( 'valueOfInterest' ) )
            // InternalQCF.g:929:1: ( 'valueOfInterest' )
            {
            // InternalQCF.g:929:1: ( 'valueOfInterest' )
            // InternalQCF.g:930:2: 'valueOfInterest'
            {
             before(grammarAccess.getCauseAccess().getValueOfInterestKeyword_4_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getValueOfInterestKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__0__Impl"


    // $ANTLR start "rule__Cause__Group_4__1"
    // InternalQCF.g:939:1: rule__Cause__Group_4__1 : rule__Cause__Group_4__1__Impl ;
    public final void rule__Cause__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:943:1: ( rule__Cause__Group_4__1__Impl )
            // InternalQCF.g:944:2: rule__Cause__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__1"


    // $ANTLR start "rule__Cause__Group_4__1__Impl"
    // InternalQCF.g:950:1: rule__Cause__Group_4__1__Impl : ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) ) ;
    public final void rule__Cause__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:954:1: ( ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) ) )
            // InternalQCF.g:955:1: ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) )
            {
            // InternalQCF.g:955:1: ( ( rule__Cause__ValueOfInterestAssignment_4_1 ) )
            // InternalQCF.g:956:2: ( rule__Cause__ValueOfInterestAssignment_4_1 )
            {
             before(grammarAccess.getCauseAccess().getValueOfInterestAssignment_4_1()); 
            // InternalQCF.g:957:2: ( rule__Cause__ValueOfInterestAssignment_4_1 )
            // InternalQCF.g:957:3: rule__Cause__ValueOfInterestAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Cause__ValueOfInterestAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getValueOfInterestAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_4__1__Impl"


    // $ANTLR start "rule__Cause__Group_5__0"
    // InternalQCF.g:966:1: rule__Cause__Group_5__0 : rule__Cause__Group_5__0__Impl rule__Cause__Group_5__1 ;
    public final void rule__Cause__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:970:1: ( rule__Cause__Group_5__0__Impl rule__Cause__Group_5__1 )
            // InternalQCF.g:971:2: rule__Cause__Group_5__0__Impl rule__Cause__Group_5__1
            {
            pushFollow(FOLLOW_11);
            rule__Cause__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__0"


    // $ANTLR start "rule__Cause__Group_5__0__Impl"
    // InternalQCF.g:978:1: rule__Cause__Group_5__0__Impl : ( 'contains' ) ;
    public final void rule__Cause__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:982:1: ( ( 'contains' ) )
            // InternalQCF.g:983:1: ( 'contains' )
            {
            // InternalQCF.g:983:1: ( 'contains' )
            // InternalQCF.g:984:2: 'contains'
            {
             before(grammarAccess.getCauseAccess().getContainsKeyword_5_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getContainsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__0__Impl"


    // $ANTLR start "rule__Cause__Group_5__1"
    // InternalQCF.g:993:1: rule__Cause__Group_5__1 : rule__Cause__Group_5__1__Impl rule__Cause__Group_5__2 ;
    public final void rule__Cause__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:997:1: ( rule__Cause__Group_5__1__Impl rule__Cause__Group_5__2 )
            // InternalQCF.g:998:2: rule__Cause__Group_5__1__Impl rule__Cause__Group_5__2
            {
            pushFollow(FOLLOW_8);
            rule__Cause__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__1"


    // $ANTLR start "rule__Cause__Group_5__1__Impl"
    // InternalQCF.g:1005:1: rule__Cause__Group_5__1__Impl : ( '{' ) ;
    public final void rule__Cause__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1009:1: ( ( '{' ) )
            // InternalQCF.g:1010:1: ( '{' )
            {
            // InternalQCF.g:1010:1: ( '{' )
            // InternalQCF.g:1011:2: '{'
            {
             before(grammarAccess.getCauseAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__1__Impl"


    // $ANTLR start "rule__Cause__Group_5__2"
    // InternalQCF.g:1020:1: rule__Cause__Group_5__2 : rule__Cause__Group_5__2__Impl rule__Cause__Group_5__3 ;
    public final void rule__Cause__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1024:1: ( rule__Cause__Group_5__2__Impl rule__Cause__Group_5__3 )
            // InternalQCF.g:1025:2: rule__Cause__Group_5__2__Impl rule__Cause__Group_5__3
            {
            pushFollow(FOLLOW_12);
            rule__Cause__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__2"


    // $ANTLR start "rule__Cause__Group_5__2__Impl"
    // InternalQCF.g:1032:1: rule__Cause__Group_5__2__Impl : ( ( ( rule__Cause__SubCausesAssignment_5_2 ) ) ( ( rule__Cause__SubCausesAssignment_5_2 )* ) ) ;
    public final void rule__Cause__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1036:1: ( ( ( ( rule__Cause__SubCausesAssignment_5_2 ) ) ( ( rule__Cause__SubCausesAssignment_5_2 )* ) ) )
            // InternalQCF.g:1037:1: ( ( ( rule__Cause__SubCausesAssignment_5_2 ) ) ( ( rule__Cause__SubCausesAssignment_5_2 )* ) )
            {
            // InternalQCF.g:1037:1: ( ( ( rule__Cause__SubCausesAssignment_5_2 ) ) ( ( rule__Cause__SubCausesAssignment_5_2 )* ) )
            // InternalQCF.g:1038:2: ( ( rule__Cause__SubCausesAssignment_5_2 ) ) ( ( rule__Cause__SubCausesAssignment_5_2 )* )
            {
            // InternalQCF.g:1038:2: ( ( rule__Cause__SubCausesAssignment_5_2 ) )
            // InternalQCF.g:1039:3: ( rule__Cause__SubCausesAssignment_5_2 )
            {
             before(grammarAccess.getCauseAccess().getSubCausesAssignment_5_2()); 
            // InternalQCF.g:1040:3: ( rule__Cause__SubCausesAssignment_5_2 )
            // InternalQCF.g:1040:4: rule__Cause__SubCausesAssignment_5_2
            {
            pushFollow(FOLLOW_9);
            rule__Cause__SubCausesAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getSubCausesAssignment_5_2()); 

            }

            // InternalQCF.g:1043:2: ( ( rule__Cause__SubCausesAssignment_5_2 )* )
            // InternalQCF.g:1044:3: ( rule__Cause__SubCausesAssignment_5_2 )*
            {
             before(grammarAccess.getCauseAccess().getSubCausesAssignment_5_2()); 
            // InternalQCF.g:1045:3: ( rule__Cause__SubCausesAssignment_5_2 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==15) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalQCF.g:1045:4: rule__Cause__SubCausesAssignment_5_2
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Cause__SubCausesAssignment_5_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getCauseAccess().getSubCausesAssignment_5_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__2__Impl"


    // $ANTLR start "rule__Cause__Group_5__3"
    // InternalQCF.g:1054:1: rule__Cause__Group_5__3 : rule__Cause__Group_5__3__Impl ;
    public final void rule__Cause__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1058:1: ( rule__Cause__Group_5__3__Impl )
            // InternalQCF.g:1059:2: rule__Cause__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__3"


    // $ANTLR start "rule__Cause__Group_5__3__Impl"
    // InternalQCF.g:1065:1: rule__Cause__Group_5__3__Impl : ( '}' ) ;
    public final void rule__Cause__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1069:1: ( ( '}' ) )
            // InternalQCF.g:1070:1: ( '}' )
            {
            // InternalQCF.g:1070:1: ( '}' )
            // InternalQCF.g:1071:2: '}'
            {
             before(grammarAccess.getCauseAccess().getRightCurlyBracketKeyword_5_3()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCauseAccess().getRightCurlyBracketKeyword_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Group_5__3__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalQCF.g:1081:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1085:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalQCF.g:1086:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalQCF.g:1093:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1097:1: ( ( RULE_ID ) )
            // InternalQCF.g:1098:1: ( RULE_ID )
            {
            // InternalQCF.g:1098:1: ( RULE_ID )
            // InternalQCF.g:1099:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalQCF.g:1108:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1112:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalQCF.g:1113:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalQCF.g:1119:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1123:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalQCF.g:1124:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalQCF.g:1124:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalQCF.g:1125:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalQCF.g:1126:2: ( rule__QualifiedName__Group_1__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==20) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalQCF.g:1126:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalQCF.g:1135:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1139:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalQCF.g:1140:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_3);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalQCF.g:1147:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1151:1: ( ( '.' ) )
            // InternalQCF.g:1152:1: ( '.' )
            {
            // InternalQCF.g:1152:1: ( '.' )
            // InternalQCF.g:1153:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalQCF.g:1162:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1166:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalQCF.g:1167:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalQCF.g:1173:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1177:1: ( ( RULE_ID ) )
            // InternalQCF.g:1178:1: ( RULE_ID )
            {
            // InternalQCF.g:1178:1: ( RULE_ID )
            // InternalQCF.g:1179:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__QCF__NameAssignment_1"
    // InternalQCF.g:1189:1: rule__QCF__NameAssignment_1 : ( ruleQualifiedName ) ;
    public final void rule__QCF__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1193:1: ( ( ruleQualifiedName ) )
            // InternalQCF.g:1194:2: ( ruleQualifiedName )
            {
            // InternalQCF.g:1194:2: ( ruleQualifiedName )
            // InternalQCF.g:1195:3: ruleQualifiedName
            {
             before(grammarAccess.getQCFAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQCFAccess().getNameQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__NameAssignment_1"


    // $ANTLR start "rule__QCF__EffectsAssignment_2"
    // InternalQCF.g:1204:1: rule__QCF__EffectsAssignment_2 : ( ruleEffect ) ;
    public final void rule__QCF__EffectsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1208:1: ( ( ruleEffect ) )
            // InternalQCF.g:1209:2: ( ruleEffect )
            {
            // InternalQCF.g:1209:2: ( ruleEffect )
            // InternalQCF.g:1210:3: ruleEffect
            {
             before(grammarAccess.getQCFAccess().getEffectsEffectParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getQCFAccess().getEffectsEffectParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QCF__EffectsAssignment_2"


    // $ANTLR start "rule__Effect__NameAssignment_2"
    // InternalQCF.g:1219:1: rule__Effect__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Effect__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1223:1: ( ( ruleEString ) )
            // InternalQCF.g:1224:2: ( ruleEString )
            {
            // InternalQCF.g:1224:2: ( ruleEString )
            // InternalQCF.g:1225:3: ruleEString
            {
             before(grammarAccess.getEffectAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__NameAssignment_2"


    // $ANTLR start "rule__Effect__DescriptionAssignment_3_1"
    // InternalQCF.g:1234:1: rule__Effect__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Effect__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1238:1: ( ( ruleEString ) )
            // InternalQCF.g:1239:2: ( ruleEString )
            {
            // InternalQCF.g:1239:2: ( ruleEString )
            // InternalQCF.g:1240:3: ruleEString
            {
             before(grammarAccess.getEffectAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Effect__CategoriesAssignment_4"
    // InternalQCF.g:1249:1: rule__Effect__CategoriesAssignment_4 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1253:1: ( ( ruleCategory ) )
            // InternalQCF.g:1254:2: ( ruleCategory )
            {
            // InternalQCF.g:1254:2: ( ruleCategory )
            // InternalQCF.g:1255:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_4"


    // $ANTLR start "rule__Category__NameAssignment_2"
    // InternalQCF.g:1264:1: rule__Category__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Category__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1268:1: ( ( ruleEString ) )
            // InternalQCF.g:1269:2: ( ruleEString )
            {
            // InternalQCF.g:1269:2: ( ruleEString )
            // InternalQCF.g:1270:3: ruleEString
            {
             before(grammarAccess.getCategoryAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__NameAssignment_2"


    // $ANTLR start "rule__Category__DescriptionAssignment_3_1"
    // InternalQCF.g:1279:1: rule__Category__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Category__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1283:1: ( ( ruleEString ) )
            // InternalQCF.g:1284:2: ( ruleEString )
            {
            // InternalQCF.g:1284:2: ( ruleEString )
            // InternalQCF.g:1285:3: ruleEString
            {
             before(grammarAccess.getCategoryAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Category__CausesAssignment_4"
    // InternalQCF.g:1294:1: rule__Category__CausesAssignment_4 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1298:1: ( ( ruleCause ) )
            // InternalQCF.g:1299:2: ( ruleCause )
            {
            // InternalQCF.g:1299:2: ( ruleCause )
            // InternalQCF.g:1300:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_4"


    // $ANTLR start "rule__Cause__NameAssignment_2"
    // InternalQCF.g:1309:1: rule__Cause__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Cause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1313:1: ( ( ruleEString ) )
            // InternalQCF.g:1314:2: ( ruleEString )
            {
            // InternalQCF.g:1314:2: ( ruleEString )
            // InternalQCF.g:1315:3: ruleEString
            {
             before(grammarAccess.getCauseAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__NameAssignment_2"


    // $ANTLR start "rule__Cause__DescriptionAssignment_3_1"
    // InternalQCF.g:1324:1: rule__Cause__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Cause__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1328:1: ( ( ruleEString ) )
            // InternalQCF.g:1329:2: ( ruleEString )
            {
            // InternalQCF.g:1329:2: ( ruleEString )
            // InternalQCF.g:1330:3: ruleEString
            {
             before(grammarAccess.getCauseAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Cause__ValueOfInterestAssignment_4_1"
    // InternalQCF.g:1339:1: rule__Cause__ValueOfInterestAssignment_4_1 : ( ruleEString ) ;
    public final void rule__Cause__ValueOfInterestAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1343:1: ( ( ruleEString ) )
            // InternalQCF.g:1344:2: ( ruleEString )
            {
            // InternalQCF.g:1344:2: ( ruleEString )
            // InternalQCF.g:1345:3: ruleEString
            {
             before(grammarAccess.getCauseAccess().getValueOfInterestEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getValueOfInterestEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__ValueOfInterestAssignment_4_1"


    // $ANTLR start "rule__Cause__SubCausesAssignment_5_2"
    // InternalQCF.g:1354:1: rule__Cause__SubCausesAssignment_5_2 : ( ruleCause ) ;
    public final void rule__Cause__SubCausesAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalQCF.g:1358:1: ( ( ruleCause ) )
            // InternalQCF.g:1359:2: ( ruleCause )
            {
            // InternalQCF.g:1359:2: ( ruleCause )
            // InternalQCF.g:1360:3: ruleCause
            {
             before(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__SubCausesAssignment_5_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000000A002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000032000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100002L});

}