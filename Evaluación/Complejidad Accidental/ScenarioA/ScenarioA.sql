SELECT d.id,
   c.fallingWheelBand,
   bw.model
   FROM DriveHalfShaft d JOIN ComplianceReport c
      ON d.idComplianceReport = c.id
   JOIN Band bw
      ON d.idBandWheel = bw.id;
