driveHalfShafts[["id", "idComplianceReport", "idAssemblySession"]].merge(reports[['id', 'fallingWheelBand']],
        how="left", left_on="idComplianceReport", right_on="id", suffixes=("", "report")).merge(assemblySessions[["id",
        "idProgram"]],how="left", left_on="idAssemblySession", right_on="id", suffixes=("","report")).merge(programs[["id",
        "number"]], how="left", left_on="idProgram", right_on="id", suffixes=("", "program")).drop(columns=["idComplianceReport",
        "idreport", "idAssemblySession", "idProgram", "idprogram"])