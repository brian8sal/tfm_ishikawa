SELECT d.id,
	c.fallingWheelBand,
	s.model,
	sp.maxLength,
	sp.minLength,
	sp.avgLength,
	sp.maxDiameter,
	sp.minDiameter,
	sp.avgDiameter,
	sp.batchID,
	p.provider,
	(SELECT pcc.thickness as wheelSide_thickness FROM PowderCoatingChecks pcc
		WHERE pcc.id = sp.idPowderCoatingInspections and pcc.zone = "Wheel"),
	(SELECT pcc.thickness as engineSide_thickness FROM PowderCoatingChecks pcc
		WHERE pcc.id = sp.idPowderCoatingInspections and pcc.zone = "Engine"),
	(SELECT pcc.thickness as center_thickness FROM PowderCoatingChecks pcc
		WHERE pcc.id = sp.idPowderCoatingInspections and pcc.zone = "Center")
	FROM DriveHalfShaft d JOIN ComplianceReport c
		ON d.idComplianceReport = c.id
	JOIN
	(Shaft s JOIN
		(ShaftBatchParameters sp JOIN
			Provider p ON p.id = sp.idProvider)
		ON s.batchId = sp.batchID)
	ON d.idShaft = s.id
