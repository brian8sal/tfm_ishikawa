/*
 * generated by Xtext 2.24.0
 */
package es.unican;


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class QCFRuntimeModule extends AbstractQCFRuntimeModule {
}
