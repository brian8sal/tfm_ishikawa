/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compliance Report</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.ComplianceReport#getFallingWheelBand <em>Falling Wheel Band</em>}</li>
 *   <li>{@link FallingBand.ComplianceReport#getFallingEngineBand <em>Falling Engine Band</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getComplianceReport()
 * @model
 * @generated
 */
public interface ComplianceReport extends EObject {
	/**
	 * Returns the value of the '<em><b>Falling Wheel Band</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Falling Wheel Band</em>' attribute.
	 * @see #setFallingWheelBand(Boolean)
	 * @see FallingBand.FallingPackage#getComplianceReport_FallingWheelBand()
	 * @model
	 * @generated
	 */
	Boolean getFallingWheelBand();

	/**
	 * Sets the value of the '{@link FallingBand.ComplianceReport#getFallingWheelBand <em>Falling Wheel Band</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Falling Wheel Band</em>' attribute.
	 * @see #getFallingWheelBand()
	 * @generated
	 */
	void setFallingWheelBand(Boolean value);

	/**
	 * Returns the value of the '<em><b>Falling Engine Band</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Falling Engine Band</em>' attribute.
	 * @see #setFallingEngineBand(Boolean)
	 * @see FallingBand.FallingPackage#getComplianceReport_FallingEngineBand()
	 * @model
	 * @generated
	 */
	Boolean getFallingEngineBand();

	/**
	 * Sets the value of the '{@link FallingBand.ComplianceReport#getFallingEngineBand <em>Falling Engine Band</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Falling Engine Band</em>' attribute.
	 * @see #getFallingEngineBand()
	 * @generated
	 */
	void setFallingEngineBand(Boolean value);

} // ComplianceReport
