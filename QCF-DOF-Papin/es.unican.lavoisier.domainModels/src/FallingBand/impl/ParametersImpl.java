/**
 */
package FallingBand.impl;

import FallingBand.Coordinate;
import FallingBand.FallingPackage;
import FallingBand.Parameters;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameters</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.impl.ParametersImpl#getTighteningForce <em>Tightening Force</em>}</li>
 *   <li>{@link FallingBand.impl.ParametersImpl#getTighteningTime <em>Tightening Time</em>}</li>
 *   <li>{@link FallingBand.impl.ParametersImpl#getWheelBandPosition <em>Wheel Band Position</em>}</li>
 *   <li>{@link FallingBand.impl.ParametersImpl#getEngineBandPosition <em>Engine Band Position</em>}</li>
 *   <li>{@link FallingBand.impl.ParametersImpl#getWheelHousingPosition <em>Wheel Housing Position</em>}</li>
 *   <li>{@link FallingBand.impl.ParametersImpl#getEngineHousingPosition <em>Engine Housing Position</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParametersImpl extends MinimalEObjectImpl.Container implements Parameters {
	/**
	 * The default value of the '{@link #getTighteningForce() <em>Tightening Force</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTighteningForce()
	 * @generated
	 * @ordered
	 */
	protected static final Double TIGHTENING_FORCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTighteningForce() <em>Tightening Force</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTighteningForce()
	 * @generated
	 * @ordered
	 */
	protected Double tighteningForce = TIGHTENING_FORCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTighteningTime() <em>Tightening Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTighteningTime()
	 * @generated
	 * @ordered
	 */
	protected static final Double TIGHTENING_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTighteningTime() <em>Tightening Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTighteningTime()
	 * @generated
	 * @ordered
	 */
	protected Double tighteningTime = TIGHTENING_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWheelBandPosition() <em>Wheel Band Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWheelBandPosition()
	 * @generated
	 * @ordered
	 */
	protected Coordinate wheelBandPosition;

	/**
	 * The cached value of the '{@link #getEngineBandPosition() <em>Engine Band Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngineBandPosition()
	 * @generated
	 * @ordered
	 */
	protected Coordinate engineBandPosition;

	/**
	 * The cached value of the '{@link #getWheelHousingPosition() <em>Wheel Housing Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWheelHousingPosition()
	 * @generated
	 * @ordered
	 */
	protected Coordinate wheelHousingPosition;

	/**
	 * The cached value of the '{@link #getEngineHousingPosition() <em>Engine Housing Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEngineHousingPosition()
	 * @generated
	 * @ordered
	 */
	protected Coordinate engineHousingPosition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParametersImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FallingPackage.Literals.PARAMETERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTighteningForce() {
		return tighteningForce;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTighteningForce(Double newTighteningForce) {
		Double oldTighteningForce = tighteningForce;
		tighteningForce = newTighteningForce;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__TIGHTENING_FORCE, oldTighteningForce, tighteningForce));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getTighteningTime() {
		return tighteningTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTighteningTime(Double newTighteningTime) {
		Double oldTighteningTime = tighteningTime;
		tighteningTime = newTighteningTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__TIGHTENING_TIME, oldTighteningTime, tighteningTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coordinate getWheelBandPosition() {
		return wheelBandPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWheelBandPosition(Coordinate newWheelBandPosition, NotificationChain msgs) {
		Coordinate oldWheelBandPosition = wheelBandPosition;
		wheelBandPosition = newWheelBandPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__WHEEL_BAND_POSITION, oldWheelBandPosition, newWheelBandPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWheelBandPosition(Coordinate newWheelBandPosition) {
		if (newWheelBandPosition != wheelBandPosition) {
			NotificationChain msgs = null;
			if (wheelBandPosition != null)
				msgs = ((InternalEObject)wheelBandPosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__WHEEL_BAND_POSITION, null, msgs);
			if (newWheelBandPosition != null)
				msgs = ((InternalEObject)newWheelBandPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__WHEEL_BAND_POSITION, null, msgs);
			msgs = basicSetWheelBandPosition(newWheelBandPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__WHEEL_BAND_POSITION, newWheelBandPosition, newWheelBandPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coordinate getEngineBandPosition() {
		return engineBandPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEngineBandPosition(Coordinate newEngineBandPosition, NotificationChain msgs) {
		Coordinate oldEngineBandPosition = engineBandPosition;
		engineBandPosition = newEngineBandPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__ENGINE_BAND_POSITION, oldEngineBandPosition, newEngineBandPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEngineBandPosition(Coordinate newEngineBandPosition) {
		if (newEngineBandPosition != engineBandPosition) {
			NotificationChain msgs = null;
			if (engineBandPosition != null)
				msgs = ((InternalEObject)engineBandPosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__ENGINE_BAND_POSITION, null, msgs);
			if (newEngineBandPosition != null)
				msgs = ((InternalEObject)newEngineBandPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__ENGINE_BAND_POSITION, null, msgs);
			msgs = basicSetEngineBandPosition(newEngineBandPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__ENGINE_BAND_POSITION, newEngineBandPosition, newEngineBandPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coordinate getWheelHousingPosition() {
		return wheelHousingPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWheelHousingPosition(Coordinate newWheelHousingPosition, NotificationChain msgs) {
		Coordinate oldWheelHousingPosition = wheelHousingPosition;
		wheelHousingPosition = newWheelHousingPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION, oldWheelHousingPosition, newWheelHousingPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWheelHousingPosition(Coordinate newWheelHousingPosition) {
		if (newWheelHousingPosition != wheelHousingPosition) {
			NotificationChain msgs = null;
			if (wheelHousingPosition != null)
				msgs = ((InternalEObject)wheelHousingPosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION, null, msgs);
			if (newWheelHousingPosition != null)
				msgs = ((InternalEObject)newWheelHousingPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION, null, msgs);
			msgs = basicSetWheelHousingPosition(newWheelHousingPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION, newWheelHousingPosition, newWheelHousingPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coordinate getEngineHousingPosition() {
		return engineHousingPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEngineHousingPosition(Coordinate newEngineHousingPosition, NotificationChain msgs) {
		Coordinate oldEngineHousingPosition = engineHousingPosition;
		engineHousingPosition = newEngineHousingPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION, oldEngineHousingPosition, newEngineHousingPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEngineHousingPosition(Coordinate newEngineHousingPosition) {
		if (newEngineHousingPosition != engineHousingPosition) {
			NotificationChain msgs = null;
			if (engineHousingPosition != null)
				msgs = ((InternalEObject)engineHousingPosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION, null, msgs);
			if (newEngineHousingPosition != null)
				msgs = ((InternalEObject)newEngineHousingPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION, null, msgs);
			msgs = basicSetEngineHousingPosition(newEngineHousingPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION, newEngineHousingPosition, newEngineHousingPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FallingPackage.PARAMETERS__WHEEL_BAND_POSITION:
				return basicSetWheelBandPosition(null, msgs);
			case FallingPackage.PARAMETERS__ENGINE_BAND_POSITION:
				return basicSetEngineBandPosition(null, msgs);
			case FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION:
				return basicSetWheelHousingPosition(null, msgs);
			case FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION:
				return basicSetEngineHousingPosition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FallingPackage.PARAMETERS__TIGHTENING_FORCE:
				return getTighteningForce();
			case FallingPackage.PARAMETERS__TIGHTENING_TIME:
				return getTighteningTime();
			case FallingPackage.PARAMETERS__WHEEL_BAND_POSITION:
				return getWheelBandPosition();
			case FallingPackage.PARAMETERS__ENGINE_BAND_POSITION:
				return getEngineBandPosition();
			case FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION:
				return getWheelHousingPosition();
			case FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION:
				return getEngineHousingPosition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FallingPackage.PARAMETERS__TIGHTENING_FORCE:
				setTighteningForce((Double)newValue);
				return;
			case FallingPackage.PARAMETERS__TIGHTENING_TIME:
				setTighteningTime((Double)newValue);
				return;
			case FallingPackage.PARAMETERS__WHEEL_BAND_POSITION:
				setWheelBandPosition((Coordinate)newValue);
				return;
			case FallingPackage.PARAMETERS__ENGINE_BAND_POSITION:
				setEngineBandPosition((Coordinate)newValue);
				return;
			case FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION:
				setWheelHousingPosition((Coordinate)newValue);
				return;
			case FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION:
				setEngineHousingPosition((Coordinate)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FallingPackage.PARAMETERS__TIGHTENING_FORCE:
				setTighteningForce(TIGHTENING_FORCE_EDEFAULT);
				return;
			case FallingPackage.PARAMETERS__TIGHTENING_TIME:
				setTighteningTime(TIGHTENING_TIME_EDEFAULT);
				return;
			case FallingPackage.PARAMETERS__WHEEL_BAND_POSITION:
				setWheelBandPosition((Coordinate)null);
				return;
			case FallingPackage.PARAMETERS__ENGINE_BAND_POSITION:
				setEngineBandPosition((Coordinate)null);
				return;
			case FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION:
				setWheelHousingPosition((Coordinate)null);
				return;
			case FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION:
				setEngineHousingPosition((Coordinate)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FallingPackage.PARAMETERS__TIGHTENING_FORCE:
				return TIGHTENING_FORCE_EDEFAULT == null ? tighteningForce != null : !TIGHTENING_FORCE_EDEFAULT.equals(tighteningForce);
			case FallingPackage.PARAMETERS__TIGHTENING_TIME:
				return TIGHTENING_TIME_EDEFAULT == null ? tighteningTime != null : !TIGHTENING_TIME_EDEFAULT.equals(tighteningTime);
			case FallingPackage.PARAMETERS__WHEEL_BAND_POSITION:
				return wheelBandPosition != null;
			case FallingPackage.PARAMETERS__ENGINE_BAND_POSITION:
				return engineBandPosition != null;
			case FallingPackage.PARAMETERS__WHEEL_HOUSING_POSITION:
				return wheelHousingPosition != null;
			case FallingPackage.PARAMETERS__ENGINE_HOUSING_POSITION:
				return engineHousingPosition != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tighteningForce: ");
		result.append(tighteningForce);
		result.append(", tighteningTime: ");
		result.append(tighteningTime);
		result.append(')');
		return result.toString();
	}

} //ParametersImpl
