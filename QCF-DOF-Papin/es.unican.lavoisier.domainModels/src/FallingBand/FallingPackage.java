/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see FallingBand.FallingFactory
 * @model kind="package"
 * @generated
 */
public interface FallingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "FallingBand";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fallingband.unican.es";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "falling";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FallingPackage eINSTANCE = FallingBand.impl.FallingPackageImpl.init();

	/**
	 * The meta object id for the '{@link FallingBand.impl.DriveHalfShaftImpl <em>Drive Half Shaft</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.DriveHalfShaftImpl
	 * @see FallingBand.impl.FallingPackageImpl#getDriveHalfShaft()
	 * @generated
	 */
	int DRIVE_HALF_SHAFT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ID = 0;

	/**
	 * The feature id for the '<em><b>Manufactured Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__MANUFACTURED_TIME = 1;

	/**
	 * The feature id for the '<em><b>Engine Band</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ENGINE_BAND = 2;

	/**
	 * The feature id for the '<em><b>Wheel Band</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__WHEEL_BAND = 3;

	/**
	 * The feature id for the '<em><b>Assembly Session</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ASSEMBLY_SESSION = 4;

	/**
	 * The feature id for the '<em><b>Report</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__REPORT = 5;

	/**
	 * The feature id for the '<em><b>Shaft</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__SHAFT = 6;

	/**
	 * The feature id for the '<em><b>Engine Housing</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__ENGINE_HOUSING = 7;

	/**
	 * The feature id for the '<em><b>Wheel Housing</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT__WHEEL_HOUSING = 8;

	/**
	 * The number of structural features of the '<em>Drive Half Shaft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Drive Half Shaft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRIVE_HALF_SHAFT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.BandImpl <em>Band</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.BandImpl
	 * @see FallingBand.impl.FallingPackageImpl#getBand()
	 * @generated
	 */
	int BAND = 1;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND__MODEL = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Band</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Band</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.BandBatchParametersImpl <em>Band Batch Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.BandBatchParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getBandBatchParameters()
	 * @generated
	 */
	int BAND_BATCH_PARAMETERS = 2;

	/**
	 * The feature id for the '<em><b>Max Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__MAX_THICKNESS = 0;

	/**
	 * The feature id for the '<em><b>Min Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__MIN_THICKNESS = 1;

	/**
	 * The feature id for the '<em><b>Avg Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__AVG_THICKNESS = 2;

	/**
	 * The feature id for the '<em><b>Batch Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__BATCH_ID = 3;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS__PROVIDER = 4;

	/**
	 * The number of structural features of the '<em>Band Batch Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Band Batch Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BAND_BATCH_PARAMETERS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.AssemblySessionImpl <em>Assembly Session</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.AssemblySessionImpl
	 * @see FallingBand.impl.FallingPackageImpl#getAssemblySession()
	 * @generated
	 */
	int ASSEMBLY_SESSION = 3;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__START = 0;

	/**
	 * The feature id for the '<em><b>Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__STOP = 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__PARAMETERS = 2;

	/**
	 * The feature id for the '<em><b>Machine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__MACHINE = 3;

	/**
	 * The feature id for the '<em><b>Program</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION__PROGRAM = 4;

	/**
	 * The number of structural features of the '<em>Assembly Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Assembly Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSEMBLY_SESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.MonitoredParametersImpl <em>Monitored Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.MonitoredParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getMonitoredParameters()
	 * @generated
	 */
	int MONITORED_PARAMETERS = 4;

	/**
	 * The feature id for the '<em><b>Temperature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS__TEMPERATURE = 0;

	/**
	 * The feature id for the '<em><b>Humidity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS__HUMIDITY = 1;

	/**
	 * The feature id for the '<em><b>Pliers Tightenings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS__PLIERS_TIGHTENINGS = 2;

	/**
	 * The number of structural features of the '<em>Monitored Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Monitored Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_PARAMETERS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.PliersDataImpl <em>Pliers Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.PliersDataImpl
	 * @see FallingBand.impl.FallingPackageImpl#getPliersData()
	 * @generated
	 */
	int PLIERS_DATA = 5;

	/**
	 * The feature id for the '<em><b>Pressure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA__PRESSURE = 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA__NUMBER = 1;

	/**
	 * The number of structural features of the '<em>Pliers Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Pliers Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLIERS_DATA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ComplianceReportImpl <em>Compliance Report</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ComplianceReportImpl
	 * @see FallingBand.impl.FallingPackageImpl#getComplianceReport()
	 * @generated
	 */
	int COMPLIANCE_REPORT = 6;

	/**
	 * The feature id for the '<em><b>Falling Wheel Band</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT__FALLING_WHEEL_BAND = 0;

	/**
	 * The feature id for the '<em><b>Falling Engine Band</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT__FALLING_ENGINE_BAND = 1;

	/**
	 * The number of structural features of the '<em>Compliance Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Compliance Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLIANCE_REPORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ProviderImpl
	 * @see FallingBand.impl.FallingPackageImpl#getProvider()
	 * @generated
	 */
	int PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER__PROVIDER = 0;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link FallingBand.impl.ShaftImpl <em>Shaft</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ShaftImpl
	 * @see FallingBand.impl.FallingPackageImpl#getShaft()
	 * @generated
	 */
	int SHAFT = 8;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT__MODEL = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Shaft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Shaft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ShaftBatchParametersImpl <em>Shaft Batch Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ShaftBatchParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getShaftBatchParameters()
	 * @generated
	 */
	int SHAFT_BATCH_PARAMETERS = 9;

	/**
	 * The feature id for the '<em><b>Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__MAX_LENGTH = 0;

	/**
	 * The feature id for the '<em><b>Min Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__MIN_LENGTH = 1;

	/**
	 * The feature id for the '<em><b>Avg Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__AVG_LENGTH = 2;

	/**
	 * The feature id for the '<em><b>Max Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__MAX_DIAMETER = 3;

	/**
	 * The feature id for the '<em><b>Min Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__MIN_DIAMETER = 4;

	/**
	 * The feature id for the '<em><b>Avg Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__AVG_DIAMETER = 5;

	/**
	 * The feature id for the '<em><b>Batch Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__BATCH_ID = 6;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Powder Coating Inspections</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS__POWDER_COATING_INSPECTIONS = 8;

	/**
	 * The number of structural features of the '<em>Shaft Batch Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Shaft Batch Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHAFT_BATCH_PARAMETERS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.PowderCoatingChecksImpl <em>Powder Coating Checks</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.PowderCoatingChecksImpl
	 * @see FallingBand.impl.FallingPackageImpl#getPowderCoatingChecks()
	 * @generated
	 */
	int POWDER_COATING_CHECKS = 10;

	/**
	 * The feature id for the '<em><b>Zone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWDER_COATING_CHECKS__ZONE = 0;

	/**
	 * The feature id for the '<em><b>Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWDER_COATING_CHECKS__THICKNESS = 1;

	/**
	 * The number of structural features of the '<em>Powder Coating Checks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWDER_COATING_CHECKS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Powder Coating Checks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWDER_COATING_CHECKS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.MachineImpl <em>Machine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.MachineImpl
	 * @see FallingBand.impl.FallingPackageImpl#getMachine()
	 * @generated
	 */
	int MACHINE = 11;

	/**
	 * The feature id for the '<em><b>Last Calibration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACHINE__LAST_CALIBRATION = 0;

	/**
	 * The feature id for the '<em><b>Programs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACHINE__PROGRAMS = 1;

	/**
	 * The number of structural features of the '<em>Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACHINE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACHINE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ProgramImpl <em>Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ProgramImpl
	 * @see FallingBand.impl.FallingPackageImpl#getProgram()
	 * @generated
	 */
	int PROGRAM = 12;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.ParametersImpl <em>Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.ParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getParameters()
	 * @generated
	 */
	int PARAMETERS = 13;

	/**
	 * The feature id for the '<em><b>Tightening Force</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS__TIGHTENING_FORCE = 0;

	/**
	 * The feature id for the '<em><b>Tightening Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS__TIGHTENING_TIME = 1;

	/**
	 * The feature id for the '<em><b>Wheel Band Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS__WHEEL_BAND_POSITION = 2;

	/**
	 * The feature id for the '<em><b>Engine Band Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS__ENGINE_BAND_POSITION = 3;

	/**
	 * The feature id for the '<em><b>Wheel Housing Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS__WHEEL_HOUSING_POSITION = 4;

	/**
	 * The feature id for the '<em><b>Engine Housing Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS__ENGINE_HOUSING_POSITION = 5;

	/**
	 * The number of structural features of the '<em>Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.CoordinateImpl <em>Coordinate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.CoordinateImpl
	 * @see FallingBand.impl.FallingPackageImpl#getCoordinate()
	 * @generated
	 */
	int COORDINATE = 14;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATE__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATE__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATE__Z = 2;

	/**
	 * The number of structural features of the '<em>Coordinate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Coordinate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.HousingImpl <em>Housing</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.HousingImpl
	 * @see FallingBand.impl.FallingPackageImpl#getHousing()
	 * @generated
	 */
	int HOUSING = 15;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING__MODEL = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Housing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Housing</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link FallingBand.impl.HousingParametersImpl <em>Housing Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FallingBand.impl.HousingParametersImpl
	 * @see FallingBand.impl.FallingPackageImpl#getHousingParameters()
	 * @generated
	 */
	int HOUSING_PARAMETERS = 16;

	/**
	 * The feature id for the '<em><b>Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__MAX_LENGTH = 0;

	/**
	 * The feature id for the '<em><b>Min Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__MIN_LENGTH = 1;

	/**
	 * The feature id for the '<em><b>Avg Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__AVG_LENGTH = 2;

	/**
	 * The feature id for the '<em><b>Max Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__MAX_DIAMETER = 3;

	/**
	 * The feature id for the '<em><b>Min Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__MIN_DIAMETER = 4;

	/**
	 * The feature id for the '<em><b>Avg Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__AVG_DIAMETER = 5;

	/**
	 * The feature id for the '<em><b>Batch Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__BATCH_ID = 6;

	/**
	 * The feature id for the '<em><b>Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS__PROVIDER = 7;

	/**
	 * The number of structural features of the '<em>Housing Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Housing Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOUSING_PARAMETERS_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link FallingBand.DriveHalfShaft <em>Drive Half Shaft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Drive Half Shaft</em>'.
	 * @see FallingBand.DriveHalfShaft
	 * @generated
	 */
	EClass getDriveHalfShaft();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.DriveHalfShaft#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see FallingBand.DriveHalfShaft#getId()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EAttribute getDriveHalfShaft_Id();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.DriveHalfShaft#getManufacturedTime <em>Manufactured Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manufactured Time</em>'.
	 * @see FallingBand.DriveHalfShaft#getManufacturedTime()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EAttribute getDriveHalfShaft_ManufacturedTime();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getEngineBand <em>Engine Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Engine Band</em>'.
	 * @see FallingBand.DriveHalfShaft#getEngineBand()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_EngineBand();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getWheelBand <em>Wheel Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wheel Band</em>'.
	 * @see FallingBand.DriveHalfShaft#getWheelBand()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_WheelBand();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getAssemblySession <em>Assembly Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assembly Session</em>'.
	 * @see FallingBand.DriveHalfShaft#getAssemblySession()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_AssemblySession();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getReport <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Report</em>'.
	 * @see FallingBand.DriveHalfShaft#getReport()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_Report();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getShaft <em>Shaft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Shaft</em>'.
	 * @see FallingBand.DriveHalfShaft#getShaft()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_Shaft();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getEngineHousing <em>Engine Housing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Engine Housing</em>'.
	 * @see FallingBand.DriveHalfShaft#getEngineHousing()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_EngineHousing();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.DriveHalfShaft#getWheelHousing <em>Wheel Housing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wheel Housing</em>'.
	 * @see FallingBand.DriveHalfShaft#getWheelHousing()
	 * @see #getDriveHalfShaft()
	 * @generated
	 */
	EReference getDriveHalfShaft_WheelHousing();

	/**
	 * Returns the meta object for class '{@link FallingBand.Band <em>Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Band</em>'.
	 * @see FallingBand.Band
	 * @generated
	 */
	EClass getBand();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Band#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see FallingBand.Band#getModel()
	 * @see #getBand()
	 * @generated
	 */
	EAttribute getBand_Model();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Band#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.Band#getParameters()
	 * @see #getBand()
	 * @generated
	 */
	EReference getBand_Parameters();

	/**
	 * Returns the meta object for class '{@link FallingBand.BandBatchParameters <em>Band Batch Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Band Batch Parameters</em>'.
	 * @see FallingBand.BandBatchParameters
	 * @generated
	 */
	EClass getBandBatchParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getMaxThickness <em>Max Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Thickness</em>'.
	 * @see FallingBand.BandBatchParameters#getMaxThickness()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_MaxThickness();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getMinThickness <em>Min Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Thickness</em>'.
	 * @see FallingBand.BandBatchParameters#getMinThickness()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_MinThickness();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getAvgThickness <em>Avg Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Thickness</em>'.
	 * @see FallingBand.BandBatchParameters#getAvgThickness()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_AvgThickness();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.BandBatchParameters#getBatchId <em>Batch Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Batch Id</em>'.
	 * @see FallingBand.BandBatchParameters#getBatchId()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EAttribute getBandBatchParameters_BatchId();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.BandBatchParameters#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provider</em>'.
	 * @see FallingBand.BandBatchParameters#getProvider()
	 * @see #getBandBatchParameters()
	 * @generated
	 */
	EReference getBandBatchParameters_Provider();

	/**
	 * Returns the meta object for class '{@link FallingBand.AssemblySession <em>Assembly Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assembly Session</em>'.
	 * @see FallingBand.AssemblySession
	 * @generated
	 */
	EClass getAssemblySession();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.AssemblySession#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see FallingBand.AssemblySession#getStart()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EAttribute getAssemblySession_Start();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.AssemblySession#getStop <em>Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stop</em>'.
	 * @see FallingBand.AssemblySession#getStop()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EAttribute getAssemblySession_Stop();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.AssemblySession#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.AssemblySession#getParameters()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EReference getAssemblySession_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.AssemblySession#getMachine <em>Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Machine</em>'.
	 * @see FallingBand.AssemblySession#getMachine()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EReference getAssemblySession_Machine();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.AssemblySession#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Program</em>'.
	 * @see FallingBand.AssemblySession#getProgram()
	 * @see #getAssemblySession()
	 * @generated
	 */
	EReference getAssemblySession_Program();

	/**
	 * Returns the meta object for class '{@link FallingBand.MonitoredParameters <em>Monitored Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitored Parameters</em>'.
	 * @see FallingBand.MonitoredParameters
	 * @generated
	 */
	EClass getMonitoredParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.MonitoredParameters#getTemperature <em>Temperature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Temperature</em>'.
	 * @see FallingBand.MonitoredParameters#getTemperature()
	 * @see #getMonitoredParameters()
	 * @generated
	 */
	EAttribute getMonitoredParameters_Temperature();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.MonitoredParameters#getHumidity <em>Humidity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Humidity</em>'.
	 * @see FallingBand.MonitoredParameters#getHumidity()
	 * @see #getMonitoredParameters()
	 * @generated
	 */
	EAttribute getMonitoredParameters_Humidity();

	/**
	 * Returns the meta object for the containment reference list '{@link FallingBand.MonitoredParameters#getPliersTightenings <em>Pliers Tightenings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pliers Tightenings</em>'.
	 * @see FallingBand.MonitoredParameters#getPliersTightenings()
	 * @see #getMonitoredParameters()
	 * @generated
	 */
	EReference getMonitoredParameters_PliersTightenings();

	/**
	 * Returns the meta object for class '{@link FallingBand.PliersData <em>Pliers Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pliers Data</em>'.
	 * @see FallingBand.PliersData
	 * @generated
	 */
	EClass getPliersData();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.PliersData#getPressure <em>Pressure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pressure</em>'.
	 * @see FallingBand.PliersData#getPressure()
	 * @see #getPliersData()
	 * @generated
	 */
	EAttribute getPliersData_Pressure();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.PliersData#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see FallingBand.PliersData#getNumber()
	 * @see #getPliersData()
	 * @generated
	 */
	EAttribute getPliersData_Number();

	/**
	 * Returns the meta object for class '{@link FallingBand.ComplianceReport <em>Compliance Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compliance Report</em>'.
	 * @see FallingBand.ComplianceReport
	 * @generated
	 */
	EClass getComplianceReport();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ComplianceReport#getFallingWheelBand <em>Falling Wheel Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Falling Wheel Band</em>'.
	 * @see FallingBand.ComplianceReport#getFallingWheelBand()
	 * @see #getComplianceReport()
	 * @generated
	 */
	EAttribute getComplianceReport_FallingWheelBand();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ComplianceReport#getFallingEngineBand <em>Falling Engine Band</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Falling Engine Band</em>'.
	 * @see FallingBand.ComplianceReport#getFallingEngineBand()
	 * @see #getComplianceReport()
	 * @generated
	 */
	EAttribute getComplianceReport_FallingEngineBand();

	/**
	 * Returns the meta object for class '{@link FallingBand.Provider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see FallingBand.Provider
	 * @generated
	 */
	EClass getProvider();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Provider#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Provider</em>'.
	 * @see FallingBand.Provider#getProvider()
	 * @see #getProvider()
	 * @generated
	 */
	EAttribute getProvider_Provider();

	/**
	 * Returns the meta object for class '{@link FallingBand.Shaft <em>Shaft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shaft</em>'.
	 * @see FallingBand.Shaft
	 * @generated
	 */
	EClass getShaft();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Shaft#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see FallingBand.Shaft#getModel()
	 * @see #getShaft()
	 * @generated
	 */
	EAttribute getShaft_Model();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Shaft#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.Shaft#getParameters()
	 * @see #getShaft()
	 * @generated
	 */
	EReference getShaft_Parameters();

	/**
	 * Returns the meta object for class '{@link FallingBand.ShaftBatchParameters <em>Shaft Batch Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shaft Batch Parameters</em>'.
	 * @see FallingBand.ShaftBatchParameters
	 * @generated
	 */
	EClass getShaftBatchParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getMaxLength <em>Max Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Length</em>'.
	 * @see FallingBand.ShaftBatchParameters#getMaxLength()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_MaxLength();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getMinLength <em>Min Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Length</em>'.
	 * @see FallingBand.ShaftBatchParameters#getMinLength()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_MinLength();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getAvgLength <em>Avg Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Length</em>'.
	 * @see FallingBand.ShaftBatchParameters#getAvgLength()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_AvgLength();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getMaxDiameter <em>Max Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Diameter</em>'.
	 * @see FallingBand.ShaftBatchParameters#getMaxDiameter()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_MaxDiameter();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getMinDiameter <em>Min Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Diameter</em>'.
	 * @see FallingBand.ShaftBatchParameters#getMinDiameter()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_MinDiameter();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getAvgDiameter <em>Avg Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Diameter</em>'.
	 * @see FallingBand.ShaftBatchParameters#getAvgDiameter()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_AvgDiameter();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.ShaftBatchParameters#getBatchId <em>Batch Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Batch Id</em>'.
	 * @see FallingBand.ShaftBatchParameters#getBatchId()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EAttribute getShaftBatchParameters_BatchId();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.ShaftBatchParameters#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provider</em>'.
	 * @see FallingBand.ShaftBatchParameters#getProvider()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EReference getShaftBatchParameters_Provider();

	/**
	 * Returns the meta object for the containment reference list '{@link FallingBand.ShaftBatchParameters#getPowderCoatingInspections <em>Powder Coating Inspections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Powder Coating Inspections</em>'.
	 * @see FallingBand.ShaftBatchParameters#getPowderCoatingInspections()
	 * @see #getShaftBatchParameters()
	 * @generated
	 */
	EReference getShaftBatchParameters_PowderCoatingInspections();

	/**
	 * Returns the meta object for class '{@link FallingBand.PowderCoatingChecks <em>Powder Coating Checks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Powder Coating Checks</em>'.
	 * @see FallingBand.PowderCoatingChecks
	 * @generated
	 */
	EClass getPowderCoatingChecks();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.PowderCoatingChecks#getZone <em>Zone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Zone</em>'.
	 * @see FallingBand.PowderCoatingChecks#getZone()
	 * @see #getPowderCoatingChecks()
	 * @generated
	 */
	EAttribute getPowderCoatingChecks_Zone();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.PowderCoatingChecks#getThickness <em>Thickness</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Thickness</em>'.
	 * @see FallingBand.PowderCoatingChecks#getThickness()
	 * @see #getPowderCoatingChecks()
	 * @generated
	 */
	EAttribute getPowderCoatingChecks_Thickness();

	/**
	 * Returns the meta object for class '{@link FallingBand.Machine <em>Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Machine</em>'.
	 * @see FallingBand.Machine
	 * @generated
	 */
	EClass getMachine();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Machine#getLastCalibration <em>Last Calibration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Calibration</em>'.
	 * @see FallingBand.Machine#getLastCalibration()
	 * @see #getMachine()
	 * @generated
	 */
	EAttribute getMachine_LastCalibration();

	/**
	 * Returns the meta object for the containment reference list '{@link FallingBand.Machine#getPrograms <em>Programs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Programs</em>'.
	 * @see FallingBand.Machine#getPrograms()
	 * @see #getMachine()
	 * @generated
	 */
	EReference getMachine_Programs();

	/**
	 * Returns the meta object for class '{@link FallingBand.Program <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Program</em>'.
	 * @see FallingBand.Program
	 * @generated
	 */
	EClass getProgram();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Program#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see FallingBand.Program#getNumber()
	 * @see #getProgram()
	 * @generated
	 */
	EAttribute getProgram_Number();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Program#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.Program#getParameters()
	 * @see #getProgram()
	 * @generated
	 */
	EReference getProgram_Parameters();

	/**
	 * Returns the meta object for class '{@link FallingBand.Parameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameters</em>'.
	 * @see FallingBand.Parameters
	 * @generated
	 */
	EClass getParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Parameters#getTighteningForce <em>Tightening Force</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tightening Force</em>'.
	 * @see FallingBand.Parameters#getTighteningForce()
	 * @see #getParameters()
	 * @generated
	 */
	EAttribute getParameters_TighteningForce();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Parameters#getTighteningTime <em>Tightening Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tightening Time</em>'.
	 * @see FallingBand.Parameters#getTighteningTime()
	 * @see #getParameters()
	 * @generated
	 */
	EAttribute getParameters_TighteningTime();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Parameters#getWheelBandPosition <em>Wheel Band Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wheel Band Position</em>'.
	 * @see FallingBand.Parameters#getWheelBandPosition()
	 * @see #getParameters()
	 * @generated
	 */
	EReference getParameters_WheelBandPosition();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Parameters#getEngineBandPosition <em>Engine Band Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Engine Band Position</em>'.
	 * @see FallingBand.Parameters#getEngineBandPosition()
	 * @see #getParameters()
	 * @generated
	 */
	EReference getParameters_EngineBandPosition();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Parameters#getWheelHousingPosition <em>Wheel Housing Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wheel Housing Position</em>'.
	 * @see FallingBand.Parameters#getWheelHousingPosition()
	 * @see #getParameters()
	 * @generated
	 */
	EReference getParameters_WheelHousingPosition();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Parameters#getEngineHousingPosition <em>Engine Housing Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Engine Housing Position</em>'.
	 * @see FallingBand.Parameters#getEngineHousingPosition()
	 * @see #getParameters()
	 * @generated
	 */
	EReference getParameters_EngineHousingPosition();

	/**
	 * Returns the meta object for class '{@link FallingBand.Coordinate <em>Coordinate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coordinate</em>'.
	 * @see FallingBand.Coordinate
	 * @generated
	 */
	EClass getCoordinate();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Coordinate#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see FallingBand.Coordinate#getX()
	 * @see #getCoordinate()
	 * @generated
	 */
	EAttribute getCoordinate_X();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Coordinate#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see FallingBand.Coordinate#getY()
	 * @see #getCoordinate()
	 * @generated
	 */
	EAttribute getCoordinate_Y();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Coordinate#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see FallingBand.Coordinate#getZ()
	 * @see #getCoordinate()
	 * @generated
	 */
	EAttribute getCoordinate_Z();

	/**
	 * Returns the meta object for class '{@link FallingBand.Housing <em>Housing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Housing</em>'.
	 * @see FallingBand.Housing
	 * @generated
	 */
	EClass getHousing();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.Housing#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see FallingBand.Housing#getModel()
	 * @see #getHousing()
	 * @generated
	 */
	EAttribute getHousing_Model();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.Housing#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see FallingBand.Housing#getParameters()
	 * @see #getHousing()
	 * @generated
	 */
	EReference getHousing_Parameters();

	/**
	 * Returns the meta object for class '{@link FallingBand.HousingParameters <em>Housing Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Housing Parameters</em>'.
	 * @see FallingBand.HousingParameters
	 * @generated
	 */
	EClass getHousingParameters();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getMaxLength <em>Max Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Length</em>'.
	 * @see FallingBand.HousingParameters#getMaxLength()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_MaxLength();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getMinLength <em>Min Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Length</em>'.
	 * @see FallingBand.HousingParameters#getMinLength()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_MinLength();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getAvgLength <em>Avg Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Length</em>'.
	 * @see FallingBand.HousingParameters#getAvgLength()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_AvgLength();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getMaxDiameter <em>Max Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Diameter</em>'.
	 * @see FallingBand.HousingParameters#getMaxDiameter()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_MaxDiameter();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getMinDiameter <em>Min Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Diameter</em>'.
	 * @see FallingBand.HousingParameters#getMinDiameter()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_MinDiameter();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getAvgDiameter <em>Avg Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Diameter</em>'.
	 * @see FallingBand.HousingParameters#getAvgDiameter()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_AvgDiameter();

	/**
	 * Returns the meta object for the attribute '{@link FallingBand.HousingParameters#getBatchId <em>Batch Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Batch Id</em>'.
	 * @see FallingBand.HousingParameters#getBatchId()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EAttribute getHousingParameters_BatchId();

	/**
	 * Returns the meta object for the containment reference '{@link FallingBand.HousingParameters#getProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provider</em>'.
	 * @see FallingBand.HousingParameters#getProvider()
	 * @see #getHousingParameters()
	 * @generated
	 */
	EReference getHousingParameters_Provider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FallingFactory getFallingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link FallingBand.impl.DriveHalfShaftImpl <em>Drive Half Shaft</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.DriveHalfShaftImpl
		 * @see FallingBand.impl.FallingPackageImpl#getDriveHalfShaft()
		 * @generated
		 */
		EClass DRIVE_HALF_SHAFT = eINSTANCE.getDriveHalfShaft();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DRIVE_HALF_SHAFT__ID = eINSTANCE.getDriveHalfShaft_Id();

		/**
		 * The meta object literal for the '<em><b>Manufactured Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DRIVE_HALF_SHAFT__MANUFACTURED_TIME = eINSTANCE.getDriveHalfShaft_ManufacturedTime();

		/**
		 * The meta object literal for the '<em><b>Engine Band</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__ENGINE_BAND = eINSTANCE.getDriveHalfShaft_EngineBand();

		/**
		 * The meta object literal for the '<em><b>Wheel Band</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__WHEEL_BAND = eINSTANCE.getDriveHalfShaft_WheelBand();

		/**
		 * The meta object literal for the '<em><b>Assembly Session</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__ASSEMBLY_SESSION = eINSTANCE.getDriveHalfShaft_AssemblySession();

		/**
		 * The meta object literal for the '<em><b>Report</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__REPORT = eINSTANCE.getDriveHalfShaft_Report();

		/**
		 * The meta object literal for the '<em><b>Shaft</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__SHAFT = eINSTANCE.getDriveHalfShaft_Shaft();

		/**
		 * The meta object literal for the '<em><b>Engine Housing</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__ENGINE_HOUSING = eINSTANCE.getDriveHalfShaft_EngineHousing();

		/**
		 * The meta object literal for the '<em><b>Wheel Housing</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DRIVE_HALF_SHAFT__WHEEL_HOUSING = eINSTANCE.getDriveHalfShaft_WheelHousing();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.BandImpl <em>Band</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.BandImpl
		 * @see FallingBand.impl.FallingPackageImpl#getBand()
		 * @generated
		 */
		EClass BAND = eINSTANCE.getBand();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND__MODEL = eINSTANCE.getBand_Model();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BAND__PARAMETERS = eINSTANCE.getBand_Parameters();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.BandBatchParametersImpl <em>Band Batch Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.BandBatchParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getBandBatchParameters()
		 * @generated
		 */
		EClass BAND_BATCH_PARAMETERS = eINSTANCE.getBandBatchParameters();

		/**
		 * The meta object literal for the '<em><b>Max Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__MAX_THICKNESS = eINSTANCE.getBandBatchParameters_MaxThickness();

		/**
		 * The meta object literal for the '<em><b>Min Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__MIN_THICKNESS = eINSTANCE.getBandBatchParameters_MinThickness();

		/**
		 * The meta object literal for the '<em><b>Avg Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__AVG_THICKNESS = eINSTANCE.getBandBatchParameters_AvgThickness();

		/**
		 * The meta object literal for the '<em><b>Batch Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BAND_BATCH_PARAMETERS__BATCH_ID = eINSTANCE.getBandBatchParameters_BatchId();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BAND_BATCH_PARAMETERS__PROVIDER = eINSTANCE.getBandBatchParameters_Provider();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.AssemblySessionImpl <em>Assembly Session</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.AssemblySessionImpl
		 * @see FallingBand.impl.FallingPackageImpl#getAssemblySession()
		 * @generated
		 */
		EClass ASSEMBLY_SESSION = eINSTANCE.getAssemblySession();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSEMBLY_SESSION__START = eINSTANCE.getAssemblySession_Start();

		/**
		 * The meta object literal for the '<em><b>Stop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSEMBLY_SESSION__STOP = eINSTANCE.getAssemblySession_Stop();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY_SESSION__PARAMETERS = eINSTANCE.getAssemblySession_Parameters();

		/**
		 * The meta object literal for the '<em><b>Machine</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY_SESSION__MACHINE = eINSTANCE.getAssemblySession_Machine();

		/**
		 * The meta object literal for the '<em><b>Program</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSEMBLY_SESSION__PROGRAM = eINSTANCE.getAssemblySession_Program();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.MonitoredParametersImpl <em>Monitored Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.MonitoredParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getMonitoredParameters()
		 * @generated
		 */
		EClass MONITORED_PARAMETERS = eINSTANCE.getMonitoredParameters();

		/**
		 * The meta object literal for the '<em><b>Temperature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORED_PARAMETERS__TEMPERATURE = eINSTANCE.getMonitoredParameters_Temperature();

		/**
		 * The meta object literal for the '<em><b>Humidity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORED_PARAMETERS__HUMIDITY = eINSTANCE.getMonitoredParameters_Humidity();

		/**
		 * The meta object literal for the '<em><b>Pliers Tightenings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED_PARAMETERS__PLIERS_TIGHTENINGS = eINSTANCE.getMonitoredParameters_PliersTightenings();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.PliersDataImpl <em>Pliers Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.PliersDataImpl
		 * @see FallingBand.impl.FallingPackageImpl#getPliersData()
		 * @generated
		 */
		EClass PLIERS_DATA = eINSTANCE.getPliersData();

		/**
		 * The meta object literal for the '<em><b>Pressure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLIERS_DATA__PRESSURE = eINSTANCE.getPliersData_Pressure();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLIERS_DATA__NUMBER = eINSTANCE.getPliersData_Number();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ComplianceReportImpl <em>Compliance Report</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ComplianceReportImpl
		 * @see FallingBand.impl.FallingPackageImpl#getComplianceReport()
		 * @generated
		 */
		EClass COMPLIANCE_REPORT = eINSTANCE.getComplianceReport();

		/**
		 * The meta object literal for the '<em><b>Falling Wheel Band</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLIANCE_REPORT__FALLING_WHEEL_BAND = eINSTANCE.getComplianceReport_FallingWheelBand();

		/**
		 * The meta object literal for the '<em><b>Falling Engine Band</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLIANCE_REPORT__FALLING_ENGINE_BAND = eINSTANCE.getComplianceReport_FallingEngineBand();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ProviderImpl
		 * @see FallingBand.impl.FallingPackageImpl#getProvider()
		 * @generated
		 */
		EClass PROVIDER = eINSTANCE.getProvider();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDER__PROVIDER = eINSTANCE.getProvider_Provider();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ShaftImpl <em>Shaft</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ShaftImpl
		 * @see FallingBand.impl.FallingPackageImpl#getShaft()
		 * @generated
		 */
		EClass SHAFT = eINSTANCE.getShaft();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT__MODEL = eINSTANCE.getShaft_Model();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHAFT__PARAMETERS = eINSTANCE.getShaft_Parameters();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ShaftBatchParametersImpl <em>Shaft Batch Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ShaftBatchParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getShaftBatchParameters()
		 * @generated
		 */
		EClass SHAFT_BATCH_PARAMETERS = eINSTANCE.getShaftBatchParameters();

		/**
		 * The meta object literal for the '<em><b>Max Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__MAX_LENGTH = eINSTANCE.getShaftBatchParameters_MaxLength();

		/**
		 * The meta object literal for the '<em><b>Min Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__MIN_LENGTH = eINSTANCE.getShaftBatchParameters_MinLength();

		/**
		 * The meta object literal for the '<em><b>Avg Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__AVG_LENGTH = eINSTANCE.getShaftBatchParameters_AvgLength();

		/**
		 * The meta object literal for the '<em><b>Max Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__MAX_DIAMETER = eINSTANCE.getShaftBatchParameters_MaxDiameter();

		/**
		 * The meta object literal for the '<em><b>Min Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__MIN_DIAMETER = eINSTANCE.getShaftBatchParameters_MinDiameter();

		/**
		 * The meta object literal for the '<em><b>Avg Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__AVG_DIAMETER = eINSTANCE.getShaftBatchParameters_AvgDiameter();

		/**
		 * The meta object literal for the '<em><b>Batch Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHAFT_BATCH_PARAMETERS__BATCH_ID = eINSTANCE.getShaftBatchParameters_BatchId();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHAFT_BATCH_PARAMETERS__PROVIDER = eINSTANCE.getShaftBatchParameters_Provider();

		/**
		 * The meta object literal for the '<em><b>Powder Coating Inspections</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHAFT_BATCH_PARAMETERS__POWDER_COATING_INSPECTIONS = eINSTANCE.getShaftBatchParameters_PowderCoatingInspections();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.PowderCoatingChecksImpl <em>Powder Coating Checks</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.PowderCoatingChecksImpl
		 * @see FallingBand.impl.FallingPackageImpl#getPowderCoatingChecks()
		 * @generated
		 */
		EClass POWDER_COATING_CHECKS = eINSTANCE.getPowderCoatingChecks();

		/**
		 * The meta object literal for the '<em><b>Zone</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POWDER_COATING_CHECKS__ZONE = eINSTANCE.getPowderCoatingChecks_Zone();

		/**
		 * The meta object literal for the '<em><b>Thickness</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POWDER_COATING_CHECKS__THICKNESS = eINSTANCE.getPowderCoatingChecks_Thickness();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.MachineImpl <em>Machine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.MachineImpl
		 * @see FallingBand.impl.FallingPackageImpl#getMachine()
		 * @generated
		 */
		EClass MACHINE = eINSTANCE.getMachine();

		/**
		 * The meta object literal for the '<em><b>Last Calibration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MACHINE__LAST_CALIBRATION = eINSTANCE.getMachine_LastCalibration();

		/**
		 * The meta object literal for the '<em><b>Programs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MACHINE__PROGRAMS = eINSTANCE.getMachine_Programs();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ProgramImpl <em>Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ProgramImpl
		 * @see FallingBand.impl.FallingPackageImpl#getProgram()
		 * @generated
		 */
		EClass PROGRAM = eINSTANCE.getProgram();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROGRAM__NUMBER = eINSTANCE.getProgram_Number();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROGRAM__PARAMETERS = eINSTANCE.getProgram_Parameters();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.ParametersImpl <em>Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.ParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getParameters()
		 * @generated
		 */
		EClass PARAMETERS = eINSTANCE.getParameters();

		/**
		 * The meta object literal for the '<em><b>Tightening Force</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETERS__TIGHTENING_FORCE = eINSTANCE.getParameters_TighteningForce();

		/**
		 * The meta object literal for the '<em><b>Tightening Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETERS__TIGHTENING_TIME = eINSTANCE.getParameters_TighteningTime();

		/**
		 * The meta object literal for the '<em><b>Wheel Band Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETERS__WHEEL_BAND_POSITION = eINSTANCE.getParameters_WheelBandPosition();

		/**
		 * The meta object literal for the '<em><b>Engine Band Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETERS__ENGINE_BAND_POSITION = eINSTANCE.getParameters_EngineBandPosition();

		/**
		 * The meta object literal for the '<em><b>Wheel Housing Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETERS__WHEEL_HOUSING_POSITION = eINSTANCE.getParameters_WheelHousingPosition();

		/**
		 * The meta object literal for the '<em><b>Engine Housing Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETERS__ENGINE_HOUSING_POSITION = eINSTANCE.getParameters_EngineHousingPosition();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.CoordinateImpl <em>Coordinate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.CoordinateImpl
		 * @see FallingBand.impl.FallingPackageImpl#getCoordinate()
		 * @generated
		 */
		EClass COORDINATE = eINSTANCE.getCoordinate();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATE__X = eINSTANCE.getCoordinate_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATE__Y = eINSTANCE.getCoordinate_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORDINATE__Z = eINSTANCE.getCoordinate_Z();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.HousingImpl <em>Housing</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.HousingImpl
		 * @see FallingBand.impl.FallingPackageImpl#getHousing()
		 * @generated
		 */
		EClass HOUSING = eINSTANCE.getHousing();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING__MODEL = eINSTANCE.getHousing_Model();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOUSING__PARAMETERS = eINSTANCE.getHousing_Parameters();

		/**
		 * The meta object literal for the '{@link FallingBand.impl.HousingParametersImpl <em>Housing Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FallingBand.impl.HousingParametersImpl
		 * @see FallingBand.impl.FallingPackageImpl#getHousingParameters()
		 * @generated
		 */
		EClass HOUSING_PARAMETERS = eINSTANCE.getHousingParameters();

		/**
		 * The meta object literal for the '<em><b>Max Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__MAX_LENGTH = eINSTANCE.getHousingParameters_MaxLength();

		/**
		 * The meta object literal for the '<em><b>Min Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__MIN_LENGTH = eINSTANCE.getHousingParameters_MinLength();

		/**
		 * The meta object literal for the '<em><b>Avg Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__AVG_LENGTH = eINSTANCE.getHousingParameters_AvgLength();

		/**
		 * The meta object literal for the '<em><b>Max Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__MAX_DIAMETER = eINSTANCE.getHousingParameters_MaxDiameter();

		/**
		 * The meta object literal for the '<em><b>Min Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__MIN_DIAMETER = eINSTANCE.getHousingParameters_MinDiameter();

		/**
		 * The meta object literal for the '<em><b>Avg Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__AVG_DIAMETER = eINSTANCE.getHousingParameters_AvgDiameter();

		/**
		 * The meta object literal for the '<em><b>Batch Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOUSING_PARAMETERS__BATCH_ID = eINSTANCE.getHousingParameters_BatchId();

		/**
		 * The meta object literal for the '<em><b>Provider</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOUSING_PARAMETERS__PROVIDER = eINSTANCE.getHousingParameters_Provider();

	}

} //FallingPackage
