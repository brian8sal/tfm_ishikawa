/*
 * generated by Xtext 2.25.0
 */
package es.unican.parser.antlr;

import com.google.inject.Inject;
import es.unican.parser.antlr.internal.InternalDOFParser;
import es.unican.services.DOFGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class DOFParser extends AbstractAntlrParser {

	@Inject
	private DOFGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalDOFParser createParser(XtextTokenStream stream) {
		return new InternalDOFParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "DOF";
	}

	public DOFGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(DOFGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
