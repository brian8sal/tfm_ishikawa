driveHalfShafts[["id", "idAssemblySession", "idComplianceReport"]].merge(assemblySessions,
    how="left", left_on="idAssemblySession", right_on="id", suffixes=("", "assemblySession")).merge((pliersData.pivot(
    index="idMonitoredParameters", columns=["number"], values=["pressure"])), how="left", left_on="idMonitoredParameters",
    right_on="idMonitoredParameters").merge(monitoredParameters, how="left", left_on="idMonitoredParameters",
    right_on="id", suffixes=("", "parameters")).merge(reports[["id", "fallingWheelBand"]], how="left", left_on="idComplianceReport",
    right_on="id", suffixes=("", "report")).drop(columns=["idAssemblySession", "idparameters", "idComplianceReport", "start",
    "stop", "temperature", "humidity", "idassemblySession", "idMonitoredParameters", "idreport", "idProgram", "idMachine"])