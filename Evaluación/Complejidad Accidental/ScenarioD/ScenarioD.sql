SELECT d.id,
	c.fallingWheelBand,
	bw.model,
	bbw.maxThickness,
	bbw.minThickness,
	bbw.avgThickness,
	pw.name
	FROM DriveHalfShaft d JOIN ComplianceReport c
		ON d.idComplianceReport = c.id
	JOIN
	(Band bw JOIN BandBatchParameters bbw
			ON bw.batchId = bbw.batchId
			JOIN Provider pw
			ON pw.id = bbw.idProvider)
	ON d.idBandWheel = bw.id;
