use master;
go

drop database driveHalfShaftDomainModel;
go

create database driveHalfShaftDomainModel;
go

use driveHalfShaftDomainModel;
go

CREATE TABLE MonitoredParameters(
	id INTEGER NOT NULL PRIMARY KEY,
	temperature DECIMAL(3,1) NOT NULL,
	humidity DECIMAL(3,1) NOT NULL
);
go

CREATE TABLE Coordinate(
	id INTEGER NOT NULL PRIMARY KEY,
	x DECIMAL(3,1) NOT NULL,
	y DECIMAL(3,1) NOT NULL,
	z DECIMAL(3,1) NOT NULL
);
go

CREATE TABLE Parameters(
	id INTEGER NOT NULL PRIMARY KEY,
	tighteningForce DECIMAL(3,1) NOT NULL,
	tighteningTime DECIMAL(3,1) NOT NULL,
	idWheelHousingPosition INTEGER,
	idWheelBandPosition INTEGER,
	idEngineBandPosition INTEGER,
	idEngineHousingPosition INTEGER,
	FOREIGN KEY (idWheelHousingPosition) REFERENCES Coordinate (id),
	FOREIGN KEY (idWheelBandPosition) REFERENCES Coordinate (id),
	FOREIGN KEY (idEngineBandPosition) REFERENCES Coordinate (id),
	FOREIGN KEY (idEngineHousingPosition) REFERENCES Coordinate (id)
);
go

CREATE TABLE Program(
	id INTEGER NOT NULL PRIMARY KEY,
	number INTEGER NOT NULL,
	idParameters INTEGER NOT NULL,
	FOREIGN KEY (idParameters) REFERENCES Parameters (id)
);
go

CREATE TABLE Machine(
	id INTEGER NOT NULL PRIMARY KEY,
	lastCalibration DATE NOT NULL,
	idProgram INTEGER NOT NULL,
	FOREIGN KEY (idProgram) REFERENCES Program (id)
);
go

CREATE TABLE AssemblySession(
	id INTEGER NOT NULL PRIMARY KEY,
	start DATE NOT NULL,
	stop DATE NOT NULL,
	idMonitoredParameters INTEGER NOT NULL,
	idProgram INTEGER NOT NULL,
	idMachine INTEGER NOT NULL,
	FOREIGN KEY (idMonitoredParameters) REFERENCES MonitoredParameters (id),
	FOREIGN KEY (idProgram) REFERENCES Program (id),
	FOREIGN KEY (idMachine) REFERENCES Machine (id)
);
go

CREATE TABLE ComplianceReport(
	id INTEGER NOT NULL PRIMARY KEY,
	fallingWheelBand BIT NOT NULL,
	fallingEngineBand BIT NOT NULL
);
go

CREATE TABLE Provider(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(20) NOT NULL
);
go

CREATE TABLE PowderCoatingChecks(
	id INTEGER NOT NULL PRIMARY KEY,
	zone VARCHAR(20) NOT NULL,
	thickness decimal(3,1) NOT NULL
);
go

CREATE TABLE BandBatchParameters(
	batchID VARCHAR(20) NOT NULL PRIMARY KEY,
	maxThickness decimal(3,1) NOT NULL,
	minThickness decimal(3,1) NOT NULL,
	avgThickness decimal(3,1) NOT NULL,
	idProvider INTEGER,
	FOREIGN KEY (idProvider) REFERENCES Provider (id)
);
go

CREATE TABLE HousingParameters(
	batchID VARCHAR(20) NOT NULL PRIMARY KEY,
	maxLength decimal(3,1) NOT NULL,
	minLength decimal(3,1) NOT NULL,
	avgLength decimal(3,1) NOT NULL,
	maxDiameter decimal(3,1) NOT NULL,
	minDiameter decimal(3,1) NOT NULL,
	avgDiameter decimal(3,1) NOT NULL,
	idProvider INTEGER,
	FOREIGN KEY (idProvider) REFERENCES Provider (id)
);
go

CREATE TABLE ShaftBatchParameters(
	batchID VARCHAR(20) NOT NULL PRIMARY KEY,
	maxLength decimal(3,1) NOT NULL,
	minLength decimal(3,1) NOT NULL,
	avgLength decimal(3,1) NOT NULL,
	maxDiameter decimal(3,1) NOT NULL,
	minDiameter decimal(3,1) NOT NULL,
	avgDiameter decimal(3,1) NOT NULL,
	idProvider INTEGER,
	idPowderCoatingInspections INTEGER NOT NULL,
	FOREIGN KEY (idProvider) REFERENCES Provider (id),
	FOREIGN KEY (idPowderCoatingInspections) REFERENCES PowderCoatingChecks (id)
);
go

CREATE TABLE Band(
	id INTEGER NOT NULL PRIMARY KEY,
	model VARCHAR(20) NOT NULL,
	batchId VARCHAR(20),
	FOREIGN KEY (batchId) REFERENCES BandBatchParameters (batchID)
);
go

CREATE TABLE Housing(
	id INTEGER NOT NULL PRIMARY KEY,
	model VARCHAR(20) NOT NULL,
	batchId VARCHAR(20),
	FOREIGN KEY (batchId) REFERENCES HousingParameters (batchID)
);
go

CREATE TABLE Shaft(
	id INTEGER NOT NULL PRIMARY KEY,
  model VARCHAR(20) NOT NULL,
  batchId VARCHAR(20),
  FOREIGN KEY (batchId) REFERENCES ShaftBatchParameters (batchID)
);
go

CREATE TABLE PliersData(
	pressure DECIMAL(3,1) NOT NULL,
	number INTEGER NOT NULL,
	idMonitoredParameters INTEGER NOT NULL,
	FOREIGN KEY (idMonitoredParameters) REFERENCES MonitoredParameters (id),
	PRIMARY KEY (idMonitoredParameters,number),
	CHECK ( number between 1 and 4)
);
go

CREATE TABLE DriveHalfShaft(
	id VARCHAR(20) NOT NULL PRIMARY KEY,
	manufacturedTime DATE NOT NULL,
	idAssemblySession INTEGER,
	idComplianceReport INTEGER,
	idBandEngine INTEGER,
	idBandWheel INTEGER,
	idHousingEngine INTEGER,
	idHousingWheel INTEGER,
	idShaft INTEGER,
	FOREIGN KEY (idAssemblySession) REFERENCES AssemblySession (id),
	FOREIGN KEY (idComplianceReport) REFERENCES ComplianceReport (id),
	FOREIGN KEY (idBandEngine) REFERENCES Band (id),
	FOREIGN KEY (idBandWheel) REFERENCES Band (id),
	FOREIGN KEY (idHousingEngine) REFERENCES Housing (id),
	FOREIGN KEY (idHousingWheel) REFERENCES Housing (id),
	FOREIGN KEY (idShaft) REFERENCES Shaft (id)
);
go
