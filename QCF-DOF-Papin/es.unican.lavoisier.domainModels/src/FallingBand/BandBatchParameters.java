/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Band Batch Parameters</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.BandBatchParameters#getMaxThickness <em>Max Thickness</em>}</li>
 *   <li>{@link FallingBand.BandBatchParameters#getMinThickness <em>Min Thickness</em>}</li>
 *   <li>{@link FallingBand.BandBatchParameters#getAvgThickness <em>Avg Thickness</em>}</li>
 *   <li>{@link FallingBand.BandBatchParameters#getBatchId <em>Batch Id</em>}</li>
 *   <li>{@link FallingBand.BandBatchParameters#getProvider <em>Provider</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getBandBatchParameters()
 * @model
 * @generated
 */
public interface BandBatchParameters extends EObject {
	/**
	 * Returns the value of the '<em><b>Max Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Thickness</em>' attribute.
	 * @see #setMaxThickness(Double)
	 * @see FallingBand.FallingPackage#getBandBatchParameters_MaxThickness()
	 * @model
	 * @generated
	 */
	Double getMaxThickness();

	/**
	 * Sets the value of the '{@link FallingBand.BandBatchParameters#getMaxThickness <em>Max Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Thickness</em>' attribute.
	 * @see #getMaxThickness()
	 * @generated
	 */
	void setMaxThickness(Double value);

	/**
	 * Returns the value of the '<em><b>Min Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Thickness</em>' attribute.
	 * @see #setMinThickness(Double)
	 * @see FallingBand.FallingPackage#getBandBatchParameters_MinThickness()
	 * @model
	 * @generated
	 */
	Double getMinThickness();

	/**
	 * Sets the value of the '{@link FallingBand.BandBatchParameters#getMinThickness <em>Min Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Thickness</em>' attribute.
	 * @see #getMinThickness()
	 * @generated
	 */
	void setMinThickness(Double value);

	/**
	 * Returns the value of the '<em><b>Avg Thickness</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Thickness</em>' attribute.
	 * @see #setAvgThickness(Double)
	 * @see FallingBand.FallingPackage#getBandBatchParameters_AvgThickness()
	 * @model
	 * @generated
	 */
	Double getAvgThickness();

	/**
	 * Sets the value of the '{@link FallingBand.BandBatchParameters#getAvgThickness <em>Avg Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Thickness</em>' attribute.
	 * @see #getAvgThickness()
	 * @generated
	 */
	void setAvgThickness(Double value);

	/**
	 * Returns the value of the '<em><b>Batch Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Batch Id</em>' attribute.
	 * @see #setBatchId(Double)
	 * @see FallingBand.FallingPackage#getBandBatchParameters_BatchId()
	 * @model
	 * @generated
	 */
	Double getBatchId();

	/**
	 * Sets the value of the '{@link FallingBand.BandBatchParameters#getBatchId <em>Batch Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Batch Id</em>' attribute.
	 * @see #getBatchId()
	 * @generated
	 */
	void setBatchId(Double value);

	/**
	 * Returns the value of the '<em><b>Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider</em>' containment reference.
	 * @see #setProvider(Provider)
	 * @see FallingBand.FallingPackage#getBandBatchParameters_Provider()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Provider getProvider();

	/**
	 * Sets the value of the '{@link FallingBand.BandBatchParameters#getProvider <em>Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provider</em>' containment reference.
	 * @see #getProvider()
	 * @generated
	 */
	void setProvider(Provider value);

} // BandBatchParameters
