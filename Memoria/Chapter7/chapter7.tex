% !TeX spellcheck = en_US
%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{Summary and Future Work}\label{chap:conclusions}

% **************************** Define Graphics Path **************************
\ifpdf
\graphicspath{{Chapter7/Figs/Raster/}{Chapter7/Figs/PDF/}{Chapter7/Figs/}}
\else
\graphicspath{{Chapter7/Figs/Vector/}{Chapter7/Figs/}}
\fi

\label{sec:conclusions}

This Master's Dissertation comprises the development of three editors and an interpreter for a series of Domain-Specific Languages (DSLs) designed to build datasets for Industry 4.0 applications. 
These languages are the result of a research line initiated a few years ago by the Software Engineering and Real-Time group of the University of Cantabria, whose objective is to reduce the complexity of dataset generation processes. 
Thus, what is reported in this work is part of a larger project in which the author have been participating in the last two years. 
Some preliminary results of this work were published in the \emph{10th International Conference on Model and Data Engineering (MEDI)} and a more complete version of it will be submitted to be considered for publication in a journal.

The solution developed in that project, and partially described in this work, aims to decrease the accidental complexity of dataset generation processes in Industry 4.0 contexts. 
Prior to this work, some researches of the  Software Engineering and Real-Time group developed a language called Lavoisier, a DSL that with high-level primitives allowed to select the data to be included in a dataset, without the need to pay attention to the low-level transformations that must be applied on them to arrange them in the required tabular format. 
Although Lavoisier demonstrated to decrease the accidental complexity of dataset generation processes by 50\% in average, 
its creators found an obstacle for its adoption in Industry 4.0 contexts. 
This obstacle was that this language represents domain data through object-oriented models, which are not commonly found in manufacturing settings. 
Nevertheless, another kind of model that might be used to describe domain data and that was frequently used in industrial contexts was discovered: the fishbone diagrams. 
Therefore, a new model-driven process for dataset generation was developed.
This process is comprised of three different DSLs.

The first DSL allows specifying Quality Control Fishbone models in a textual manner, being the equivalent of the conventional fishbone diagram. 
It is the only one of the three languages that is somewhat dispensable, but it allowed the Ishikawa diagrams to be represented and incorporated into the process to facilitate traceability.

Secondly, we created a new kind of fishbone diagram, called Data-Oriented Fishbone models, that can be used to represent influence relationships between domain data. 
In these models, causes are connected to domain data through special code blocks called \emph{data feeders}. 
This way, domain data is used to characterize causes in a fishbone model, so that fishbone models can be used to specify influence relationships between domain data. 
Code for the data feeders is based on Lavoisier, so we benefit on the advantages of this language. 

Finally, we created a third language, called \emph{Papin}, to select causes of a DOF that should be included in a dataset. 
Papin is a very simple language that references just causes and prevents industrial engineers from having to deal with object-oriented data models. 
These Papin specifications are processed by the Papin interpreter, which, with the help of some functions of the Lavoisier interpreter, is able to automatically generate a dataset. 

This work has focused on the development of the tools required to adequately support this new model-driven process. 
These tools are an editor for QCF models, an editor for DOF models, and an editor and an interpreter for Papin specifications. 
To develop each editor, a metamodel and a concrete syntax were created, plus the customization code required for implementing several extra features, such as validators and quickfixes. 

On a personal note, this Master's Dissertation has helped me to consolidate my knowledge and skills regarding software engineering and has allowed me to discover many others. 
Thanks to it, I have been able to deepen the knowledge that I began to acquire in Master courses such as Language Engineering. 
I have mastered tools like Xtext and EMF and I have grasped the meaning of DSLs or MDE, among many other things. 
Moreover, I have had my first experiences as a researcher. 
In this regard, I could learn to make a proper review of the state of the art and I could participate in the whole research proceeding that lead to the development of the new model-driven process for dataset generation that is partially reported in this work.
More specifically, I participated in the inception of the original idea, in its refinement until becoming a complete process, in the resolution of the different problems that were found, in the analysis of the different alternatives that were considered, in the development of the associated tooling (which is described in this document), in the design and execution of its evaluation process and in the dissemination of its results.
I consider all this an excellent complement to the different professional skills I have been acquiring during my university degrees.

%% Alternative 1

%This work has presented a model-driven process to automatize the generation of datasets for data analysis systems in industrial contexts, such as Industry 4.0 applications. This process adapts a previous work~\citep{DelaVega2020} to work with fishbone models instead of object-oriented domain models. These fishbone models can be more suitable than object-oriented domain models in manufacturing settings as industrial engineers are used to deal with the former ones whereas the latter ones are rarely found in these contexts.

%% Alternative 2

%% This work has presented a model-driven process to automatize the generation of datasets for data analysis systems in industrial contexts, such as Industry 4.0 applications. In a previous work~\citep{DelaVega2020}, we tackled the dataset creation problem from a general perspective. In that work, we developed a language, called \emph{Lavoisier}, that is able to reduce in average by 40\%-60\%, and up to 80\%, the complexity of a dataset creation process, as compared to state-of-the-art solutions. To represent data available in a domain to be included in a dataset, Lavoisier relies on object-oriented domain models, but these models are rarely found in industrial settings. However, industrial engineers are used to deal with fishbone diagrams, a kind of model typically used to represent cause-effect relationships.

%To build this process, two new languages were designed. First, we created a new kind of fishbone diagram, called Data-Oriented Fishbone models, that can be used to represent influence relationships between domain data. In these models, causes are connected to domain data through special code blocks called \emph{data feeders}. This way, domain data is used to characterize causes in a fishbone model, so that fishbone models can be used to specify influence relationships between domain data. Code for the data feeders is based on Lavoisier, so we benefit on the advantages of this language. Lavoisier was designed to be used by people without expertise in data science, and some empirical proved it~\citep{DelaVega2019}. Therefore, these data feeders might be even written by industrial engineers themselves. Nevertheless, this hypothesis is still pending of being checked by means of controlled empirical experiments.

%In addition, we created a second language, called \emph{Papin}, to select causes to be included in a dataset. This language is a very simple language that references just causes and prevents industrial engineers from having to deal with object-oriented data models. These Papin specifications are processed by the Papin interpreter, which, with the help of some functions of the Lavoisier interpreter, is able to automatically generate a dataset.

%Expressiveness of the languages created in this work has been evaluated by applying them to five case studies, four of them coming from the literature and the remaining one from an industrial partner. No major problems were reported. Moreover, we evaluated whether our approach kept the benefits of Lavoisier related to accidental complexity reduction. For this purpose, we created scripts for dataset generation in different scenarios using SQL, Pandas, Lavoisier and our approach. It was concluded that, as compared to Lavoisier, our approach introduces an overhead associated to the need of connecting causes with domain data. Despite this overhead, our approach reduces accidental complexity of dataset creation processes as compared to SQL and Pandas for non-trivial scenarios. As compared with Lavoisier, our approach is better or worse depending on the number of datasets to be generated from a domain model. In our approach, specifying a DOF implies an effort that does not pay off when this DOF is used to generate a small number of datasets. On the other hand, since Papin specifications are quite simple as compared to Lavoisier ones, specifying a DOF starts to provide benefits the more datasets are generated from it.

%In summary, it can be stated that our approach helps to decrease the complexity required to create a dataset, which would help to reduce development time, and, therefore, costs, of Industry 4.0 applications. Moreover, it might help to decrease the dependence on data scientists, whose fees are often expensive and whose availability might be scarce. Data scientists would still be needed to create object-oriented domain models, but they could be skipped to build DOFs and Papin specifications.

On another note, as future work, we have planned to perform some controlled experiments that allow us to check whether industrial engineers are actually able to use our languages. 
Moreover, we will also investigate whether alternative concrete syntaxes might be more suitable than the current one.
We will develop facilities to generate the skeleton of a DOF from its corresponding QCF model, to help to reduce the effort associated to building DOFs. 
Finally, we will try to improve the support for specifying aggregated values in \emph{data feeders}, so that columns representing, for instance, the average value of a collection can be incorporated into a dataset.

