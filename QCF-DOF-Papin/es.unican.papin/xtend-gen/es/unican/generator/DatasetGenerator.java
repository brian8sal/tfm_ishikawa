package es.unican.generator;

import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class DatasetGenerator extends ColumnSet {
  private List<EObject> instances;
  
  public DatasetGenerator(final List<EObject> instances) {
    super();
    this.instances = instances;
  }
  
  @Override
  public String toString() {
    this.getRows().keySet();
    final StringBuilder s = new StringBuilder();
    s.append(IterableExtensions.join(this.getColumnNames(), ",")).append("\n");
    for (final EObject instance : this.instances) {
      final Function1<ValueWrapper, String> _function = (ValueWrapper value) -> {
        return value.toString();
      };
      s.append(IterableExtensions.join(ListExtensions.<ValueWrapper, String>map(this.getRows().get(instance), _function), ",")).append("\n");
    }
    return s.toString();
  }
}
