% !TeX spellcheck = en_US

\section{Development of an Editor for QCFs}
\label{sec:developmentQCF}

\subsection{Metamodel}\label{subsec:metamodelQCF}

As specified in previous subsection, the first step to create the DSL for QCFs must be its metamodel definition. This metamodel was developed in Ecore and can be seen in Figure~\ref{fig:qcfMetamodel}. The elements that compose Ishikawa diagrams are effects, categories, causes and subcauses, and these are the ones that were included in the QCF metamodel. As it can be seen, each instance of the metamodel will have a QCF root element, which contains an effect. The effect contains one category at least, and each category contains one cause as a minimum. Causes may or may not contain subcauses.

\begin{figure}[h]
	\centering
	% Requires \usepackage{graphicx}
	\includegraphics[width=0.65\linewidth]{Chapter4/Figs/qualityControlFishboneMetamodel.pdf} \\
	\caption{QCF metamodel.}
	\label{fig:qcfMetamodel}
\end{figure}

\subsection{Concrete Syntax}
\label{subsec:concreteSyntaxQCF}

The next step after defining the abstract syntax of our languages is to provide a concrete syntax. In our case, we opted for creating a textual syntax for all these languages because we considered that: (1) writing plain text is faster than dragging and dropping boxes and arrows; (2) for DOF and Papin, textual specifications seem to be more natural than graphical ones; and, (3) as previously commented, textual models can be more easily versioned using configuration management tools than graphical ones. 

These textual syntaxes, as said before, were defined using Xtext. To illustrate briefly how this was made, Listing~\ref{lst:qcfGrammar} shows the grammar of the Quality Control Fishbone models. 

To start with, the name of both the language and the grammar is defined (line 1). 
Additionally, the \emph{Terminals} grammar (line 1) is imported. 
These grammar will be used to define elements such as identifiers or numbers. 
Lines 3-4 perform some configurations so that Xtext can work with Ecore. 
After these declarations, the actual rules of the grammar, which are six, are specified. 
The first one (lines 6-8) serves as the root of the AST (\emph{Abstract Syntax Tree}). 
This rule produces QCFs, and it specifies that when it is executed, a new instance of the QCF class of the DSL metamodel must be created (line 6). 
To declare a QCF, we use the \texttt{qcf} keyword, followed by the name we want to use for the fishbone diagram plus, an effect declaration. 
Effects are processed by its own rule (lines 10-13). 
As it can be seen, line 8 specifies that the effect produced by the rule of lines 10-13 will be stored in the effect attribute of the QCF metaclass (see Figure~\ref{fig:qcfMetamodel}).

The declaration of the other rules is very similar. 
For example, the rule for causes (lines 20-24) states that an instance of the \emph{Cause} metaclass will be created (line 20). Then it is declared that a cause definition always starts with the \emph{cause} keyword (line 21) as it is indicated by the single quotes, followed by a string that must fulfill the rules and constraints for specifying identifiers, as indicated by the \emph{EString} keyword. 
This string is assigned to the attribute \emph{name} of the \emph{Cause} metaclass. 
Moreover, a \emph{Cause} may optionally contain a \emph{description} (line 22) and a \emph{valueOfInterest} (line 23). 
Lastly, a collection of at least one \emph{subCause} must be specified using the \emph{contains} keyword followed by the causes.

\noindent
\begin{minipage}{\linewidth}
	\lstinputlisting[style=qcf,caption={Grammar for the Quality Control Fishbone language proposed.},label=lst:qcfGrammar,captionpos=b,basicstyle=\scriptsize]{listings/qcfGrammar.txt}
\end{minipage}	

\subsection{Extra Features}
\label{subsec:extraFeaturesQCF}

An advantage of using Xtext is that it is able to automatically generate, from a grammar specification linked to a metamodel, a full-fledged editor plus a parser. The generated editor offers a smooth inclusion of facilities, such as syntax highlighting, auto-completion, content-assistants or live-validation. 

One of the facilities that were used for the development of our DSLs was validation. 
Validation in Xtext is very useful, since the complete correctness of a program usually escapes the parsing phase, being required some additional checks. 
Xtext offers a default validator to be activated when generating the artifacts, which checks that names of elements are unique
(\emph{NamesAreUniqueValidator}). 
Nevertheless, for this project such validator was not enough and a customized one was necessary. 
The customized validator is written using \emph{Xtend}, a mode management language, or \emph{Java}. 
In this case, we used Java. 
A snippet of this validator is shown in Listing~\ref{lst:qcfValidatorSnippet}.

\noindent
\begin{minipage}{\linewidth}
	\lstinputlisting[style=java,caption={Snippet of the customized validator for the Quality Control Fishbone language.},label=lst:qcfValidatorSnippet,captionpos=b,basicstyle=\scriptsize]{listings/qcfValitadorSnippet.txt}
\end{minipage}	

The implementation of a validator is based on the definition of a set of annotated methods with a single parameter that 
might end signaling \emph{errors} and \emph{warnings}. 
Xtext takes care of the validation by invoking each method annotated with \emph{@Check} and passing all instances compatible with its parameter type. 
For instance, the method in line 8 will be invoked by each category in the specified QCF.
Within each method a specific check is performed. 
For example, method in line 8 checks that category names are not empty or blank.
When a check fails, the \emph{error} or \emph{warning} method is called. 
These methods include as parameters: (1) the message to be shown by the IDE; (2) the element that it affects; and (3) its issue code. 

The snippet included in Listing~\ref{lst:qcfValidatorSnippet} shows four methods that will check that: (1) names of each category are not empty, (2) names of each category are not null; (3) names of each category are unique; and, (4) that there is at least one category in the model, respectively. 
Some of these aspects could be handled during the parsing, but doing it here allows showing the errors in a more descriptive way, and also allows solving them with quickfixes.

