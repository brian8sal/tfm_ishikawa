/**
 */
package dataOrientedFishbone_Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Mapped Cause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getNotMappedCause()
 * @model
 * @generated
 */
public interface NotMappedCause extends Cause {
} // NotMappedCause
