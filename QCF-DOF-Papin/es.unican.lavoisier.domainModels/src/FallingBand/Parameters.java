/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameters</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.Parameters#getTighteningForce <em>Tightening Force</em>}</li>
 *   <li>{@link FallingBand.Parameters#getTighteningTime <em>Tightening Time</em>}</li>
 *   <li>{@link FallingBand.Parameters#getWheelBandPosition <em>Wheel Band Position</em>}</li>
 *   <li>{@link FallingBand.Parameters#getEngineBandPosition <em>Engine Band Position</em>}</li>
 *   <li>{@link FallingBand.Parameters#getWheelHousingPosition <em>Wheel Housing Position</em>}</li>
 *   <li>{@link FallingBand.Parameters#getEngineHousingPosition <em>Engine Housing Position</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getParameters()
 * @model
 * @generated
 */
public interface Parameters extends EObject {
	/**
	 * Returns the value of the '<em><b>Tightening Force</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tightening Force</em>' attribute.
	 * @see #setTighteningForce(Double)
	 * @see FallingBand.FallingPackage#getParameters_TighteningForce()
	 * @model
	 * @generated
	 */
	Double getTighteningForce();

	/**
	 * Sets the value of the '{@link FallingBand.Parameters#getTighteningForce <em>Tightening Force</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tightening Force</em>' attribute.
	 * @see #getTighteningForce()
	 * @generated
	 */
	void setTighteningForce(Double value);

	/**
	 * Returns the value of the '<em><b>Tightening Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tightening Time</em>' attribute.
	 * @see #setTighteningTime(Double)
	 * @see FallingBand.FallingPackage#getParameters_TighteningTime()
	 * @model
	 * @generated
	 */
	Double getTighteningTime();

	/**
	 * Sets the value of the '{@link FallingBand.Parameters#getTighteningTime <em>Tightening Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tightening Time</em>' attribute.
	 * @see #getTighteningTime()
	 * @generated
	 */
	void setTighteningTime(Double value);

	/**
	 * Returns the value of the '<em><b>Wheel Band Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wheel Band Position</em>' containment reference.
	 * @see #setWheelBandPosition(Coordinate)
	 * @see FallingBand.FallingPackage#getParameters_WheelBandPosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Coordinate getWheelBandPosition();

	/**
	 * Sets the value of the '{@link FallingBand.Parameters#getWheelBandPosition <em>Wheel Band Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wheel Band Position</em>' containment reference.
	 * @see #getWheelBandPosition()
	 * @generated
	 */
	void setWheelBandPosition(Coordinate value);

	/**
	 * Returns the value of the '<em><b>Engine Band Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Engine Band Position</em>' containment reference.
	 * @see #setEngineBandPosition(Coordinate)
	 * @see FallingBand.FallingPackage#getParameters_EngineBandPosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Coordinate getEngineBandPosition();

	/**
	 * Sets the value of the '{@link FallingBand.Parameters#getEngineBandPosition <em>Engine Band Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Engine Band Position</em>' containment reference.
	 * @see #getEngineBandPosition()
	 * @generated
	 */
	void setEngineBandPosition(Coordinate value);

	/**
	 * Returns the value of the '<em><b>Wheel Housing Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wheel Housing Position</em>' containment reference.
	 * @see #setWheelHousingPosition(Coordinate)
	 * @see FallingBand.FallingPackage#getParameters_WheelHousingPosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Coordinate getWheelHousingPosition();

	/**
	 * Sets the value of the '{@link FallingBand.Parameters#getWheelHousingPosition <em>Wheel Housing Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wheel Housing Position</em>' containment reference.
	 * @see #getWheelHousingPosition()
	 * @generated
	 */
	void setWheelHousingPosition(Coordinate value);

	/**
	 * Returns the value of the '<em><b>Engine Housing Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Engine Housing Position</em>' containment reference.
	 * @see #setEngineHousingPosition(Coordinate)
	 * @see FallingBand.FallingPackage#getParameters_EngineHousingPosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Coordinate getEngineHousingPosition();

	/**
	 * Sets the value of the '{@link FallingBand.Parameters#getEngineHousingPosition <em>Engine Housing Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Engine Housing Position</em>' containment reference.
	 * @see #getEngineHousingPosition()
	 * @generated
	 */
	void setEngineHousingPosition(Coordinate value);

} // Parameters
