\begin{figure}[!tb]
\centering
% Requires \usepackage{graphicx}
\includegraphics[width=0.75\linewidth]{Chapter2/Figs/driveHalfShaft.eps} \\
\caption{Elements of a drive half-shaft.}
\label{fig:driveHalfShaftSketch}
\end{figure}
\efloatseparator
 
\begin{figure}[!tb]
\centering
\includegraphics[width=0.75\linewidth]{Chapter2/Figs/dataMiningProcessExpanded.eps}
\caption{Detailed data mining process.}
\label{fig:kdd}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
% Requires \usepackage{graphicx}
\includegraphics[width=\linewidth]{Chapter2/Figs/driveHalfShaftDomainModel.pdf} \\
\caption{A fragment of the domain model for the production of drive half-shafts.}
\label{fig:domainModel}
\end{figure}
\efloatseparator
 
\begin{figure}[!tb]
\centering
\renewcommand{\arraystretch}{0.85}
\begin{scriptsize}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\toprule
id &  \dots & humidity & temperature & 1\_pressure & 2\_pressure & 3\_pressure & 4\_pressure \\
\midrule
5209 & \dots  & 56.7 &20.4 & 7.9 & 7.9 & 7.8 & 7.9 \\
6556 & \dots  & 58.0 & 19.8 & 7.2 & 7.1 & 7.1 & 7.1 \\
\dots & \dots  & \dots  & \dots  & \dots  & \dots & \dots & \dots \\
\bottomrule
\end{tabular} \\
\end{scriptsize}
\caption{A tabular dataset for drive half-shaft analysis.}
\label{fig:finalTabularResult}
\end{figure}
\efloatseparator
 
\begin{figure}[!b]
\centering
% Requires \usepackage{graphicx}
\includegraphics[width=\linewidth]{Chapter2/Figs/fallingBandIshikawa.pdf} \\
\caption{A fishbone diagram for the drive half-shaft running example (taken from~\citep{Dziuba2013}).}
\label{fig:fishboneDiagram}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
% Requires \usepackage{graphicx}
\includegraphics[width=\linewidth]{Chapter3/Figs/solutionOverview.pdf} \\
\caption{Overview of our solution.}
\label{fig:solutionOverview}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
% Requires \usepackage{graphicx}
\includegraphics[width=\linewidth]{Chapter4/Figs/implementation/implementation.eps} \\
\caption{Elements of our model-driven environment.}
\label{fig:implementationOverview}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
% Requires \usepackage{graphicx}
\includegraphics[width=\linewidth]{Chapter4/Figs/dataOrientedFishboneMetamodel.pdf} \\
\caption{Simplified version of the DOF metamodel.}
\label{fig:dofMetamodel}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
\includegraphics[width=\linewidth]{Chapter5/Figs/evaluation/scenarioA.pdf} \\
\caption{Scripts size for scenario a.}
\label{fig:results:scenarioa}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
\includegraphics[width=\linewidth]{Chapter5/Figs/evaluation/wholeDOF.pdf}
\caption{Script sizes, for all scenarios, of the option that includes the whole DOF.}
\label{fig:results:wholeDOF}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
\includegraphics[width=\linewidth]{Chapter5/Figs/evaluation/withoutRealizes.pdf}
\caption{DOF size including and excluding traceability information.}
\label{fig:results:realizes}
\end{figure}
\efloatseparator
 
\begin{figure}[!bt]
\centering
\includegraphics[width=\linewidth]{Chapter5/Figs/evaluation/allScenarios.pdf}
\caption{Script sizes for all scenarios.}
\label{fig:results:allScenarios}
\end{figure}
