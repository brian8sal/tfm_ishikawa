/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelFactory
 * @model kind="package"
 * @generated
 */
public interface DataOrientedFishbone_ModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dataOrientedFishbone_Model";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.unican.es/DataOrientedFishbone";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dof";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DataOrientedFishbone_ModelPackage eINSTANCE = dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.DOFImpl <em>DOF</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.DOFImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getDOF()
	 * @generated
	 */
	int DOF = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOF__NAME = 0;

	/**
	 * The feature id for the '<em><b>Effect</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOF__EFFECT = 1;

	/**
	 * The number of structural features of the '<em>DOF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOF_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>DOF</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.EffectImpl <em>Effect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.EffectImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getEffect()
	 * @generated
	 */
	int EFFECT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Data Feeder</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT__DATA_FEEDER = 1;

	/**
	 * The feature id for the '<em><b>Categories</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT__CATEGORIES = 2;

	/**
	 * The number of structural features of the '<em>Effect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Effect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.CategoryImpl <em>Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.CategoryImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getCategory()
	 * @generated
	 */
	int CATEGORY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Causes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CAUSES = 1;

	/**
	 * The number of structural features of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.CauseImpl <em>Cause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.CauseImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getCause()
	 * @generated
	 */
	int CAUSE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Realizes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__REALIZES = 1;

	/**
	 * The number of structural features of the '<em>Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.CompoundCauseImpl <em>Compound Cause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.CompoundCauseImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getCompoundCause()
	 * @generated
	 */
	int COMPOUND_CAUSE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_CAUSE__NAME = CAUSE__NAME;

	/**
	 * The feature id for the '<em><b>Realizes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_CAUSE__REALIZES = CAUSE__REALIZES;

	/**
	 * The feature id for the '<em><b>Sub Causes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_CAUSE__SUB_CAUSES = CAUSE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Compound Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_CAUSE_FEATURE_COUNT = CAUSE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Compound Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_CAUSE_OPERATION_COUNT = CAUSE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.DataLinkedCauseImpl <em>Data Linked Cause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.DataLinkedCauseImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getDataLinkedCause()
	 * @generated
	 */
	int DATA_LINKED_CAUSE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINKED_CAUSE__NAME = CAUSE__NAME;

	/**
	 * The feature id for the '<em><b>Realizes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINKED_CAUSE__REALIZES = CAUSE__REALIZES;

	/**
	 * The feature id for the '<em><b>Data Feeder</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINKED_CAUSE__DATA_FEEDER = CAUSE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Linked Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINKED_CAUSE_FEATURE_COUNT = CAUSE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Data Linked Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINKED_CAUSE_OPERATION_COUNT = CAUSE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.NotMappedCauseImpl <em>Not Mapped Cause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.NotMappedCauseImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getNotMappedCause()
	 * @generated
	 */
	int NOT_MAPPED_CAUSE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_MAPPED_CAUSE__NAME = CAUSE__NAME;

	/**
	 * The feature id for the '<em><b>Realizes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_MAPPED_CAUSE__REALIZES = CAUSE__REALIZES;

	/**
	 * The number of structural features of the '<em>Not Mapped Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_MAPPED_CAUSE_FEATURE_COUNT = CAUSE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Not Mapped Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_MAPPED_CAUSE_OPERATION_COUNT = CAUSE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.DataFeederImpl <em>Data Feeder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.DataFeederImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getDataFeeder()
	 * @generated
	 */
	int DATA_FEEDER = 7;

	/**
	 * The feature id for the '<em><b>Domain Element Selector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR = 0;

	/**
	 * The feature id for the '<em><b>Attribute Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEEDER__ATTRIBUTE_FILTER = 1;

	/**
	 * The feature id for the '<em><b>Included References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEEDER__INCLUDED_REFERENCES = 2;

	/**
	 * The number of structural features of the '<em>Data Feeder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEEDER_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Data Feeder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FEEDER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.AttributeFilterImpl <em>Attribute Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.AttributeFilterImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getAttributeFilter()
	 * @generated
	 */
	int ATTRIBUTE_FILTER = 8;

	/**
	 * The number of structural features of the '<em>Attribute Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FILTER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Attribute Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FILTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dataOrientedFishbone_Model.impl.IncludedReferenceImpl <em>Included Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dataOrientedFishbone_Model.impl.IncludedReferenceImpl
	 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getIncludedReference()
	 * @generated
	 */
	int INCLUDED_REFERENCE = 9;

	/**
	 * The number of structural features of the '<em>Included Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDED_REFERENCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Included Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDED_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.DOF <em>DOF</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DOF</em>'.
	 * @see dataOrientedFishbone_Model.DOF
	 * @generated
	 */
	EClass getDOF();

	/**
	 * Returns the meta object for the attribute '{@link dataOrientedFishbone_Model.DOF#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dataOrientedFishbone_Model.DOF#getName()
	 * @see #getDOF()
	 * @generated
	 */
	EAttribute getDOF_Name();

	/**
	 * Returns the meta object for the containment reference '{@link dataOrientedFishbone_Model.DOF#getEffect <em>Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Effect</em>'.
	 * @see dataOrientedFishbone_Model.DOF#getEffect()
	 * @see #getDOF()
	 * @generated
	 */
	EReference getDOF_Effect();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.Effect <em>Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Effect</em>'.
	 * @see dataOrientedFishbone_Model.Effect
	 * @generated
	 */
	EClass getEffect();

	/**
	 * Returns the meta object for the attribute '{@link dataOrientedFishbone_Model.Effect#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dataOrientedFishbone_Model.Effect#getName()
	 * @see #getEffect()
	 * @generated
	 */
	EAttribute getEffect_Name();

	/**
	 * Returns the meta object for the containment reference '{@link dataOrientedFishbone_Model.Effect#getDataFeeder <em>Data Feeder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Feeder</em>'.
	 * @see dataOrientedFishbone_Model.Effect#getDataFeeder()
	 * @see #getEffect()
	 * @generated
	 */
	EReference getEffect_DataFeeder();

	/**
	 * Returns the meta object for the containment reference list '{@link dataOrientedFishbone_Model.Effect#getCategories <em>Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Categories</em>'.
	 * @see dataOrientedFishbone_Model.Effect#getCategories()
	 * @see #getEffect()
	 * @generated
	 */
	EReference getEffect_Categories();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.Category <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category</em>'.
	 * @see dataOrientedFishbone_Model.Category
	 * @generated
	 */
	EClass getCategory();

	/**
	 * Returns the meta object for the attribute '{@link dataOrientedFishbone_Model.Category#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dataOrientedFishbone_Model.Category#getName()
	 * @see #getCategory()
	 * @generated
	 */
	EAttribute getCategory_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link dataOrientedFishbone_Model.Category#getCauses <em>Causes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Causes</em>'.
	 * @see dataOrientedFishbone_Model.Category#getCauses()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Causes();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.Cause <em>Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cause</em>'.
	 * @see dataOrientedFishbone_Model.Cause
	 * @generated
	 */
	EClass getCause();

	/**
	 * Returns the meta object for the attribute '{@link dataOrientedFishbone_Model.Cause#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dataOrientedFishbone_Model.Cause#getName()
	 * @see #getCause()
	 * @generated
	 */
	EAttribute getCause_Name();

	/**
	 * Returns the meta object for the attribute '{@link dataOrientedFishbone_Model.Cause#getRealizes <em>Realizes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Realizes</em>'.
	 * @see dataOrientedFishbone_Model.Cause#getRealizes()
	 * @see #getCause()
	 * @generated
	 */
	EAttribute getCause_Realizes();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.CompoundCause <em>Compound Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compound Cause</em>'.
	 * @see dataOrientedFishbone_Model.CompoundCause
	 * @generated
	 */
	EClass getCompoundCause();

	/**
	 * Returns the meta object for the containment reference list '{@link dataOrientedFishbone_Model.CompoundCause#getSubCauses <em>Sub Causes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Causes</em>'.
	 * @see dataOrientedFishbone_Model.CompoundCause#getSubCauses()
	 * @see #getCompoundCause()
	 * @generated
	 */
	EReference getCompoundCause_SubCauses();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.DataLinkedCause <em>Data Linked Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Linked Cause</em>'.
	 * @see dataOrientedFishbone_Model.DataLinkedCause
	 * @generated
	 */
	EClass getDataLinkedCause();

	/**
	 * Returns the meta object for the containment reference '{@link dataOrientedFishbone_Model.DataLinkedCause#getDataFeeder <em>Data Feeder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Feeder</em>'.
	 * @see dataOrientedFishbone_Model.DataLinkedCause#getDataFeeder()
	 * @see #getDataLinkedCause()
	 * @generated
	 */
	EReference getDataLinkedCause_DataFeeder();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.NotMappedCause <em>Not Mapped Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Mapped Cause</em>'.
	 * @see dataOrientedFishbone_Model.NotMappedCause
	 * @generated
	 */
	EClass getNotMappedCause();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.DataFeeder <em>Data Feeder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Feeder</em>'.
	 * @see dataOrientedFishbone_Model.DataFeeder
	 * @generated
	 */
	EClass getDataFeeder();

	/**
	 * Returns the meta object for the attribute '{@link dataOrientedFishbone_Model.DataFeeder#getDomainElementSelector <em>Domain Element Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain Element Selector</em>'.
	 * @see dataOrientedFishbone_Model.DataFeeder#getDomainElementSelector()
	 * @see #getDataFeeder()
	 * @generated
	 */
	EAttribute getDataFeeder_DomainElementSelector();

	/**
	 * Returns the meta object for the containment reference '{@link dataOrientedFishbone_Model.DataFeeder#getAttributeFilter <em>Attribute Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Attribute Filter</em>'.
	 * @see dataOrientedFishbone_Model.DataFeeder#getAttributeFilter()
	 * @see #getDataFeeder()
	 * @generated
	 */
	EReference getDataFeeder_AttributeFilter();

	/**
	 * Returns the meta object for the containment reference list '{@link dataOrientedFishbone_Model.DataFeeder#getIncludedReferences <em>Included References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Included References</em>'.
	 * @see dataOrientedFishbone_Model.DataFeeder#getIncludedReferences()
	 * @see #getDataFeeder()
	 * @generated
	 */
	EReference getDataFeeder_IncludedReferences();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.AttributeFilter <em>Attribute Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Filter</em>'.
	 * @see dataOrientedFishbone_Model.AttributeFilter
	 * @generated
	 */
	EClass getAttributeFilter();

	/**
	 * Returns the meta object for class '{@link dataOrientedFishbone_Model.IncludedReference <em>Included Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Included Reference</em>'.
	 * @see dataOrientedFishbone_Model.IncludedReference
	 * @generated
	 */
	EClass getIncludedReference();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DataOrientedFishbone_ModelFactory getDataOrientedFishbone_ModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.DOFImpl <em>DOF</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.DOFImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getDOF()
		 * @generated
		 */
		EClass DOF = eINSTANCE.getDOF();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOF__NAME = eINSTANCE.getDOF_Name();

		/**
		 * The meta object literal for the '<em><b>Effect</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOF__EFFECT = eINSTANCE.getDOF_Effect();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.EffectImpl <em>Effect</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.EffectImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getEffect()
		 * @generated
		 */
		EClass EFFECT = eINSTANCE.getEffect();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EFFECT__NAME = eINSTANCE.getEffect_Name();

		/**
		 * The meta object literal for the '<em><b>Data Feeder</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EFFECT__DATA_FEEDER = eINSTANCE.getEffect_DataFeeder();

		/**
		 * The meta object literal for the '<em><b>Categories</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EFFECT__CATEGORIES = eINSTANCE.getEffect_Categories();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.CategoryImpl <em>Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.CategoryImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getCategory()
		 * @generated
		 */
		EClass CATEGORY = eINSTANCE.getCategory();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATEGORY__NAME = eINSTANCE.getCategory_Name();

		/**
		 * The meta object literal for the '<em><b>Causes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__CAUSES = eINSTANCE.getCategory_Causes();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.CauseImpl <em>Cause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.CauseImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getCause()
		 * @generated
		 */
		EClass CAUSE = eINSTANCE.getCause();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CAUSE__NAME = eINSTANCE.getCause_Name();

		/**
		 * The meta object literal for the '<em><b>Realizes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CAUSE__REALIZES = eINSTANCE.getCause_Realizes();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.CompoundCauseImpl <em>Compound Cause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.CompoundCauseImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getCompoundCause()
		 * @generated
		 */
		EClass COMPOUND_CAUSE = eINSTANCE.getCompoundCause();

		/**
		 * The meta object literal for the '<em><b>Sub Causes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_CAUSE__SUB_CAUSES = eINSTANCE.getCompoundCause_SubCauses();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.DataLinkedCauseImpl <em>Data Linked Cause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.DataLinkedCauseImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getDataLinkedCause()
		 * @generated
		 */
		EClass DATA_LINKED_CAUSE = eINSTANCE.getDataLinkedCause();

		/**
		 * The meta object literal for the '<em><b>Data Feeder</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LINKED_CAUSE__DATA_FEEDER = eINSTANCE.getDataLinkedCause_DataFeeder();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.NotMappedCauseImpl <em>Not Mapped Cause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.NotMappedCauseImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getNotMappedCause()
		 * @generated
		 */
		EClass NOT_MAPPED_CAUSE = eINSTANCE.getNotMappedCause();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.DataFeederImpl <em>Data Feeder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.DataFeederImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getDataFeeder()
		 * @generated
		 */
		EClass DATA_FEEDER = eINSTANCE.getDataFeeder();

		/**
		 * The meta object literal for the '<em><b>Domain Element Selector</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_FEEDER__DOMAIN_ELEMENT_SELECTOR = eINSTANCE.getDataFeeder_DomainElementSelector();

		/**
		 * The meta object literal for the '<em><b>Attribute Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FEEDER__ATTRIBUTE_FILTER = eINSTANCE.getDataFeeder_AttributeFilter();

		/**
		 * The meta object literal for the '<em><b>Included References</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FEEDER__INCLUDED_REFERENCES = eINSTANCE.getDataFeeder_IncludedReferences();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.AttributeFilterImpl <em>Attribute Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.AttributeFilterImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getAttributeFilter()
		 * @generated
		 */
		EClass ATTRIBUTE_FILTER = eINSTANCE.getAttributeFilter();

		/**
		 * The meta object literal for the '{@link dataOrientedFishbone_Model.impl.IncludedReferenceImpl <em>Included Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dataOrientedFishbone_Model.impl.IncludedReferenceImpl
		 * @see dataOrientedFishbone_Model.impl.DataOrientedFishbone_ModelPackageImpl#getIncludedReference()
		 * @generated
		 */
		EClass INCLUDED_REFERENCE = eINSTANCE.getIncludedReference();

	}

} //DataOrientedFishbone_ModelPackage
