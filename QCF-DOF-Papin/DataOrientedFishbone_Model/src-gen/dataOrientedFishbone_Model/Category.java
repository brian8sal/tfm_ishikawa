/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dataOrientedFishbone_Model.Category#getName <em>Name</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.Category#getCauses <em>Causes</em>}</li>
 * </ul>
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getCategory()
 * @model
 * @generated
 */
public interface Category extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getCategory_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dataOrientedFishbone_Model.Category#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Causes</b></em>' containment reference list.
	 * The list contents are of type {@link dataOrientedFishbone_Model.Cause}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Causes</em>' containment reference list.
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getCategory_Causes()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Cause> getCauses();

} // Category
