driveHalfShafts[["id", "idBandWheel", "idComplianceReport"]].merge(bands, how="left", left_on="idBandWheel",
    right_on="id", suffixes=("", "wheelBand")).merge(reports[['id', 'fallingWheelBand']], how="left",
    left_on="idComplianceReport", right_on="id", suffixes=("", "report")).merge(bandBatchParameters, how="left",
    left_on="batchId", right_on="batchID", suffixes=("", "parameters")).merge(providers, how="left", left_on="idProvider",
    right_on="id", suffixes=("", "provider")).drop(columns=["idBandWheel", "idComplianceReport",
     "idwheelBand", "idreport", "batchID", "idProvider", "idprovider"])