SELECT a.id, m.temperature, m.humidity, 
  (SELECT p.pressure FROM  PliersData p
     WHERE p.idMonitoredParameters = m.id and p.number = 1),
  (SELECT p.pressure FROM  PliersData p
     WHERE p.idMonitoredParameters = m.id and p.number = 2),
  (SELECT  p.pressure FROM  PliersData p 
     WHERE p.idMonitoredParameters = m.id and p.number = 3),
  (SELECT p.pressure FROM  PliersData p
     WHERE p.idMonitoredParameters = m.id and p.number = 4)
FROM AssemblySession a JOIN MonitoredParameters m 
     ON a.idMonitoredParameters = m.id