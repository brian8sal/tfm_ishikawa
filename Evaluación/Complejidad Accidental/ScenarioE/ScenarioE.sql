SELECT d.id,
   c.fallingWheelBand,
   cw.x as wheelBandPositionX,
   cw.y as wheelBandPositionY,
   cw.z as wheelBandPositionZ,
   ce.x as engineBandPositionX,
   ce.y as engineBandPositionY,
   ce.z as engineBandPositionZ
   FROM DriveHalfShaft d JOIN ComplianceReport c
      ON d.idComplianceReport = c.id
   JOIN
   AssemblySession a
       ON d.idAssemblySession = a.id
   JOIN Program p
       ON a.idProgram = p.id
   JOIN Parameters pp
       ON p.idParameters = pp.id
   JOIN Coordinate cw
      ON pp.idWheelBandPosition = cw.id
   JOIN Coordinate ce
      ON pp.idEngineBandPosition = ce.id;
