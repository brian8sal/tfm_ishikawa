/*
 * generated by Xtext 2.24.0
 */
package es.unican.scoping;

import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
public class DOFScopeProvider extends AbstractDeclarativeScopeProvider {
	//public class DOFScopeProvider extends AbstractDeclarativeScopeProvider  {

}
