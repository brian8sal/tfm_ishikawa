/**
 */
package FallingBand.impl;

import FallingBand.FallingPackage;
import FallingBand.HousingParameters;
import FallingBand.Provider;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Housing Parameters</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getMaxLength <em>Max Length</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getMinLength <em>Min Length</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getAvgLength <em>Avg Length</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getMaxDiameter <em>Max Diameter</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getMinDiameter <em>Min Diameter</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getAvgDiameter <em>Avg Diameter</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getBatchId <em>Batch Id</em>}</li>
 *   <li>{@link FallingBand.impl.HousingParametersImpl#getProvider <em>Provider</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HousingParametersImpl extends MinimalEObjectImpl.Container implements HousingParameters {
	/**
	 * The default value of the '{@link #getMaxLength() <em>Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxLength()
	 * @generated
	 * @ordered
	 */
	protected static final Double MAX_LENGTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaxLength() <em>Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxLength()
	 * @generated
	 * @ordered
	 */
	protected Double maxLength = MAX_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinLength() <em>Min Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLength()
	 * @generated
	 * @ordered
	 */
	protected static final Double MIN_LENGTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinLength() <em>Min Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLength()
	 * @generated
	 * @ordered
	 */
	protected Double minLength = MIN_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getAvgLength() <em>Avg Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvgLength()
	 * @generated
	 * @ordered
	 */
	protected static final Double AVG_LENGTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAvgLength() <em>Avg Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvgLength()
	 * @generated
	 * @ordered
	 */
	protected Double avgLength = AVG_LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxDiameter() <em>Max Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxDiameter()
	 * @generated
	 * @ordered
	 */
	protected static final Double MAX_DIAMETER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaxDiameter() <em>Max Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxDiameter()
	 * @generated
	 * @ordered
	 */
	protected Double maxDiameter = MAX_DIAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinDiameter() <em>Min Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinDiameter()
	 * @generated
	 * @ordered
	 */
	protected static final Double MIN_DIAMETER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinDiameter() <em>Min Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinDiameter()
	 * @generated
	 * @ordered
	 */
	protected Double minDiameter = MIN_DIAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #getAvgDiameter() <em>Avg Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvgDiameter()
	 * @generated
	 * @ordered
	 */
	protected static final Double AVG_DIAMETER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAvgDiameter() <em>Avg Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvgDiameter()
	 * @generated
	 * @ordered
	 */
	protected Double avgDiameter = AVG_DIAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #getBatchId() <em>Batch Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatchId()
	 * @generated
	 * @ordered
	 */
	protected static final Double BATCH_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBatchId() <em>Batch Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatchId()
	 * @generated
	 * @ordered
	 */
	protected Double batchId = BATCH_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProvider() <em>Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvider()
	 * @generated
	 * @ordered
	 */
	protected Provider provider;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HousingParametersImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FallingPackage.Literals.HOUSING_PARAMETERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMaxLength() {
		return maxLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxLength(Double newMaxLength) {
		Double oldMaxLength = maxLength;
		maxLength = newMaxLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__MAX_LENGTH, oldMaxLength, maxLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMinLength() {
		return minLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinLength(Double newMinLength) {
		Double oldMinLength = minLength;
		minLength = newMinLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__MIN_LENGTH, oldMinLength, minLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAvgLength() {
		return avgLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvgLength(Double newAvgLength) {
		Double oldAvgLength = avgLength;
		avgLength = newAvgLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__AVG_LENGTH, oldAvgLength, avgLength));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMaxDiameter() {
		return maxDiameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxDiameter(Double newMaxDiameter) {
		Double oldMaxDiameter = maxDiameter;
		maxDiameter = newMaxDiameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__MAX_DIAMETER, oldMaxDiameter, maxDiameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMinDiameter() {
		return minDiameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinDiameter(Double newMinDiameter) {
		Double oldMinDiameter = minDiameter;
		minDiameter = newMinDiameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__MIN_DIAMETER, oldMinDiameter, minDiameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAvgDiameter() {
		return avgDiameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvgDiameter(Double newAvgDiameter) {
		Double oldAvgDiameter = avgDiameter;
		avgDiameter = newAvgDiameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__AVG_DIAMETER, oldAvgDiameter, avgDiameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getBatchId() {
		return batchId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatchId(Double newBatchId) {
		Double oldBatchId = batchId;
		batchId = newBatchId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__BATCH_ID, oldBatchId, batchId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provider getProvider() {
		return provider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProvider(Provider newProvider, NotificationChain msgs) {
		Provider oldProvider = provider;
		provider = newProvider;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__PROVIDER, oldProvider, newProvider);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvider(Provider newProvider) {
		if (newProvider != provider) {
			NotificationChain msgs = null;
			if (provider != null)
				msgs = ((InternalEObject)provider).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.HOUSING_PARAMETERS__PROVIDER, null, msgs);
			if (newProvider != null)
				msgs = ((InternalEObject)newProvider).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.HOUSING_PARAMETERS__PROVIDER, null, msgs);
			msgs = basicSetProvider(newProvider, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.HOUSING_PARAMETERS__PROVIDER, newProvider, newProvider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FallingPackage.HOUSING_PARAMETERS__PROVIDER:
				return basicSetProvider(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FallingPackage.HOUSING_PARAMETERS__MAX_LENGTH:
				return getMaxLength();
			case FallingPackage.HOUSING_PARAMETERS__MIN_LENGTH:
				return getMinLength();
			case FallingPackage.HOUSING_PARAMETERS__AVG_LENGTH:
				return getAvgLength();
			case FallingPackage.HOUSING_PARAMETERS__MAX_DIAMETER:
				return getMaxDiameter();
			case FallingPackage.HOUSING_PARAMETERS__MIN_DIAMETER:
				return getMinDiameter();
			case FallingPackage.HOUSING_PARAMETERS__AVG_DIAMETER:
				return getAvgDiameter();
			case FallingPackage.HOUSING_PARAMETERS__BATCH_ID:
				return getBatchId();
			case FallingPackage.HOUSING_PARAMETERS__PROVIDER:
				return getProvider();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FallingPackage.HOUSING_PARAMETERS__MAX_LENGTH:
				setMaxLength((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__MIN_LENGTH:
				setMinLength((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__AVG_LENGTH:
				setAvgLength((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__MAX_DIAMETER:
				setMaxDiameter((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__MIN_DIAMETER:
				setMinDiameter((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__AVG_DIAMETER:
				setAvgDiameter((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__BATCH_ID:
				setBatchId((Double)newValue);
				return;
			case FallingPackage.HOUSING_PARAMETERS__PROVIDER:
				setProvider((Provider)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FallingPackage.HOUSING_PARAMETERS__MAX_LENGTH:
				setMaxLength(MAX_LENGTH_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__MIN_LENGTH:
				setMinLength(MIN_LENGTH_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__AVG_LENGTH:
				setAvgLength(AVG_LENGTH_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__MAX_DIAMETER:
				setMaxDiameter(MAX_DIAMETER_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__MIN_DIAMETER:
				setMinDiameter(MIN_DIAMETER_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__AVG_DIAMETER:
				setAvgDiameter(AVG_DIAMETER_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__BATCH_ID:
				setBatchId(BATCH_ID_EDEFAULT);
				return;
			case FallingPackage.HOUSING_PARAMETERS__PROVIDER:
				setProvider((Provider)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FallingPackage.HOUSING_PARAMETERS__MAX_LENGTH:
				return MAX_LENGTH_EDEFAULT == null ? maxLength != null : !MAX_LENGTH_EDEFAULT.equals(maxLength);
			case FallingPackage.HOUSING_PARAMETERS__MIN_LENGTH:
				return MIN_LENGTH_EDEFAULT == null ? minLength != null : !MIN_LENGTH_EDEFAULT.equals(minLength);
			case FallingPackage.HOUSING_PARAMETERS__AVG_LENGTH:
				return AVG_LENGTH_EDEFAULT == null ? avgLength != null : !AVG_LENGTH_EDEFAULT.equals(avgLength);
			case FallingPackage.HOUSING_PARAMETERS__MAX_DIAMETER:
				return MAX_DIAMETER_EDEFAULT == null ? maxDiameter != null : !MAX_DIAMETER_EDEFAULT.equals(maxDiameter);
			case FallingPackage.HOUSING_PARAMETERS__MIN_DIAMETER:
				return MIN_DIAMETER_EDEFAULT == null ? minDiameter != null : !MIN_DIAMETER_EDEFAULT.equals(minDiameter);
			case FallingPackage.HOUSING_PARAMETERS__AVG_DIAMETER:
				return AVG_DIAMETER_EDEFAULT == null ? avgDiameter != null : !AVG_DIAMETER_EDEFAULT.equals(avgDiameter);
			case FallingPackage.HOUSING_PARAMETERS__BATCH_ID:
				return BATCH_ID_EDEFAULT == null ? batchId != null : !BATCH_ID_EDEFAULT.equals(batchId);
			case FallingPackage.HOUSING_PARAMETERS__PROVIDER:
				return provider != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (maxLength: ");
		result.append(maxLength);
		result.append(", minLength: ");
		result.append(minLength);
		result.append(", avgLength: ");
		result.append(avgLength);
		result.append(", maxDiameter: ");
		result.append(maxDiameter);
		result.append(", minDiameter: ");
		result.append(minDiameter);
		result.append(", avgDiameter: ");
		result.append(avgDiameter);
		result.append(", batchId: ");
		result.append(batchId);
		result.append(')');
		return result.toString();
	}

} //HousingParametersImpl
