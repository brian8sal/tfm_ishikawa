package es.unican.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import es.unican.services.QCFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQCFParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'qcf'", "'effect'", "'description'", "'category'", "'cause'", "'valueOfInterest'", "'contains'", "'{'", "'}'", "'.'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalQCFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQCFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQCFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalQCF.g"; }



     	private QCFGrammarAccess grammarAccess;

        public InternalQCFParser(TokenStream input, QCFGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "QCF";
       	}

       	@Override
       	protected QCFGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleQCF"
    // InternalQCF.g:64:1: entryRuleQCF returns [EObject current=null] : iv_ruleQCF= ruleQCF EOF ;
    public final EObject entryRuleQCF() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQCF = null;


        try {
            // InternalQCF.g:64:44: (iv_ruleQCF= ruleQCF EOF )
            // InternalQCF.g:65:2: iv_ruleQCF= ruleQCF EOF
            {
             newCompositeNode(grammarAccess.getQCFRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQCF=ruleQCF();

            state._fsp--;

             current =iv_ruleQCF; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQCF"


    // $ANTLR start "ruleQCF"
    // InternalQCF.g:71:1: ruleQCF returns [EObject current=null] : (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_effects_2_0= ruleEffect ) ) ) ;
    public final EObject ruleQCF() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_effects_2_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:77:2: ( (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_effects_2_0= ruleEffect ) ) ) )
            // InternalQCF.g:78:2: (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_effects_2_0= ruleEffect ) ) )
            {
            // InternalQCF.g:78:2: (otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_effects_2_0= ruleEffect ) ) )
            // InternalQCF.g:79:3: otherlv_0= 'qcf' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_effects_2_0= ruleEffect ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getQCFAccess().getQcfKeyword_0());
            		
            // InternalQCF.g:83:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalQCF.g:84:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalQCF.g:84:4: (lv_name_1_0= ruleQualifiedName )
            // InternalQCF.g:85:5: lv_name_1_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getQCFAccess().getNameQualifiedNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQCFRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"es.unican.QCF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:102:3: ( (lv_effects_2_0= ruleEffect ) )
            // InternalQCF.g:103:4: (lv_effects_2_0= ruleEffect )
            {
            // InternalQCF.g:103:4: (lv_effects_2_0= ruleEffect )
            // InternalQCF.g:104:5: lv_effects_2_0= ruleEffect
            {

            					newCompositeNode(grammarAccess.getQCFAccess().getEffectsEffectParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_effects_2_0=ruleEffect();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQCFRule());
            					}
            					add(
            						current,
            						"effects",
            						lv_effects_2_0,
            						"es.unican.QCF.Effect");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQCF"


    // $ANTLR start "entryRuleEffect"
    // InternalQCF.g:125:1: entryRuleEffect returns [EObject current=null] : iv_ruleEffect= ruleEffect EOF ;
    public final EObject entryRuleEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffect = null;


        try {
            // InternalQCF.g:125:47: (iv_ruleEffect= ruleEffect EOF )
            // InternalQCF.g:126:2: iv_ruleEffect= ruleEffect EOF
            {
             newCompositeNode(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffect=ruleEffect();

            state._fsp--;

             current =iv_ruleEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalQCF.g:132:1: ruleEffect returns [EObject current=null] : ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_categories_5_0= ruleCategory ) )+ ) ;
    public final EObject ruleEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        EObject lv_categories_5_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:138:2: ( ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_categories_5_0= ruleCategory ) )+ ) )
            // InternalQCF.g:139:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_categories_5_0= ruleCategory ) )+ )
            {
            // InternalQCF.g:139:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_categories_5_0= ruleCategory ) )+ )
            // InternalQCF.g:140:3: () otherlv_1= 'effect' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_categories_5_0= ruleCategory ) )+
            {
            // InternalQCF.g:140:3: ()
            // InternalQCF.g:141:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEffectAccess().getEffectAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getEffectAccess().getEffectKeyword_1());
            		
            // InternalQCF.g:151:3: ( (lv_name_2_0= ruleEString ) )
            // InternalQCF.g:152:4: (lv_name_2_0= ruleEString )
            {
            // InternalQCF.g:152:4: (lv_name_2_0= ruleEString )
            // InternalQCF.g:153:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.QCF.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:170:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalQCF.g:171:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getEffectAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalQCF.g:175:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalQCF.g:176:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalQCF.g:176:5: (lv_description_4_0= ruleEString )
                    // InternalQCF.g:177:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getEffectAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEffectRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:195:3: ( (lv_categories_5_0= ruleCategory ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalQCF.g:196:4: (lv_categories_5_0= ruleCategory )
            	    {
            	    // InternalQCF.g:196:4: (lv_categories_5_0= ruleCategory )
            	    // InternalQCF.g:197:5: lv_categories_5_0= ruleCategory
            	    {

            	    					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_categories_5_0=ruleCategory();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEffectRule());
            	    					}
            	    					add(
            	    						current,
            	    						"categories",
            	    						lv_categories_5_0,
            	    						"es.unican.QCF.Category");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalQCF.g:218:1: entryRuleCategory returns [EObject current=null] : iv_ruleCategory= ruleCategory EOF ;
    public final EObject entryRuleCategory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategory = null;


        try {
            // InternalQCF.g:218:49: (iv_ruleCategory= ruleCategory EOF )
            // InternalQCF.g:219:2: iv_ruleCategory= ruleCategory EOF
            {
             newCompositeNode(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCategory=ruleCategory();

            state._fsp--;

             current =iv_ruleCategory; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalQCF.g:225:1: ruleCategory returns [EObject current=null] : ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_causes_5_0= ruleCause ) )+ ) ;
    public final EObject ruleCategory() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        EObject lv_causes_5_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:231:2: ( ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_causes_5_0= ruleCause ) )+ ) )
            // InternalQCF.g:232:2: ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_causes_5_0= ruleCause ) )+ )
            {
            // InternalQCF.g:232:2: ( () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_causes_5_0= ruleCause ) )+ )
            // InternalQCF.g:233:3: () otherlv_1= 'category' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? ( (lv_causes_5_0= ruleCause ) )+
            {
            // InternalQCF.g:233:3: ()
            // InternalQCF.g:234:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCategoryAccess().getCategoryAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getCategoryAccess().getCategoryKeyword_1());
            		
            // InternalQCF.g:244:3: ( (lv_name_2_0= ruleEString ) )
            // InternalQCF.g:245:4: (lv_name_2_0= ruleEString )
            {
            // InternalQCF.g:245:4: (lv_name_2_0= ruleEString )
            // InternalQCF.g:246:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCategoryAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_8);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCategoryRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.QCF.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:263:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalQCF.g:264:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getCategoryAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalQCF.g:268:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalQCF.g:269:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalQCF.g:269:5: (lv_description_4_0= ruleEString )
                    // InternalQCF.g:270:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCategoryAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCategoryRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:288:3: ( (lv_causes_5_0= ruleCause ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==15) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalQCF.g:289:4: (lv_causes_5_0= ruleCause )
            	    {
            	    // InternalQCF.g:289:4: (lv_causes_5_0= ruleCause )
            	    // InternalQCF.g:290:5: lv_causes_5_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_causes_5_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCategoryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"causes",
            	    						lv_causes_5_0,
            	    						"es.unican.QCF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleCause"
    // InternalQCF.g:311:1: entryRuleCause returns [EObject current=null] : iv_ruleCause= ruleCause EOF ;
    public final EObject entryRuleCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCause = null;


        try {
            // InternalQCF.g:311:46: (iv_ruleCause= ruleCause EOF )
            // InternalQCF.g:312:2: iv_ruleCause= ruleCause EOF
            {
             newCompositeNode(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCause=ruleCause();

            state._fsp--;

             current =iv_ruleCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalQCF.g:318:1: ruleCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}' )? ) ;
    public final EObject ruleCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_valueOfInterest_6_0 = null;

        EObject lv_subCauses_9_0 = null;



        	enterRule();

        try {
            // InternalQCF.g:324:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}' )? ) )
            // InternalQCF.g:325:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}' )? )
            {
            // InternalQCF.g:325:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}' )? )
            // InternalQCF.g:326:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )? (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}' )?
            {
            // InternalQCF.g:326:3: ()
            // InternalQCF.g:327:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCauseAccess().getCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getCauseAccess().getCauseKeyword_1());
            		
            // InternalQCF.g:337:3: ( (lv_name_2_0= ruleEString ) )
            // InternalQCF.g:338:4: (lv_name_2_0= ruleEString )
            {
            // InternalQCF.g:338:4: (lv_name_2_0= ruleEString )
            // InternalQCF.g:339:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCauseAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_10);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.QCF.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalQCF.g:356:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==13) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalQCF.g:357:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getCauseAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalQCF.g:361:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalQCF.g:362:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalQCF.g:362:5: (lv_description_4_0= ruleEString )
                    // InternalQCF.g:363:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCauseAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:381:3: (otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalQCF.g:382:4: otherlv_5= 'valueOfInterest' ( (lv_valueOfInterest_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,16,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getCauseAccess().getValueOfInterestKeyword_4_0());
                    			
                    // InternalQCF.g:386:4: ( (lv_valueOfInterest_6_0= ruleEString ) )
                    // InternalQCF.g:387:5: (lv_valueOfInterest_6_0= ruleEString )
                    {
                    // InternalQCF.g:387:5: (lv_valueOfInterest_6_0= ruleEString )
                    // InternalQCF.g:388:6: lv_valueOfInterest_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCauseAccess().getValueOfInterestEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_12);
                    lv_valueOfInterest_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    						}
                    						set(
                    							current,
                    							"valueOfInterest",
                    							lv_valueOfInterest_6_0,
                    							"es.unican.QCF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalQCF.g:406:3: (otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==17) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalQCF.g:407:4: otherlv_7= 'contains' otherlv_8= '{' ( (lv_subCauses_9_0= ruleCause ) )+ otherlv_10= '}'
                    {
                    otherlv_7=(Token)match(input,17,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getCauseAccess().getContainsKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,18,FOLLOW_8); 

                    				newLeafNode(otherlv_8, grammarAccess.getCauseAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalQCF.g:415:4: ( (lv_subCauses_9_0= ruleCause ) )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==15) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalQCF.g:416:5: (lv_subCauses_9_0= ruleCause )
                    	    {
                    	    // InternalQCF.g:416:5: (lv_subCauses_9_0= ruleCause )
                    	    // InternalQCF.g:417:6: lv_subCauses_9_0= ruleCause
                    	    {

                    	    						newCompositeNode(grammarAccess.getCauseAccess().getSubCausesCauseParserRuleCall_5_2_0());
                    	    					
                    	    pushFollow(FOLLOW_14);
                    	    lv_subCauses_9_0=ruleCause();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getCauseRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"subCauses",
                    	    							lv_subCauses_9_0,
                    	    							"es.unican.QCF.Cause");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);

                    otherlv_10=(Token)match(input,19,FOLLOW_2); 

                    				newLeafNode(otherlv_10, grammarAccess.getCauseAccess().getRightCurlyBracketKeyword_5_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleEString"
    // InternalQCF.g:443:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalQCF.g:443:47: (iv_ruleEString= ruleEString EOF )
            // InternalQCF.g:444:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalQCF.g:450:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalQCF.g:456:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalQCF.g:457:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalQCF.g:457:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STRING) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_ID) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalQCF.g:458:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalQCF.g:466:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalQCF.g:477:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalQCF.g:477:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalQCF.g:478:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalQCF.g:484:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalQCF.g:490:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalQCF.g:491:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalQCF.g:491:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalQCF.g:492:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalQCF.g:499:3: (kw= '.' this_ID_2= RULE_ID )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==20) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalQCF.g:500:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,20,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_15); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000006002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000000A002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000032002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000030002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000000000008A000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000100002L});

}