\contentsline {lstlisting}{\numberline {2.1}Data for a drive half-shaft instance}{10}{lstlisting.2.1}%
\contentsline {lstlisting}{\numberline {2.2}SQL script to rearrange \emph {Assembly} information as tabular data.}{11}{lstlisting.2.2}%
\contentsline {lstlisting}{\numberline {2.3}An example of Lavoisier specification.}{12}{lstlisting.2.3}%
\contentsline {lstlisting}{\numberline {3.1}A Data-Oriented Fishbone model for our running example.}{20}{lstlisting.3.1}%
\contentsline {lstlisting}{\numberline {3.2}An example of Papin specification.}{22}{lstlisting.3.2}%
\contentsline {lstlisting}{\numberline {4.1}Grammar for the Quality Control Fishbone language proposed.}{29}{lstlisting.4.1}%
\contentsline {lstlisting}{\numberline {4.2}Snippet of the customized validator for the Quality Control Fishbone language.}{31}{lstlisting.4.2}%
\contentsline {lstlisting}{\numberline {4.3}Snippet of grammar for the causes of Data-Oriented Fishbone language proposed.}{33}{lstlisting.4.3}%
\contentsline {lstlisting}{\numberline {4.4}Snippet of the quickfixes class defined for the Data-Oriented Fishbone language.}{34}{lstlisting.4.4}%
\contentsline {lstlisting}{\numberline {4.5}Snippet of the grammar for Papin.}{36}{lstlisting.4.5}%
\contentsline {lstlisting}{listings/completeFallingBandDOF.txt}{51}{lstlisting.a.A.-1}%
