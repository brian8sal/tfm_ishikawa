/**
 */
package FallingBand.impl;

import FallingBand.BandBatchParameters;
import FallingBand.FallingPackage;
import FallingBand.Provider;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Band Batch Parameters</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.impl.BandBatchParametersImpl#getMaxThickness <em>Max Thickness</em>}</li>
 *   <li>{@link FallingBand.impl.BandBatchParametersImpl#getMinThickness <em>Min Thickness</em>}</li>
 *   <li>{@link FallingBand.impl.BandBatchParametersImpl#getAvgThickness <em>Avg Thickness</em>}</li>
 *   <li>{@link FallingBand.impl.BandBatchParametersImpl#getBatchId <em>Batch Id</em>}</li>
 *   <li>{@link FallingBand.impl.BandBatchParametersImpl#getProvider <em>Provider</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BandBatchParametersImpl extends MinimalEObjectImpl.Container implements BandBatchParameters {
	/**
	 * The default value of the '{@link #getMaxThickness() <em>Max Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxThickness()
	 * @generated
	 * @ordered
	 */
	protected static final Double MAX_THICKNESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaxThickness() <em>Max Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxThickness()
	 * @generated
	 * @ordered
	 */
	protected Double maxThickness = MAX_THICKNESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinThickness() <em>Min Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinThickness()
	 * @generated
	 * @ordered
	 */
	protected static final Double MIN_THICKNESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinThickness() <em>Min Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinThickness()
	 * @generated
	 * @ordered
	 */
	protected Double minThickness = MIN_THICKNESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getAvgThickness() <em>Avg Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvgThickness()
	 * @generated
	 * @ordered
	 */
	protected static final Double AVG_THICKNESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAvgThickness() <em>Avg Thickness</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvgThickness()
	 * @generated
	 * @ordered
	 */
	protected Double avgThickness = AVG_THICKNESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getBatchId() <em>Batch Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatchId()
	 * @generated
	 * @ordered
	 */
	protected static final Double BATCH_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBatchId() <em>Batch Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatchId()
	 * @generated
	 * @ordered
	 */
	protected Double batchId = BATCH_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProvider() <em>Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvider()
	 * @generated
	 * @ordered
	 */
	protected Provider provider;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BandBatchParametersImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FallingPackage.Literals.BAND_BATCH_PARAMETERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMaxThickness() {
		return maxThickness;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxThickness(Double newMaxThickness) {
		Double oldMaxThickness = maxThickness;
		maxThickness = newMaxThickness;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.BAND_BATCH_PARAMETERS__MAX_THICKNESS, oldMaxThickness, maxThickness));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getMinThickness() {
		return minThickness;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinThickness(Double newMinThickness) {
		Double oldMinThickness = minThickness;
		minThickness = newMinThickness;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.BAND_BATCH_PARAMETERS__MIN_THICKNESS, oldMinThickness, minThickness));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getAvgThickness() {
		return avgThickness;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvgThickness(Double newAvgThickness) {
		Double oldAvgThickness = avgThickness;
		avgThickness = newAvgThickness;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.BAND_BATCH_PARAMETERS__AVG_THICKNESS, oldAvgThickness, avgThickness));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getBatchId() {
		return batchId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatchId(Double newBatchId) {
		Double oldBatchId = batchId;
		batchId = newBatchId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.BAND_BATCH_PARAMETERS__BATCH_ID, oldBatchId, batchId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Provider getProvider() {
		return provider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProvider(Provider newProvider, NotificationChain msgs) {
		Provider oldProvider = provider;
		provider = newProvider;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER, oldProvider, newProvider);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvider(Provider newProvider) {
		if (newProvider != provider) {
			NotificationChain msgs = null;
			if (provider != null)
				msgs = ((InternalEObject)provider).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER, null, msgs);
			if (newProvider != null)
				msgs = ((InternalEObject)newProvider).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER, null, msgs);
			msgs = basicSetProvider(newProvider, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER, newProvider, newProvider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER:
				return basicSetProvider(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FallingPackage.BAND_BATCH_PARAMETERS__MAX_THICKNESS:
				return getMaxThickness();
			case FallingPackage.BAND_BATCH_PARAMETERS__MIN_THICKNESS:
				return getMinThickness();
			case FallingPackage.BAND_BATCH_PARAMETERS__AVG_THICKNESS:
				return getAvgThickness();
			case FallingPackage.BAND_BATCH_PARAMETERS__BATCH_ID:
				return getBatchId();
			case FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER:
				return getProvider();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FallingPackage.BAND_BATCH_PARAMETERS__MAX_THICKNESS:
				setMaxThickness((Double)newValue);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__MIN_THICKNESS:
				setMinThickness((Double)newValue);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__AVG_THICKNESS:
				setAvgThickness((Double)newValue);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__BATCH_ID:
				setBatchId((Double)newValue);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER:
				setProvider((Provider)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FallingPackage.BAND_BATCH_PARAMETERS__MAX_THICKNESS:
				setMaxThickness(MAX_THICKNESS_EDEFAULT);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__MIN_THICKNESS:
				setMinThickness(MIN_THICKNESS_EDEFAULT);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__AVG_THICKNESS:
				setAvgThickness(AVG_THICKNESS_EDEFAULT);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__BATCH_ID:
				setBatchId(BATCH_ID_EDEFAULT);
				return;
			case FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER:
				setProvider((Provider)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FallingPackage.BAND_BATCH_PARAMETERS__MAX_THICKNESS:
				return MAX_THICKNESS_EDEFAULT == null ? maxThickness != null : !MAX_THICKNESS_EDEFAULT.equals(maxThickness);
			case FallingPackage.BAND_BATCH_PARAMETERS__MIN_THICKNESS:
				return MIN_THICKNESS_EDEFAULT == null ? minThickness != null : !MIN_THICKNESS_EDEFAULT.equals(minThickness);
			case FallingPackage.BAND_BATCH_PARAMETERS__AVG_THICKNESS:
				return AVG_THICKNESS_EDEFAULT == null ? avgThickness != null : !AVG_THICKNESS_EDEFAULT.equals(avgThickness);
			case FallingPackage.BAND_BATCH_PARAMETERS__BATCH_ID:
				return BATCH_ID_EDEFAULT == null ? batchId != null : !BATCH_ID_EDEFAULT.equals(batchId);
			case FallingPackage.BAND_BATCH_PARAMETERS__PROVIDER:
				return provider != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (maxThickness: ");
		result.append(maxThickness);
		result.append(", minThickness: ");
		result.append(minThickness);
		result.append(", avgThickness: ");
		result.append(avgThickness);
		result.append(", batchId: ");
		result.append(batchId);
		result.append(')');
		return result.toString();
	}

} //BandBatchParametersImpl
