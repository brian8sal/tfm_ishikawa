package es.unican.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import es.unican.services.DOFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDOFParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'dof'", "'import'", "'effect'", "'is'", "'category'", "'include'", "'by'", "'{'", "'}'", "'.'", "'calculate'", "'as'", "'('", "')'", "'count'", "'sum'", "'avg'", "'max'", "'min'", "'only_as'", "'['", "','", "']'", "'where'", "'and'", "'or'", "'='", "'!='", "'>'", "'>='", "'<'", "'<='", "'cause'", "'realizes'", "'contains'", "'notMapped'", "'.*'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalDOFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDOFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDOFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDOF.g"; }



     	private DOFGrammarAccess grammarAccess;

        public InternalDOFParser(TokenStream input, DOFGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "DOF";
       	}

       	@Override
       	protected DOFGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDOF"
    // InternalDOF.g:64:1: entryRuleDOF returns [EObject current=null] : iv_ruleDOF= ruleDOF EOF ;
    public final EObject entryRuleDOF() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDOF = null;


        try {
            // InternalDOF.g:64:44: (iv_ruleDOF= ruleDOF EOF )
            // InternalDOF.g:65:2: iv_ruleDOF= ruleDOF EOF
            {
             newCompositeNode(grammarAccess.getDOFRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDOF=ruleDOF();

            state._fsp--;

             current =iv_ruleDOF; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOF"


    // $ANTLR start "ruleDOF"
    // InternalDOF.g:71:1: ruleDOF returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) ) ;
    public final EObject ruleDOF() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_imports_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_effect_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:77:2: ( ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) ) )
            // InternalDOF.g:78:2: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) )
            {
            // InternalDOF.g:78:2: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) ) )
            // InternalDOF.g:79:3: ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'dof' ( (lv_name_2_0= ruleQualifiedName ) ) ( (lv_effect_3_0= ruleEffect ) )
            {
            // InternalDOF.g:79:3: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDOF.g:80:4: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalDOF.g:80:4: (lv_imports_0_0= ruleImport )
            	    // InternalDOF.g:81:5: lv_imports_0_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDOFRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_0_0,
            	    						"es.unican.DOF.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getDOFAccess().getDofKeyword_1());
            		
            // InternalDOF.g:102:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:103:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:103:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:104:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDOFRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:121:3: ( (lv_effect_3_0= ruleEffect ) )
            // InternalDOF.g:122:4: (lv_effect_3_0= ruleEffect )
            {
            // InternalDOF.g:122:4: (lv_effect_3_0= ruleEffect )
            // InternalDOF.g:123:5: lv_effect_3_0= ruleEffect
            {

            					newCompositeNode(grammarAccess.getDOFAccess().getEffectEffectParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_effect_3_0=ruleEffect();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDOFRule());
            					}
            					set(
            						current,
            						"effect",
            						lv_effect_3_0,
            						"es.unican.DOF.Effect");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOF"


    // $ANTLR start "entryRuleImport"
    // InternalDOF.g:144:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalDOF.g:144:47: (iv_ruleImport= ruleImport EOF )
            // InternalDOF.g:145:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalDOF.g:151:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:157:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalDOF.g:158:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalDOF.g:158:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalDOF.g:159:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalDOF.g:163:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:164:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:164:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalDOF.g:165:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"es.unican.DOF.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleEffect"
    // InternalDOF.g:186:1: entryRuleEffect returns [EObject current=null] : iv_ruleEffect= ruleEffect EOF ;
    public final EObject entryRuleEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffect = null;


        try {
            // InternalDOF.g:186:47: (iv_ruleEffect= ruleEffect EOF )
            // InternalDOF.g:187:2: iv_ruleEffect= ruleEffect EOF
            {
             newCompositeNode(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffect=ruleEffect();

            state._fsp--;

             current =iv_ruleEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalDOF.g:193:1: ruleEffect returns [EObject current=null] : ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* ) ;
    public final EObject ruleEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_dataFeeder_4_0 = null;

        EObject lv_categories_5_0 = null;

        EObject lv_categories_6_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:199:2: ( ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* ) )
            // InternalDOF.g:200:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )
            {
            // InternalDOF.g:200:2: ( () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )
            // InternalDOF.g:201:3: () otherlv_1= 'effect' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= 'is' ( (lv_dataFeeder_4_0= ruleDataFeeder ) ) ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )*
            {
            // InternalDOF.g:201:3: ()
            // InternalDOF.g:202:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEffectAccess().getEffectAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getEffectAccess().getEffectKeyword_1());
            		
            // InternalDOF.g:212:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalDOF.g:213:4: (lv_name_2_0= RULE_ID )
            {
            // InternalDOF.g:213:4: (lv_name_2_0= RULE_ID )
            // InternalDOF.g:214:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEffectRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getEffectAccess().getIsKeyword_3());
            		
            // InternalDOF.g:234:3: ( (lv_dataFeeder_4_0= ruleDataFeeder ) )
            // InternalDOF.g:235:4: (lv_dataFeeder_4_0= ruleDataFeeder )
            {
            // InternalDOF.g:235:4: (lv_dataFeeder_4_0= ruleDataFeeder )
            // InternalDOF.g:236:5: lv_dataFeeder_4_0= ruleDataFeeder
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_dataFeeder_4_0=ruleDataFeeder();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					set(
            						current,
            						"dataFeeder",
            						lv_dataFeeder_4_0,
            						"es.unican.DOF.DataFeeder");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:253:3: ( (lv_categories_5_0= ruleCategory ) )
            // InternalDOF.g:254:4: (lv_categories_5_0= ruleCategory )
            {
            // InternalDOF.g:254:4: (lv_categories_5_0= ruleCategory )
            // InternalDOF.g:255:5: lv_categories_5_0= ruleCategory
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_8);
            lv_categories_5_0=ruleCategory();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					add(
            						current,
            						"categories",
            						lv_categories_5_0,
            						"es.unican.DOF.Category");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:272:3: ( (lv_categories_6_0= ruleCategory ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalDOF.g:273:4: (lv_categories_6_0= ruleCategory )
            	    {
            	    // InternalDOF.g:273:4: (lv_categories_6_0= ruleCategory )
            	    // InternalDOF.g:274:5: lv_categories_6_0= ruleCategory
            	    {

            	    					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_categories_6_0=ruleCategory();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEffectRule());
            	    					}
            	    					add(
            	    						current,
            	    						"categories",
            	    						lv_categories_6_0,
            	    						"es.unican.DOF.Category");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalDOF.g:295:1: entryRuleCategory returns [EObject current=null] : iv_ruleCategory= ruleCategory EOF ;
    public final EObject entryRuleCategory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategory = null;


        try {
            // InternalDOF.g:295:49: (iv_ruleCategory= ruleCategory EOF )
            // InternalDOF.g:296:2: iv_ruleCategory= ruleCategory EOF
            {
             newCompositeNode(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCategory=ruleCategory();

            state._fsp--;

             current =iv_ruleCategory; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalDOF.g:302:1: ruleCategory returns [EObject current=null] : ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* ) ;
    public final EObject ruleCategory() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        EObject lv_causes_3_0 = null;

        EObject lv_causes_4_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:308:2: ( ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* ) )
            // InternalDOF.g:309:2: ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* )
            {
            // InternalDOF.g:309:2: ( () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )* )
            // InternalDOF.g:310:3: () otherlv_1= 'category' ( (lv_name_2_0= RULE_ID ) ) ( (lv_causes_3_0= ruleCause ) ) ( (lv_causes_4_0= ruleCause ) )*
            {
            // InternalDOF.g:310:3: ()
            // InternalDOF.g:311:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCategoryAccess().getCategoryAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCategoryAccess().getCategoryKeyword_1());
            		
            // InternalDOF.g:321:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalDOF.g:322:4: (lv_name_2_0= RULE_ID )
            {
            // InternalDOF.g:322:4: (lv_name_2_0= RULE_ID )
            // InternalDOF.g:323:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_2_0, grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCategoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:339:3: ( (lv_causes_3_0= ruleCause ) )
            // InternalDOF.g:340:4: (lv_causes_3_0= ruleCause )
            {
            // InternalDOF.g:340:4: (lv_causes_3_0= ruleCause )
            // InternalDOF.g:341:5: lv_causes_3_0= ruleCause
            {

            					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_10);
            lv_causes_3_0=ruleCause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCategoryRule());
            					}
            					add(
            						current,
            						"causes",
            						lv_causes_3_0,
            						"es.unican.DOF.Cause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:358:3: ( (lv_causes_4_0= ruleCause ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==43) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDOF.g:359:4: (lv_causes_4_0= ruleCause )
            	    {
            	    // InternalDOF.g:359:4: (lv_causes_4_0= ruleCause )
            	    // InternalDOF.g:360:5: lv_causes_4_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_causes_4_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCategoryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"causes",
            	    						lv_causes_4_0,
            	    						"es.unican.DOF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleDataFeeder"
    // InternalDOF.g:381:1: entryRuleDataFeeder returns [EObject current=null] : iv_ruleDataFeeder= ruleDataFeeder EOF ;
    public final EObject entryRuleDataFeeder() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataFeeder = null;


        try {
            // InternalDOF.g:381:51: (iv_ruleDataFeeder= ruleDataFeeder EOF )
            // InternalDOF.g:382:2: iv_ruleDataFeeder= ruleDataFeeder EOF
            {
             newCompositeNode(grammarAccess.getDataFeederRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataFeeder=ruleDataFeeder();

            state._fsp--;

             current =iv_ruleDataFeeder; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataFeeder"


    // $ANTLR start "ruleDataFeeder"
    // InternalDOF.g:388:1: ruleDataFeeder returns [EObject current=null] : ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? ) ;
    public final EObject ruleDataFeeder() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_attributeFilter_2_0 = null;

        EObject lv_instancesFilter_3_0 = null;

        EObject lv_includedReferences_5_0 = null;

        EObject lv_typeFilter_6_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:394:2: ( ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? ) )
            // InternalDOF.g:395:2: ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? )
            {
            // InternalDOF.g:395:2: ( () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )? )
            // InternalDOF.g:396:3: () ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )? (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )* ( (lv_typeFilter_6_0= ruleTypeFilter ) )?
            {
            // InternalDOF.g:396:3: ()
            // InternalDOF.g:397:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDataFeederAccess().getDataFeederAction_0(),
            					current);
            			

            }

            // InternalDOF.g:403:3: ( (lv_name_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:404:4: (lv_name_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:404:4: (lv_name_1_0= ruleQualifiedNameWithWildcard )
            // InternalDOF.g:405:5: lv_name_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getDataFeederAccess().getNameQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_11);
            lv_name_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataFeederRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"es.unican.DOF.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:422:3: ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==31) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalDOF.g:423:4: (lv_attributeFilter_2_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:423:4: (lv_attributeFilter_2_0= ruleAttributeFilter )
                    // InternalDOF.g:424:5: lv_attributeFilter_2_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataFeederAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_12);
                    lv_attributeFilter_2_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataFeederRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_2_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:441:3: ( (lv_instancesFilter_3_0= ruleInstancesFilter ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==34) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalDOF.g:442:4: (lv_instancesFilter_3_0= ruleInstancesFilter )
                    {
                    // InternalDOF.g:442:4: (lv_instancesFilter_3_0= ruleInstancesFilter )
                    // InternalDOF.g:443:5: lv_instancesFilter_3_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getDataFeederAccess().getInstancesFilterInstancesFilterParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_instancesFilter_3_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataFeederRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_3_0,
                    						"es.unican.DOF.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:460:3: (otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==16) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDOF.g:461:4: otherlv_4= 'include' ( (lv_includedReferences_5_0= ruleIncludedReference ) )
            	    {
            	    otherlv_4=(Token)match(input,16,FOLLOW_14); 

            	    				newLeafNode(otherlv_4, grammarAccess.getDataFeederAccess().getIncludeKeyword_4_0());
            	    			
            	    // InternalDOF.g:465:4: ( (lv_includedReferences_5_0= ruleIncludedReference ) )
            	    // InternalDOF.g:466:5: (lv_includedReferences_5_0= ruleIncludedReference )
            	    {
            	    // InternalDOF.g:466:5: (lv_includedReferences_5_0= ruleIncludedReference )
            	    // InternalDOF.g:467:6: lv_includedReferences_5_0= ruleIncludedReference
            	    {

            	    						newCompositeNode(grammarAccess.getDataFeederAccess().getIncludedReferencesIncludedReferenceParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_13);
            	    lv_includedReferences_5_0=ruleIncludedReference();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getDataFeederRule());
            	    						}
            	    						add(
            	    							current,
            	    							"includedReferences",
            	    							lv_includedReferences_5_0,
            	    							"es.unican.DOF.IncludedReference");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalDOF.g:485:3: ( (lv_typeFilter_6_0= ruleTypeFilter ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==22||LA7_0==30) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalDOF.g:486:4: (lv_typeFilter_6_0= ruleTypeFilter )
                    {
                    // InternalDOF.g:486:4: (lv_typeFilter_6_0= ruleTypeFilter )
                    // InternalDOF.g:487:5: lv_typeFilter_6_0= ruleTypeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataFeederAccess().getTypeFilterTypeFilterParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_typeFilter_6_0=ruleTypeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataFeederRule());
                    					}
                    					set(
                    						current,
                    						"typeFilter",
                    						lv_typeFilter_6_0,
                    						"es.unican.DOF.TypeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataFeeder"


    // $ANTLR start "entryRuleIncludedReference"
    // InternalDOF.g:508:1: entryRuleIncludedReference returns [EObject current=null] : iv_ruleIncludedReference= ruleIncludedReference EOF ;
    public final EObject entryRuleIncludedReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncludedReference = null;


        try {
            // InternalDOF.g:508:58: (iv_ruleIncludedReference= ruleIncludedReference EOF )
            // InternalDOF.g:509:2: iv_ruleIncludedReference= ruleIncludedReference EOF
            {
             newCompositeNode(grammarAccess.getIncludedReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIncludedReference=ruleIncludedReference();

            state._fsp--;

             current =iv_ruleIncludedReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncludedReference"


    // $ANTLR start "ruleIncludedReference"
    // InternalDOF.g:515:1: ruleIncludedReference returns [EObject current=null] : (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference ) ;
    public final EObject ruleIncludedReference() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleReference_0 = null;

        EObject this_AggregatedReference_1 = null;



        	enterRule();

        try {
            // InternalDOF.g:521:2: ( (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference ) )
            // InternalDOF.g:522:2: (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference )
            {
            // InternalDOF.g:522:2: (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            else if ( (LA8_0==21) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalDOF.g:523:3: this_SimpleReference_0= ruleSimpleReference
                    {

                    			newCompositeNode(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleReference_0=ruleSimpleReference();

                    state._fsp--;


                    			current = this_SimpleReference_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:532:3: this_AggregatedReference_1= ruleAggregatedReference
                    {

                    			newCompositeNode(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AggregatedReference_1=ruleAggregatedReference();

                    state._fsp--;


                    			current = this_AggregatedReference_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncludedReference"


    // $ANTLR start "entryRuleSimpleReference"
    // InternalDOF.g:544:1: entryRuleSimpleReference returns [EObject current=null] : iv_ruleSimpleReference= ruleSimpleReference EOF ;
    public final EObject entryRuleSimpleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleReference = null;


        try {
            // InternalDOF.g:544:56: (iv_ruleSimpleReference= ruleSimpleReference EOF )
            // InternalDOF.g:545:2: iv_ruleSimpleReference= ruleSimpleReference EOF
            {
             newCompositeNode(grammarAccess.getSimpleReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleReference=ruleSimpleReference();

            state._fsp--;

             current =iv_ruleSimpleReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleReference"


    // $ANTLR start "ruleSimpleReference"
    // InternalDOF.g:551:1: ruleSimpleReference returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? ( (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) | (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? ) )? ) ;
    public final EObject ruleSimpleReference() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_attributeFilter_1_0 = null;

        EObject lv_pivotingId_3_0 = null;

        EObject lv_instancesFilter_4_0 = null;

        EObject lv_includedReferences_7_0 = null;

        EObject lv_typeFilter_8_0 = null;

        EObject lv_includedReferences_11_0 = null;

        EObject lv_typeFilter_12_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:557:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? ( (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) | (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? ) )? ) )
            // InternalDOF.g:558:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? ( (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) | (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? ) )? )
            {
            // InternalDOF.g:558:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? ( (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) | (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? ) )? )
            // InternalDOF.g:559:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )? ( (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) | (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? ) )?
            {
            // InternalDOF.g:559:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDOF.g:560:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDOF.g:560:4: (lv_name_0_0= RULE_ID )
            // InternalDOF.g:561:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            					newLeafNode(lv_name_0_0, grammarAccess.getSimpleReferenceAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleReferenceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:577:3: ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==31) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDOF.g:578:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:578:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    // InternalDOF.g:579:5: lv_attributeFilter_1_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_16);
                    lv_attributeFilter_1_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_1_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:596:3: (otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )? )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==17) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDOF.g:597:4: otherlv_2= 'by' ( (lv_pivotingId_3_0= rulePath ) ) ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )?
                    {
                    otherlv_2=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_2, grammarAccess.getSimpleReferenceAccess().getByKeyword_2_0());
                    			
                    // InternalDOF.g:601:4: ( (lv_pivotingId_3_0= rulePath ) )
                    // InternalDOF.g:602:5: (lv_pivotingId_3_0= rulePath )
                    {
                    // InternalDOF.g:602:5: (lv_pivotingId_3_0= rulePath )
                    // InternalDOF.g:603:6: lv_pivotingId_3_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_pivotingId_3_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    						}
                    						set(
                    							current,
                    							"pivotingId",
                    							lv_pivotingId_3_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDOF.g:620:4: ( (lv_instancesFilter_4_0= ruleInstancesFilter ) )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==34) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalDOF.g:621:5: (lv_instancesFilter_4_0= ruleInstancesFilter )
                            {
                            // InternalDOF.g:621:5: (lv_instancesFilter_4_0= ruleInstancesFilter )
                            // InternalDOF.g:622:6: lv_instancesFilter_4_0= ruleInstancesFilter
                            {

                            						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_2_2_0());
                            					
                            pushFollow(FOLLOW_18);
                            lv_instancesFilter_4_0=ruleInstancesFilter();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            						}
                            						set(
                            							current,
                            							"instancesFilter",
                            							lv_instancesFilter_4_0,
                            							"es.unican.DOF.InstancesFilter");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalDOF.g:640:3: ( (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) | (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? ) )?
            int alt16=3;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==18) ) {
                alt16=1;
            }
            else if ( (LA16_0==20) ) {
                alt16=2;
            }
            switch (alt16) {
                case 1 :
                    // InternalDOF.g:641:4: (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )
                    {
                    // InternalDOF.g:641:4: (otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )
                    // InternalDOF.g:642:5: otherlv_5= '{' otherlv_6= 'include' ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}'
                    {
                    otherlv_5=(Token)match(input,18,FOLLOW_19); 

                    					newLeafNode(otherlv_5, grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_3_0_0());
                    				
                    otherlv_6=(Token)match(input,16,FOLLOW_20); 

                    					newLeafNode(otherlv_6, grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_3_0_1());
                    				
                    // InternalDOF.g:650:5: ( (lv_includedReferences_7_0= ruleIncludedReference ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==RULE_ID||LA12_0==21) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalDOF.g:651:6: (lv_includedReferences_7_0= ruleIncludedReference )
                    	    {
                    	    // InternalDOF.g:651:6: (lv_includedReferences_7_0= ruleIncludedReference )
                    	    // InternalDOF.g:652:7: lv_includedReferences_7_0= ruleIncludedReference
                    	    {

                    	    							newCompositeNode(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_0_2_0());
                    	    						
                    	    pushFollow(FOLLOW_20);
                    	    lv_includedReferences_7_0=ruleIncludedReference();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"includedReferences",
                    	    								lv_includedReferences_7_0,
                    	    								"es.unican.DOF.IncludedReference");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    // InternalDOF.g:669:5: ( (lv_typeFilter_8_0= ruleTypeFilter ) )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==22||LA13_0==30) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalDOF.g:670:6: (lv_typeFilter_8_0= ruleTypeFilter )
                            {
                            // InternalDOF.g:670:6: (lv_typeFilter_8_0= ruleTypeFilter )
                            // InternalDOF.g:671:7: lv_typeFilter_8_0= ruleTypeFilter
                            {

                            							newCompositeNode(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_0_3_0());
                            						
                            pushFollow(FOLLOW_21);
                            lv_typeFilter_8_0=ruleTypeFilter();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            							}
                            							set(
                            								current,
                            								"typeFilter",
                            								lv_typeFilter_8_0,
                            								"es.unican.DOF.TypeFilter");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }

                    otherlv_9=(Token)match(input,19,FOLLOW_2); 

                    					newLeafNode(otherlv_9, grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_3_0_4());
                    				

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:694:4: (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? )
                    {
                    // InternalDOF.g:694:4: (otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )? )
                    // InternalDOF.g:695:5: otherlv_10= '.' ( (lv_includedReferences_11_0= ruleIncludedReference ) )* ( (lv_typeFilter_12_0= ruleTypeFilter ) )?
                    {
                    otherlv_10=(Token)match(input,20,FOLLOW_22); 

                    					newLeafNode(otherlv_10, grammarAccess.getSimpleReferenceAccess().getFullStopKeyword_3_1_0());
                    				
                    // InternalDOF.g:699:5: ( (lv_includedReferences_11_0= ruleIncludedReference ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==RULE_ID) ) {
                            alt14=1;
                        }
                        else if ( (LA14_0==21) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalDOF.g:700:6: (lv_includedReferences_11_0= ruleIncludedReference )
                    	    {
                    	    // InternalDOF.g:700:6: (lv_includedReferences_11_0= ruleIncludedReference )
                    	    // InternalDOF.g:701:7: lv_includedReferences_11_0= ruleIncludedReference
                    	    {

                    	    							newCompositeNode(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_22);
                    	    lv_includedReferences_11_0=ruleIncludedReference();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"includedReferences",
                    	    								lv_includedReferences_11_0,
                    	    								"es.unican.DOF.IncludedReference");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    // InternalDOF.g:718:5: ( (lv_typeFilter_12_0= ruleTypeFilter ) )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==22) ) {
                        alt15=1;
                    }
                    else if ( (LA15_0==30) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // InternalDOF.g:719:6: (lv_typeFilter_12_0= ruleTypeFilter )
                            {
                            // InternalDOF.g:719:6: (lv_typeFilter_12_0= ruleTypeFilter )
                            // InternalDOF.g:720:7: lv_typeFilter_12_0= ruleTypeFilter
                            {

                            							newCompositeNode(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_1_2_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_typeFilter_12_0=ruleTypeFilter();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            							}
                            							set(
                            								current,
                            								"typeFilter",
                            								lv_typeFilter_12_0,
                            								"es.unican.DOF.TypeFilter");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleReference"


    // $ANTLR start "entryRuleAggregatedReference"
    // InternalDOF.g:743:1: entryRuleAggregatedReference returns [EObject current=null] : iv_ruleAggregatedReference= ruleAggregatedReference EOF ;
    public final EObject entryRuleAggregatedReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAggregatedReference = null;


        try {
            // InternalDOF.g:743:60: (iv_ruleAggregatedReference= ruleAggregatedReference EOF )
            // InternalDOF.g:744:2: iv_ruleAggregatedReference= ruleAggregatedReference EOF
            {
             newCompositeNode(grammarAccess.getAggregatedReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggregatedReference=ruleAggregatedReference();

            state._fsp--;

             current =iv_ruleAggregatedReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggregatedReference"


    // $ANTLR start "ruleAggregatedReference"
    // InternalDOF.g:750:1: ruleAggregatedReference returns [EObject current=null] : (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? ) ;
    public final EObject ruleAggregatedReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_function_3_0 = null;

        EObject lv_aggValue_5_0 = null;

        EObject lv_pivotingId_8_0 = null;

        EObject lv_instancesFilter_9_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:756:2: ( (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? ) )
            // InternalDOF.g:757:2: (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? )
            {
            // InternalDOF.g:757:2: (otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )? )
            // InternalDOF.g:758:3: otherlv_0= 'calculate' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'as' ( (lv_function_3_0= ruleAggFunction ) ) otherlv_4= '(' ( (lv_aggValue_5_0= rulePath ) ) otherlv_6= ')' (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )? ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )?
            {
            otherlv_0=(Token)match(input,21,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0());
            		
            // InternalDOF.g:762:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDOF.g:763:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDOF.g:763:4: (lv_name_1_0= RULE_ID )
            // InternalDOF.g:764:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_23); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAggregatedReferenceAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAggregatedReferenceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,22,FOLLOW_24); 

            			newLeafNode(otherlv_2, grammarAccess.getAggregatedReferenceAccess().getAsKeyword_2());
            		
            // InternalDOF.g:784:3: ( (lv_function_3_0= ruleAggFunction ) )
            // InternalDOF.g:785:4: (lv_function_3_0= ruleAggFunction )
            {
            // InternalDOF.g:785:4: (lv_function_3_0= ruleAggFunction )
            // InternalDOF.g:786:5: lv_function_3_0= ruleAggFunction
            {

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_25);
            lv_function_3_0=ruleAggFunction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
            					}
            					set(
            						current,
            						"function",
            						lv_function_3_0,
            						"es.unican.DOF.AggFunction");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,23,FOLLOW_4); 

            			newLeafNode(otherlv_4, grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_4());
            		
            // InternalDOF.g:807:3: ( (lv_aggValue_5_0= rulePath ) )
            // InternalDOF.g:808:4: (lv_aggValue_5_0= rulePath )
            {
            // InternalDOF.g:808:4: (lv_aggValue_5_0= rulePath )
            // InternalDOF.g:809:5: lv_aggValue_5_0= rulePath
            {

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_26);
            lv_aggValue_5_0=rulePath();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
            					}
            					set(
            						current,
            						"aggValue",
            						lv_aggValue_5_0,
            						"es.unican.DOF.Path");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,24,FOLLOW_27); 

            			newLeafNode(otherlv_6, grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_6());
            		
            // InternalDOF.g:830:3: (otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==17) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDOF.g:831:4: otherlv_7= 'by' ( (lv_pivotingId_8_0= rulePath ) )
                    {
                    otherlv_7=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_7, grammarAccess.getAggregatedReferenceAccess().getByKeyword_7_0());
                    			
                    // InternalDOF.g:835:4: ( (lv_pivotingId_8_0= rulePath ) )
                    // InternalDOF.g:836:5: (lv_pivotingId_8_0= rulePath )
                    {
                    // InternalDOF.g:836:5: (lv_pivotingId_8_0= rulePath )
                    // InternalDOF.g:837:6: lv_pivotingId_8_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_pivotingId_8_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
                    						}
                    						set(
                    							current,
                    							"pivotingId",
                    							lv_pivotingId_8_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDOF.g:855:3: ( (lv_instancesFilter_9_0= ruleInstancesFilter ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==34) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDOF.g:856:4: (lv_instancesFilter_9_0= ruleInstancesFilter )
                    {
                    // InternalDOF.g:856:4: (lv_instancesFilter_9_0= ruleInstancesFilter )
                    // InternalDOF.g:857:5: lv_instancesFilter_9_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_8_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_instancesFilter_9_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_9_0,
                    						"es.unican.DOF.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggregatedReference"


    // $ANTLR start "entryRuleAggFunction"
    // InternalDOF.g:878:1: entryRuleAggFunction returns [String current=null] : iv_ruleAggFunction= ruleAggFunction EOF ;
    public final String entryRuleAggFunction() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAggFunction = null;


        try {
            // InternalDOF.g:878:51: (iv_ruleAggFunction= ruleAggFunction EOF )
            // InternalDOF.g:879:2: iv_ruleAggFunction= ruleAggFunction EOF
            {
             newCompositeNode(grammarAccess.getAggFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggFunction=ruleAggFunction();

            state._fsp--;

             current =iv_ruleAggFunction.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggFunction"


    // $ANTLR start "ruleAggFunction"
    // InternalDOF.g:885:1: ruleAggFunction returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' ) ;
    public final AntlrDatatypeRuleToken ruleAggFunction() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDOF.g:891:2: ( (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' ) )
            // InternalDOF.g:892:2: (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' )
            {
            // InternalDOF.g:892:2: (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' )
            int alt19=5;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt19=1;
                }
                break;
            case 26:
                {
                alt19=2;
                }
                break;
            case 27:
                {
                alt19=3;
                }
                break;
            case 28:
                {
                alt19=4;
                }
                break;
            case 29:
                {
                alt19=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalDOF.g:893:3: kw= 'count'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getCountKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:899:3: kw= 'sum'
                    {
                    kw=(Token)match(input,26,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getSumKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalDOF.g:905:3: kw= 'avg'
                    {
                    kw=(Token)match(input,27,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getAvgKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalDOF.g:911:3: kw= 'max'
                    {
                    kw=(Token)match(input,28,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getMaxKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalDOF.g:917:3: kw= 'min'
                    {
                    kw=(Token)match(input,29,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getMinKeyword_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggFunction"


    // $ANTLR start "entryRuleTypeFilter"
    // InternalDOF.g:926:1: entryRuleTypeFilter returns [EObject current=null] : iv_ruleTypeFilter= ruleTypeFilter EOF ;
    public final EObject entryRuleTypeFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeFilter = null;


        try {
            // InternalDOF.g:926:51: (iv_ruleTypeFilter= ruleTypeFilter EOF )
            // InternalDOF.g:927:2: iv_ruleTypeFilter= ruleTypeFilter EOF
            {
             newCompositeNode(grammarAccess.getTypeFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeFilter=ruleTypeFilter();

            state._fsp--;

             current =iv_ruleTypeFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeFilter"


    // $ANTLR start "ruleTypeFilter"
    // InternalDOF.g:933:1: ruleTypeFilter returns [EObject current=null] : (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection ) ;
    public final EObject ruleTypeFilter() throws RecognitionException {
        EObject current = null;

        EObject this_TypeCompletion_0 = null;

        EObject this_TypeSelection_1 = null;



        	enterRule();

        try {
            // InternalDOF.g:939:2: ( (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection ) )
            // InternalDOF.g:940:2: (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection )
            {
            // InternalDOF.g:940:2: (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==22) ) {
                alt20=1;
            }
            else if ( (LA20_0==30) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalDOF.g:941:3: this_TypeCompletion_0= ruleTypeCompletion
                    {

                    			newCompositeNode(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_TypeCompletion_0=ruleTypeCompletion();

                    state._fsp--;


                    			current = this_TypeCompletion_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:950:3: this_TypeSelection_1= ruleTypeSelection
                    {

                    			newCompositeNode(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_TypeSelection_1=ruleTypeSelection();

                    state._fsp--;


                    			current = this_TypeSelection_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeFilter"


    // $ANTLR start "entryRuleTypeCompletion"
    // InternalDOF.g:962:1: entryRuleTypeCompletion returns [EObject current=null] : iv_ruleTypeCompletion= ruleTypeCompletion EOF ;
    public final EObject entryRuleTypeCompletion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeCompletion = null;


        try {
            // InternalDOF.g:962:55: (iv_ruleTypeCompletion= ruleTypeCompletion EOF )
            // InternalDOF.g:963:2: iv_ruleTypeCompletion= ruleTypeCompletion EOF
            {
             newCompositeNode(grammarAccess.getTypeCompletionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeCompletion=ruleTypeCompletion();

            state._fsp--;

             current =iv_ruleTypeCompletion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeCompletion"


    // $ANTLR start "ruleTypeCompletion"
    // InternalDOF.g:969:1: ruleTypeCompletion returns [EObject current=null] : (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ ;
    public final EObject ruleTypeCompletion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeCustomizations_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:975:2: ( (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ )
            // InternalDOF.g:976:2: (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            {
            // InternalDOF.g:976:2: (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            int cnt21=0;
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==22) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalDOF.g:977:3: otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    {
            	    otherlv_0=(Token)match(input,22,FOLLOW_4); 

            	    			newLeafNode(otherlv_0, grammarAccess.getTypeCompletionAccess().getAsKeyword_0());
            	    		
            	    // InternalDOF.g:981:3: ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    // InternalDOF.g:982:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    {
            	    // InternalDOF.g:982:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    // InternalDOF.g:983:5: lv_typeCustomizations_1_0= ruleTypeCustomization
            	    {

            	    					newCompositeNode(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_29);
            	    lv_typeCustomizations_1_0=ruleTypeCustomization();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeCompletionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"typeCustomizations",
            	    						lv_typeCustomizations_1_0,
            	    						"es.unican.DOF.TypeCustomization");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt21 >= 1 ) break loop21;
                        EarlyExitException eee =
                            new EarlyExitException(21, input);
                        throw eee;
                }
                cnt21++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeCompletion"


    // $ANTLR start "entryRuleTypeSelection"
    // InternalDOF.g:1004:1: entryRuleTypeSelection returns [EObject current=null] : iv_ruleTypeSelection= ruleTypeSelection EOF ;
    public final EObject entryRuleTypeSelection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeSelection = null;


        try {
            // InternalDOF.g:1004:54: (iv_ruleTypeSelection= ruleTypeSelection EOF )
            // InternalDOF.g:1005:2: iv_ruleTypeSelection= ruleTypeSelection EOF
            {
             newCompositeNode(grammarAccess.getTypeSelectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeSelection=ruleTypeSelection();

            state._fsp--;

             current =iv_ruleTypeSelection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeSelection"


    // $ANTLR start "ruleTypeSelection"
    // InternalDOF.g:1011:1: ruleTypeSelection returns [EObject current=null] : (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ ;
    public final EObject ruleTypeSelection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeCustomizations_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1017:2: ( (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ )
            // InternalDOF.g:1018:2: (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            {
            // InternalDOF.g:1018:2: (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            int cnt22=0;
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==30) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalDOF.g:1019:3: otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    {
            	    otherlv_0=(Token)match(input,30,FOLLOW_4); 

            	    			newLeafNode(otherlv_0, grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0());
            	    		
            	    // InternalDOF.g:1023:3: ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    // InternalDOF.g:1024:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    {
            	    // InternalDOF.g:1024:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    // InternalDOF.g:1025:5: lv_typeCustomizations_1_0= ruleTypeCustomization
            	    {

            	    					newCompositeNode(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_typeCustomizations_1_0=ruleTypeCustomization();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeSelectionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"typeCustomizations",
            	    						lv_typeCustomizations_1_0,
            	    						"es.unican.DOF.TypeCustomization");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt22 >= 1 ) break loop22;
                        EarlyExitException eee =
                            new EarlyExitException(22, input);
                        throw eee;
                }
                cnt22++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeSelection"


    // $ANTLR start "entryRuleTypeCustomization"
    // InternalDOF.g:1046:1: entryRuleTypeCustomization returns [EObject current=null] : iv_ruleTypeCustomization= ruleTypeCustomization EOF ;
    public final EObject entryRuleTypeCustomization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeCustomization = null;


        try {
            // InternalDOF.g:1046:58: (iv_ruleTypeCustomization= ruleTypeCustomization EOF )
            // InternalDOF.g:1047:2: iv_ruleTypeCustomization= ruleTypeCustomization EOF
            {
             newCompositeNode(grammarAccess.getTypeCustomizationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeCustomization=ruleTypeCustomization();

            state._fsp--;

             current =iv_ruleTypeCustomization; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeCustomization"


    // $ANTLR start "ruleTypeCustomization"
    // InternalDOF.g:1053:1: ruleTypeCustomization returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? ) ;
    public final EObject ruleTypeCustomization() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_attributeFilter_1_0 = null;

        EObject lv_includedReferences_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1059:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? ) )
            // InternalDOF.g:1060:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? )
            {
            // InternalDOF.g:1060:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? )
            // InternalDOF.g:1061:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )?
            {
            // InternalDOF.g:1061:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDOF.g:1062:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDOF.g:1062:4: (lv_name_0_0= RULE_ID )
            // InternalDOF.g:1063:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_31); 

            					newLeafNode(lv_name_0_0, grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTypeCustomizationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:1079:3: ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==31) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalDOF.g:1080:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:1080:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    // InternalDOF.g:1081:5: lv_attributeFilter_1_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_32);
                    lv_attributeFilter_1_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTypeCustomizationRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_1_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:1098:3: (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==18) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalDOF.g:1099:4: otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}'
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_33); 

                    				newLeafNode(otherlv_2, grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0());
                    			
                    // InternalDOF.g:1103:4: ( (lv_includedReferences_3_0= ruleIncludedReference ) )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==RULE_ID||LA24_0==21) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalDOF.g:1104:5: (lv_includedReferences_3_0= ruleIncludedReference )
                    	    {
                    	    // InternalDOF.g:1104:5: (lv_includedReferences_3_0= ruleIncludedReference )
                    	    // InternalDOF.g:1105:6: lv_includedReferences_3_0= ruleIncludedReference
                    	    {

                    	    						newCompositeNode(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_33);
                    	    lv_includedReferences_3_0=ruleIncludedReference();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getTypeCustomizationRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"includedReferences",
                    	    							lv_includedReferences_3_0,
                    	    							"es.unican.DOF.IncludedReference");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,19,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeCustomization"


    // $ANTLR start "entryRuleAttributeFilter"
    // InternalDOF.g:1131:1: entryRuleAttributeFilter returns [EObject current=null] : iv_ruleAttributeFilter= ruleAttributeFilter EOF ;
    public final EObject entryRuleAttributeFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeFilter = null;


        try {
            // InternalDOF.g:1131:56: (iv_ruleAttributeFilter= ruleAttributeFilter EOF )
            // InternalDOF.g:1132:2: iv_ruleAttributeFilter= ruleAttributeFilter EOF
            {
             newCompositeNode(grammarAccess.getAttributeFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeFilter=ruleAttributeFilter();

            state._fsp--;

             current =iv_ruleAttributeFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeFilter"


    // $ANTLR start "ruleAttributeFilter"
    // InternalDOF.g:1138:1: ruleAttributeFilter returns [EObject current=null] : ( () otherlv_1= '[' ( ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )* )? otherlv_5= ']' ) ;
    public final EObject ruleAttributeFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_attributes_2_0=null;
        Token otherlv_3=null;
        Token lv_attributes_4_0=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalDOF.g:1144:2: ( ( () otherlv_1= '[' ( ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )* )? otherlv_5= ']' ) )
            // InternalDOF.g:1145:2: ( () otherlv_1= '[' ( ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )* )? otherlv_5= ']' )
            {
            // InternalDOF.g:1145:2: ( () otherlv_1= '[' ( ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )* )? otherlv_5= ']' )
            // InternalDOF.g:1146:3: () otherlv_1= '[' ( ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )* )? otherlv_5= ']'
            {
            // InternalDOF.g:1146:3: ()
            // InternalDOF.g:1147:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAttributeFilterAccess().getAttributeFilterAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,31,FOLLOW_34); 

            			newLeafNode(otherlv_1, grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_1());
            		
            // InternalDOF.g:1157:3: ( ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )* )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_ID) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalDOF.g:1158:4: ( (lv_attributes_2_0= RULE_ID ) ) (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )*
                    {
                    // InternalDOF.g:1158:4: ( (lv_attributes_2_0= RULE_ID ) )
                    // InternalDOF.g:1159:5: (lv_attributes_2_0= RULE_ID )
                    {
                    // InternalDOF.g:1159:5: (lv_attributes_2_0= RULE_ID )
                    // InternalDOF.g:1160:6: lv_attributes_2_0= RULE_ID
                    {
                    lv_attributes_2_0=(Token)match(input,RULE_ID,FOLLOW_35); 

                    						newLeafNode(lv_attributes_2_0, grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeFilterRule());
                    						}
                    						addWithLastConsumed(
                    							current,
                    							"attributes",
                    							lv_attributes_2_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }

                    // InternalDOF.g:1176:4: (otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) ) )*
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==32) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // InternalDOF.g:1177:5: otherlv_3= ',' ( (lv_attributes_4_0= RULE_ID ) )
                    	    {
                    	    otherlv_3=(Token)match(input,32,FOLLOW_4); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalDOF.g:1181:5: ( (lv_attributes_4_0= RULE_ID ) )
                    	    // InternalDOF.g:1182:6: (lv_attributes_4_0= RULE_ID )
                    	    {
                    	    // InternalDOF.g:1182:6: (lv_attributes_4_0= RULE_ID )
                    	    // InternalDOF.g:1183:7: lv_attributes_4_0= RULE_ID
                    	    {
                    	    lv_attributes_4_0=(Token)match(input,RULE_ID,FOLLOW_35); 

                    	    							newLeafNode(lv_attributes_4_0, grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_1_0());
                    	    						

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getAttributeFilterRule());
                    	    							}
                    	    							addWithLastConsumed(
                    	    								current,
                    	    								"attributes",
                    	    								lv_attributes_4_0,
                    	    								"org.eclipse.xtext.common.Terminals.ID");
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,33,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeFilter"


    // $ANTLR start "entryRuleInstancesFilter"
    // InternalDOF.g:1209:1: entryRuleInstancesFilter returns [EObject current=null] : iv_ruleInstancesFilter= ruleInstancesFilter EOF ;
    public final EObject entryRuleInstancesFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstancesFilter = null;


        try {
            // InternalDOF.g:1209:56: (iv_ruleInstancesFilter= ruleInstancesFilter EOF )
            // InternalDOF.g:1210:2: iv_ruleInstancesFilter= ruleInstancesFilter EOF
            {
             newCompositeNode(grammarAccess.getInstancesFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstancesFilter=ruleInstancesFilter();

            state._fsp--;

             current =iv_ruleInstancesFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstancesFilter"


    // $ANTLR start "ruleInstancesFilter"
    // InternalDOF.g:1216:1: ruleInstancesFilter returns [EObject current=null] : (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction ) ;
    public final EObject ruleInstancesFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_AndConjunction_1 = null;



        	enterRule();

        try {
            // InternalDOF.g:1222:2: ( (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction ) )
            // InternalDOF.g:1223:2: (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction )
            {
            // InternalDOF.g:1223:2: (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction )
            // InternalDOF.g:1224:3: otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction
            {
            otherlv_0=(Token)match(input,34,FOLLOW_36); 

            			newLeafNode(otherlv_0, grammarAccess.getInstancesFilterAccess().getWhereKeyword_0());
            		

            			newCompositeNode(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1());
            		
            pushFollow(FOLLOW_2);
            this_AndConjunction_1=ruleAndConjunction();

            state._fsp--;


            			current = this_AndConjunction_1;
            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstancesFilter"


    // $ANTLR start "entryRuleAndConjunction"
    // InternalDOF.g:1240:1: entryRuleAndConjunction returns [EObject current=null] : iv_ruleAndConjunction= ruleAndConjunction EOF ;
    public final EObject entryRuleAndConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndConjunction = null;


        try {
            // InternalDOF.g:1240:55: (iv_ruleAndConjunction= ruleAndConjunction EOF )
            // InternalDOF.g:1241:2: iv_ruleAndConjunction= ruleAndConjunction EOF
            {
             newCompositeNode(grammarAccess.getAndConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAndConjunction=ruleAndConjunction();

            state._fsp--;

             current =iv_ruleAndConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndConjunction"


    // $ANTLR start "ruleAndConjunction"
    // InternalDOF.g:1247:1: ruleAndConjunction returns [EObject current=null] : (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* ) ;
    public final EObject ruleAndConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_OrConjunction_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1253:2: ( (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* ) )
            // InternalDOF.g:1254:2: (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* )
            {
            // InternalDOF.g:1254:2: (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* )
            // InternalDOF.g:1255:3: this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_37);
            this_OrConjunction_0=ruleOrConjunction();

            state._fsp--;


            			current = this_OrConjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:1263:3: ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==35) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalDOF.g:1264:4: () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) )
            	    {
            	    // InternalDOF.g:1264:4: ()
            	    // InternalDOF.g:1265:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,35,FOLLOW_36); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1());
            	    			
            	    // InternalDOF.g:1275:4: ( (lv_right_3_0= ruleOrConjunction ) )
            	    // InternalDOF.g:1276:5: (lv_right_3_0= ruleOrConjunction )
            	    {
            	    // InternalDOF.g:1276:5: (lv_right_3_0= ruleOrConjunction )
            	    // InternalDOF.g:1277:6: lv_right_3_0= ruleOrConjunction
            	    {

            	    						newCompositeNode(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_37);
            	    lv_right_3_0=ruleOrConjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"es.unican.DOF.OrConjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndConjunction"


    // $ANTLR start "entryRuleOrConjunction"
    // InternalDOF.g:1299:1: entryRuleOrConjunction returns [EObject current=null] : iv_ruleOrConjunction= ruleOrConjunction EOF ;
    public final EObject entryRuleOrConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrConjunction = null;


        try {
            // InternalDOF.g:1299:54: (iv_ruleOrConjunction= ruleOrConjunction EOF )
            // InternalDOF.g:1300:2: iv_ruleOrConjunction= ruleOrConjunction EOF
            {
             newCompositeNode(grammarAccess.getOrConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrConjunction=ruleOrConjunction();

            state._fsp--;

             current =iv_ruleOrConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrConjunction"


    // $ANTLR start "ruleOrConjunction"
    // InternalDOF.g:1306:1: ruleOrConjunction returns [EObject current=null] : (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* ) ;
    public final EObject ruleOrConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Primary_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1312:2: ( (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* ) )
            // InternalDOF.g:1313:2: (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* )
            {
            // InternalDOF.g:1313:2: (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* )
            // InternalDOF.g:1314:3: this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0());
            		
            pushFollow(FOLLOW_38);
            this_Primary_0=rulePrimary();

            state._fsp--;


            			current = this_Primary_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:1322:3: ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==36) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalDOF.g:1323:4: () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) )
            	    {
            	    // InternalDOF.g:1323:4: ()
            	    // InternalDOF.g:1324:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,36,FOLLOW_36); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1());
            	    			
            	    // InternalDOF.g:1334:4: ( (lv_right_3_0= rulePrimary ) )
            	    // InternalDOF.g:1335:5: (lv_right_3_0= rulePrimary )
            	    {
            	    // InternalDOF.g:1335:5: (lv_right_3_0= rulePrimary )
            	    // InternalDOF.g:1336:6: lv_right_3_0= rulePrimary
            	    {

            	    						newCompositeNode(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_38);
            	    lv_right_3_0=rulePrimary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"es.unican.DOF.Primary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrConjunction"


    // $ANTLR start "entryRulePrimary"
    // InternalDOF.g:1358:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalDOF.g:1358:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalDOF.g:1359:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalDOF.g:1365:1: rulePrimary returns [EObject current=null] : (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Comparison_0 = null;

        EObject this_AndConjunction_2 = null;



        	enterRule();

        try {
            // InternalDOF.g:1371:2: ( (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) ) )
            // InternalDOF.g:1372:2: (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) )
            {
            // InternalDOF.g:1372:2: (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_ID) ) {
                alt30=1;
            }
            else if ( (LA30_0==23) ) {
                alt30=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalDOF.g:1373:3: this_Comparison_0= ruleComparison
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Comparison_0=ruleComparison();

                    state._fsp--;


                    			current = this_Comparison_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:1382:3: (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' )
                    {
                    // InternalDOF.g:1382:3: (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' )
                    // InternalDOF.g:1383:4: otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,23,FOLLOW_36); 

                    				newLeafNode(otherlv_1, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1());
                    			
                    pushFollow(FOLLOW_26);
                    this_AndConjunction_2=ruleAndConjunction();

                    state._fsp--;


                    				current = this_AndConjunction_2;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_3=(Token)match(input,24,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleComparison"
    // InternalDOF.g:1404:1: entryRuleComparison returns [EObject current=null] : iv_ruleComparison= ruleComparison EOF ;
    public final EObject entryRuleComparison() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparison = null;


        try {
            // InternalDOF.g:1404:51: (iv_ruleComparison= ruleComparison EOF )
            // InternalDOF.g:1405:2: iv_ruleComparison= ruleComparison EOF
            {
             newCompositeNode(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparison=ruleComparison();

            state._fsp--;

             current =iv_ruleComparison; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalDOF.g:1411:1: ruleComparison returns [EObject current=null] : ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) ) ;
    public final EObject ruleComparison() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_6=null;
        Token lv_value_7_0=null;
        Token otherlv_10=null;
        Token lv_value_11_0=null;
        Token otherlv_14=null;
        Token lv_value_15_0=null;
        Token otherlv_18=null;
        Token lv_value_19_0=null;
        Token otherlv_22=null;
        Token lv_value_23_0=null;
        EObject lv_path_1_0 = null;

        EObject lv_path_5_0 = null;

        EObject lv_path_9_0 = null;

        EObject lv_path_13_0 = null;

        EObject lv_path_17_0 = null;

        EObject lv_path_21_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1417:2: ( ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) ) )
            // InternalDOF.g:1418:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )
            {
            // InternalDOF.g:1418:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )
            int alt31=6;
            alt31 = dfa31.predict(input);
            switch (alt31) {
                case 1 :
                    // InternalDOF.g:1419:3: ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1419:3: ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalDOF.g:1420:4: () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1420:4: ()
                    // InternalDOF.g:1421:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getEqualityAction_0_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1427:4: ( (lv_path_1_0= rulePath ) )
                    // InternalDOF.g:1428:5: (lv_path_1_0= rulePath )
                    {
                    // InternalDOF.g:1428:5: (lv_path_1_0= rulePath )
                    // InternalDOF.g:1429:6: lv_path_1_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_39);
                    lv_path_1_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_1_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,37,FOLLOW_40); 

                    				newLeafNode(otherlv_2, grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2());
                    			
                    // InternalDOF.g:1450:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalDOF.g:1451:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalDOF.g:1451:5: (lv_value_3_0= RULE_STRING )
                    // InternalDOF.g:1452:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:1470:3: ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1470:3: ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) )
                    // InternalDOF.g:1471:4: () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1471:4: ()
                    // InternalDOF.g:1472:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getInequalityAction_1_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1478:4: ( (lv_path_5_0= rulePath ) )
                    // InternalDOF.g:1479:5: (lv_path_5_0= rulePath )
                    {
                    // InternalDOF.g:1479:5: (lv_path_5_0= rulePath )
                    // InternalDOF.g:1480:6: lv_path_5_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_41);
                    lv_path_5_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_5_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,38,FOLLOW_40); 

                    				newLeafNode(otherlv_6, grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2());
                    			
                    // InternalDOF.g:1501:4: ( (lv_value_7_0= RULE_STRING ) )
                    // InternalDOF.g:1502:5: (lv_value_7_0= RULE_STRING )
                    {
                    // InternalDOF.g:1502:5: (lv_value_7_0= RULE_STRING )
                    // InternalDOF.g:1503:6: lv_value_7_0= RULE_STRING
                    {
                    lv_value_7_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_7_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:1521:3: ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1521:3: ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) )
                    // InternalDOF.g:1522:4: () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1522:4: ()
                    // InternalDOF.g:1523:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getMoreThanAction_2_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1529:4: ( (lv_path_9_0= rulePath ) )
                    // InternalDOF.g:1530:5: (lv_path_9_0= rulePath )
                    {
                    // InternalDOF.g:1530:5: (lv_path_9_0= rulePath )
                    // InternalDOF.g:1531:6: lv_path_9_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_42);
                    lv_path_9_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_9_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_10=(Token)match(input,39,FOLLOW_40); 

                    				newLeafNode(otherlv_10, grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2());
                    			
                    // InternalDOF.g:1552:4: ( (lv_value_11_0= RULE_STRING ) )
                    // InternalDOF.g:1553:5: (lv_value_11_0= RULE_STRING )
                    {
                    // InternalDOF.g:1553:5: (lv_value_11_0= RULE_STRING )
                    // InternalDOF.g:1554:6: lv_value_11_0= RULE_STRING
                    {
                    lv_value_11_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_11_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_11_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalDOF.g:1572:3: ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1572:3: ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) )
                    // InternalDOF.g:1573:4: () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1573:4: ()
                    // InternalDOF.g:1574:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1580:4: ( (lv_path_13_0= rulePath ) )
                    // InternalDOF.g:1581:5: (lv_path_13_0= rulePath )
                    {
                    // InternalDOF.g:1581:5: (lv_path_13_0= rulePath )
                    // InternalDOF.g:1582:6: lv_path_13_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_43);
                    lv_path_13_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_13_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_14=(Token)match(input,40,FOLLOW_40); 

                    				newLeafNode(otherlv_14, grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2());
                    			
                    // InternalDOF.g:1603:4: ( (lv_value_15_0= RULE_STRING ) )
                    // InternalDOF.g:1604:5: (lv_value_15_0= RULE_STRING )
                    {
                    // InternalDOF.g:1604:5: (lv_value_15_0= RULE_STRING )
                    // InternalDOF.g:1605:6: lv_value_15_0= RULE_STRING
                    {
                    lv_value_15_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_15_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_15_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalDOF.g:1623:3: ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1623:3: ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) )
                    // InternalDOF.g:1624:4: () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1624:4: ()
                    // InternalDOF.g:1625:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getLessThanAction_4_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1631:4: ( (lv_path_17_0= rulePath ) )
                    // InternalDOF.g:1632:5: (lv_path_17_0= rulePath )
                    {
                    // InternalDOF.g:1632:5: (lv_path_17_0= rulePath )
                    // InternalDOF.g:1633:6: lv_path_17_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_44);
                    lv_path_17_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_17_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_18=(Token)match(input,41,FOLLOW_40); 

                    				newLeafNode(otherlv_18, grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2());
                    			
                    // InternalDOF.g:1654:4: ( (lv_value_19_0= RULE_STRING ) )
                    // InternalDOF.g:1655:5: (lv_value_19_0= RULE_STRING )
                    {
                    // InternalDOF.g:1655:5: (lv_value_19_0= RULE_STRING )
                    // InternalDOF.g:1656:6: lv_value_19_0= RULE_STRING
                    {
                    lv_value_19_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_19_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_19_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalDOF.g:1674:3: ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) )
                    {
                    // InternalDOF.g:1674:3: ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) )
                    // InternalDOF.g:1675:4: () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) )
                    {
                    // InternalDOF.g:1675:4: ()
                    // InternalDOF.g:1676:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0(),
                    						current);
                    				

                    }

                    // InternalDOF.g:1682:4: ( (lv_path_21_0= rulePath ) )
                    // InternalDOF.g:1683:5: (lv_path_21_0= rulePath )
                    {
                    // InternalDOF.g:1683:5: (lv_path_21_0= rulePath )
                    // InternalDOF.g:1684:6: lv_path_21_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_45);
                    lv_path_21_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_21_0,
                    							"es.unican.DOF.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_22=(Token)match(input,42,FOLLOW_40); 

                    				newLeafNode(otherlv_22, grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2());
                    			
                    // InternalDOF.g:1705:4: ( (lv_value_23_0= RULE_STRING ) )
                    // InternalDOF.g:1706:5: (lv_value_23_0= RULE_STRING )
                    {
                    // InternalDOF.g:1706:5: (lv_value_23_0= RULE_STRING )
                    // InternalDOF.g:1707:6: lv_value_23_0= RULE_STRING
                    {
                    lv_value_23_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_23_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_23_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleCause"
    // InternalDOF.g:1728:1: entryRuleCause returns [EObject current=null] : iv_ruleCause= ruleCause EOF ;
    public final EObject entryRuleCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCause = null;


        try {
            // InternalDOF.g:1728:46: (iv_ruleCause= ruleCause EOF )
            // InternalDOF.g:1729:2: iv_ruleCause= ruleCause EOF
            {
             newCompositeNode(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCause=ruleCause();

            state._fsp--;

             current =iv_ruleCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalDOF.g:1735:1: ruleCause returns [EObject current=null] : (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause ) ;
    public final EObject ruleCause() throws RecognitionException {
        EObject current = null;

        EObject this_CompoundCause_0 = null;

        EObject this_DataLinkedCause_1 = null;

        EObject this_NotMappedCause_2 = null;



        	enterRule();

        try {
            // InternalDOF.g:1741:2: ( (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause ) )
            // InternalDOF.g:1742:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )
            {
            // InternalDOF.g:1742:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )
            int alt32=3;
            alt32 = dfa32.predict(input);
            switch (alt32) {
                case 1 :
                    // InternalDOF.g:1743:3: this_CompoundCause_0= ruleCompoundCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_CompoundCause_0=ruleCompoundCause();

                    state._fsp--;


                    			current = this_CompoundCause_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:1752:3: this_DataLinkedCause_1= ruleDataLinkedCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataLinkedCause_1=ruleDataLinkedCause();

                    state._fsp--;


                    			current = this_DataLinkedCause_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDOF.g:1761:3: this_NotMappedCause_2= ruleNotMappedCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_NotMappedCause_2=ruleNotMappedCause();

                    state._fsp--;


                    			current = this_NotMappedCause_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleCompoundCause"
    // InternalDOF.g:1773:1: entryRuleCompoundCause returns [EObject current=null] : iv_ruleCompoundCause= ruleCompoundCause EOF ;
    public final EObject entryRuleCompoundCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompoundCause = null;


        try {
            // InternalDOF.g:1773:54: (iv_ruleCompoundCause= ruleCompoundCause EOF )
            // InternalDOF.g:1774:2: iv_ruleCompoundCause= ruleCompoundCause EOF
            {
             newCompositeNode(grammarAccess.getCompoundCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompoundCause=ruleCompoundCause();

            state._fsp--;

             current =iv_ruleCompoundCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompoundCause"


    // $ANTLR start "ruleCompoundCause"
    // InternalDOF.g:1780:1: ruleCompoundCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' ) ;
    public final EObject ruleCompoundCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_subCauses_7_0 = null;

        EObject lv_subCauses_8_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1786:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' ) )
            // InternalDOF.g:1787:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' )
            {
            // InternalDOF.g:1787:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}' )
            // InternalDOF.g:1788:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'contains' otherlv_6= '{' ( (lv_subCauses_7_0= ruleCause ) ) ( (lv_subCauses_8_0= ruleCause ) )* otherlv_9= '}'
            {
            // InternalDOF.g:1788:3: ()
            // InternalDOF.g:1789:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCompoundCauseAccess().getCompoundCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,43,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCompoundCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:1799:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:1800:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:1800:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:1801:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getCompoundCauseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_46);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:1818:3: (otherlv_3= 'realizes' ( ( ruleEString ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==44) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalDOF.g:1819:4: otherlv_3= 'realizes' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,44,FOLLOW_40); 

                    				newLeafNode(otherlv_3, grammarAccess.getCompoundCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:1823:4: ( ( ruleEString ) )
                    // InternalDOF.g:1824:5: ( ruleEString )
                    {
                    // InternalDOF.g:1824:5: ( ruleEString )
                    // InternalDOF.g:1825:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCompoundCauseRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getCompoundCauseAccess().getRealizesCauseCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_47);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,45,FOLLOW_48); 

            			newLeafNode(otherlv_5, grammarAccess.getCompoundCauseAccess().getContainsKeyword_4());
            		
            otherlv_6=(Token)match(input,18,FOLLOW_9); 

            			newLeafNode(otherlv_6, grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalDOF.g:1848:3: ( (lv_subCauses_7_0= ruleCause ) )
            // InternalDOF.g:1849:4: (lv_subCauses_7_0= ruleCause )
            {
            // InternalDOF.g:1849:4: (lv_subCauses_7_0= ruleCause )
            // InternalDOF.g:1850:5: lv_subCauses_7_0= ruleCause
            {

            					newCompositeNode(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_49);
            lv_subCauses_7_0=ruleCause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            					}
            					add(
            						current,
            						"subCauses",
            						lv_subCauses_7_0,
            						"es.unican.DOF.Cause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:1867:3: ( (lv_subCauses_8_0= ruleCause ) )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==43) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalDOF.g:1868:4: (lv_subCauses_8_0= ruleCause )
            	    {
            	    // InternalDOF.g:1868:4: (lv_subCauses_8_0= ruleCause )
            	    // InternalDOF.g:1869:5: lv_subCauses_8_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_49);
            	    lv_subCauses_8_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"subCauses",
            	    						lv_subCauses_8_0,
            	    						"es.unican.DOF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

            otherlv_9=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompoundCause"


    // $ANTLR start "entryRuleDataLinkedCause"
    // InternalDOF.g:1894:1: entryRuleDataLinkedCause returns [EObject current=null] : iv_ruleDataLinkedCause= ruleDataLinkedCause EOF ;
    public final EObject entryRuleDataLinkedCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataLinkedCause = null;


        try {
            // InternalDOF.g:1894:56: (iv_ruleDataLinkedCause= ruleDataLinkedCause EOF )
            // InternalDOF.g:1895:2: iv_ruleDataLinkedCause= ruleDataLinkedCause EOF
            {
             newCompositeNode(grammarAccess.getDataLinkedCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataLinkedCause=ruleDataLinkedCause();

            state._fsp--;

             current =iv_ruleDataLinkedCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataLinkedCause"


    // $ANTLR start "ruleDataLinkedCause"
    // InternalDOF.g:1901:1: ruleDataLinkedCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? ) ;
    public final EObject ruleDataLinkedCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_attributeFilter_6_0 = null;

        EObject lv_instancesFilter_7_0 = null;

        EObject lv_includedReferences_8_0 = null;

        EObject lv_typeFilter_9_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:1907:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? ) )
            // InternalDOF.g:1908:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? )
            {
            // InternalDOF.g:1908:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? )
            // InternalDOF.g:1909:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'is' ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )? ( (lv_includedReferences_8_0= ruleIncludedReference ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )?
            {
            // InternalDOF.g:1909:3: ()
            // InternalDOF.g:1910:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDataLinkedCauseAccess().getDataLinkedCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,43,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:1920:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:1921:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:1921:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:1922:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_50);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:1939:3: (otherlv_3= 'realizes' ( ( ruleEString ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==44) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalDOF.g:1940:4: otherlv_3= 'realizes' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,44,FOLLOW_40); 

                    				newLeafNode(otherlv_3, grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:1944:4: ( ( ruleEString ) )
                    // InternalDOF.g:1945:5: ( ruleEString )
                    {
                    // InternalDOF.g:1945:5: ( ruleEString )
                    // InternalDOF.g:1946:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDataLinkedCauseRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_51); 

            			newLeafNode(otherlv_5, grammarAccess.getDataLinkedCauseAccess().getIsKeyword_4());
            		
            // InternalDOF.g:1965:3: ( (lv_attributeFilter_6_0= ruleAttributeFilter ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==31) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalDOF.g:1966:4: (lv_attributeFilter_6_0= ruleAttributeFilter )
                    {
                    // InternalDOF.g:1966:4: (lv_attributeFilter_6_0= ruleAttributeFilter )
                    // InternalDOF.g:1967:5: lv_attributeFilter_6_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_52);
                    lv_attributeFilter_6_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_6_0,
                    						"es.unican.DOF.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:1984:3: ( (lv_instancesFilter_7_0= ruleInstancesFilter ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==34) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalDOF.g:1985:4: (lv_instancesFilter_7_0= ruleInstancesFilter )
                    {
                    // InternalDOF.g:1985:4: (lv_instancesFilter_7_0= ruleInstancesFilter )
                    // InternalDOF.g:1986:5: lv_instancesFilter_7_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0());
                    				
                    pushFollow(FOLLOW_22);
                    lv_instancesFilter_7_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_7_0,
                    						"es.unican.DOF.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDOF.g:2003:3: ( (lv_includedReferences_8_0= ruleIncludedReference ) )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==RULE_ID||LA38_0==21) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalDOF.g:2004:4: (lv_includedReferences_8_0= ruleIncludedReference )
            	    {
            	    // InternalDOF.g:2004:4: (lv_includedReferences_8_0= ruleIncludedReference )
            	    // InternalDOF.g:2005:5: lv_includedReferences_8_0= ruleIncludedReference
            	    {

            	    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_22);
            	    lv_includedReferences_8_0=ruleIncludedReference();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"includedReferences",
            	    						lv_includedReferences_8_0,
            	    						"es.unican.DOF.IncludedReference");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

            // InternalDOF.g:2022:3: ( (lv_typeFilter_9_0= ruleTypeFilter ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==22||LA39_0==30) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalDOF.g:2023:4: (lv_typeFilter_9_0= ruleTypeFilter )
                    {
                    // InternalDOF.g:2023:4: (lv_typeFilter_9_0= ruleTypeFilter )
                    // InternalDOF.g:2024:5: lv_typeFilter_9_0= ruleTypeFilter
                    {

                    					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getTypeFilterTypeFilterParserRuleCall_8_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_typeFilter_9_0=ruleTypeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    					}
                    					set(
                    						current,
                    						"typeFilter",
                    						lv_typeFilter_9_0,
                    						"es.unican.DOF.TypeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataLinkedCause"


    // $ANTLR start "entryRuleNotMappedCause"
    // InternalDOF.g:2045:1: entryRuleNotMappedCause returns [EObject current=null] : iv_ruleNotMappedCause= ruleNotMappedCause EOF ;
    public final EObject entryRuleNotMappedCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotMappedCause = null;


        try {
            // InternalDOF.g:2045:55: (iv_ruleNotMappedCause= ruleNotMappedCause EOF )
            // InternalDOF.g:2046:2: iv_ruleNotMappedCause= ruleNotMappedCause EOF
            {
             newCompositeNode(grammarAccess.getNotMappedCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotMappedCause=ruleNotMappedCause();

            state._fsp--;

             current =iv_ruleNotMappedCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotMappedCause"


    // $ANTLR start "ruleNotMappedCause"
    // InternalDOF.g:2052:1: ruleNotMappedCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' ) ;
    public final EObject ruleNotMappedCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:2058:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' ) )
            // InternalDOF.g:2059:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' )
            {
            // InternalDOF.g:2059:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped' )
            // InternalDOF.g:2060:3: () otherlv_1= 'cause' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'realizes' ( ( ruleEString ) ) )? otherlv_5= 'notMapped'
            {
            // InternalDOF.g:2060:3: ()
            // InternalDOF.g:2061:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,43,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:2071:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalDOF.g:2072:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalDOF.g:2072:4: (lv_name_2_0= ruleQualifiedName )
            // InternalDOF.g:2073:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getNotMappedCauseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_53);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNotMappedCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"es.unican.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:2090:3: (otherlv_3= 'realizes' ( ( ruleEString ) ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==44) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalDOF.g:2091:4: otherlv_3= 'realizes' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,44,FOLLOW_40); 

                    				newLeafNode(otherlv_3, grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:2095:4: ( ( ruleEString ) )
                    // InternalDOF.g:2096:5: ( ruleEString )
                    {
                    // InternalDOF.g:2096:5: ( ruleEString )
                    // InternalDOF.g:2097:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getNotMappedCauseRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getNotMappedCauseAccess().getRealizesCauseCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_54);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,46,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotMappedCause"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalDOF.g:2120:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalDOF.g:2120:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalDOF.g:2121:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalDOF.g:2127:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:2133:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalDOF.g:2134:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalDOF.g:2134:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalDOF.g:2135:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_55);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:2145:3: (kw= '.*' )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==47) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalDOF.g:2146:4: kw= '.*'
                    {
                    kw=(Token)match(input,47,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDOF.g:2156:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalDOF.g:2156:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalDOF.g:2157:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDOF.g:2163:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalDOF.g:2169:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalDOF.g:2170:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalDOF.g:2170:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalDOF.g:2171:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_56); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalDOF.g:2178:3: (kw= '.' this_ID_2= RULE_ID )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( (LA42_0==20) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalDOF.g:2179:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,20,FOLLOW_4); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_56); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRulePath"
    // InternalDOF.g:2196:1: entryRulePath returns [EObject current=null] : iv_rulePath= rulePath EOF ;
    public final EObject entryRulePath() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePath = null;


        try {
            // InternalDOF.g:2196:45: (iv_rulePath= rulePath EOF )
            // InternalDOF.g:2197:2: iv_rulePath= rulePath EOF
            {
             newCompositeNode(grammarAccess.getPathRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePath=rulePath();

            state._fsp--;

             current =iv_rulePath; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalDOF.g:2203:1: rulePath returns [EObject current=null] : ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* ) ;
    public final EObject rulePath() throws RecognitionException {
        EObject current = null;

        Token lv_jumps_0_0=null;
        Token otherlv_1=null;
        Token lv_jumps_2_0=null;


        	enterRule();

        try {
            // InternalDOF.g:2209:2: ( ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* ) )
            // InternalDOF.g:2210:2: ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* )
            {
            // InternalDOF.g:2210:2: ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* )
            // InternalDOF.g:2211:3: ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )*
            {
            // InternalDOF.g:2211:3: ( (lv_jumps_0_0= RULE_ID ) )
            // InternalDOF.g:2212:4: (lv_jumps_0_0= RULE_ID )
            {
            // InternalDOF.g:2212:4: (lv_jumps_0_0= RULE_ID )
            // InternalDOF.g:2213:5: lv_jumps_0_0= RULE_ID
            {
            lv_jumps_0_0=(Token)match(input,RULE_ID,FOLLOW_56); 

            					newLeafNode(lv_jumps_0_0, grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPathRule());
            					}
            					addWithLastConsumed(
            						current,
            						"jumps",
            						lv_jumps_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:2229:3: (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==20) ) {
                    int LA43_2 = input.LA(2);

                    if ( (LA43_2==RULE_ID) ) {
                        alt43=1;
                    }


                }


                switch (alt43) {
            	case 1 :
            	    // InternalDOF.g:2230:4: otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,20,FOLLOW_4); 

            	    				newLeafNode(otherlv_1, grammarAccess.getPathAccess().getFullStopKeyword_1_0());
            	    			
            	    // InternalDOF.g:2234:4: ( (lv_jumps_2_0= RULE_ID ) )
            	    // InternalDOF.g:2235:5: (lv_jumps_2_0= RULE_ID )
            	    {
            	    // InternalDOF.g:2235:5: (lv_jumps_2_0= RULE_ID )
            	    // InternalDOF.g:2236:6: lv_jumps_2_0= RULE_ID
            	    {
            	    lv_jumps_2_0=(Token)match(input,RULE_ID,FOLLOW_56); 

            	    						newLeafNode(lv_jumps_2_0, grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getPathRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"jumps",
            	    							lv_jumps_2_0,
            	    							"org.eclipse.xtext.common.Terminals.ID");
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleEString"
    // InternalDOF.g:2257:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalDOF.g:2257:47: (iv_ruleEString= ruleEString EOF )
            // InternalDOF.g:2258:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDOF.g:2264:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;


        	enterRule();

        try {
            // InternalDOF.g:2270:2: (this_STRING_0= RULE_STRING )
            // InternalDOF.g:2271:2: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            		current.merge(this_STRING_0);
            	

            		newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


    protected DFA31 dfa31 = new DFA31(this);
    protected DFA32 dfa32 = new DFA32(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\4\1\24\1\4\6\uffff\1\24";
    static final String dfa_3s = "\1\4\1\52\1\4\6\uffff\1\52";
    static final String dfa_4s = "\3\uffff\1\3\1\5\1\2\1\4\1\1\1\6\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\2\20\uffff\1\7\1\5\1\3\1\6\1\4\1\10",
            "\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\2\20\uffff\1\7\1\5\1\3\1\6\1\4\1\10"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA31 extends DFA {

        public DFA31(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 31;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1418:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )";
        }
    }
    static final String dfa_7s = "\1\53\1\4\1\16\1\4\1\5\3\uffff\2\16";
    static final String dfa_8s = "\1\53\1\4\1\56\1\4\1\5\3\uffff\2\56";
    static final String dfa_9s = "\5\uffff\1\1\1\2\1\3\2\uffff";
    static final String[] dfa_10s = {
            "\1\1",
            "\1\2",
            "\1\6\5\uffff\1\3\27\uffff\1\4\1\5\1\7",
            "\1\10",
            "\1\11",
            "",
            "",
            "",
            "\1\6\5\uffff\1\3\27\uffff\1\4\1\5\1\7",
            "\1\6\36\uffff\1\5\1\7"
    };
    static final char[] dfa_7 = DFA.unpackEncodedStringToUnsignedChars(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final short[][] dfa_10 = unpackEncodedStringArray(dfa_10s);

    class DFA32 extends DFA {

        public DFA32(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 32;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_7;
            this.max = dfa_8;
            this.accept = dfa_9;
            this.special = dfa_5;
            this.transition = dfa_10;
        }
        public String getDescription() {
            return "1742:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000004C0410002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000440410002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000040410002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000080160002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000160002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000400140002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000140002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040680010L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000040600012L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x000000003E000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000400020002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000080040002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000280010L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000200000010L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000300000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000800010L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000300000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000080000080000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000100000004000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x00000004C0600012L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000440600012L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000500000000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000100002L});

}