package es.unican.generator;

import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
public class ColumnSet {
  private List<String> columnNames;
  
  private Map<EObject, List<ValueWrapper>> rows;
  
  public ColumnSet() {
    ArrayList<String> _arrayList = new ArrayList<String>();
    this.columnNames = _arrayList;
    HashMap<EObject, List<ValueWrapper>> _hashMap = new HashMap<EObject, List<ValueWrapper>>();
    this.rows = _hashMap;
  }
  
  public List<String> getColumnNames() {
    return this.columnNames;
  }
  
  public Map<EObject, List<ValueWrapper>> getRows() {
    return this.rows;
  }
  
  public boolean addColumnNames(final List<String> names) {
    return this.columnNames.addAll(names);
  }
  
  public Object addColumnValues(final EObject instance, final List<ValueWrapper> values) {
    Object _xifexpression = null;
    List<ValueWrapper> _get = this.rows.get(instance);
    boolean _equals = Objects.equal(_get, null);
    if (_equals) {
      ArrayList<ValueWrapper> _arrayList = new ArrayList<ValueWrapper>(values);
      _xifexpression = this.rows.put(instance, _arrayList);
    } else {
      _xifexpression = Boolean.valueOf(this.rows.get(instance).addAll(values));
    }
    return _xifexpression;
  }
  
  public void addColumnSet(final ColumnSet columnSet) {
    this.columnNames.addAll(columnSet.getColumnNames());
    final Map<EObject, List<ValueWrapper>> columnSetRows = columnSet.rows;
    Set<EObject> _keySet = columnSetRows.keySet();
    for (final EObject instance : _keySet) {
      List<ValueWrapper> _get = this.rows.get(instance);
      boolean _equals = Objects.equal(_get, null);
      if (_equals) {
        List<ValueWrapper> _get_1 = columnSetRows.get(instance);
        ArrayList<ValueWrapper> _arrayList = new ArrayList<ValueWrapper>(_get_1);
        this.rows.put(instance, _arrayList);
      } else {
        this.rows.get(instance).addAll(columnSetRows.get(instance));
      }
    }
  }
}
