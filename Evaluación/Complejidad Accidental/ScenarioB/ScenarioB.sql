SELECT d.id,
   c.fallingWheelBand,
   p.number
   FROM DriveHalfShaft d
   JOIN ComplianceReport c
      ON d.idComplianceReport = c.id
   JOIN AssemblySession a
       ON d.idAssemblySession = a.id
   JOIN Program p
       ON a.idProgram = p.id;
