% !TeX spellcheck = en_US
%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{Dataset Generation from Fishbone Diagrams}\label{chap:solution}

% **************************** Define Graphics Path **************************
\ifpdf
\graphicspath{{Chapter3/Figs/Raster/}{Chapter3/Figs/PDF/}{Chapter3/Figs/}}
\else
\graphicspath{{Chapter3/Figs/Vector/}{Chapter3/Figs/}}
\fi

\section{A MDD Process for Dataset Generation in Industry 4.0}
\label{sec:overview}

This chapter describes the results of the research work we carried for adapting Lavoisier to work with fishbone diagrams. Figure~\ref{fig:solutionOverview} shows the general scheme for our solution. The inputs are a fishbone diagram (Figure~\ref{fig:solutionOverview}, label 1) and an object-oriented model (Figure~\ref{fig:solutionOverview}, label 2) that describes data available in a domain model. This fishbone diagram is a classical fishbone diagram, such as the one depicted in Figure~\ref{fig:fishboneDiagram}, created for quality control purposes. Because of this, we will call it \emph{Quality Control Fishbone} (QCF) diagram. 



The object-oriented domain model is created by data scientists to fulfill two objectives: (1) to provide a well-formed description of data available in a manufacturing setting; and, (2) to supply an access layer to retrieve domain data. This model must conform to the Ecore metamodel~\citep{Steinberg2008}, so that it can be accessed and navigated using model management tools.

\begin{figure}[!bt]
	\centering
	% Requires \usepackage{graphicx}
	\includegraphics[width=0.85\linewidth]{Chapter3/Figs/solutionOverview.pdf} \\
	\caption{Overview of our solution.}
	\label{fig:solutionOverview}
\end{figure}

Industrial engineers are encouraged to review domain models and data scientists to check fishbone diagrams, as it is indicated by the go and back arrows between these elements. By inspecting domain models, industrial engineers might discover new causes that could be added to the fishbone diagrams. On the other hand, data scientists could find elements in fishbone diagrams that might be included in the domain data model.

Using these models as inputs, a new kind of fishbone diagram, which was called \emph{Data-Oriented Fishbone} (DOF) diagram (Figure~\ref{fig:solutionOverview}, label 3), would be created. A DOF reshapes a Quality Control Fishbone diagram linking causes with data that characterize them. The connections between causes and domain data are established using special blocks of code that we named as \emph{data feeders}. A data feeder specifies, using Lavoisier-like primitives, what data of a domain model must be retrieved to represent a certain cause. Therefore, a DOF specifies influence relationships between domain data.

Finally, industrial engineers specify what concrete causes of a DOF must be included in a dataset using a language called \emph{Papin}, which was designed for this purpose. Papin specifications (Figure~\ref{fig:solutionOverview}, label 4) are automatically processed by the Papin interpreter, which, using the information in the data feeders, retrieves and transforms domain data to automatically create a dataset (Figure~\ref{fig:solutionOverview}, label 5). 

\section{Data-Oriented Fishbone Models}\label{sec:dataOrientedFishbone}

A \emph{Data-Oriented Fishbone} model links causes in a fishbone diagram with data in an object-oriented domain model. For this purpose, it uses a special kind of code blocks called \emph{data feeders}. A data feeder is comprised of two elements: a \emph{path} and a \emph{Lavoisier expression}. The path, starting always from a main class, traverses the domain data model to reach a class, an attribute or a collection that can be used to characterize a concrete cause. For instance, the cause \emph{Band Inadequate Parameters} (Figure~\ref{fig:fishboneDiagram}, cause 1.1.1) might be analyzed using the data in the \emph{BandBatchParameters} class.

As in Lavoisier, when a class is referenced by one of these paths, all its attributes are considered to be included in the output dataset, whereas all the references are excluded by default. This can be modified by attaching a \emph{Lavoisier expression} to the end of the path. This expression specifies what concrete attributes and references of the selected class should be included in the output dataset.

%% Finally, we would like to highlight that, when the target element of a path is an attribute, we cannot attach a Lavoisier expression to this path, as it would not make sense. In this case, just the referenced attribute is added to the output dataset, and no further customizations are needed.

As an example, Listing~\ref{lst:dataOriented} shows a fragment of a Data-Oriented Fishbone model for the Quality Control Fishbone diagram depicted in Figure~\ref{fig:fishboneDiagram}.

\begin{minipage}{\linewidth}
	\lstinputlisting[style=dof,caption={A Data-Oriented Fishbone model for our running example.},label=lst:dataOriented,captionpos=b,basicstyle=\scriptsize]{listings/dataOrientedFishboneModel.txt}
\end{minipage}	

A DOF must always start with the declaration of an effect. To associate domain data to the effect, we must take into account that, for analyzing a phenomenon, data analysis algorithms often require to be fed with data of entities that manifest that effect and entities that do not. For instance, to find patterns that lead to falling bands, these algorithms might need to compare drive half-shafts with and without falling bands. Therefore, in a DOF, the effect is associated to the domain entity whose instances might exhibit it.

Listing~\ref{lst:dataOriented}, lines 1-2 shows an example of an effect declaration. This declaration contains a name for the effect, \emph{FallingBand}, and, after the \emph{is} keyword, a data feeder referencing a class from the data model. In our case, falling bands are analyzed by inspecting instances of \emph{DriveHalfShafts}. Moreover, each half-shaft is characterized by its compliance report, which is added by including the \emph{report} reference. The class referenced by the effect is considered the main class for the output dataset. Furthermore, all the paths in the data feeders of the causes will start from this main class.

After the effect declaration, to construct a DOF, we specify \emph{category blocks} (Listing~\ref{lst:dataOriented}, lines 3-13, 14-17 and 19-21). These blocks simply mimic the category structure of the associated Quality Control Fishbone diagram. Then, we attach causes to these categories. In a DOF, three kinds of causes can be created: (1) compound causes; (2) data-linked causes; and, (3) not mapped causes.

\emph{Compound causes} represent causes containing one or more subcauses. Compound causes do not have data feeders and they are characterized by combining data associated to its subcauses. \emph{Data-linked causes} are causes that do not contain subcauses and that are linked to domain data by means of data feeders.  Finally, \emph{not mapped causes} specify causes that are not associated to domain data. These causes serve to specify causes for which domain data is not available yet, but that we want to keep in a DOF to preserve traceability between that DOF and its associated QCF.

Listing~\ref{lst:dataOriented}, lines 4-12 show an example of a \emph{compound cause}, whose name is \emph{Bands} and that realizes the quality-oriented cause \emph{Unsuitable Band} (see Figure~\ref{fig:fishboneDiagram}, cause 1.1). To keep traceability between causes of a DOF and its corresponding QCF, causes in a DOF have an optional \emph{realizes} statement, which serves to specify which cause of a QCF is associated to each cause in a DOF. 

A DOF tries to replicate the same structure of a QCF, but it might need to modify this structure sometimes. For instance, Figure~\ref{fig:fishboneDiagram} contains the cause \emph{Unsuitable Band} (cause 1.1), but there are two bands tightened to a shaft, one on the engine side and the other one on the wheel side. Therefore, when creating the DOF, we decompose this cause into two subcauses: \emph{WheelBand} (Listing~\ref{lst:dataOriented}, lines 5-10) and \emph{EngineBand} (Listing~\ref{lst:dataOriented}, line 11). \emph{WheelBand} and \emph{EngineBand} are also compound causes, but comprised of the data-linked causes, \emph{Model} (Listing~\ref{lst:dataOriented}, lines 6-7) and \emph{Parameters} (Listing~\ref{lst:dataOriented}, lines 8-9). In \emph{data-linked causes}, the path referencing a domain model element must be specified after the \emph{is} keyword. This target element can be: (1) a single attribute; (2) a reference to a class; or, (3) a collection of references to classes.

In Listing~\ref{lst:dataOriented}, lines 6-7, the pointed element is an attribute, which means this \emph{Model} cause will be characterized by a single value associated to that attribute. Listing~\ref{lst:dataOriented}, lines 8-9 show an example of data-linked cause where the target element is a class. Moreover, in this case, it is specified, by means of a Lavoisier expression, that the \emph{provider} reference of the pointed class must also be used to characterize this cause. Finally, Listing~\ref{lst:dataOriented}, lines 15-17 illustrate a case where the path of a data feeder points to a collection. Collections are handled as multibounded references. This means we need to identify instances in this collection so that data of each individual instance can be spread over a set of well-defined columns. In our concrete case, each measure of the pressure in the pliers pneumatic system is identified by its associated tightening number.

Finally, Listing~\ref{lst:dataOriented}, lines 19-21 contain an example of a not mapped cause. Obviously, working conditions, such as, for example, dusty or noisy environments, might be a cause of falling bands. However, we do not have data about these issues, so we cannot link these causes with domain data and they must remain as a not mapped cause. Keeping these causes is useful in order to maintain traceability between fishbone models and to record hints about what new data might be gathered to improve these analysis. 


\section{Papin: Dataset Specification by Cause Selection}
\label{sec:papinSyntax}

\noindent
\begin{minipage}{\linewidth}
	\lstinputlisting[style=papin,caption={An example of Papin specification.},captionpos=b,label=lst:papinExample,basicstyle=\scriptsize]{listings/papinScript.txt}
\end{minipage}	

After building a DOF, \emph{Papin} is used to specify which causes of this model should be included in a dataset for analyzing the DOF effect. Listing~\ref{lst:papinExample} provides an example of \emph{Papin specification}. A Papin specification always starts defining a name for the output dataset (Listing~\ref{lst:papinExample}, line 1), which is \emph{FallingBand\_MaterialsAndPressures} in our case. Then, we must provide the name of the DOF from which causes will be selected (Listing~\ref{lst:papinExample}, line 2). In our example, this fishbone model is the one depicted in Listing~\ref{lst:dataOriented}, which is identified as \emph{FallingBand}. Then, categories to be included in the output dataset must be explicitly listed. In our case, two categories, \emph{Material} and \emph{Machine}, are selected.

By default, all the causes included in a category are added to the output dataset when a category is selected. For example, in Listing~\ref{lst:papinExample}, line 3, all causes inside the \emph{Material} category are implicitly added to the target dataset. This behavior also applies to compound causes. If we wanted to select just a subset of causes of a compound element, we must explicitly list the concrete subcauses to be selected inside an inclusion block after the element name. Listing~\ref{lst:papinExample}, lines 4-6 show an example of an inclusion block. In this case, just the \emph{PneumaticPliersPressure} cause of the \emph{Machine} category is added to the output dataset.

These specifications are processed by the Papin interpreter as follows: first of all, domain data associated to the selected data-linked causes is retrieved using the data feeders associated to these causes. As a result, for each data-linked cause, we get a tabular structure with one column per each attribute that characterizes that cause, plus one column for the identifier of the instances being analyzed\footnote{If entities being analyzed required being identified by several values, a column per each one of these values would be added to this tabular structure.}. For example, the \emph{Model} cause, contained in the \emph{WheelBand} cause (Listing~\ref{lst:dataOriented}, lines 6-7), when selected, generates a tabular structure with \emph{id} and \emph{model} as columns, where \emph{id} is the identifier for a drive half-shaft and \emph{model} is the model number of the band in the wheel side, respectively.

Since these tabular structures are computed using the same transformation patterns executed by the \emph{Lavoisier} interpreter uses, each one of them satisfies the \emph{one entity, one row} constraint. So, each one of these tabular structures can be seen as a projection of the output dataset that contains just the data associated to a specific cause. Therefore, to generate the complete dataset, we would just need to combine these projections. This task can be easily executed by performing \emph{joins}. The resulting table is combined by a new \emph{join} with the effect data, and the CSV file for the output dataset is finally generated from this final tabular structure.


