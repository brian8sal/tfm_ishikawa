package es.unican.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import es.unican.services.PapinGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPapinParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'domainModelNSURI'", "'domainModelInstance'", "'dataset'", "'using'", "'{'", "'}'", "'include'", "'by'", "'calculate'", "'as'", "'('", "')'", "'count'", "'sum'", "'avg'", "'max'", "'min'", "'only_as'", "'['", "','", "']'", "'where'", "'and'", "'or'", "'='", "'!='", "'>'", "'>='", "'<'", "'<='", "'.'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPapinParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPapinParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPapinParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPapin.g"; }



     	private PapinGrammarAccess grammarAccess;

        public InternalPapinParser(TokenStream input, PapinGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Datasets";
       	}

       	@Override
       	protected PapinGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDatasets"
    // InternalPapin.g:64:1: entryRuleDatasets returns [EObject current=null] : iv_ruleDatasets= ruleDatasets EOF ;
    public final EObject entryRuleDatasets() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDatasets = null;


        try {
            // InternalPapin.g:64:49: (iv_ruleDatasets= ruleDatasets EOF )
            // InternalPapin.g:65:2: iv_ruleDatasets= ruleDatasets EOF
            {
             newCompositeNode(grammarAccess.getDatasetsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDatasets=ruleDatasets();

            state._fsp--;

             current =iv_ruleDatasets; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDatasets"


    // $ANTLR start "ruleDatasets"
    // InternalPapin.g:71:1: ruleDatasets returns [EObject current=null] : ( (otherlv_0= 'domainModelNSURI' )+ ( (lv_domainModelNSURI_1_0= RULE_STRING ) ) (otherlv_2= 'domainModelInstance' )+ ( (lv_domainModelInstance_3_0= RULE_STRING ) ) ( (lv_datasets_4_0= ruleDataset ) )* ) ;
    public final EObject ruleDatasets() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_domainModelNSURI_1_0=null;
        Token otherlv_2=null;
        Token lv_domainModelInstance_3_0=null;
        EObject lv_datasets_4_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:77:2: ( ( (otherlv_0= 'domainModelNSURI' )+ ( (lv_domainModelNSURI_1_0= RULE_STRING ) ) (otherlv_2= 'domainModelInstance' )+ ( (lv_domainModelInstance_3_0= RULE_STRING ) ) ( (lv_datasets_4_0= ruleDataset ) )* ) )
            // InternalPapin.g:78:2: ( (otherlv_0= 'domainModelNSURI' )+ ( (lv_domainModelNSURI_1_0= RULE_STRING ) ) (otherlv_2= 'domainModelInstance' )+ ( (lv_domainModelInstance_3_0= RULE_STRING ) ) ( (lv_datasets_4_0= ruleDataset ) )* )
            {
            // InternalPapin.g:78:2: ( (otherlv_0= 'domainModelNSURI' )+ ( (lv_domainModelNSURI_1_0= RULE_STRING ) ) (otherlv_2= 'domainModelInstance' )+ ( (lv_domainModelInstance_3_0= RULE_STRING ) ) ( (lv_datasets_4_0= ruleDataset ) )* )
            // InternalPapin.g:79:3: (otherlv_0= 'domainModelNSURI' )+ ( (lv_domainModelNSURI_1_0= RULE_STRING ) ) (otherlv_2= 'domainModelInstance' )+ ( (lv_domainModelInstance_3_0= RULE_STRING ) ) ( (lv_datasets_4_0= ruleDataset ) )*
            {
            // InternalPapin.g:79:3: (otherlv_0= 'domainModelNSURI' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPapin.g:80:4: otherlv_0= 'domainModelNSURI'
            	    {
            	    otherlv_0=(Token)match(input,11,FOLLOW_3); 

            	    				newLeafNode(otherlv_0, grammarAccess.getDatasetsAccess().getDomainModelNSURIKeyword_0());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            // InternalPapin.g:85:3: ( (lv_domainModelNSURI_1_0= RULE_STRING ) )
            // InternalPapin.g:86:4: (lv_domainModelNSURI_1_0= RULE_STRING )
            {
            // InternalPapin.g:86:4: (lv_domainModelNSURI_1_0= RULE_STRING )
            // InternalPapin.g:87:5: lv_domainModelNSURI_1_0= RULE_STRING
            {
            lv_domainModelNSURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_4); 

            					newLeafNode(lv_domainModelNSURI_1_0, grammarAccess.getDatasetsAccess().getDomainModelNSURISTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDatasetsRule());
            					}
            					setWithLastConsumed(
            						current,
            						"domainModelNSURI",
            						lv_domainModelNSURI_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalPapin.g:103:3: (otherlv_2= 'domainModelInstance' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPapin.g:104:4: otherlv_2= 'domainModelInstance'
            	    {
            	    otherlv_2=(Token)match(input,12,FOLLOW_5); 

            	    				newLeafNode(otherlv_2, grammarAccess.getDatasetsAccess().getDomainModelInstanceKeyword_2());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            // InternalPapin.g:109:3: ( (lv_domainModelInstance_3_0= RULE_STRING ) )
            // InternalPapin.g:110:4: (lv_domainModelInstance_3_0= RULE_STRING )
            {
            // InternalPapin.g:110:4: (lv_domainModelInstance_3_0= RULE_STRING )
            // InternalPapin.g:111:5: lv_domainModelInstance_3_0= RULE_STRING
            {
            lv_domainModelInstance_3_0=(Token)match(input,RULE_STRING,FOLLOW_6); 

            					newLeafNode(lv_domainModelInstance_3_0, grammarAccess.getDatasetsAccess().getDomainModelInstanceSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDatasetsRule());
            					}
            					setWithLastConsumed(
            						current,
            						"domainModelInstance",
            						lv_domainModelInstance_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalPapin.g:127:3: ( (lv_datasets_4_0= ruleDataset ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==13) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalPapin.g:128:4: (lv_datasets_4_0= ruleDataset )
            	    {
            	    // InternalPapin.g:128:4: (lv_datasets_4_0= ruleDataset )
            	    // InternalPapin.g:129:5: lv_datasets_4_0= ruleDataset
            	    {

            	    					newCompositeNode(grammarAccess.getDatasetsAccess().getDatasetsDatasetParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_datasets_4_0=ruleDataset();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDatasetsRule());
            	    					}
            	    					add(
            	    						current,
            	    						"datasets",
            	    						lv_datasets_4_0,
            	    						"es.unican.Papin.Dataset");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDatasets"


    // $ANTLR start "entryRuleDataset"
    // InternalPapin.g:150:1: entryRuleDataset returns [EObject current=null] : iv_ruleDataset= ruleDataset EOF ;
    public final EObject entryRuleDataset() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataset = null;


        try {
            // InternalPapin.g:150:48: (iv_ruleDataset= ruleDataset EOF )
            // InternalPapin.g:151:2: iv_ruleDataset= ruleDataset EOF
            {
             newCompositeNode(grammarAccess.getDatasetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataset=ruleDataset();

            state._fsp--;

             current =iv_ruleDataset; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataset"


    // $ANTLR start "ruleDataset"
    // InternalPapin.g:157:1: ruleDataset returns [EObject current=null] : (otherlv_0= 'dataset' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'using' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( (lv_attributeFilter_5_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_6_0= ruleInstancesFilter ) )? ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) ;
    public final EObject ruleDataset() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_9=null;
        EObject lv_attributeFilter_5_0 = null;

        EObject lv_instancesFilter_6_0 = null;

        EObject lv_includedReferences_7_0 = null;

        EObject lv_typeFilter_8_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:163:2: ( (otherlv_0= 'dataset' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'using' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( (lv_attributeFilter_5_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_6_0= ruleInstancesFilter ) )? ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' ) )
            // InternalPapin.g:164:2: (otherlv_0= 'dataset' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'using' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( (lv_attributeFilter_5_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_6_0= ruleInstancesFilter ) )? ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )
            {
            // InternalPapin.g:164:2: (otherlv_0= 'dataset' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'using' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( (lv_attributeFilter_5_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_6_0= ruleInstancesFilter ) )? ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}' )
            // InternalPapin.g:165:3: otherlv_0= 'dataset' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'using' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( (lv_attributeFilter_5_0= ruleAttributeFilter ) )? ( (lv_instancesFilter_6_0= ruleInstancesFilter ) )? ( (lv_includedReferences_7_0= ruleIncludedReference ) )* ( (lv_typeFilter_8_0= ruleTypeFilter ) )? otherlv_9= '}'
            {
            otherlv_0=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getDatasetAccess().getDatasetKeyword_0());
            		
            // InternalPapin.g:169:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalPapin.g:170:4: (lv_name_1_0= RULE_ID )
            {
            // InternalPapin.g:170:4: (lv_name_1_0= RULE_ID )
            // InternalPapin.g:171:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_1_0, grammarAccess.getDatasetAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDatasetRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getDatasetAccess().getUsingKeyword_2());
            		
            // InternalPapin.g:191:3: ( ( ruleQualifiedName ) )
            // InternalPapin.g:192:4: ( ruleQualifiedName )
            {
            // InternalPapin.g:192:4: ( ruleQualifiedName )
            // InternalPapin.g:193:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDatasetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getDatasetAccess().getUsingEffectCrossReference_3_0());
            				
            pushFollow(FOLLOW_9);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_10); 

            			newLeafNode(otherlv_4, grammarAccess.getDatasetAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalPapin.g:211:3: ( (lv_attributeFilter_5_0= ruleAttributeFilter ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==29) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalPapin.g:212:4: (lv_attributeFilter_5_0= ruleAttributeFilter )
                    {
                    // InternalPapin.g:212:4: (lv_attributeFilter_5_0= ruleAttributeFilter )
                    // InternalPapin.g:213:5: lv_attributeFilter_5_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getDatasetAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_11);
                    lv_attributeFilter_5_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDatasetRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_5_0,
                    						"es.unican.Papin.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPapin.g:230:3: ( (lv_instancesFilter_6_0= ruleInstancesFilter ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==32) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPapin.g:231:4: (lv_instancesFilter_6_0= ruleInstancesFilter )
                    {
                    // InternalPapin.g:231:4: (lv_instancesFilter_6_0= ruleInstancesFilter )
                    // InternalPapin.g:232:5: lv_instancesFilter_6_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getDatasetAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0());
                    				
                    pushFollow(FOLLOW_12);
                    lv_instancesFilter_6_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDatasetRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_6_0,
                    						"es.unican.Papin.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPapin.g:249:3: ( (lv_includedReferences_7_0= ruleIncludedReference ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==17||LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPapin.g:250:4: (lv_includedReferences_7_0= ruleIncludedReference )
            	    {
            	    // InternalPapin.g:250:4: (lv_includedReferences_7_0= ruleIncludedReference )
            	    // InternalPapin.g:251:5: lv_includedReferences_7_0= ruleIncludedReference
            	    {

            	    					newCompositeNode(grammarAccess.getDatasetAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_includedReferences_7_0=ruleIncludedReference();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDatasetRule());
            	    					}
            	    					add(
            	    						current,
            	    						"includedReferences",
            	    						lv_includedReferences_7_0,
            	    						"es.unican.Papin.IncludedReference");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalPapin.g:268:3: ( (lv_typeFilter_8_0= ruleTypeFilter ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20||LA7_0==28) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalPapin.g:269:4: (lv_typeFilter_8_0= ruleTypeFilter )
                    {
                    // InternalPapin.g:269:4: (lv_typeFilter_8_0= ruleTypeFilter )
                    // InternalPapin.g:270:5: lv_typeFilter_8_0= ruleTypeFilter
                    {

                    					newCompositeNode(grammarAccess.getDatasetAccess().getTypeFilterTypeFilterParserRuleCall_8_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_typeFilter_8_0=ruleTypeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDatasetRule());
                    					}
                    					set(
                    						current,
                    						"typeFilter",
                    						lv_typeFilter_8_0,
                    						"es.unican.Papin.TypeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getDatasetAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataset"


    // $ANTLR start "entryRuleIncludedReference"
    // InternalPapin.g:295:1: entryRuleIncludedReference returns [EObject current=null] : iv_ruleIncludedReference= ruleIncludedReference EOF ;
    public final EObject entryRuleIncludedReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIncludedReference = null;


        try {
            // InternalPapin.g:295:58: (iv_ruleIncludedReference= ruleIncludedReference EOF )
            // InternalPapin.g:296:2: iv_ruleIncludedReference= ruleIncludedReference EOF
            {
             newCompositeNode(grammarAccess.getIncludedReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIncludedReference=ruleIncludedReference();

            state._fsp--;

             current =iv_ruleIncludedReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIncludedReference"


    // $ANTLR start "ruleIncludedReference"
    // InternalPapin.g:302:1: ruleIncludedReference returns [EObject current=null] : (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference ) ;
    public final EObject ruleIncludedReference() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleReference_0 = null;

        EObject this_AggregatedReference_1 = null;



        	enterRule();

        try {
            // InternalPapin.g:308:2: ( (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference ) )
            // InternalPapin.g:309:2: (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference )
            {
            // InternalPapin.g:309:2: (this_SimpleReference_0= ruleSimpleReference | this_AggregatedReference_1= ruleAggregatedReference )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==17) ) {
                alt8=1;
            }
            else if ( (LA8_0==19) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalPapin.g:310:3: this_SimpleReference_0= ruleSimpleReference
                    {

                    			newCompositeNode(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleReference_0=ruleSimpleReference();

                    state._fsp--;


                    			current = this_SimpleReference_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPapin.g:319:3: this_AggregatedReference_1= ruleAggregatedReference
                    {

                    			newCompositeNode(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AggregatedReference_1=ruleAggregatedReference();

                    state._fsp--;


                    			current = this_AggregatedReference_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIncludedReference"


    // $ANTLR start "entryRuleSimpleReference"
    // InternalPapin.g:331:1: entryRuleSimpleReference returns [EObject current=null] : iv_ruleSimpleReference= ruleSimpleReference EOF ;
    public final EObject entryRuleSimpleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleReference = null;


        try {
            // InternalPapin.g:331:56: (iv_ruleSimpleReference= ruleSimpleReference EOF )
            // InternalPapin.g:332:2: iv_ruleSimpleReference= ruleSimpleReference EOF
            {
             newCompositeNode(grammarAccess.getSimpleReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleReference=ruleSimpleReference();

            state._fsp--;

             current =iv_ruleSimpleReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleReference"


    // $ANTLR start "ruleSimpleReference"
    // InternalPapin.g:338:1: ruleSimpleReference returns [EObject current=null] : (otherlv_0= 'include' ( ( ruleQualifiedName ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? (otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )? )? (otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}' )? ) ;
    public final EObject ruleSimpleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_attributeFilter_2_0 = null;

        EObject lv_pivotingId_4_0 = null;

        EObject lv_instancesFilter_5_0 = null;

        EObject lv_typeFilter_9_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:344:2: ( (otherlv_0= 'include' ( ( ruleQualifiedName ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? (otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )? )? (otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}' )? ) )
            // InternalPapin.g:345:2: (otherlv_0= 'include' ( ( ruleQualifiedName ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? (otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )? )? (otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}' )? )
            {
            // InternalPapin.g:345:2: (otherlv_0= 'include' ( ( ruleQualifiedName ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? (otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )? )? (otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}' )? )
            // InternalPapin.g:346:3: otherlv_0= 'include' ( ( ruleQualifiedName ) ) ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )? (otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )? )? (otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}' )?
            {
            otherlv_0=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_0());
            		
            // InternalPapin.g:350:3: ( ( ruleQualifiedName ) )
            // InternalPapin.g:351:4: ( ruleQualifiedName )
            {
            // InternalPapin.g:351:4: ( ruleQualifiedName )
            // InternalPapin.g:352:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleReferenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSimpleReferenceAccess().getCatCategoryCrossReference_1_0());
            				
            pushFollow(FOLLOW_14);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPapin.g:366:3: ( (lv_attributeFilter_2_0= ruleAttributeFilter ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==29) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalPapin.g:367:4: (lv_attributeFilter_2_0= ruleAttributeFilter )
                    {
                    // InternalPapin.g:367:4: (lv_attributeFilter_2_0= ruleAttributeFilter )
                    // InternalPapin.g:368:5: lv_attributeFilter_2_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_15);
                    lv_attributeFilter_2_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_2_0,
                    						"es.unican.Papin.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPapin.g:385:3: (otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )? )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==18) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalPapin.g:386:4: otherlv_3= 'by' ( (lv_pivotingId_4_0= rulePath ) ) ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )?
                    {
                    otherlv_3=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_3, grammarAccess.getSimpleReferenceAccess().getByKeyword_3_0());
                    			
                    // InternalPapin.g:390:4: ( (lv_pivotingId_4_0= rulePath ) )
                    // InternalPapin.g:391:5: (lv_pivotingId_4_0= rulePath )
                    {
                    // InternalPapin.g:391:5: (lv_pivotingId_4_0= rulePath )
                    // InternalPapin.g:392:6: lv_pivotingId_4_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_16);
                    lv_pivotingId_4_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                    						}
                    						set(
                    							current,
                    							"pivotingId",
                    							lv_pivotingId_4_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPapin.g:409:4: ( (lv_instancesFilter_5_0= ruleInstancesFilter ) )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==32) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalPapin.g:410:5: (lv_instancesFilter_5_0= ruleInstancesFilter )
                            {
                            // InternalPapin.g:410:5: (lv_instancesFilter_5_0= ruleInstancesFilter )
                            // InternalPapin.g:411:6: lv_instancesFilter_5_0= ruleInstancesFilter
                            {

                            						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_3_2_0());
                            					
                            pushFollow(FOLLOW_17);
                            lv_instancesFilter_5_0=ruleInstancesFilter();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            						}
                            						set(
                            							current,
                            							"instancesFilter",
                            							lv_instancesFilter_5_0,
                            							"es.unican.Papin.InstancesFilter");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalPapin.g:429:3: (otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==15) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPapin.g:430:4: otherlv_6= '{' (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )* ( (lv_typeFilter_9_0= ruleTypeFilter ) )? otherlv_10= '}'
                    {
                    otherlv_6=(Token)match(input,15,FOLLOW_18); 

                    				newLeafNode(otherlv_6, grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_4_0());
                    			
                    // InternalPapin.g:434:4: (otherlv_7= 'include' ( ( ruleQualifiedName ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==17) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalPapin.g:435:5: otherlv_7= 'include' ( ( ruleQualifiedName ) )
                    	    {
                    	    otherlv_7=(Token)match(input,17,FOLLOW_7); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_4_1_0());
                    	    				
                    	    // InternalPapin.g:439:5: ( ( ruleQualifiedName ) )
                    	    // InternalPapin.g:440:6: ( ruleQualifiedName )
                    	    {
                    	    // InternalPapin.g:440:6: ( ruleQualifiedName )
                    	    // InternalPapin.g:441:7: ruleQualifiedName
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getSimpleReferenceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getSimpleReferenceAccess().getCausesDataLinkedCauseCrossReference_4_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_18);
                    	    ruleQualifiedName();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    // InternalPapin.g:456:4: ( (lv_typeFilter_9_0= ruleTypeFilter ) )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==20||LA13_0==28) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalPapin.g:457:5: (lv_typeFilter_9_0= ruleTypeFilter )
                            {
                            // InternalPapin.g:457:5: (lv_typeFilter_9_0= ruleTypeFilter )
                            // InternalPapin.g:458:6: lv_typeFilter_9_0= ruleTypeFilter
                            {

                            						newCompositeNode(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_4_2_0());
                            					
                            pushFollow(FOLLOW_13);
                            lv_typeFilter_9_0=ruleTypeFilter();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getSimpleReferenceRule());
                            						}
                            						set(
                            							current,
                            							"typeFilter",
                            							lv_typeFilter_9_0,
                            							"es.unican.Papin.TypeFilter");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,16,FOLLOW_2); 

                    				newLeafNode(otherlv_10, grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_4_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleReference"


    // $ANTLR start "entryRuleAggregatedReference"
    // InternalPapin.g:484:1: entryRuleAggregatedReference returns [EObject current=null] : iv_ruleAggregatedReference= ruleAggregatedReference EOF ;
    public final EObject entryRuleAggregatedReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAggregatedReference = null;


        try {
            // InternalPapin.g:484:60: (iv_ruleAggregatedReference= ruleAggregatedReference EOF )
            // InternalPapin.g:485:2: iv_ruleAggregatedReference= ruleAggregatedReference EOF
            {
             newCompositeNode(grammarAccess.getAggregatedReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggregatedReference=ruleAggregatedReference();

            state._fsp--;

             current =iv_ruleAggregatedReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggregatedReference"


    // $ANTLR start "ruleAggregatedReference"
    // InternalPapin.g:491:1: ruleAggregatedReference returns [EObject current=null] : (otherlv_0= 'calculate' ( ( ruleQualifiedName ) ) ( ( ruleQualifiedName ) )? otherlv_3= 'as' ( (lv_function_4_0= ruleAggFunction ) ) otherlv_5= '(' ( (lv_aggValue_6_0= rulePath ) ) otherlv_7= ')' (otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) ) )? ( (lv_instancesFilter_10_0= ruleInstancesFilter ) )? ) ;
    public final EObject ruleAggregatedReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_function_4_0 = null;

        EObject lv_aggValue_6_0 = null;

        EObject lv_pivotingId_9_0 = null;

        EObject lv_instancesFilter_10_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:497:2: ( (otherlv_0= 'calculate' ( ( ruleQualifiedName ) ) ( ( ruleQualifiedName ) )? otherlv_3= 'as' ( (lv_function_4_0= ruleAggFunction ) ) otherlv_5= '(' ( (lv_aggValue_6_0= rulePath ) ) otherlv_7= ')' (otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) ) )? ( (lv_instancesFilter_10_0= ruleInstancesFilter ) )? ) )
            // InternalPapin.g:498:2: (otherlv_0= 'calculate' ( ( ruleQualifiedName ) ) ( ( ruleQualifiedName ) )? otherlv_3= 'as' ( (lv_function_4_0= ruleAggFunction ) ) otherlv_5= '(' ( (lv_aggValue_6_0= rulePath ) ) otherlv_7= ')' (otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) ) )? ( (lv_instancesFilter_10_0= ruleInstancesFilter ) )? )
            {
            // InternalPapin.g:498:2: (otherlv_0= 'calculate' ( ( ruleQualifiedName ) ) ( ( ruleQualifiedName ) )? otherlv_3= 'as' ( (lv_function_4_0= ruleAggFunction ) ) otherlv_5= '(' ( (lv_aggValue_6_0= rulePath ) ) otherlv_7= ')' (otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) ) )? ( (lv_instancesFilter_10_0= ruleInstancesFilter ) )? )
            // InternalPapin.g:499:3: otherlv_0= 'calculate' ( ( ruleQualifiedName ) ) ( ( ruleQualifiedName ) )? otherlv_3= 'as' ( (lv_function_4_0= ruleAggFunction ) ) otherlv_5= '(' ( (lv_aggValue_6_0= rulePath ) ) otherlv_7= ')' (otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) ) )? ( (lv_instancesFilter_10_0= ruleInstancesFilter ) )?
            {
            otherlv_0=(Token)match(input,19,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0());
            		
            // InternalPapin.g:503:3: ( ( ruleQualifiedName ) )
            // InternalPapin.g:504:4: ( ruleQualifiedName )
            {
            // InternalPapin.g:504:4: ( ruleQualifiedName )
            // InternalPapin.g:505:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAggregatedReferenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getCatCategoryCrossReference_1_0());
            				
            pushFollow(FOLLOW_19);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPapin.g:519:3: ( ( ruleQualifiedName ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_ID) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPapin.g:520:4: ( ruleQualifiedName )
                    {
                    // InternalPapin.g:520:4: ( ruleQualifiedName )
                    // InternalPapin.g:521:5: ruleQualifiedName
                    {

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAggregatedReferenceRule());
                    					}
                    				

                    					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getCausesDataLinkedCauseCrossReference_2_0());
                    				
                    pushFollow(FOLLOW_20);
                    ruleQualifiedName();

                    state._fsp--;


                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,20,FOLLOW_21); 

            			newLeafNode(otherlv_3, grammarAccess.getAggregatedReferenceAccess().getAsKeyword_3());
            		
            // InternalPapin.g:539:3: ( (lv_function_4_0= ruleAggFunction ) )
            // InternalPapin.g:540:4: (lv_function_4_0= ruleAggFunction )
            {
            // InternalPapin.g:540:4: (lv_function_4_0= ruleAggFunction )
            // InternalPapin.g:541:5: lv_function_4_0= ruleAggFunction
            {

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_22);
            lv_function_4_0=ruleAggFunction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
            					}
            					set(
            						current,
            						"function",
            						lv_function_4_0,
            						"es.unican.Papin.AggFunction");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,21,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_5());
            		
            // InternalPapin.g:562:3: ( (lv_aggValue_6_0= rulePath ) )
            // InternalPapin.g:563:4: (lv_aggValue_6_0= rulePath )
            {
            // InternalPapin.g:563:4: (lv_aggValue_6_0= rulePath )
            // InternalPapin.g:564:5: lv_aggValue_6_0= rulePath
            {

            					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_23);
            lv_aggValue_6_0=rulePath();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
            					}
            					set(
            						current,
            						"aggValue",
            						lv_aggValue_6_0,
            						"es.unican.Papin.Path");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,22,FOLLOW_24); 

            			newLeafNode(otherlv_7, grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_7());
            		
            // InternalPapin.g:585:3: (otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==18) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalPapin.g:586:4: otherlv_8= 'by' ( (lv_pivotingId_9_0= rulePath ) )
                    {
                    otherlv_8=(Token)match(input,18,FOLLOW_7); 

                    				newLeafNode(otherlv_8, grammarAccess.getAggregatedReferenceAccess().getByKeyword_8_0());
                    			
                    // InternalPapin.g:590:4: ( (lv_pivotingId_9_0= rulePath ) )
                    // InternalPapin.g:591:5: (lv_pivotingId_9_0= rulePath )
                    {
                    // InternalPapin.g:591:5: (lv_pivotingId_9_0= rulePath )
                    // InternalPapin.g:592:6: lv_pivotingId_9_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_25);
                    lv_pivotingId_9_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
                    						}
                    						set(
                    							current,
                    							"pivotingId",
                    							lv_pivotingId_9_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPapin.g:610:3: ( (lv_instancesFilter_10_0= ruleInstancesFilter ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==32) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalPapin.g:611:4: (lv_instancesFilter_10_0= ruleInstancesFilter )
                    {
                    // InternalPapin.g:611:4: (lv_instancesFilter_10_0= ruleInstancesFilter )
                    // InternalPapin.g:612:5: lv_instancesFilter_10_0= ruleInstancesFilter
                    {

                    					newCompositeNode(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_9_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_instancesFilter_10_0=ruleInstancesFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAggregatedReferenceRule());
                    					}
                    					set(
                    						current,
                    						"instancesFilter",
                    						lv_instancesFilter_10_0,
                    						"es.unican.Papin.InstancesFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggregatedReference"


    // $ANTLR start "entryRuleAggFunction"
    // InternalPapin.g:633:1: entryRuleAggFunction returns [String current=null] : iv_ruleAggFunction= ruleAggFunction EOF ;
    public final String entryRuleAggFunction() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAggFunction = null;


        try {
            // InternalPapin.g:633:51: (iv_ruleAggFunction= ruleAggFunction EOF )
            // InternalPapin.g:634:2: iv_ruleAggFunction= ruleAggFunction EOF
            {
             newCompositeNode(grammarAccess.getAggFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggFunction=ruleAggFunction();

            state._fsp--;

             current =iv_ruleAggFunction.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggFunction"


    // $ANTLR start "ruleAggFunction"
    // InternalPapin.g:640:1: ruleAggFunction returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' ) ;
    public final AntlrDatatypeRuleToken ruleAggFunction() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPapin.g:646:2: ( (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' ) )
            // InternalPapin.g:647:2: (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' )
            {
            // InternalPapin.g:647:2: (kw= 'count' | kw= 'sum' | kw= 'avg' | kw= 'max' | kw= 'min' )
            int alt18=5;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt18=1;
                }
                break;
            case 24:
                {
                alt18=2;
                }
                break;
            case 25:
                {
                alt18=3;
                }
                break;
            case 26:
                {
                alt18=4;
                }
                break;
            case 27:
                {
                alt18=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalPapin.g:648:3: kw= 'count'
                    {
                    kw=(Token)match(input,23,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getCountKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPapin.g:654:3: kw= 'sum'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getSumKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalPapin.g:660:3: kw= 'avg'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getAvgKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalPapin.g:666:3: kw= 'max'
                    {
                    kw=(Token)match(input,26,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getMaxKeyword_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalPapin.g:672:3: kw= 'min'
                    {
                    kw=(Token)match(input,27,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAggFunctionAccess().getMinKeyword_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggFunction"


    // $ANTLR start "entryRuleTypeFilter"
    // InternalPapin.g:681:1: entryRuleTypeFilter returns [EObject current=null] : iv_ruleTypeFilter= ruleTypeFilter EOF ;
    public final EObject entryRuleTypeFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeFilter = null;


        try {
            // InternalPapin.g:681:51: (iv_ruleTypeFilter= ruleTypeFilter EOF )
            // InternalPapin.g:682:2: iv_ruleTypeFilter= ruleTypeFilter EOF
            {
             newCompositeNode(grammarAccess.getTypeFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeFilter=ruleTypeFilter();

            state._fsp--;

             current =iv_ruleTypeFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeFilter"


    // $ANTLR start "ruleTypeFilter"
    // InternalPapin.g:688:1: ruleTypeFilter returns [EObject current=null] : (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection ) ;
    public final EObject ruleTypeFilter() throws RecognitionException {
        EObject current = null;

        EObject this_TypeCompletion_0 = null;

        EObject this_TypeSelection_1 = null;



        	enterRule();

        try {
            // InternalPapin.g:694:2: ( (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection ) )
            // InternalPapin.g:695:2: (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection )
            {
            // InternalPapin.g:695:2: (this_TypeCompletion_0= ruleTypeCompletion | this_TypeSelection_1= ruleTypeSelection )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==20) ) {
                alt19=1;
            }
            else if ( (LA19_0==28) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalPapin.g:696:3: this_TypeCompletion_0= ruleTypeCompletion
                    {

                    			newCompositeNode(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_TypeCompletion_0=ruleTypeCompletion();

                    state._fsp--;


                    			current = this_TypeCompletion_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPapin.g:705:3: this_TypeSelection_1= ruleTypeSelection
                    {

                    			newCompositeNode(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_TypeSelection_1=ruleTypeSelection();

                    state._fsp--;


                    			current = this_TypeSelection_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeFilter"


    // $ANTLR start "entryRuleTypeCompletion"
    // InternalPapin.g:717:1: entryRuleTypeCompletion returns [EObject current=null] : iv_ruleTypeCompletion= ruleTypeCompletion EOF ;
    public final EObject entryRuleTypeCompletion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeCompletion = null;


        try {
            // InternalPapin.g:717:55: (iv_ruleTypeCompletion= ruleTypeCompletion EOF )
            // InternalPapin.g:718:2: iv_ruleTypeCompletion= ruleTypeCompletion EOF
            {
             newCompositeNode(grammarAccess.getTypeCompletionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeCompletion=ruleTypeCompletion();

            state._fsp--;

             current =iv_ruleTypeCompletion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeCompletion"


    // $ANTLR start "ruleTypeCompletion"
    // InternalPapin.g:724:1: ruleTypeCompletion returns [EObject current=null] : (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ ;
    public final EObject ruleTypeCompletion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeCustomizations_1_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:730:2: ( (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ )
            // InternalPapin.g:731:2: (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            {
            // InternalPapin.g:731:2: (otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            int cnt20=0;
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==20) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalPapin.g:732:3: otherlv_0= 'as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    {
            	    otherlv_0=(Token)match(input,20,FOLLOW_7); 

            	    			newLeafNode(otherlv_0, grammarAccess.getTypeCompletionAccess().getAsKeyword_0());
            	    		
            	    // InternalPapin.g:736:3: ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    // InternalPapin.g:737:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    {
            	    // InternalPapin.g:737:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    // InternalPapin.g:738:5: lv_typeCustomizations_1_0= ruleTypeCustomization
            	    {

            	    					newCompositeNode(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_26);
            	    lv_typeCustomizations_1_0=ruleTypeCustomization();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeCompletionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"typeCustomizations",
            	    						lv_typeCustomizations_1_0,
            	    						"es.unican.Papin.TypeCustomization");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt20 >= 1 ) break loop20;
                        EarlyExitException eee =
                            new EarlyExitException(20, input);
                        throw eee;
                }
                cnt20++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeCompletion"


    // $ANTLR start "entryRuleTypeSelection"
    // InternalPapin.g:759:1: entryRuleTypeSelection returns [EObject current=null] : iv_ruleTypeSelection= ruleTypeSelection EOF ;
    public final EObject entryRuleTypeSelection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeSelection = null;


        try {
            // InternalPapin.g:759:54: (iv_ruleTypeSelection= ruleTypeSelection EOF )
            // InternalPapin.g:760:2: iv_ruleTypeSelection= ruleTypeSelection EOF
            {
             newCompositeNode(grammarAccess.getTypeSelectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeSelection=ruleTypeSelection();

            state._fsp--;

             current =iv_ruleTypeSelection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeSelection"


    // $ANTLR start "ruleTypeSelection"
    // InternalPapin.g:766:1: ruleTypeSelection returns [EObject current=null] : (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ ;
    public final EObject ruleTypeSelection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeCustomizations_1_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:772:2: ( (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+ )
            // InternalPapin.g:773:2: (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            {
            // InternalPapin.g:773:2: (otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) ) )+
            int cnt21=0;
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==28) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalPapin.g:774:3: otherlv_0= 'only_as' ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    {
            	    otherlv_0=(Token)match(input,28,FOLLOW_7); 

            	    			newLeafNode(otherlv_0, grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0());
            	    		
            	    // InternalPapin.g:778:3: ( (lv_typeCustomizations_1_0= ruleTypeCustomization ) )
            	    // InternalPapin.g:779:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    {
            	    // InternalPapin.g:779:4: (lv_typeCustomizations_1_0= ruleTypeCustomization )
            	    // InternalPapin.g:780:5: lv_typeCustomizations_1_0= ruleTypeCustomization
            	    {

            	    					newCompositeNode(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_27);
            	    lv_typeCustomizations_1_0=ruleTypeCustomization();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTypeSelectionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"typeCustomizations",
            	    						lv_typeCustomizations_1_0,
            	    						"es.unican.Papin.TypeCustomization");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt21 >= 1 ) break loop21;
                        EarlyExitException eee =
                            new EarlyExitException(21, input);
                        throw eee;
                }
                cnt21++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeSelection"


    // $ANTLR start "entryRuleTypeCustomization"
    // InternalPapin.g:801:1: entryRuleTypeCustomization returns [EObject current=null] : iv_ruleTypeCustomization= ruleTypeCustomization EOF ;
    public final EObject entryRuleTypeCustomization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeCustomization = null;


        try {
            // InternalPapin.g:801:58: (iv_ruleTypeCustomization= ruleTypeCustomization EOF )
            // InternalPapin.g:802:2: iv_ruleTypeCustomization= ruleTypeCustomization EOF
            {
             newCompositeNode(grammarAccess.getTypeCustomizationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeCustomization=ruleTypeCustomization();

            state._fsp--;

             current =iv_ruleTypeCustomization; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeCustomization"


    // $ANTLR start "ruleTypeCustomization"
    // InternalPapin.g:808:1: ruleTypeCustomization returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? ) ;
    public final EObject ruleTypeCustomization() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_attributeFilter_1_0 = null;

        EObject lv_includedReferences_3_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:814:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? ) )
            // InternalPapin.g:815:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? )
            {
            // InternalPapin.g:815:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )? )
            // InternalPapin.g:816:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )? (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )?
            {
            // InternalPapin.g:816:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalPapin.g:817:4: (lv_name_0_0= RULE_ID )
            {
            // InternalPapin.g:817:4: (lv_name_0_0= RULE_ID )
            // InternalPapin.g:818:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_28); 

            					newLeafNode(lv_name_0_0, grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTypeCustomizationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalPapin.g:834:3: ( (lv_attributeFilter_1_0= ruleAttributeFilter ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==29) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalPapin.g:835:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    {
                    // InternalPapin.g:835:4: (lv_attributeFilter_1_0= ruleAttributeFilter )
                    // InternalPapin.g:836:5: lv_attributeFilter_1_0= ruleAttributeFilter
                    {

                    					newCompositeNode(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_17);
                    lv_attributeFilter_1_0=ruleAttributeFilter();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTypeCustomizationRule());
                    					}
                    					set(
                    						current,
                    						"attributeFilter",
                    						lv_attributeFilter_1_0,
                    						"es.unican.Papin.AttributeFilter");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalPapin.g:853:3: (otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}' )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==15) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalPapin.g:854:4: otherlv_2= '{' ( (lv_includedReferences_3_0= ruleIncludedReference ) )* otherlv_4= '}'
                    {
                    otherlv_2=(Token)match(input,15,FOLLOW_29); 

                    				newLeafNode(otherlv_2, grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0());
                    			
                    // InternalPapin.g:858:4: ( (lv_includedReferences_3_0= ruleIncludedReference ) )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==17||LA23_0==19) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // InternalPapin.g:859:5: (lv_includedReferences_3_0= ruleIncludedReference )
                    	    {
                    	    // InternalPapin.g:859:5: (lv_includedReferences_3_0= ruleIncludedReference )
                    	    // InternalPapin.g:860:6: lv_includedReferences_3_0= ruleIncludedReference
                    	    {

                    	    						newCompositeNode(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_29);
                    	    lv_includedReferences_3_0=ruleIncludedReference();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getTypeCustomizationRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"includedReferences",
                    	    							lv_includedReferences_3_0,
                    	    							"es.unican.Papin.IncludedReference");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,16,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeCustomization"


    // $ANTLR start "entryRuleAttributeFilter"
    // InternalPapin.g:886:1: entryRuleAttributeFilter returns [EObject current=null] : iv_ruleAttributeFilter= ruleAttributeFilter EOF ;
    public final EObject entryRuleAttributeFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeFilter = null;


        try {
            // InternalPapin.g:886:56: (iv_ruleAttributeFilter= ruleAttributeFilter EOF )
            // InternalPapin.g:887:2: iv_ruleAttributeFilter= ruleAttributeFilter EOF
            {
             newCompositeNode(grammarAccess.getAttributeFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeFilter=ruleAttributeFilter();

            state._fsp--;

             current =iv_ruleAttributeFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeFilter"


    // $ANTLR start "ruleAttributeFilter"
    // InternalPapin.g:893:1: ruleAttributeFilter returns [EObject current=null] : (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' ) ;
    public final EObject ruleAttributeFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_attributes_1_0=null;
        Token otherlv_2=null;
        Token lv_attributes_3_0=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalPapin.g:899:2: ( (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' ) )
            // InternalPapin.g:900:2: (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' )
            {
            // InternalPapin.g:900:2: (otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']' )
            // InternalPapin.g:901:3: otherlv_0= '[' ( (lv_attributes_1_0= RULE_ID ) ) (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )* otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalPapin.g:905:3: ( (lv_attributes_1_0= RULE_ID ) )
            // InternalPapin.g:906:4: (lv_attributes_1_0= RULE_ID )
            {
            // InternalPapin.g:906:4: (lv_attributes_1_0= RULE_ID )
            // InternalPapin.g:907:5: lv_attributes_1_0= RULE_ID
            {
            lv_attributes_1_0=(Token)match(input,RULE_ID,FOLLOW_30); 

            					newLeafNode(lv_attributes_1_0, grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeFilterRule());
            					}
            					addWithLastConsumed(
            						current,
            						"attributes",
            						lv_attributes_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalPapin.g:923:3: (otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==30) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalPapin.g:924:4: otherlv_2= ',' ( (lv_attributes_3_0= RULE_ID ) )
            	    {
            	    otherlv_2=(Token)match(input,30,FOLLOW_7); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_0());
            	    			
            	    // InternalPapin.g:928:4: ( (lv_attributes_3_0= RULE_ID ) )
            	    // InternalPapin.g:929:5: (lv_attributes_3_0= RULE_ID )
            	    {
            	    // InternalPapin.g:929:5: (lv_attributes_3_0= RULE_ID )
            	    // InternalPapin.g:930:6: lv_attributes_3_0= RULE_ID
            	    {
            	    lv_attributes_3_0=(Token)match(input,RULE_ID,FOLLOW_30); 

            	    						newLeafNode(lv_attributes_3_0, grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getAttributeFilterRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"attributes",
            	    							lv_attributes_3_0,
            	    							"org.eclipse.xtext.common.Terminals.ID");
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_4=(Token)match(input,31,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeFilter"


    // $ANTLR start "entryRuleInstancesFilter"
    // InternalPapin.g:955:1: entryRuleInstancesFilter returns [EObject current=null] : iv_ruleInstancesFilter= ruleInstancesFilter EOF ;
    public final EObject entryRuleInstancesFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstancesFilter = null;


        try {
            // InternalPapin.g:955:56: (iv_ruleInstancesFilter= ruleInstancesFilter EOF )
            // InternalPapin.g:956:2: iv_ruleInstancesFilter= ruleInstancesFilter EOF
            {
             newCompositeNode(grammarAccess.getInstancesFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstancesFilter=ruleInstancesFilter();

            state._fsp--;

             current =iv_ruleInstancesFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstancesFilter"


    // $ANTLR start "ruleInstancesFilter"
    // InternalPapin.g:962:1: ruleInstancesFilter returns [EObject current=null] : (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction ) ;
    public final EObject ruleInstancesFilter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_AndConjunction_1 = null;



        	enterRule();

        try {
            // InternalPapin.g:968:2: ( (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction ) )
            // InternalPapin.g:969:2: (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction )
            {
            // InternalPapin.g:969:2: (otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction )
            // InternalPapin.g:970:3: otherlv_0= 'where' this_AndConjunction_1= ruleAndConjunction
            {
            otherlv_0=(Token)match(input,32,FOLLOW_31); 

            			newLeafNode(otherlv_0, grammarAccess.getInstancesFilterAccess().getWhereKeyword_0());
            		

            			newCompositeNode(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1());
            		
            pushFollow(FOLLOW_2);
            this_AndConjunction_1=ruleAndConjunction();

            state._fsp--;


            			current = this_AndConjunction_1;
            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstancesFilter"


    // $ANTLR start "entryRuleAndConjunction"
    // InternalPapin.g:986:1: entryRuleAndConjunction returns [EObject current=null] : iv_ruleAndConjunction= ruleAndConjunction EOF ;
    public final EObject entryRuleAndConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndConjunction = null;


        try {
            // InternalPapin.g:986:55: (iv_ruleAndConjunction= ruleAndConjunction EOF )
            // InternalPapin.g:987:2: iv_ruleAndConjunction= ruleAndConjunction EOF
            {
             newCompositeNode(grammarAccess.getAndConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAndConjunction=ruleAndConjunction();

            state._fsp--;

             current =iv_ruleAndConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndConjunction"


    // $ANTLR start "ruleAndConjunction"
    // InternalPapin.g:993:1: ruleAndConjunction returns [EObject current=null] : (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* ) ;
    public final EObject ruleAndConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_OrConjunction_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:999:2: ( (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* ) )
            // InternalPapin.g:1000:2: (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* )
            {
            // InternalPapin.g:1000:2: (this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )* )
            // InternalPapin.g:1001:3: this_OrConjunction_0= ruleOrConjunction ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )*
            {

            			newCompositeNode(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0());
            		
            pushFollow(FOLLOW_32);
            this_OrConjunction_0=ruleOrConjunction();

            state._fsp--;


            			current = this_OrConjunction_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPapin.g:1009:3: ( () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==33) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalPapin.g:1010:4: () otherlv_2= 'and' ( (lv_right_3_0= ruleOrConjunction ) )
            	    {
            	    // InternalPapin.g:1010:4: ()
            	    // InternalPapin.g:1011:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,33,FOLLOW_31); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1());
            	    			
            	    // InternalPapin.g:1021:4: ( (lv_right_3_0= ruleOrConjunction ) )
            	    // InternalPapin.g:1022:5: (lv_right_3_0= ruleOrConjunction )
            	    {
            	    // InternalPapin.g:1022:5: (lv_right_3_0= ruleOrConjunction )
            	    // InternalPapin.g:1023:6: lv_right_3_0= ruleOrConjunction
            	    {

            	    						newCompositeNode(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_32);
            	    lv_right_3_0=ruleOrConjunction();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAndConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"es.unican.Papin.OrConjunction");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndConjunction"


    // $ANTLR start "entryRuleOrConjunction"
    // InternalPapin.g:1045:1: entryRuleOrConjunction returns [EObject current=null] : iv_ruleOrConjunction= ruleOrConjunction EOF ;
    public final EObject entryRuleOrConjunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrConjunction = null;


        try {
            // InternalPapin.g:1045:54: (iv_ruleOrConjunction= ruleOrConjunction EOF )
            // InternalPapin.g:1046:2: iv_ruleOrConjunction= ruleOrConjunction EOF
            {
             newCompositeNode(grammarAccess.getOrConjunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrConjunction=ruleOrConjunction();

            state._fsp--;

             current =iv_ruleOrConjunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrConjunction"


    // $ANTLR start "ruleOrConjunction"
    // InternalPapin.g:1052:1: ruleOrConjunction returns [EObject current=null] : (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* ) ;
    public final EObject ruleOrConjunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Primary_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:1058:2: ( (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* ) )
            // InternalPapin.g:1059:2: (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* )
            {
            // InternalPapin.g:1059:2: (this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )* )
            // InternalPapin.g:1060:3: this_Primary_0= rulePrimary ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )*
            {

            			newCompositeNode(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0());
            		
            pushFollow(FOLLOW_33);
            this_Primary_0=rulePrimary();

            state._fsp--;


            			current = this_Primary_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalPapin.g:1068:3: ( () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==34) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalPapin.g:1069:4: () otherlv_2= 'or' ( (lv_right_3_0= rulePrimary ) )
            	    {
            	    // InternalPapin.g:1069:4: ()
            	    // InternalPapin.g:1070:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0(),
            	    						current);
            	    				

            	    }

            	    otherlv_2=(Token)match(input,34,FOLLOW_31); 

            	    				newLeafNode(otherlv_2, grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1());
            	    			
            	    // InternalPapin.g:1080:4: ( (lv_right_3_0= rulePrimary ) )
            	    // InternalPapin.g:1081:5: (lv_right_3_0= rulePrimary )
            	    {
            	    // InternalPapin.g:1081:5: (lv_right_3_0= rulePrimary )
            	    // InternalPapin.g:1082:6: lv_right_3_0= rulePrimary
            	    {

            	    						newCompositeNode(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_33);
            	    lv_right_3_0=rulePrimary();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getOrConjunctionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"right",
            	    							lv_right_3_0,
            	    							"es.unican.Papin.Primary");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrConjunction"


    // $ANTLR start "entryRulePrimary"
    // InternalPapin.g:1104:1: entryRulePrimary returns [EObject current=null] : iv_rulePrimary= rulePrimary EOF ;
    public final EObject entryRulePrimary() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimary = null;


        try {
            // InternalPapin.g:1104:48: (iv_rulePrimary= rulePrimary EOF )
            // InternalPapin.g:1105:2: iv_rulePrimary= rulePrimary EOF
            {
             newCompositeNode(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrimary=rulePrimary();

            state._fsp--;

             current =iv_rulePrimary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalPapin.g:1111:1: rulePrimary returns [EObject current=null] : (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) ) ;
    public final EObject rulePrimary() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Comparison_0 = null;

        EObject this_AndConjunction_2 = null;



        	enterRule();

        try {
            // InternalPapin.g:1117:2: ( (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) ) )
            // InternalPapin.g:1118:2: (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) )
            {
            // InternalPapin.g:1118:2: (this_Comparison_0= ruleComparison | (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_ID) ) {
                alt28=1;
            }
            else if ( (LA28_0==21) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalPapin.g:1119:3: this_Comparison_0= ruleComparison
                    {

                    			newCompositeNode(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Comparison_0=ruleComparison();

                    state._fsp--;


                    			current = this_Comparison_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPapin.g:1128:3: (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' )
                    {
                    // InternalPapin.g:1128:3: (otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')' )
                    // InternalPapin.g:1129:4: otherlv_1= '(' this_AndConjunction_2= ruleAndConjunction otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_31); 

                    				newLeafNode(otherlv_1, grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0());
                    			

                    				newCompositeNode(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1());
                    			
                    pushFollow(FOLLOW_23);
                    this_AndConjunction_2=ruleAndConjunction();

                    state._fsp--;


                    				current = this_AndConjunction_2;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_3=(Token)match(input,22,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleComparison"
    // InternalPapin.g:1150:1: entryRuleComparison returns [EObject current=null] : iv_ruleComparison= ruleComparison EOF ;
    public final EObject entryRuleComparison() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparison = null;


        try {
            // InternalPapin.g:1150:51: (iv_ruleComparison= ruleComparison EOF )
            // InternalPapin.g:1151:2: iv_ruleComparison= ruleComparison EOF
            {
             newCompositeNode(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComparison=ruleComparison();

            state._fsp--;

             current =iv_ruleComparison; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalPapin.g:1157:1: ruleComparison returns [EObject current=null] : ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) ) ;
    public final EObject ruleComparison() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_6=null;
        Token lv_value_7_0=null;
        Token otherlv_10=null;
        Token lv_value_11_0=null;
        Token otherlv_14=null;
        Token lv_value_15_0=null;
        Token otherlv_18=null;
        Token lv_value_19_0=null;
        Token otherlv_22=null;
        Token lv_value_23_0=null;
        EObject lv_path_1_0 = null;

        EObject lv_path_5_0 = null;

        EObject lv_path_9_0 = null;

        EObject lv_path_13_0 = null;

        EObject lv_path_17_0 = null;

        EObject lv_path_21_0 = null;



        	enterRule();

        try {
            // InternalPapin.g:1163:2: ( ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) ) )
            // InternalPapin.g:1164:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )
            {
            // InternalPapin.g:1164:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )
            int alt29=6;
            alt29 = dfa29.predict(input);
            switch (alt29) {
                case 1 :
                    // InternalPapin.g:1165:3: ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
                    {
                    // InternalPapin.g:1165:3: ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) )
                    // InternalPapin.g:1166:4: () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) )
                    {
                    // InternalPapin.g:1166:4: ()
                    // InternalPapin.g:1167:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getEqualityAction_0_0(),
                    						current);
                    				

                    }

                    // InternalPapin.g:1173:4: ( (lv_path_1_0= rulePath ) )
                    // InternalPapin.g:1174:5: (lv_path_1_0= rulePath )
                    {
                    // InternalPapin.g:1174:5: (lv_path_1_0= rulePath )
                    // InternalPapin.g:1175:6: lv_path_1_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_34);
                    lv_path_1_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_1_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,35,FOLLOW_35); 

                    				newLeafNode(otherlv_2, grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2());
                    			
                    // InternalPapin.g:1196:4: ( (lv_value_3_0= RULE_STRING ) )
                    // InternalPapin.g:1197:5: (lv_value_3_0= RULE_STRING )
                    {
                    // InternalPapin.g:1197:5: (lv_value_3_0= RULE_STRING )
                    // InternalPapin.g:1198:6: lv_value_3_0= RULE_STRING
                    {
                    lv_value_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPapin.g:1216:3: ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) )
                    {
                    // InternalPapin.g:1216:3: ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) )
                    // InternalPapin.g:1217:4: () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) )
                    {
                    // InternalPapin.g:1217:4: ()
                    // InternalPapin.g:1218:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getInequalityAction_1_0(),
                    						current);
                    				

                    }

                    // InternalPapin.g:1224:4: ( (lv_path_5_0= rulePath ) )
                    // InternalPapin.g:1225:5: (lv_path_5_0= rulePath )
                    {
                    // InternalPapin.g:1225:5: (lv_path_5_0= rulePath )
                    // InternalPapin.g:1226:6: lv_path_5_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_36);
                    lv_path_5_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_5_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,36,FOLLOW_35); 

                    				newLeafNode(otherlv_6, grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2());
                    			
                    // InternalPapin.g:1247:4: ( (lv_value_7_0= RULE_STRING ) )
                    // InternalPapin.g:1248:5: (lv_value_7_0= RULE_STRING )
                    {
                    // InternalPapin.g:1248:5: (lv_value_7_0= RULE_STRING )
                    // InternalPapin.g:1249:6: lv_value_7_0= RULE_STRING
                    {
                    lv_value_7_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_7_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPapin.g:1267:3: ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) )
                    {
                    // InternalPapin.g:1267:3: ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) )
                    // InternalPapin.g:1268:4: () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) )
                    {
                    // InternalPapin.g:1268:4: ()
                    // InternalPapin.g:1269:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getMoreThanAction_2_0(),
                    						current);
                    				

                    }

                    // InternalPapin.g:1275:4: ( (lv_path_9_0= rulePath ) )
                    // InternalPapin.g:1276:5: (lv_path_9_0= rulePath )
                    {
                    // InternalPapin.g:1276:5: (lv_path_9_0= rulePath )
                    // InternalPapin.g:1277:6: lv_path_9_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_37);
                    lv_path_9_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_9_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_10=(Token)match(input,37,FOLLOW_35); 

                    				newLeafNode(otherlv_10, grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2());
                    			
                    // InternalPapin.g:1298:4: ( (lv_value_11_0= RULE_STRING ) )
                    // InternalPapin.g:1299:5: (lv_value_11_0= RULE_STRING )
                    {
                    // InternalPapin.g:1299:5: (lv_value_11_0= RULE_STRING )
                    // InternalPapin.g:1300:6: lv_value_11_0= RULE_STRING
                    {
                    lv_value_11_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_11_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_11_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalPapin.g:1318:3: ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) )
                    {
                    // InternalPapin.g:1318:3: ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) )
                    // InternalPapin.g:1319:4: () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) )
                    {
                    // InternalPapin.g:1319:4: ()
                    // InternalPapin.g:1320:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0(),
                    						current);
                    				

                    }

                    // InternalPapin.g:1326:4: ( (lv_path_13_0= rulePath ) )
                    // InternalPapin.g:1327:5: (lv_path_13_0= rulePath )
                    {
                    // InternalPapin.g:1327:5: (lv_path_13_0= rulePath )
                    // InternalPapin.g:1328:6: lv_path_13_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_38);
                    lv_path_13_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_13_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_14=(Token)match(input,38,FOLLOW_35); 

                    				newLeafNode(otherlv_14, grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2());
                    			
                    // InternalPapin.g:1349:4: ( (lv_value_15_0= RULE_STRING ) )
                    // InternalPapin.g:1350:5: (lv_value_15_0= RULE_STRING )
                    {
                    // InternalPapin.g:1350:5: (lv_value_15_0= RULE_STRING )
                    // InternalPapin.g:1351:6: lv_value_15_0= RULE_STRING
                    {
                    lv_value_15_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_15_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_15_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalPapin.g:1369:3: ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) )
                    {
                    // InternalPapin.g:1369:3: ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) )
                    // InternalPapin.g:1370:4: () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) )
                    {
                    // InternalPapin.g:1370:4: ()
                    // InternalPapin.g:1371:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getLessThanAction_4_0(),
                    						current);
                    				

                    }

                    // InternalPapin.g:1377:4: ( (lv_path_17_0= rulePath ) )
                    // InternalPapin.g:1378:5: (lv_path_17_0= rulePath )
                    {
                    // InternalPapin.g:1378:5: (lv_path_17_0= rulePath )
                    // InternalPapin.g:1379:6: lv_path_17_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_39);
                    lv_path_17_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_17_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_18=(Token)match(input,39,FOLLOW_35); 

                    				newLeafNode(otherlv_18, grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2());
                    			
                    // InternalPapin.g:1400:4: ( (lv_value_19_0= RULE_STRING ) )
                    // InternalPapin.g:1401:5: (lv_value_19_0= RULE_STRING )
                    {
                    // InternalPapin.g:1401:5: (lv_value_19_0= RULE_STRING )
                    // InternalPapin.g:1402:6: lv_value_19_0= RULE_STRING
                    {
                    lv_value_19_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_19_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_19_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 6 :
                    // InternalPapin.g:1420:3: ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) )
                    {
                    // InternalPapin.g:1420:3: ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) )
                    // InternalPapin.g:1421:4: () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) )
                    {
                    // InternalPapin.g:1421:4: ()
                    // InternalPapin.g:1422:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0(),
                    						current);
                    				

                    }

                    // InternalPapin.g:1428:4: ( (lv_path_21_0= rulePath ) )
                    // InternalPapin.g:1429:5: (lv_path_21_0= rulePath )
                    {
                    // InternalPapin.g:1429:5: (lv_path_21_0= rulePath )
                    // InternalPapin.g:1430:6: lv_path_21_0= rulePath
                    {

                    						newCompositeNode(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_path_21_0=rulePath();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getComparisonRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_21_0,
                    							"es.unican.Papin.Path");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_22=(Token)match(input,40,FOLLOW_35); 

                    				newLeafNode(otherlv_22, grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2());
                    			
                    // InternalPapin.g:1451:4: ( (lv_value_23_0= RULE_STRING ) )
                    // InternalPapin.g:1452:5: (lv_value_23_0= RULE_STRING )
                    {
                    // InternalPapin.g:1452:5: (lv_value_23_0= RULE_STRING )
                    // InternalPapin.g:1453:6: lv_value_23_0= RULE_STRING
                    {
                    lv_value_23_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_23_0, grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getComparisonRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_23_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRulePath"
    // InternalPapin.g:1474:1: entryRulePath returns [EObject current=null] : iv_rulePath= rulePath EOF ;
    public final EObject entryRulePath() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePath = null;


        try {
            // InternalPapin.g:1474:45: (iv_rulePath= rulePath EOF )
            // InternalPapin.g:1475:2: iv_rulePath= rulePath EOF
            {
             newCompositeNode(grammarAccess.getPathRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePath=rulePath();

            state._fsp--;

             current =iv_rulePath; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalPapin.g:1481:1: rulePath returns [EObject current=null] : ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* ) ;
    public final EObject rulePath() throws RecognitionException {
        EObject current = null;

        Token lv_jumps_0_0=null;
        Token otherlv_1=null;
        Token lv_jumps_2_0=null;


        	enterRule();

        try {
            // InternalPapin.g:1487:2: ( ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* ) )
            // InternalPapin.g:1488:2: ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* )
            {
            // InternalPapin.g:1488:2: ( ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )* )
            // InternalPapin.g:1489:3: ( (lv_jumps_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )*
            {
            // InternalPapin.g:1489:3: ( (lv_jumps_0_0= RULE_ID ) )
            // InternalPapin.g:1490:4: (lv_jumps_0_0= RULE_ID )
            {
            // InternalPapin.g:1490:4: (lv_jumps_0_0= RULE_ID )
            // InternalPapin.g:1491:5: lv_jumps_0_0= RULE_ID
            {
            lv_jumps_0_0=(Token)match(input,RULE_ID,FOLLOW_41); 

            					newLeafNode(lv_jumps_0_0, grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPathRule());
            					}
            					addWithLastConsumed(
            						current,
            						"jumps",
            						lv_jumps_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalPapin.g:1507:3: (otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==41) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalPapin.g:1508:4: otherlv_1= '.' ( (lv_jumps_2_0= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,41,FOLLOW_7); 

            	    				newLeafNode(otherlv_1, grammarAccess.getPathAccess().getFullStopKeyword_1_0());
            	    			
            	    // InternalPapin.g:1512:4: ( (lv_jumps_2_0= RULE_ID ) )
            	    // InternalPapin.g:1513:5: (lv_jumps_2_0= RULE_ID )
            	    {
            	    // InternalPapin.g:1513:5: (lv_jumps_2_0= RULE_ID )
            	    // InternalPapin.g:1514:6: lv_jumps_2_0= RULE_ID
            	    {
            	    lv_jumps_2_0=(Token)match(input,RULE_ID,FOLLOW_41); 

            	    						newLeafNode(lv_jumps_2_0, grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getPathRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"jumps",
            	    							lv_jumps_2_0,
            	    							"org.eclipse.xtext.common.Terminals.ID");
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalPapin.g:1535:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalPapin.g:1535:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalPapin.g:1536:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalPapin.g:1542:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalPapin.g:1548:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalPapin.g:1549:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalPapin.g:1549:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalPapin.g:1550:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_41); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalPapin.g:1557:3: (kw= '.' this_ID_2= RULE_ID )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==41) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalPapin.g:1558:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,41,FOLLOW_7); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_41); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"

    // Delegated rules


    protected DFA29 dfa29 = new DFA29(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\5\1\43\1\5\6\uffff\1\43";
    static final String dfa_3s = "\1\5\1\51\1\5\6\uffff\1\51";
    static final String dfa_4s = "\3\uffff\1\2\1\4\1\1\1\6\1\3\1\5\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\5\1\3\1\7\1\4\1\10\1\6\1\2",
            "\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\5\1\3\1\7\1\4\1\10\1\6\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA29 extends DFA {

        public DFA29(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 29;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1164:2: ( ( () ( (lv_path_1_0= rulePath ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_STRING ) ) ) | ( () ( (lv_path_5_0= rulePath ) ) otherlv_6= '!=' ( (lv_value_7_0= RULE_STRING ) ) ) | ( () ( (lv_path_9_0= rulePath ) ) otherlv_10= '>' ( (lv_value_11_0= RULE_STRING ) ) ) | ( () ( (lv_path_13_0= rulePath ) ) otherlv_14= '>=' ( (lv_value_15_0= RULE_STRING ) ) ) | ( () ( (lv_path_17_0= rulePath ) ) otherlv_18= '<' ( (lv_value_19_0= RULE_STRING ) ) ) | ( () ( (lv_path_21_0= rulePath ) ) otherlv_22= '<=' ( (lv_value_23_0= RULE_STRING ) ) ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000810L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000001301B0000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000001101B0000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000101B0000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000020048002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000048002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000100008002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000010130000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000100020L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x000000000F800000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000100040002L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000020008002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x00000000000B0000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000200020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000020000000002L});

}