% !TeX spellcheck = en_US

\section{Testing and Evaluation}
\label{sec:testing}

In parallel to the development of the editors for the DSLs and the interpreter for Papin, we tested them. 
All the testing was done manually, due to the lack of proper tools for the automatic testing in the Xtext environment. 
For each DSL, it was checked that the corresponding editor supported each feature contained in the grammar of that language. 
Moreover, we checked that some specifications that did not conform to the language grammar were detected as improperly formed ones by the corresponding editor. 

In addition, each extra feature we included in the editors was also tested. 
For each validator, we designed several situations in which the constraints enforced by this validator were violated. 
Then, we verified that the corresponding validator was able to detect such violations and alert DSL users adequately. 
Similarly, for each detected violation for which there was an available quickfix, we checked that the quickfix proposal was appropriate and that the quickfix were correctly applied when selected. 

Finally, since this work was developed inside a larger research project, the expressiveness and actual benefits of the whole set of DSLs were also assessed as part of this research work.
Concerning expressiveness, the developed DSLs were used we use to build datasets for five external case studies. 
Four of them were taken from the literature~\citep{Piekara2012,Dziuba2013,Dave2018,Siwiec2020} and the remaining one from an industrial partner.

The first case study~\citep{Piekara2012} is about finding causes in the production process of hydraulic nipples that might lead to defects in the final products. The second case study~\citep{Dziuba2013} is an extended version of the running example used throughout this paper. The third case study~\citep{Dave2018} analyzes the production process of car firewalls, the piece that isolates the passenger compartment from the engine, to try to find causes for the appearance of rust in some of these firewalls. The fourth case study~\citep{Siwiec2020} aims to find out what was at the origin of the porosities found on the outlet nozzle of some manufactured turbines. Finally, the industrial case study, provided by an industrial partner of the automotive sector, analyzes data related to the tightening torque of an alternator pulley to try to reduce the number of alternators that were rejected due to malfunctioning.

We were able to specify a DOF and several Papin specifications for each of one these case studies without major problems.  Therefore, we can conclude that the expressiveness of Data-Oriented Fishbone models and Papin is adequate. Nevertheless, some issues, not related to language expressiveness, where detected during this evaluation process. 

First, we detected that a same piece of data might be attached to two or more causes in some cases. For example, 
in~\cite{Dave2018}, the machine used to manufacture the firewalls has a liquid that is in contact with both the pieces of the firewall and the machine itself. Therefore, the pH level of this liquid might affect both these pieces, which would be in the \emph{Material} category, and the machine, which would be in the \emph{Machine} category. Consequently, if we selected both categories when generating a dataset, the pH level of this liquid would be included twice in such dataset, which might lead to a few problems with some data mining algorithms. To avoid this, we added a step to the Papin interpreter to detect and remove duplicate columns. 

Second, we realized that, in all these case studies, we copied first the corresponding QCF in the DOF editor and then we started to modify it by adding data feeders and \emph{realizes} statements. It would be interesting if this process could be automated and an initial skeleton of a DOF, with even the \emph{realizes} statements automatically filled up, could be generated from a QCF. We will try to address this issue as part of our future work.

Concerning benefits of this new set of DSLs, we analyzed whether it helped to reduce accidental complexity of the dataset generation processes as compared to state-of-the-art technologies, such as SQL or Pandas, as well as Lavoisier. 
For that purpose, we designed a set of dataset generation scenarios. 
This set of scenarios aimed to cover each potential situation for which a dataset might be generated. 
For each scenario, we developed dataset generation scripts in SQL, Pandas, Lavoisier and DOF plus Papin.
Then, we calculated several metrics for each scenario and we compared the results.
The interested reader can find the complete set of dataset generation scenarios, the evaluation metrics and the values for these metrics in the supplementary material to this work. 
They have not been included here for the sake of brevity and because this evaluation process is beyond the scope of this work, which focuses on the development of the tools required for supporting the newly created DSLs. 

As a result of this evaluation process, it could be stated that the developed DSLs perform better than their SQL and Pandas counterparts, with reductions in accidental complexity by 50\% and up to 80\%.  
As compared to Lavoisier, our approach introduces an overhead due to the need of specifying the Quality Control Fishbone model and the Data-Oriented Fishbone model, which are not required in the Lavoisier case.
When we are interested in generating just one or two datasets from a object-oriented domain model, this extra effort does not pay off. 
On the other hand, when we want to explore the influence of different sets of causes on a concrete effect, which implies the generation of several datasets from a same object-oriented model, this new approach performs better than Lavoisier. 
In this latter case, since Papin is a very simple language, it is quicker and easier to write a Papin specification for selecting a new set of causes than writing or updating Lavoisier statements for the same purpose. 
So, in these situations, our approach provides advantages as compared to Lavoisier.









