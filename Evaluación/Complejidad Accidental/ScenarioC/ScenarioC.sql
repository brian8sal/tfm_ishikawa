SELECT d.id,
   c.fallingWheelBand,
   (SELECT p.pressure as pressure_01 FROM  PliersData p
      WHERE p.idMonitoredParameters = m.id and p.number = 1),
   (SELECT p.pressure as pressure_02 FROM  PliersData p
      WHERE p.idMonitoredParameters = m.id and p.number = 2),
   (SELECT p.pressure as pressure_03 FROM  PliersData p
      WHERE p.idMonitoredParameters = m.id and p.number = 3),
   (SELECT p.pressure as pressure_04 FROM PliersData p
      WHERE p.idMonitoredParameters = m.id and p.number = 4)
   FROM DriveHalfShaft d JOIN ComplianceReport c
      ON d.idComplianceReport = c.id
   JOIN AssemblySession a
      ON d.idAssemblySession = a.id
   JOIN MonitoredParameters m
      ON a.idMonitoredParameters = m.id;
