%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{Related Work}\label{chap:relatedWork}

% **************************** Define Graphics Path **************************
\ifpdf
\graphicspath{{Chapter6/Figs/Raster/}{Chapter6/Figs/PDF/}{Chapter6/Figs/}}
\else
\graphicspath{{Chapter6/Figs/Vector/}{Chapter6/Figs/}}
\fi

To the best of our knowledge, this is the first work that enriches fishbone diagrams with data so that datasets can be automatically generated from them. Nevertheless, there exists in the literature other work that connects fishbone diagrams with data models.

For example, \cite{Xu2020} develop an approach to generate fishbone diagrams from databases containing problem reports. In these reports, causes for each problem are already identified. These reports are automatically analyzed using natural language processing and machine learning techniques to generate as output sets of causes that might lead to each reported problem. Using this information, fishbone diagrams are automatically constructed. This approach requires that causes for each reported  problem are identified, whereas, in our work, we want to build datasets to feed data analysis algorithms that can help to discover these causes.

\cite{Shigemitsu2008} propose using fishbone diagrams as part of the requirements engineering process for developing systems where the problem to be solved is known, but the causes of that problem are unknown and, therefore, the solutions to be implemented for these causes are also unknown. Fishbone diagrams are used to decompose the problem, into several subproblems. Then, each atomic subproblem is associated to a software function, and, following some guidelines, a UML class diagram is generated. However, these diagrams contain just functions and no data, so they are not useful to specify domain data.

\cite{Yurin2018} present a model-driven process for the automated development of rule-based knowledge bases from fishbone diagrams. In this process, rules of a knowledge base are firstly modeled as cause-effect relationships of a fishbone model. By using fishbone diagrams, domain experts can have a system-wide focus on these rules. As in \cite{Xu2020}, cause-effect relationships are perfectly known, since they are expert knowledge. Contrarily, in our approach, we build datasets to find cause-effect relationships that can be used as expert knowledge. On the other hand, similarly to \cite{Yurin2018}, we use fishbone diagrams to provide a global view of influences between data, so that decisions about what data must be in a dataset can be more easily made.

\cite{Gwiazda2006} addresses the inclusion of quantitative information in fishbone diagrams. In order to do it, \emph{weighted Ishikawa diagrams} are proposed. These diagrams are fishbone diagrams in which weights are added to the causes. To calculate them, weights of the categories are established using a form of \emph{Saaty matrix}. Then, the weights of the causes and sub-causes are calculated by distributing the category weight between them.

\cite{Yun2009} use fishbone diagrams to control quality of data mining processes. In these diagrams, ribs and bones represent steps of a data mining process. Issues that might affect to quality of the results are identified as causes and attached to the corresponding step. These fishbone diagrams help to visualize a data analysis process, but they cannot be used to generate datasets.

Concerning the problem of generating datasets from hierarchical and nested data, there is a research field aiming to automatically reduce multi-relational data to a single-table structure that can be digested by data mining algorithms, which is known in the community as \emph{propositionalisation}~\citep{Boulle2019,Knobbe2001,Kanter2015,Samorani2015}. Generally speaking, these approaches work as follows: starting from an entity of interest, e.g., \emph{DriveHalfShaft}, an algorithm randomly generates dataset columns by applying aggregation functions, such as \emph{count}, \emph{average}, \emph{max} or \emph{min}, over the relationships between the selected entity and other ones in the model.

This random exploration has the potential to discover previously unknown aggregate values that might be relevant for data analysis. On the other hand, domain experts cannot have a fine-grained control of which data would be included in the output dataset. In addition, multi-bounded references cannot be analysed at the instance level, since their information needs always to be summarised by means of aggregation functions. This limitation might hamper finding patterns related to values of these individual instances. Moreover, it should be taken into account that this random exploration might exhibit scalability and performance problems. The exploration takes place over an enormous search space of candidate columns, from which many of them may not be useful at all.

Other researchers have tackled the dataset generation problem from a different angle. Instead of focusing on producing tabular datasets from linked and hierarchical data, they have modified some data mining algorithms so that they accept these data in their original structure as input, which is known as \emph{Multi-Relational Data Mining (MRDM)}~\citep{Dxeroski2009}. Solutions based on MRDM have been proven useful to perform analysis over relational datasets coming from diverse domains, such as medicine~\citep{Cilia2011}, financial~\citep{Manjunath2013} or time-sequence analysis~\citep{Nica2018,Abreu2019}. However, at the time of writing this work, most of these modified algorithms are not as powerful, versatile and efficient as those data mining algorithms that work with single-table datasets.

