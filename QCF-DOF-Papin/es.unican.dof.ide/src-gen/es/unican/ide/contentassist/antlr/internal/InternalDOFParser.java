package es.unican.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import es.unican.services.DOFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDOFParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'count'", "'sum'", "'avg'", "'max'", "'min'", "'dof'", "'import'", "'effect'", "'is'", "'category'", "'include'", "'by'", "'{'", "'}'", "'.'", "'calculate'", "'as'", "'('", "')'", "'only_as'", "'['", "']'", "','", "'where'", "'and'", "'or'", "'='", "'!='", "'>'", "'>='", "'<'", "'<='", "'cause'", "'contains'", "'realizes'", "'notMapped'", "'.*'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalDOFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDOFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDOFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDOF.g"; }


    	private DOFGrammarAccess grammarAccess;

    	public void setGrammarAccess(DOFGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDOF"
    // InternalDOF.g:53:1: entryRuleDOF : ruleDOF EOF ;
    public final void entryRuleDOF() throws RecognitionException {
        try {
            // InternalDOF.g:54:1: ( ruleDOF EOF )
            // InternalDOF.g:55:1: ruleDOF EOF
            {
             before(grammarAccess.getDOFRule()); 
            pushFollow(FOLLOW_1);
            ruleDOF();

            state._fsp--;

             after(grammarAccess.getDOFRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDOF"


    // $ANTLR start "ruleDOF"
    // InternalDOF.g:62:1: ruleDOF : ( ( rule__DOF__Group__0 ) ) ;
    public final void ruleDOF() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:66:2: ( ( ( rule__DOF__Group__0 ) ) )
            // InternalDOF.g:67:2: ( ( rule__DOF__Group__0 ) )
            {
            // InternalDOF.g:67:2: ( ( rule__DOF__Group__0 ) )
            // InternalDOF.g:68:3: ( rule__DOF__Group__0 )
            {
             before(grammarAccess.getDOFAccess().getGroup()); 
            // InternalDOF.g:69:3: ( rule__DOF__Group__0 )
            // InternalDOF.g:69:4: rule__DOF__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDOF"


    // $ANTLR start "entryRuleImport"
    // InternalDOF.g:78:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalDOF.g:79:1: ( ruleImport EOF )
            // InternalDOF.g:80:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalDOF.g:87:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:91:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalDOF.g:92:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalDOF.g:92:2: ( ( rule__Import__Group__0 ) )
            // InternalDOF.g:93:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalDOF.g:94:3: ( rule__Import__Group__0 )
            // InternalDOF.g:94:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleEffect"
    // InternalDOF.g:103:1: entryRuleEffect : ruleEffect EOF ;
    public final void entryRuleEffect() throws RecognitionException {
        try {
            // InternalDOF.g:104:1: ( ruleEffect EOF )
            // InternalDOF.g:105:1: ruleEffect EOF
            {
             before(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getEffectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalDOF.g:112:1: ruleEffect : ( ( rule__Effect__Group__0 ) ) ;
    public final void ruleEffect() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:116:2: ( ( ( rule__Effect__Group__0 ) ) )
            // InternalDOF.g:117:2: ( ( rule__Effect__Group__0 ) )
            {
            // InternalDOF.g:117:2: ( ( rule__Effect__Group__0 ) )
            // InternalDOF.g:118:3: ( rule__Effect__Group__0 )
            {
             before(grammarAccess.getEffectAccess().getGroup()); 
            // InternalDOF.g:119:3: ( rule__Effect__Group__0 )
            // InternalDOF.g:119:4: rule__Effect__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalDOF.g:128:1: entryRuleCategory : ruleCategory EOF ;
    public final void entryRuleCategory() throws RecognitionException {
        try {
            // InternalDOF.g:129:1: ( ruleCategory EOF )
            // InternalDOF.g:130:1: ruleCategory EOF
            {
             before(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getCategoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalDOF.g:137:1: ruleCategory : ( ( rule__Category__Group__0 ) ) ;
    public final void ruleCategory() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:141:2: ( ( ( rule__Category__Group__0 ) ) )
            // InternalDOF.g:142:2: ( ( rule__Category__Group__0 ) )
            {
            // InternalDOF.g:142:2: ( ( rule__Category__Group__0 ) )
            // InternalDOF.g:143:3: ( rule__Category__Group__0 )
            {
             before(grammarAccess.getCategoryAccess().getGroup()); 
            // InternalDOF.g:144:3: ( rule__Category__Group__0 )
            // InternalDOF.g:144:4: rule__Category__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleDataFeeder"
    // InternalDOF.g:153:1: entryRuleDataFeeder : ruleDataFeeder EOF ;
    public final void entryRuleDataFeeder() throws RecognitionException {
        try {
            // InternalDOF.g:154:1: ( ruleDataFeeder EOF )
            // InternalDOF.g:155:1: ruleDataFeeder EOF
            {
             before(grammarAccess.getDataFeederRule()); 
            pushFollow(FOLLOW_1);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getDataFeederRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataFeeder"


    // $ANTLR start "ruleDataFeeder"
    // InternalDOF.g:162:1: ruleDataFeeder : ( ( rule__DataFeeder__Group__0 ) ) ;
    public final void ruleDataFeeder() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:166:2: ( ( ( rule__DataFeeder__Group__0 ) ) )
            // InternalDOF.g:167:2: ( ( rule__DataFeeder__Group__0 ) )
            {
            // InternalDOF.g:167:2: ( ( rule__DataFeeder__Group__0 ) )
            // InternalDOF.g:168:3: ( rule__DataFeeder__Group__0 )
            {
             before(grammarAccess.getDataFeederAccess().getGroup()); 
            // InternalDOF.g:169:3: ( rule__DataFeeder__Group__0 )
            // InternalDOF.g:169:4: rule__DataFeeder__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataFeeder"


    // $ANTLR start "entryRuleIncludedReference"
    // InternalDOF.g:178:1: entryRuleIncludedReference : ruleIncludedReference EOF ;
    public final void entryRuleIncludedReference() throws RecognitionException {
        try {
            // InternalDOF.g:179:1: ( ruleIncludedReference EOF )
            // InternalDOF.g:180:1: ruleIncludedReference EOF
            {
             before(grammarAccess.getIncludedReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getIncludedReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIncludedReference"


    // $ANTLR start "ruleIncludedReference"
    // InternalDOF.g:187:1: ruleIncludedReference : ( ( rule__IncludedReference__Alternatives ) ) ;
    public final void ruleIncludedReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:191:2: ( ( ( rule__IncludedReference__Alternatives ) ) )
            // InternalDOF.g:192:2: ( ( rule__IncludedReference__Alternatives ) )
            {
            // InternalDOF.g:192:2: ( ( rule__IncludedReference__Alternatives ) )
            // InternalDOF.g:193:3: ( rule__IncludedReference__Alternatives )
            {
             before(grammarAccess.getIncludedReferenceAccess().getAlternatives()); 
            // InternalDOF.g:194:3: ( rule__IncludedReference__Alternatives )
            // InternalDOF.g:194:4: rule__IncludedReference__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__IncludedReference__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIncludedReferenceAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIncludedReference"


    // $ANTLR start "entryRuleSimpleReference"
    // InternalDOF.g:203:1: entryRuleSimpleReference : ruleSimpleReference EOF ;
    public final void entryRuleSimpleReference() throws RecognitionException {
        try {
            // InternalDOF.g:204:1: ( ruleSimpleReference EOF )
            // InternalDOF.g:205:1: ruleSimpleReference EOF
            {
             before(grammarAccess.getSimpleReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleReference();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleReference"


    // $ANTLR start "ruleSimpleReference"
    // InternalDOF.g:212:1: ruleSimpleReference : ( ( rule__SimpleReference__Group__0 ) ) ;
    public final void ruleSimpleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:216:2: ( ( ( rule__SimpleReference__Group__0 ) ) )
            // InternalDOF.g:217:2: ( ( rule__SimpleReference__Group__0 ) )
            {
            // InternalDOF.g:217:2: ( ( rule__SimpleReference__Group__0 ) )
            // InternalDOF.g:218:3: ( rule__SimpleReference__Group__0 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup()); 
            // InternalDOF.g:219:3: ( rule__SimpleReference__Group__0 )
            // InternalDOF.g:219:4: rule__SimpleReference__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleReference"


    // $ANTLR start "entryRuleAggregatedReference"
    // InternalDOF.g:228:1: entryRuleAggregatedReference : ruleAggregatedReference EOF ;
    public final void entryRuleAggregatedReference() throws RecognitionException {
        try {
            // InternalDOF.g:229:1: ( ruleAggregatedReference EOF )
            // InternalDOF.g:230:1: ruleAggregatedReference EOF
            {
             before(grammarAccess.getAggregatedReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleAggregatedReference();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggregatedReference"


    // $ANTLR start "ruleAggregatedReference"
    // InternalDOF.g:237:1: ruleAggregatedReference : ( ( rule__AggregatedReference__Group__0 ) ) ;
    public final void ruleAggregatedReference() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:241:2: ( ( ( rule__AggregatedReference__Group__0 ) ) )
            // InternalDOF.g:242:2: ( ( rule__AggregatedReference__Group__0 ) )
            {
            // InternalDOF.g:242:2: ( ( rule__AggregatedReference__Group__0 ) )
            // InternalDOF.g:243:3: ( rule__AggregatedReference__Group__0 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getGroup()); 
            // InternalDOF.g:244:3: ( rule__AggregatedReference__Group__0 )
            // InternalDOF.g:244:4: rule__AggregatedReference__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggregatedReference"


    // $ANTLR start "entryRuleAggFunction"
    // InternalDOF.g:253:1: entryRuleAggFunction : ruleAggFunction EOF ;
    public final void entryRuleAggFunction() throws RecognitionException {
        try {
            // InternalDOF.g:254:1: ( ruleAggFunction EOF )
            // InternalDOF.g:255:1: ruleAggFunction EOF
            {
             before(grammarAccess.getAggFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleAggFunction();

            state._fsp--;

             after(grammarAccess.getAggFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggFunction"


    // $ANTLR start "ruleAggFunction"
    // InternalDOF.g:262:1: ruleAggFunction : ( ( rule__AggFunction__Alternatives ) ) ;
    public final void ruleAggFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:266:2: ( ( ( rule__AggFunction__Alternatives ) ) )
            // InternalDOF.g:267:2: ( ( rule__AggFunction__Alternatives ) )
            {
            // InternalDOF.g:267:2: ( ( rule__AggFunction__Alternatives ) )
            // InternalDOF.g:268:3: ( rule__AggFunction__Alternatives )
            {
             before(grammarAccess.getAggFunctionAccess().getAlternatives()); 
            // InternalDOF.g:269:3: ( rule__AggFunction__Alternatives )
            // InternalDOF.g:269:4: rule__AggFunction__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AggFunction__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAggFunctionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggFunction"


    // $ANTLR start "entryRuleTypeFilter"
    // InternalDOF.g:278:1: entryRuleTypeFilter : ruleTypeFilter EOF ;
    public final void entryRuleTypeFilter() throws RecognitionException {
        try {
            // InternalDOF.g:279:1: ( ruleTypeFilter EOF )
            // InternalDOF.g:280:1: ruleTypeFilter EOF
            {
             before(grammarAccess.getTypeFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getTypeFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeFilter"


    // $ANTLR start "ruleTypeFilter"
    // InternalDOF.g:287:1: ruleTypeFilter : ( ( rule__TypeFilter__Alternatives ) ) ;
    public final void ruleTypeFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:291:2: ( ( ( rule__TypeFilter__Alternatives ) ) )
            // InternalDOF.g:292:2: ( ( rule__TypeFilter__Alternatives ) )
            {
            // InternalDOF.g:292:2: ( ( rule__TypeFilter__Alternatives ) )
            // InternalDOF.g:293:3: ( rule__TypeFilter__Alternatives )
            {
             before(grammarAccess.getTypeFilterAccess().getAlternatives()); 
            // InternalDOF.g:294:3: ( rule__TypeFilter__Alternatives )
            // InternalDOF.g:294:4: rule__TypeFilter__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TypeFilter__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeFilterAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeFilter"


    // $ANTLR start "entryRuleTypeCompletion"
    // InternalDOF.g:303:1: entryRuleTypeCompletion : ruleTypeCompletion EOF ;
    public final void entryRuleTypeCompletion() throws RecognitionException {
        try {
            // InternalDOF.g:304:1: ( ruleTypeCompletion EOF )
            // InternalDOF.g:305:1: ruleTypeCompletion EOF
            {
             before(grammarAccess.getTypeCompletionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeCompletion();

            state._fsp--;

             after(grammarAccess.getTypeCompletionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeCompletion"


    // $ANTLR start "ruleTypeCompletion"
    // InternalDOF.g:312:1: ruleTypeCompletion : ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) ) ;
    public final void ruleTypeCompletion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:316:2: ( ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) ) )
            // InternalDOF.g:317:2: ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) )
            {
            // InternalDOF.g:317:2: ( ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* ) )
            // InternalDOF.g:318:3: ( ( rule__TypeCompletion__Group__0 ) ) ( ( rule__TypeCompletion__Group__0 )* )
            {
            // InternalDOF.g:318:3: ( ( rule__TypeCompletion__Group__0 ) )
            // InternalDOF.g:319:4: ( rule__TypeCompletion__Group__0 )
            {
             before(grammarAccess.getTypeCompletionAccess().getGroup()); 
            // InternalDOF.g:320:4: ( rule__TypeCompletion__Group__0 )
            // InternalDOF.g:320:5: rule__TypeCompletion__Group__0
            {
            pushFollow(FOLLOW_3);
            rule__TypeCompletion__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCompletionAccess().getGroup()); 

            }

            // InternalDOF.g:323:3: ( ( rule__TypeCompletion__Group__0 )* )
            // InternalDOF.g:324:4: ( rule__TypeCompletion__Group__0 )*
            {
             before(grammarAccess.getTypeCompletionAccess().getGroup()); 
            // InternalDOF.g:325:4: ( rule__TypeCompletion__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==27) ) {
                    int LA1_2 = input.LA(2);

                    if ( (LA1_2==RULE_ID) ) {
                        alt1=1;
                    }


                }


                switch (alt1) {
            	case 1 :
            	    // InternalDOF.g:325:5: rule__TypeCompletion__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__TypeCompletion__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getTypeCompletionAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeCompletion"


    // $ANTLR start "entryRuleTypeSelection"
    // InternalDOF.g:335:1: entryRuleTypeSelection : ruleTypeSelection EOF ;
    public final void entryRuleTypeSelection() throws RecognitionException {
        try {
            // InternalDOF.g:336:1: ( ruleTypeSelection EOF )
            // InternalDOF.g:337:1: ruleTypeSelection EOF
            {
             before(grammarAccess.getTypeSelectionRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeSelection();

            state._fsp--;

             after(grammarAccess.getTypeSelectionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeSelection"


    // $ANTLR start "ruleTypeSelection"
    // InternalDOF.g:344:1: ruleTypeSelection : ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) ) ;
    public final void ruleTypeSelection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:348:2: ( ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) ) )
            // InternalDOF.g:349:2: ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) )
            {
            // InternalDOF.g:349:2: ( ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* ) )
            // InternalDOF.g:350:3: ( ( rule__TypeSelection__Group__0 ) ) ( ( rule__TypeSelection__Group__0 )* )
            {
            // InternalDOF.g:350:3: ( ( rule__TypeSelection__Group__0 ) )
            // InternalDOF.g:351:4: ( rule__TypeSelection__Group__0 )
            {
             before(grammarAccess.getTypeSelectionAccess().getGroup()); 
            // InternalDOF.g:352:4: ( rule__TypeSelection__Group__0 )
            // InternalDOF.g:352:5: rule__TypeSelection__Group__0
            {
            pushFollow(FOLLOW_4);
            rule__TypeSelection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeSelectionAccess().getGroup()); 

            }

            // InternalDOF.g:355:3: ( ( rule__TypeSelection__Group__0 )* )
            // InternalDOF.g:356:4: ( rule__TypeSelection__Group__0 )*
            {
             before(grammarAccess.getTypeSelectionAccess().getGroup()); 
            // InternalDOF.g:357:4: ( rule__TypeSelection__Group__0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==30) ) {
                    int LA2_2 = input.LA(2);

                    if ( (LA2_2==RULE_ID) ) {
                        alt2=1;
                    }


                }


                switch (alt2) {
            	case 1 :
            	    // InternalDOF.g:357:5: rule__TypeSelection__Group__0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__TypeSelection__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getTypeSelectionAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeSelection"


    // $ANTLR start "entryRuleTypeCustomization"
    // InternalDOF.g:367:1: entryRuleTypeCustomization : ruleTypeCustomization EOF ;
    public final void entryRuleTypeCustomization() throws RecognitionException {
        try {
            // InternalDOF.g:368:1: ( ruleTypeCustomization EOF )
            // InternalDOF.g:369:1: ruleTypeCustomization EOF
            {
             before(grammarAccess.getTypeCustomizationRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeCustomization"


    // $ANTLR start "ruleTypeCustomization"
    // InternalDOF.g:376:1: ruleTypeCustomization : ( ( rule__TypeCustomization__Group__0 ) ) ;
    public final void ruleTypeCustomization() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:380:2: ( ( ( rule__TypeCustomization__Group__0 ) ) )
            // InternalDOF.g:381:2: ( ( rule__TypeCustomization__Group__0 ) )
            {
            // InternalDOF.g:381:2: ( ( rule__TypeCustomization__Group__0 ) )
            // InternalDOF.g:382:3: ( rule__TypeCustomization__Group__0 )
            {
             before(grammarAccess.getTypeCustomizationAccess().getGroup()); 
            // InternalDOF.g:383:3: ( rule__TypeCustomization__Group__0 )
            // InternalDOF.g:383:4: rule__TypeCustomization__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCustomizationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeCustomization"


    // $ANTLR start "entryRuleAttributeFilter"
    // InternalDOF.g:392:1: entryRuleAttributeFilter : ruleAttributeFilter EOF ;
    public final void entryRuleAttributeFilter() throws RecognitionException {
        try {
            // InternalDOF.g:393:1: ( ruleAttributeFilter EOF )
            // InternalDOF.g:394:1: ruleAttributeFilter EOF
            {
             before(grammarAccess.getAttributeFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getAttributeFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttributeFilter"


    // $ANTLR start "ruleAttributeFilter"
    // InternalDOF.g:401:1: ruleAttributeFilter : ( ( rule__AttributeFilter__Group__0 ) ) ;
    public final void ruleAttributeFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:405:2: ( ( ( rule__AttributeFilter__Group__0 ) ) )
            // InternalDOF.g:406:2: ( ( rule__AttributeFilter__Group__0 ) )
            {
            // InternalDOF.g:406:2: ( ( rule__AttributeFilter__Group__0 ) )
            // InternalDOF.g:407:3: ( rule__AttributeFilter__Group__0 )
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup()); 
            // InternalDOF.g:408:3: ( rule__AttributeFilter__Group__0 )
            // InternalDOF.g:408:4: rule__AttributeFilter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttributeFilter"


    // $ANTLR start "entryRuleInstancesFilter"
    // InternalDOF.g:417:1: entryRuleInstancesFilter : ruleInstancesFilter EOF ;
    public final void entryRuleInstancesFilter() throws RecognitionException {
        try {
            // InternalDOF.g:418:1: ( ruleInstancesFilter EOF )
            // InternalDOF.g:419:1: ruleInstancesFilter EOF
            {
             before(grammarAccess.getInstancesFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getInstancesFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstancesFilter"


    // $ANTLR start "ruleInstancesFilter"
    // InternalDOF.g:426:1: ruleInstancesFilter : ( ( rule__InstancesFilter__Group__0 ) ) ;
    public final void ruleInstancesFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:430:2: ( ( ( rule__InstancesFilter__Group__0 ) ) )
            // InternalDOF.g:431:2: ( ( rule__InstancesFilter__Group__0 ) )
            {
            // InternalDOF.g:431:2: ( ( rule__InstancesFilter__Group__0 ) )
            // InternalDOF.g:432:3: ( rule__InstancesFilter__Group__0 )
            {
             before(grammarAccess.getInstancesFilterAccess().getGroup()); 
            // InternalDOF.g:433:3: ( rule__InstancesFilter__Group__0 )
            // InternalDOF.g:433:4: rule__InstancesFilter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInstancesFilterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstancesFilter"


    // $ANTLR start "entryRuleAndConjunction"
    // InternalDOF.g:442:1: entryRuleAndConjunction : ruleAndConjunction EOF ;
    public final void entryRuleAndConjunction() throws RecognitionException {
        try {
            // InternalDOF.g:443:1: ( ruleAndConjunction EOF )
            // InternalDOF.g:444:1: ruleAndConjunction EOF
            {
             before(grammarAccess.getAndConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAndConjunction"


    // $ANTLR start "ruleAndConjunction"
    // InternalDOF.g:451:1: ruleAndConjunction : ( ( rule__AndConjunction__Group__0 ) ) ;
    public final void ruleAndConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:455:2: ( ( ( rule__AndConjunction__Group__0 ) ) )
            // InternalDOF.g:456:2: ( ( rule__AndConjunction__Group__0 ) )
            {
            // InternalDOF.g:456:2: ( ( rule__AndConjunction__Group__0 ) )
            // InternalDOF.g:457:3: ( rule__AndConjunction__Group__0 )
            {
             before(grammarAccess.getAndConjunctionAccess().getGroup()); 
            // InternalDOF.g:458:3: ( rule__AndConjunction__Group__0 )
            // InternalDOF.g:458:4: rule__AndConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAndConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAndConjunction"


    // $ANTLR start "entryRuleOrConjunction"
    // InternalDOF.g:467:1: entryRuleOrConjunction : ruleOrConjunction EOF ;
    public final void entryRuleOrConjunction() throws RecognitionException {
        try {
            // InternalDOF.g:468:1: ( ruleOrConjunction EOF )
            // InternalDOF.g:469:1: ruleOrConjunction EOF
            {
             before(grammarAccess.getOrConjunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getOrConjunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrConjunction"


    // $ANTLR start "ruleOrConjunction"
    // InternalDOF.g:476:1: ruleOrConjunction : ( ( rule__OrConjunction__Group__0 ) ) ;
    public final void ruleOrConjunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:480:2: ( ( ( rule__OrConjunction__Group__0 ) ) )
            // InternalDOF.g:481:2: ( ( rule__OrConjunction__Group__0 ) )
            {
            // InternalDOF.g:481:2: ( ( rule__OrConjunction__Group__0 ) )
            // InternalDOF.g:482:3: ( rule__OrConjunction__Group__0 )
            {
             before(grammarAccess.getOrConjunctionAccess().getGroup()); 
            // InternalDOF.g:483:3: ( rule__OrConjunction__Group__0 )
            // InternalDOF.g:483:4: rule__OrConjunction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrConjunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrConjunction"


    // $ANTLR start "entryRulePrimary"
    // InternalDOF.g:492:1: entryRulePrimary : rulePrimary EOF ;
    public final void entryRulePrimary() throws RecognitionException {
        try {
            // InternalDOF.g:493:1: ( rulePrimary EOF )
            // InternalDOF.g:494:1: rulePrimary EOF
            {
             before(grammarAccess.getPrimaryRule()); 
            pushFollow(FOLLOW_1);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getPrimaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimary"


    // $ANTLR start "rulePrimary"
    // InternalDOF.g:501:1: rulePrimary : ( ( rule__Primary__Alternatives ) ) ;
    public final void rulePrimary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:505:2: ( ( ( rule__Primary__Alternatives ) ) )
            // InternalDOF.g:506:2: ( ( rule__Primary__Alternatives ) )
            {
            // InternalDOF.g:506:2: ( ( rule__Primary__Alternatives ) )
            // InternalDOF.g:507:3: ( rule__Primary__Alternatives )
            {
             before(grammarAccess.getPrimaryAccess().getAlternatives()); 
            // InternalDOF.g:508:3: ( rule__Primary__Alternatives )
            // InternalDOF.g:508:4: rule__Primary__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimary"


    // $ANTLR start "entryRuleComparison"
    // InternalDOF.g:517:1: entryRuleComparison : ruleComparison EOF ;
    public final void entryRuleComparison() throws RecognitionException {
        try {
            // InternalDOF.g:518:1: ( ruleComparison EOF )
            // InternalDOF.g:519:1: ruleComparison EOF
            {
             before(grammarAccess.getComparisonRule()); 
            pushFollow(FOLLOW_1);
            ruleComparison();

            state._fsp--;

             after(grammarAccess.getComparisonRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalDOF.g:526:1: ruleComparison : ( ( rule__Comparison__Alternatives ) ) ;
    public final void ruleComparison() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:530:2: ( ( ( rule__Comparison__Alternatives ) ) )
            // InternalDOF.g:531:2: ( ( rule__Comparison__Alternatives ) )
            {
            // InternalDOF.g:531:2: ( ( rule__Comparison__Alternatives ) )
            // InternalDOF.g:532:3: ( rule__Comparison__Alternatives )
            {
             before(grammarAccess.getComparisonAccess().getAlternatives()); 
            // InternalDOF.g:533:3: ( rule__Comparison__Alternatives )
            // InternalDOF.g:533:4: rule__Comparison__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleCause"
    // InternalDOF.g:542:1: entryRuleCause : ruleCause EOF ;
    public final void entryRuleCause() throws RecognitionException {
        try {
            // InternalDOF.g:543:1: ( ruleCause EOF )
            // InternalDOF.g:544:1: ruleCause EOF
            {
             before(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalDOF.g:551:1: ruleCause : ( ( rule__Cause__Alternatives ) ) ;
    public final void ruleCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:555:2: ( ( ( rule__Cause__Alternatives ) ) )
            // InternalDOF.g:556:2: ( ( rule__Cause__Alternatives ) )
            {
            // InternalDOF.g:556:2: ( ( rule__Cause__Alternatives ) )
            // InternalDOF.g:557:3: ( rule__Cause__Alternatives )
            {
             before(grammarAccess.getCauseAccess().getAlternatives()); 
            // InternalDOF.g:558:3: ( rule__Cause__Alternatives )
            // InternalDOF.g:558:4: rule__Cause__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleCompoundCause"
    // InternalDOF.g:567:1: entryRuleCompoundCause : ruleCompoundCause EOF ;
    public final void entryRuleCompoundCause() throws RecognitionException {
        try {
            // InternalDOF.g:568:1: ( ruleCompoundCause EOF )
            // InternalDOF.g:569:1: ruleCompoundCause EOF
            {
             before(grammarAccess.getCompoundCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCompoundCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompoundCause"


    // $ANTLR start "ruleCompoundCause"
    // InternalDOF.g:576:1: ruleCompoundCause : ( ( rule__CompoundCause__Group__0 ) ) ;
    public final void ruleCompoundCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:580:2: ( ( ( rule__CompoundCause__Group__0 ) ) )
            // InternalDOF.g:581:2: ( ( rule__CompoundCause__Group__0 ) )
            {
            // InternalDOF.g:581:2: ( ( rule__CompoundCause__Group__0 ) )
            // InternalDOF.g:582:3: ( rule__CompoundCause__Group__0 )
            {
             before(grammarAccess.getCompoundCauseAccess().getGroup()); 
            // InternalDOF.g:583:3: ( rule__CompoundCause__Group__0 )
            // InternalDOF.g:583:4: rule__CompoundCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompoundCause"


    // $ANTLR start "entryRuleDataLinkedCause"
    // InternalDOF.g:592:1: entryRuleDataLinkedCause : ruleDataLinkedCause EOF ;
    public final void entryRuleDataLinkedCause() throws RecognitionException {
        try {
            // InternalDOF.g:593:1: ( ruleDataLinkedCause EOF )
            // InternalDOF.g:594:1: ruleDataLinkedCause EOF
            {
             before(grammarAccess.getDataLinkedCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleDataLinkedCause();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataLinkedCause"


    // $ANTLR start "ruleDataLinkedCause"
    // InternalDOF.g:601:1: ruleDataLinkedCause : ( ( rule__DataLinkedCause__Group__0 ) ) ;
    public final void ruleDataLinkedCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:605:2: ( ( ( rule__DataLinkedCause__Group__0 ) ) )
            // InternalDOF.g:606:2: ( ( rule__DataLinkedCause__Group__0 ) )
            {
            // InternalDOF.g:606:2: ( ( rule__DataLinkedCause__Group__0 ) )
            // InternalDOF.g:607:3: ( rule__DataLinkedCause__Group__0 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getGroup()); 
            // InternalDOF.g:608:3: ( rule__DataLinkedCause__Group__0 )
            // InternalDOF.g:608:4: rule__DataLinkedCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataLinkedCause"


    // $ANTLR start "entryRuleNotMappedCause"
    // InternalDOF.g:617:1: entryRuleNotMappedCause : ruleNotMappedCause EOF ;
    public final void entryRuleNotMappedCause() throws RecognitionException {
        try {
            // InternalDOF.g:618:1: ( ruleNotMappedCause EOF )
            // InternalDOF.g:619:1: ruleNotMappedCause EOF
            {
             before(grammarAccess.getNotMappedCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleNotMappedCause();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNotMappedCause"


    // $ANTLR start "ruleNotMappedCause"
    // InternalDOF.g:626:1: ruleNotMappedCause : ( ( rule__NotMappedCause__Group__0 ) ) ;
    public final void ruleNotMappedCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:630:2: ( ( ( rule__NotMappedCause__Group__0 ) ) )
            // InternalDOF.g:631:2: ( ( rule__NotMappedCause__Group__0 ) )
            {
            // InternalDOF.g:631:2: ( ( rule__NotMappedCause__Group__0 ) )
            // InternalDOF.g:632:3: ( rule__NotMappedCause__Group__0 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getGroup()); 
            // InternalDOF.g:633:3: ( rule__NotMappedCause__Group__0 )
            // InternalDOF.g:633:4: rule__NotMappedCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNotMappedCause"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalDOF.g:642:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // InternalDOF.g:643:1: ( ruleQualifiedNameWithWildcard EOF )
            // InternalDOF.g:644:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalDOF.g:651:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:655:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // InternalDOF.g:656:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // InternalDOF.g:656:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // InternalDOF.g:657:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // InternalDOF.g:658:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            // InternalDOF.g:658:4: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDOF.g:667:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalDOF.g:668:1: ( ruleQualifiedName EOF )
            // InternalDOF.g:669:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDOF.g:676:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:680:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalDOF.g:681:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalDOF.g:681:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalDOF.g:682:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalDOF.g:683:3: ( rule__QualifiedName__Group__0 )
            // InternalDOF.g:683:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRulePath"
    // InternalDOF.g:692:1: entryRulePath : rulePath EOF ;
    public final void entryRulePath() throws RecognitionException {
        try {
            // InternalDOF.g:693:1: ( rulePath EOF )
            // InternalDOF.g:694:1: rulePath EOF
            {
             before(grammarAccess.getPathRule()); 
            pushFollow(FOLLOW_1);
            rulePath();

            state._fsp--;

             after(grammarAccess.getPathRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalDOF.g:701:1: rulePath : ( ( rule__Path__Group__0 ) ) ;
    public final void rulePath() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:705:2: ( ( ( rule__Path__Group__0 ) ) )
            // InternalDOF.g:706:2: ( ( rule__Path__Group__0 ) )
            {
            // InternalDOF.g:706:2: ( ( rule__Path__Group__0 ) )
            // InternalDOF.g:707:3: ( rule__Path__Group__0 )
            {
             before(grammarAccess.getPathAccess().getGroup()); 
            // InternalDOF.g:708:3: ( rule__Path__Group__0 )
            // InternalDOF.g:708:4: rule__Path__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleEString"
    // InternalDOF.g:717:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalDOF.g:718:1: ( ruleEString EOF )
            // InternalDOF.g:719:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDOF.g:726:1: ruleEString : ( RULE_STRING ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:730:2: ( ( RULE_STRING ) )
            // InternalDOF.g:731:2: ( RULE_STRING )
            {
            // InternalDOF.g:731:2: ( RULE_STRING )
            // InternalDOF.g:732:3: RULE_STRING
            {
             before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "rule__IncludedReference__Alternatives"
    // InternalDOF.g:741:1: rule__IncludedReference__Alternatives : ( ( ruleSimpleReference ) | ( ruleAggregatedReference ) );
    public final void rule__IncludedReference__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:745:1: ( ( ruleSimpleReference ) | ( ruleAggregatedReference ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            else if ( (LA3_0==26) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDOF.g:746:2: ( ruleSimpleReference )
                    {
                    // InternalDOF.g:746:2: ( ruleSimpleReference )
                    // InternalDOF.g:747:3: ruleSimpleReference
                    {
                     before(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSimpleReference();

                    state._fsp--;

                     after(grammarAccess.getIncludedReferenceAccess().getSimpleReferenceParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:752:2: ( ruleAggregatedReference )
                    {
                    // InternalDOF.g:752:2: ( ruleAggregatedReference )
                    // InternalDOF.g:753:3: ruleAggregatedReference
                    {
                     before(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAggregatedReference();

                    state._fsp--;

                     after(grammarAccess.getIncludedReferenceAccess().getAggregatedReferenceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IncludedReference__Alternatives"


    // $ANTLR start "rule__SimpleReference__Alternatives_3"
    // InternalDOF.g:762:1: rule__SimpleReference__Alternatives_3 : ( ( ( rule__SimpleReference__Group_3_0__0 ) ) | ( ( rule__SimpleReference__Group_3_1__0 ) ) );
    public final void rule__SimpleReference__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:766:1: ( ( ( rule__SimpleReference__Group_3_0__0 ) ) | ( ( rule__SimpleReference__Group_3_1__0 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==23) ) {
                alt4=1;
            }
            else if ( (LA4_0==25) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalDOF.g:767:2: ( ( rule__SimpleReference__Group_3_0__0 ) )
                    {
                    // InternalDOF.g:767:2: ( ( rule__SimpleReference__Group_3_0__0 ) )
                    // InternalDOF.g:768:3: ( rule__SimpleReference__Group_3_0__0 )
                    {
                     before(grammarAccess.getSimpleReferenceAccess().getGroup_3_0()); 
                    // InternalDOF.g:769:3: ( rule__SimpleReference__Group_3_0__0 )
                    // InternalDOF.g:769:4: rule__SimpleReference__Group_3_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_3_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSimpleReferenceAccess().getGroup_3_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:773:2: ( ( rule__SimpleReference__Group_3_1__0 ) )
                    {
                    // InternalDOF.g:773:2: ( ( rule__SimpleReference__Group_3_1__0 ) )
                    // InternalDOF.g:774:3: ( rule__SimpleReference__Group_3_1__0 )
                    {
                     before(grammarAccess.getSimpleReferenceAccess().getGroup_3_1()); 
                    // InternalDOF.g:775:3: ( rule__SimpleReference__Group_3_1__0 )
                    // InternalDOF.g:775:4: rule__SimpleReference__Group_3_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_3_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getSimpleReferenceAccess().getGroup_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Alternatives_3"


    // $ANTLR start "rule__AggFunction__Alternatives"
    // InternalDOF.g:783:1: rule__AggFunction__Alternatives : ( ( 'count' ) | ( 'sum' ) | ( 'avg' ) | ( 'max' ) | ( 'min' ) );
    public final void rule__AggFunction__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:787:1: ( ( 'count' ) | ( 'sum' ) | ( 'avg' ) | ( 'max' ) | ( 'min' ) )
            int alt5=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt5=1;
                }
                break;
            case 12:
                {
                alt5=2;
                }
                break;
            case 13:
                {
                alt5=3;
                }
                break;
            case 14:
                {
                alt5=4;
                }
                break;
            case 15:
                {
                alt5=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalDOF.g:788:2: ( 'count' )
                    {
                    // InternalDOF.g:788:2: ( 'count' )
                    // InternalDOF.g:789:3: 'count'
                    {
                     before(grammarAccess.getAggFunctionAccess().getCountKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getCountKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:794:2: ( 'sum' )
                    {
                    // InternalDOF.g:794:2: ( 'sum' )
                    // InternalDOF.g:795:3: 'sum'
                    {
                     before(grammarAccess.getAggFunctionAccess().getSumKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getSumKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:800:2: ( 'avg' )
                    {
                    // InternalDOF.g:800:2: ( 'avg' )
                    // InternalDOF.g:801:3: 'avg'
                    {
                     before(grammarAccess.getAggFunctionAccess().getAvgKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getAvgKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDOF.g:806:2: ( 'max' )
                    {
                    // InternalDOF.g:806:2: ( 'max' )
                    // InternalDOF.g:807:3: 'max'
                    {
                     before(grammarAccess.getAggFunctionAccess().getMaxKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getMaxKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDOF.g:812:2: ( 'min' )
                    {
                    // InternalDOF.g:812:2: ( 'min' )
                    // InternalDOF.g:813:3: 'min'
                    {
                     before(grammarAccess.getAggFunctionAccess().getMinKeyword_4()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getAggFunctionAccess().getMinKeyword_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggFunction__Alternatives"


    // $ANTLR start "rule__TypeFilter__Alternatives"
    // InternalDOF.g:822:1: rule__TypeFilter__Alternatives : ( ( ruleTypeCompletion ) | ( ruleTypeSelection ) );
    public final void rule__TypeFilter__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:826:1: ( ( ruleTypeCompletion ) | ( ruleTypeSelection ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==27) ) {
                alt6=1;
            }
            else if ( (LA6_0==30) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalDOF.g:827:2: ( ruleTypeCompletion )
                    {
                    // InternalDOF.g:827:2: ( ruleTypeCompletion )
                    // InternalDOF.g:828:3: ruleTypeCompletion
                    {
                     before(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeCompletion();

                    state._fsp--;

                     after(grammarAccess.getTypeFilterAccess().getTypeCompletionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:833:2: ( ruleTypeSelection )
                    {
                    // InternalDOF.g:833:2: ( ruleTypeSelection )
                    // InternalDOF.g:834:3: ruleTypeSelection
                    {
                     before(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleTypeSelection();

                    state._fsp--;

                     after(grammarAccess.getTypeFilterAccess().getTypeSelectionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeFilter__Alternatives"


    // $ANTLR start "rule__Primary__Alternatives"
    // InternalDOF.g:843:1: rule__Primary__Alternatives : ( ( ruleComparison ) | ( ( rule__Primary__Group_1__0 ) ) );
    public final void rule__Primary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:847:1: ( ( ruleComparison ) | ( ( rule__Primary__Group_1__0 ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==28) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalDOF.g:848:2: ( ruleComparison )
                    {
                    // InternalDOF.g:848:2: ( ruleComparison )
                    // InternalDOF.g:849:3: ruleComparison
                    {
                     before(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleComparison();

                    state._fsp--;

                     after(grammarAccess.getPrimaryAccess().getComparisonParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:854:2: ( ( rule__Primary__Group_1__0 ) )
                    {
                    // InternalDOF.g:854:2: ( ( rule__Primary__Group_1__0 ) )
                    // InternalDOF.g:855:3: ( rule__Primary__Group_1__0 )
                    {
                     before(grammarAccess.getPrimaryAccess().getGroup_1()); 
                    // InternalDOF.g:856:3: ( rule__Primary__Group_1__0 )
                    // InternalDOF.g:856:4: rule__Primary__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Primary__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Alternatives"


    // $ANTLR start "rule__Comparison__Alternatives"
    // InternalDOF.g:864:1: rule__Comparison__Alternatives : ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) );
    public final void rule__Comparison__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:868:1: ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) )
            int alt8=6;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // InternalDOF.g:869:2: ( ( rule__Comparison__Group_0__0 ) )
                    {
                    // InternalDOF.g:869:2: ( ( rule__Comparison__Group_0__0 ) )
                    // InternalDOF.g:870:3: ( rule__Comparison__Group_0__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_0()); 
                    // InternalDOF.g:871:3: ( rule__Comparison__Group_0__0 )
                    // InternalDOF.g:871:4: rule__Comparison__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:875:2: ( ( rule__Comparison__Group_1__0 ) )
                    {
                    // InternalDOF.g:875:2: ( ( rule__Comparison__Group_1__0 ) )
                    // InternalDOF.g:876:3: ( rule__Comparison__Group_1__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_1()); 
                    // InternalDOF.g:877:3: ( rule__Comparison__Group_1__0 )
                    // InternalDOF.g:877:4: rule__Comparison__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:881:2: ( ( rule__Comparison__Group_2__0 ) )
                    {
                    // InternalDOF.g:881:2: ( ( rule__Comparison__Group_2__0 ) )
                    // InternalDOF.g:882:3: ( rule__Comparison__Group_2__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_2()); 
                    // InternalDOF.g:883:3: ( rule__Comparison__Group_2__0 )
                    // InternalDOF.g:883:4: rule__Comparison__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDOF.g:887:2: ( ( rule__Comparison__Group_3__0 ) )
                    {
                    // InternalDOF.g:887:2: ( ( rule__Comparison__Group_3__0 ) )
                    // InternalDOF.g:888:3: ( rule__Comparison__Group_3__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_3()); 
                    // InternalDOF.g:889:3: ( rule__Comparison__Group_3__0 )
                    // InternalDOF.g:889:4: rule__Comparison__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDOF.g:893:2: ( ( rule__Comparison__Group_4__0 ) )
                    {
                    // InternalDOF.g:893:2: ( ( rule__Comparison__Group_4__0 ) )
                    // InternalDOF.g:894:3: ( rule__Comparison__Group_4__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_4()); 
                    // InternalDOF.g:895:3: ( rule__Comparison__Group_4__0 )
                    // InternalDOF.g:895:4: rule__Comparison__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDOF.g:899:2: ( ( rule__Comparison__Group_5__0 ) )
                    {
                    // InternalDOF.g:899:2: ( ( rule__Comparison__Group_5__0 ) )
                    // InternalDOF.g:900:3: ( rule__Comparison__Group_5__0 )
                    {
                     before(grammarAccess.getComparisonAccess().getGroup_5()); 
                    // InternalDOF.g:901:3: ( rule__Comparison__Group_5__0 )
                    // InternalDOF.g:901:4: rule__Comparison__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Comparison__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getComparisonAccess().getGroup_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Alternatives"


    // $ANTLR start "rule__Cause__Alternatives"
    // InternalDOF.g:909:1: rule__Cause__Alternatives : ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) );
    public final void rule__Cause__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:913:1: ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) )
            int alt9=3;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // InternalDOF.g:914:2: ( ruleCompoundCause )
                    {
                    // InternalDOF.g:914:2: ( ruleCompoundCause )
                    // InternalDOF.g:915:3: ruleCompoundCause
                    {
                     before(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleCompoundCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:920:2: ( ruleDataLinkedCause )
                    {
                    // InternalDOF.g:920:2: ( ruleDataLinkedCause )
                    // InternalDOF.g:921:3: ruleDataLinkedCause
                    {
                     before(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleDataLinkedCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:926:2: ( ruleNotMappedCause )
                    {
                    // InternalDOF.g:926:2: ( ruleNotMappedCause )
                    // InternalDOF.g:927:3: ruleNotMappedCause
                    {
                     before(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNotMappedCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Alternatives"


    // $ANTLR start "rule__DOF__Group__0"
    // InternalDOF.g:936:1: rule__DOF__Group__0 : rule__DOF__Group__0__Impl rule__DOF__Group__1 ;
    public final void rule__DOF__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:940:1: ( rule__DOF__Group__0__Impl rule__DOF__Group__1 )
            // InternalDOF.g:941:2: rule__DOF__Group__0__Impl rule__DOF__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__DOF__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__0"


    // $ANTLR start "rule__DOF__Group__0__Impl"
    // InternalDOF.g:948:1: rule__DOF__Group__0__Impl : ( ( rule__DOF__ImportsAssignment_0 )* ) ;
    public final void rule__DOF__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:952:1: ( ( ( rule__DOF__ImportsAssignment_0 )* ) )
            // InternalDOF.g:953:1: ( ( rule__DOF__ImportsAssignment_0 )* )
            {
            // InternalDOF.g:953:1: ( ( rule__DOF__ImportsAssignment_0 )* )
            // InternalDOF.g:954:2: ( rule__DOF__ImportsAssignment_0 )*
            {
             before(grammarAccess.getDOFAccess().getImportsAssignment_0()); 
            // InternalDOF.g:955:2: ( rule__DOF__ImportsAssignment_0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==17) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalDOF.g:955:3: rule__DOF__ImportsAssignment_0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__DOF__ImportsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getDOFAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__0__Impl"


    // $ANTLR start "rule__DOF__Group__1"
    // InternalDOF.g:963:1: rule__DOF__Group__1 : rule__DOF__Group__1__Impl rule__DOF__Group__2 ;
    public final void rule__DOF__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:967:1: ( rule__DOF__Group__1__Impl rule__DOF__Group__2 )
            // InternalDOF.g:968:2: rule__DOF__Group__1__Impl rule__DOF__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__DOF__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__1"


    // $ANTLR start "rule__DOF__Group__1__Impl"
    // InternalDOF.g:975:1: rule__DOF__Group__1__Impl : ( 'dof' ) ;
    public final void rule__DOF__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:979:1: ( ( 'dof' ) )
            // InternalDOF.g:980:1: ( 'dof' )
            {
            // InternalDOF.g:980:1: ( 'dof' )
            // InternalDOF.g:981:2: 'dof'
            {
             before(grammarAccess.getDOFAccess().getDofKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getDOFAccess().getDofKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__1__Impl"


    // $ANTLR start "rule__DOF__Group__2"
    // InternalDOF.g:990:1: rule__DOF__Group__2 : rule__DOF__Group__2__Impl rule__DOF__Group__3 ;
    public final void rule__DOF__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:994:1: ( rule__DOF__Group__2__Impl rule__DOF__Group__3 )
            // InternalDOF.g:995:2: rule__DOF__Group__2__Impl rule__DOF__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__DOF__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__2"


    // $ANTLR start "rule__DOF__Group__2__Impl"
    // InternalDOF.g:1002:1: rule__DOF__Group__2__Impl : ( ( rule__DOF__NameAssignment_2 ) ) ;
    public final void rule__DOF__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1006:1: ( ( ( rule__DOF__NameAssignment_2 ) ) )
            // InternalDOF.g:1007:1: ( ( rule__DOF__NameAssignment_2 ) )
            {
            // InternalDOF.g:1007:1: ( ( rule__DOF__NameAssignment_2 ) )
            // InternalDOF.g:1008:2: ( rule__DOF__NameAssignment_2 )
            {
             before(grammarAccess.getDOFAccess().getNameAssignment_2()); 
            // InternalDOF.g:1009:2: ( rule__DOF__NameAssignment_2 )
            // InternalDOF.g:1009:3: rule__DOF__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DOF__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__2__Impl"


    // $ANTLR start "rule__DOF__Group__3"
    // InternalDOF.g:1017:1: rule__DOF__Group__3 : rule__DOF__Group__3__Impl ;
    public final void rule__DOF__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1021:1: ( rule__DOF__Group__3__Impl )
            // InternalDOF.g:1022:2: rule__DOF__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__3"


    // $ANTLR start "rule__DOF__Group__3__Impl"
    // InternalDOF.g:1028:1: rule__DOF__Group__3__Impl : ( ( rule__DOF__EffectAssignment_3 ) ) ;
    public final void rule__DOF__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1032:1: ( ( ( rule__DOF__EffectAssignment_3 ) ) )
            // InternalDOF.g:1033:1: ( ( rule__DOF__EffectAssignment_3 ) )
            {
            // InternalDOF.g:1033:1: ( ( rule__DOF__EffectAssignment_3 ) )
            // InternalDOF.g:1034:2: ( rule__DOF__EffectAssignment_3 )
            {
             before(grammarAccess.getDOFAccess().getEffectAssignment_3()); 
            // InternalDOF.g:1035:2: ( rule__DOF__EffectAssignment_3 )
            // InternalDOF.g:1035:3: rule__DOF__EffectAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__DOF__EffectAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getEffectAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__3__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalDOF.g:1044:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1048:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalDOF.g:1049:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalDOF.g:1056:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1060:1: ( ( 'import' ) )
            // InternalDOF.g:1061:1: ( 'import' )
            {
            // InternalDOF.g:1061:1: ( 'import' )
            // InternalDOF.g:1062:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalDOF.g:1071:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1075:1: ( rule__Import__Group__1__Impl )
            // InternalDOF.g:1076:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalDOF.g:1082:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1086:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // InternalDOF.g:1087:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // InternalDOF.g:1087:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // InternalDOF.g:1088:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // InternalDOF.g:1089:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            // InternalDOF.g:1089:3: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__0"
    // InternalDOF.g:1098:1: rule__Effect__Group__0 : rule__Effect__Group__0__Impl rule__Effect__Group__1 ;
    public final void rule__Effect__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1102:1: ( rule__Effect__Group__0__Impl rule__Effect__Group__1 )
            // InternalDOF.g:1103:2: rule__Effect__Group__0__Impl rule__Effect__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Effect__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0"


    // $ANTLR start "rule__Effect__Group__0__Impl"
    // InternalDOF.g:1110:1: rule__Effect__Group__0__Impl : ( () ) ;
    public final void rule__Effect__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1114:1: ( ( () ) )
            // InternalDOF.g:1115:1: ( () )
            {
            // InternalDOF.g:1115:1: ( () )
            // InternalDOF.g:1116:2: ()
            {
             before(grammarAccess.getEffectAccess().getEffectAction_0()); 
            // InternalDOF.g:1117:2: ()
            // InternalDOF.g:1117:3: 
            {
            }

             after(grammarAccess.getEffectAccess().getEffectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0__Impl"


    // $ANTLR start "rule__Effect__Group__1"
    // InternalDOF.g:1125:1: rule__Effect__Group__1 : rule__Effect__Group__1__Impl rule__Effect__Group__2 ;
    public final void rule__Effect__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1129:1: ( rule__Effect__Group__1__Impl rule__Effect__Group__2 )
            // InternalDOF.g:1130:2: rule__Effect__Group__1__Impl rule__Effect__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Effect__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1"


    // $ANTLR start "rule__Effect__Group__1__Impl"
    // InternalDOF.g:1137:1: rule__Effect__Group__1__Impl : ( 'effect' ) ;
    public final void rule__Effect__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1141:1: ( ( 'effect' ) )
            // InternalDOF.g:1142:1: ( 'effect' )
            {
            // InternalDOF.g:1142:1: ( 'effect' )
            // InternalDOF.g:1143:2: 'effect'
            {
             before(grammarAccess.getEffectAccess().getEffectKeyword_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getEffectKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__2"
    // InternalDOF.g:1152:1: rule__Effect__Group__2 : rule__Effect__Group__2__Impl rule__Effect__Group__3 ;
    public final void rule__Effect__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1156:1: ( rule__Effect__Group__2__Impl rule__Effect__Group__3 )
            // InternalDOF.g:1157:2: rule__Effect__Group__2__Impl rule__Effect__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Effect__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2"


    // $ANTLR start "rule__Effect__Group__2__Impl"
    // InternalDOF.g:1164:1: rule__Effect__Group__2__Impl : ( ( rule__Effect__NameAssignment_2 ) ) ;
    public final void rule__Effect__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1168:1: ( ( ( rule__Effect__NameAssignment_2 ) ) )
            // InternalDOF.g:1169:1: ( ( rule__Effect__NameAssignment_2 ) )
            {
            // InternalDOF.g:1169:1: ( ( rule__Effect__NameAssignment_2 ) )
            // InternalDOF.g:1170:2: ( rule__Effect__NameAssignment_2 )
            {
             before(grammarAccess.getEffectAccess().getNameAssignment_2()); 
            // InternalDOF.g:1171:2: ( rule__Effect__NameAssignment_2 )
            // InternalDOF.g:1171:3: rule__Effect__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Effect__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2__Impl"


    // $ANTLR start "rule__Effect__Group__3"
    // InternalDOF.g:1179:1: rule__Effect__Group__3 : rule__Effect__Group__3__Impl rule__Effect__Group__4 ;
    public final void rule__Effect__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1183:1: ( rule__Effect__Group__3__Impl rule__Effect__Group__4 )
            // InternalDOF.g:1184:2: rule__Effect__Group__3__Impl rule__Effect__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Effect__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3"


    // $ANTLR start "rule__Effect__Group__3__Impl"
    // InternalDOF.g:1191:1: rule__Effect__Group__3__Impl : ( 'is' ) ;
    public final void rule__Effect__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1195:1: ( ( 'is' ) )
            // InternalDOF.g:1196:1: ( 'is' )
            {
            // InternalDOF.g:1196:1: ( 'is' )
            // InternalDOF.g:1197:2: 'is'
            {
             before(grammarAccess.getEffectAccess().getIsKeyword_3()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getIsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3__Impl"


    // $ANTLR start "rule__Effect__Group__4"
    // InternalDOF.g:1206:1: rule__Effect__Group__4 : rule__Effect__Group__4__Impl rule__Effect__Group__5 ;
    public final void rule__Effect__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1210:1: ( rule__Effect__Group__4__Impl rule__Effect__Group__5 )
            // InternalDOF.g:1211:2: rule__Effect__Group__4__Impl rule__Effect__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__Effect__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4"


    // $ANTLR start "rule__Effect__Group__4__Impl"
    // InternalDOF.g:1218:1: rule__Effect__Group__4__Impl : ( ( rule__Effect__DataFeederAssignment_4 ) ) ;
    public final void rule__Effect__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1222:1: ( ( ( rule__Effect__DataFeederAssignment_4 ) ) )
            // InternalDOF.g:1223:1: ( ( rule__Effect__DataFeederAssignment_4 ) )
            {
            // InternalDOF.g:1223:1: ( ( rule__Effect__DataFeederAssignment_4 ) )
            // InternalDOF.g:1224:2: ( rule__Effect__DataFeederAssignment_4 )
            {
             before(grammarAccess.getEffectAccess().getDataFeederAssignment_4()); 
            // InternalDOF.g:1225:2: ( rule__Effect__DataFeederAssignment_4 )
            // InternalDOF.g:1225:3: rule__Effect__DataFeederAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Effect__DataFeederAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getDataFeederAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4__Impl"


    // $ANTLR start "rule__Effect__Group__5"
    // InternalDOF.g:1233:1: rule__Effect__Group__5 : rule__Effect__Group__5__Impl rule__Effect__Group__6 ;
    public final void rule__Effect__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1237:1: ( rule__Effect__Group__5__Impl rule__Effect__Group__6 )
            // InternalDOF.g:1238:2: rule__Effect__Group__5__Impl rule__Effect__Group__6
            {
            pushFollow(FOLLOW_10);
            rule__Effect__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__5"


    // $ANTLR start "rule__Effect__Group__5__Impl"
    // InternalDOF.g:1245:1: rule__Effect__Group__5__Impl : ( ( rule__Effect__CategoriesAssignment_5 ) ) ;
    public final void rule__Effect__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1249:1: ( ( ( rule__Effect__CategoriesAssignment_5 ) ) )
            // InternalDOF.g:1250:1: ( ( rule__Effect__CategoriesAssignment_5 ) )
            {
            // InternalDOF.g:1250:1: ( ( rule__Effect__CategoriesAssignment_5 ) )
            // InternalDOF.g:1251:2: ( rule__Effect__CategoriesAssignment_5 )
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_5()); 
            // InternalDOF.g:1252:2: ( rule__Effect__CategoriesAssignment_5 )
            // InternalDOF.g:1252:3: rule__Effect__CategoriesAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Effect__CategoriesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__5__Impl"


    // $ANTLR start "rule__Effect__Group__6"
    // InternalDOF.g:1260:1: rule__Effect__Group__6 : rule__Effect__Group__6__Impl ;
    public final void rule__Effect__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1264:1: ( rule__Effect__Group__6__Impl )
            // InternalDOF.g:1265:2: rule__Effect__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__6"


    // $ANTLR start "rule__Effect__Group__6__Impl"
    // InternalDOF.g:1271:1: rule__Effect__Group__6__Impl : ( ( rule__Effect__CategoriesAssignment_6 )* ) ;
    public final void rule__Effect__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1275:1: ( ( ( rule__Effect__CategoriesAssignment_6 )* ) )
            // InternalDOF.g:1276:1: ( ( rule__Effect__CategoriesAssignment_6 )* )
            {
            // InternalDOF.g:1276:1: ( ( rule__Effect__CategoriesAssignment_6 )* )
            // InternalDOF.g:1277:2: ( rule__Effect__CategoriesAssignment_6 )*
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_6()); 
            // InternalDOF.g:1278:2: ( rule__Effect__CategoriesAssignment_6 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==20) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalDOF.g:1278:3: rule__Effect__CategoriesAssignment_6
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Effect__CategoriesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__6__Impl"


    // $ANTLR start "rule__Category__Group__0"
    // InternalDOF.g:1287:1: rule__Category__Group__0 : rule__Category__Group__0__Impl rule__Category__Group__1 ;
    public final void rule__Category__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1291:1: ( rule__Category__Group__0__Impl rule__Category__Group__1 )
            // InternalDOF.g:1292:2: rule__Category__Group__0__Impl rule__Category__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Category__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0"


    // $ANTLR start "rule__Category__Group__0__Impl"
    // InternalDOF.g:1299:1: rule__Category__Group__0__Impl : ( () ) ;
    public final void rule__Category__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1303:1: ( ( () ) )
            // InternalDOF.g:1304:1: ( () )
            {
            // InternalDOF.g:1304:1: ( () )
            // InternalDOF.g:1305:2: ()
            {
             before(grammarAccess.getCategoryAccess().getCategoryAction_0()); 
            // InternalDOF.g:1306:2: ()
            // InternalDOF.g:1306:3: 
            {
            }

             after(grammarAccess.getCategoryAccess().getCategoryAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0__Impl"


    // $ANTLR start "rule__Category__Group__1"
    // InternalDOF.g:1314:1: rule__Category__Group__1 : rule__Category__Group__1__Impl rule__Category__Group__2 ;
    public final void rule__Category__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1318:1: ( rule__Category__Group__1__Impl rule__Category__Group__2 )
            // InternalDOF.g:1319:2: rule__Category__Group__1__Impl rule__Category__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Category__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1"


    // $ANTLR start "rule__Category__Group__1__Impl"
    // InternalDOF.g:1326:1: rule__Category__Group__1__Impl : ( 'category' ) ;
    public final void rule__Category__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1330:1: ( ( 'category' ) )
            // InternalDOF.g:1331:1: ( 'category' )
            {
            // InternalDOF.g:1331:1: ( 'category' )
            // InternalDOF.g:1332:2: 'category'
            {
             before(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getCategoryKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1__Impl"


    // $ANTLR start "rule__Category__Group__2"
    // InternalDOF.g:1341:1: rule__Category__Group__2 : rule__Category__Group__2__Impl rule__Category__Group__3 ;
    public final void rule__Category__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1345:1: ( rule__Category__Group__2__Impl rule__Category__Group__3 )
            // InternalDOF.g:1346:2: rule__Category__Group__2__Impl rule__Category__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__Category__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2"


    // $ANTLR start "rule__Category__Group__2__Impl"
    // InternalDOF.g:1353:1: rule__Category__Group__2__Impl : ( ( rule__Category__NameAssignment_2 ) ) ;
    public final void rule__Category__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1357:1: ( ( ( rule__Category__NameAssignment_2 ) ) )
            // InternalDOF.g:1358:1: ( ( rule__Category__NameAssignment_2 ) )
            {
            // InternalDOF.g:1358:1: ( ( rule__Category__NameAssignment_2 ) )
            // InternalDOF.g:1359:2: ( rule__Category__NameAssignment_2 )
            {
             before(grammarAccess.getCategoryAccess().getNameAssignment_2()); 
            // InternalDOF.g:1360:2: ( rule__Category__NameAssignment_2 )
            // InternalDOF.g:1360:3: rule__Category__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Category__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2__Impl"


    // $ANTLR start "rule__Category__Group__3"
    // InternalDOF.g:1368:1: rule__Category__Group__3 : rule__Category__Group__3__Impl rule__Category__Group__4 ;
    public final void rule__Category__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1372:1: ( rule__Category__Group__3__Impl rule__Category__Group__4 )
            // InternalDOF.g:1373:2: rule__Category__Group__3__Impl rule__Category__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__Category__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3"


    // $ANTLR start "rule__Category__Group__3__Impl"
    // InternalDOF.g:1380:1: rule__Category__Group__3__Impl : ( ( rule__Category__CausesAssignment_3 ) ) ;
    public final void rule__Category__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1384:1: ( ( ( rule__Category__CausesAssignment_3 ) ) )
            // InternalDOF.g:1385:1: ( ( rule__Category__CausesAssignment_3 ) )
            {
            // InternalDOF.g:1385:1: ( ( rule__Category__CausesAssignment_3 ) )
            // InternalDOF.g:1386:2: ( rule__Category__CausesAssignment_3 )
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_3()); 
            // InternalDOF.g:1387:2: ( rule__Category__CausesAssignment_3 )
            // InternalDOF.g:1387:3: rule__Category__CausesAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Category__CausesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getCausesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3__Impl"


    // $ANTLR start "rule__Category__Group__4"
    // InternalDOF.g:1395:1: rule__Category__Group__4 : rule__Category__Group__4__Impl ;
    public final void rule__Category__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1399:1: ( rule__Category__Group__4__Impl )
            // InternalDOF.g:1400:2: rule__Category__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4"


    // $ANTLR start "rule__Category__Group__4__Impl"
    // InternalDOF.g:1406:1: rule__Category__Group__4__Impl : ( ( rule__Category__CausesAssignment_4 )* ) ;
    public final void rule__Category__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1410:1: ( ( ( rule__Category__CausesAssignment_4 )* ) )
            // InternalDOF.g:1411:1: ( ( rule__Category__CausesAssignment_4 )* )
            {
            // InternalDOF.g:1411:1: ( ( rule__Category__CausesAssignment_4 )* )
            // InternalDOF.g:1412:2: ( rule__Category__CausesAssignment_4 )*
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 
            // InternalDOF.g:1413:2: ( rule__Category__CausesAssignment_4 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==43) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalDOF.g:1413:3: rule__Category__CausesAssignment_4
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Category__CausesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4__Impl"


    // $ANTLR start "rule__DataFeeder__Group__0"
    // InternalDOF.g:1422:1: rule__DataFeeder__Group__0 : rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1 ;
    public final void rule__DataFeeder__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1426:1: ( rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1 )
            // InternalDOF.g:1427:2: rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__DataFeeder__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__0"


    // $ANTLR start "rule__DataFeeder__Group__0__Impl"
    // InternalDOF.g:1434:1: rule__DataFeeder__Group__0__Impl : ( () ) ;
    public final void rule__DataFeeder__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1438:1: ( ( () ) )
            // InternalDOF.g:1439:1: ( () )
            {
            // InternalDOF.g:1439:1: ( () )
            // InternalDOF.g:1440:2: ()
            {
             before(grammarAccess.getDataFeederAccess().getDataFeederAction_0()); 
            // InternalDOF.g:1441:2: ()
            // InternalDOF.g:1441:3: 
            {
            }

             after(grammarAccess.getDataFeederAccess().getDataFeederAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__0__Impl"


    // $ANTLR start "rule__DataFeeder__Group__1"
    // InternalDOF.g:1449:1: rule__DataFeeder__Group__1 : rule__DataFeeder__Group__1__Impl rule__DataFeeder__Group__2 ;
    public final void rule__DataFeeder__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1453:1: ( rule__DataFeeder__Group__1__Impl rule__DataFeeder__Group__2 )
            // InternalDOF.g:1454:2: rule__DataFeeder__Group__1__Impl rule__DataFeeder__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__1"


    // $ANTLR start "rule__DataFeeder__Group__1__Impl"
    // InternalDOF.g:1461:1: rule__DataFeeder__Group__1__Impl : ( ( rule__DataFeeder__NameAssignment_1 ) ) ;
    public final void rule__DataFeeder__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1465:1: ( ( ( rule__DataFeeder__NameAssignment_1 ) ) )
            // InternalDOF.g:1466:1: ( ( rule__DataFeeder__NameAssignment_1 ) )
            {
            // InternalDOF.g:1466:1: ( ( rule__DataFeeder__NameAssignment_1 ) )
            // InternalDOF.g:1467:2: ( rule__DataFeeder__NameAssignment_1 )
            {
             before(grammarAccess.getDataFeederAccess().getNameAssignment_1()); 
            // InternalDOF.g:1468:2: ( rule__DataFeeder__NameAssignment_1 )
            // InternalDOF.g:1468:3: rule__DataFeeder__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__1__Impl"


    // $ANTLR start "rule__DataFeeder__Group__2"
    // InternalDOF.g:1476:1: rule__DataFeeder__Group__2 : rule__DataFeeder__Group__2__Impl rule__DataFeeder__Group__3 ;
    public final void rule__DataFeeder__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1480:1: ( rule__DataFeeder__Group__2__Impl rule__DataFeeder__Group__3 )
            // InternalDOF.g:1481:2: rule__DataFeeder__Group__2__Impl rule__DataFeeder__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__2"


    // $ANTLR start "rule__DataFeeder__Group__2__Impl"
    // InternalDOF.g:1488:1: rule__DataFeeder__Group__2__Impl : ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? ) ;
    public final void rule__DataFeeder__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1492:1: ( ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? ) )
            // InternalDOF.g:1493:1: ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? )
            {
            // InternalDOF.g:1493:1: ( ( rule__DataFeeder__AttributeFilterAssignment_2 )? )
            // InternalDOF.g:1494:2: ( rule__DataFeeder__AttributeFilterAssignment_2 )?
            {
             before(grammarAccess.getDataFeederAccess().getAttributeFilterAssignment_2()); 
            // InternalDOF.g:1495:2: ( rule__DataFeeder__AttributeFilterAssignment_2 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==31) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalDOF.g:1495:3: rule__DataFeeder__AttributeFilterAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataFeeder__AttributeFilterAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataFeederAccess().getAttributeFilterAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__2__Impl"


    // $ANTLR start "rule__DataFeeder__Group__3"
    // InternalDOF.g:1503:1: rule__DataFeeder__Group__3 : rule__DataFeeder__Group__3__Impl rule__DataFeeder__Group__4 ;
    public final void rule__DataFeeder__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1507:1: ( rule__DataFeeder__Group__3__Impl rule__DataFeeder__Group__4 )
            // InternalDOF.g:1508:2: rule__DataFeeder__Group__3__Impl rule__DataFeeder__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__3"


    // $ANTLR start "rule__DataFeeder__Group__3__Impl"
    // InternalDOF.g:1515:1: rule__DataFeeder__Group__3__Impl : ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? ) ;
    public final void rule__DataFeeder__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1519:1: ( ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? ) )
            // InternalDOF.g:1520:1: ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? )
            {
            // InternalDOF.g:1520:1: ( ( rule__DataFeeder__InstancesFilterAssignment_3 )? )
            // InternalDOF.g:1521:2: ( rule__DataFeeder__InstancesFilterAssignment_3 )?
            {
             before(grammarAccess.getDataFeederAccess().getInstancesFilterAssignment_3()); 
            // InternalDOF.g:1522:2: ( rule__DataFeeder__InstancesFilterAssignment_3 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==34) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalDOF.g:1522:3: rule__DataFeeder__InstancesFilterAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataFeeder__InstancesFilterAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataFeederAccess().getInstancesFilterAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__3__Impl"


    // $ANTLR start "rule__DataFeeder__Group__4"
    // InternalDOF.g:1530:1: rule__DataFeeder__Group__4 : rule__DataFeeder__Group__4__Impl rule__DataFeeder__Group__5 ;
    public final void rule__DataFeeder__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1534:1: ( rule__DataFeeder__Group__4__Impl rule__DataFeeder__Group__5 )
            // InternalDOF.g:1535:2: rule__DataFeeder__Group__4__Impl rule__DataFeeder__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__DataFeeder__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__4"


    // $ANTLR start "rule__DataFeeder__Group__4__Impl"
    // InternalDOF.g:1542:1: rule__DataFeeder__Group__4__Impl : ( ( rule__DataFeeder__Group_4__0 )* ) ;
    public final void rule__DataFeeder__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1546:1: ( ( ( rule__DataFeeder__Group_4__0 )* ) )
            // InternalDOF.g:1547:1: ( ( rule__DataFeeder__Group_4__0 )* )
            {
            // InternalDOF.g:1547:1: ( ( rule__DataFeeder__Group_4__0 )* )
            // InternalDOF.g:1548:2: ( rule__DataFeeder__Group_4__0 )*
            {
             before(grammarAccess.getDataFeederAccess().getGroup_4()); 
            // InternalDOF.g:1549:2: ( rule__DataFeeder__Group_4__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==21) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalDOF.g:1549:3: rule__DataFeeder__Group_4__0
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__DataFeeder__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getDataFeederAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__4__Impl"


    // $ANTLR start "rule__DataFeeder__Group__5"
    // InternalDOF.g:1557:1: rule__DataFeeder__Group__5 : rule__DataFeeder__Group__5__Impl ;
    public final void rule__DataFeeder__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1561:1: ( rule__DataFeeder__Group__5__Impl )
            // InternalDOF.g:1562:2: rule__DataFeeder__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__5"


    // $ANTLR start "rule__DataFeeder__Group__5__Impl"
    // InternalDOF.g:1568:1: rule__DataFeeder__Group__5__Impl : ( ( rule__DataFeeder__TypeFilterAssignment_5 )? ) ;
    public final void rule__DataFeeder__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1572:1: ( ( ( rule__DataFeeder__TypeFilterAssignment_5 )? ) )
            // InternalDOF.g:1573:1: ( ( rule__DataFeeder__TypeFilterAssignment_5 )? )
            {
            // InternalDOF.g:1573:1: ( ( rule__DataFeeder__TypeFilterAssignment_5 )? )
            // InternalDOF.g:1574:2: ( rule__DataFeeder__TypeFilterAssignment_5 )?
            {
             before(grammarAccess.getDataFeederAccess().getTypeFilterAssignment_5()); 
            // InternalDOF.g:1575:2: ( rule__DataFeeder__TypeFilterAssignment_5 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==27||LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDOF.g:1575:3: rule__DataFeeder__TypeFilterAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataFeeder__TypeFilterAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataFeederAccess().getTypeFilterAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__5__Impl"


    // $ANTLR start "rule__DataFeeder__Group_4__0"
    // InternalDOF.g:1584:1: rule__DataFeeder__Group_4__0 : rule__DataFeeder__Group_4__0__Impl rule__DataFeeder__Group_4__1 ;
    public final void rule__DataFeeder__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1588:1: ( rule__DataFeeder__Group_4__0__Impl rule__DataFeeder__Group_4__1 )
            // InternalDOF.g:1589:2: rule__DataFeeder__Group_4__0__Impl rule__DataFeeder__Group_4__1
            {
            pushFollow(FOLLOW_16);
            rule__DataFeeder__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__0"


    // $ANTLR start "rule__DataFeeder__Group_4__0__Impl"
    // InternalDOF.g:1596:1: rule__DataFeeder__Group_4__0__Impl : ( 'include' ) ;
    public final void rule__DataFeeder__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1600:1: ( ( 'include' ) )
            // InternalDOF.g:1601:1: ( 'include' )
            {
            // InternalDOF.g:1601:1: ( 'include' )
            // InternalDOF.g:1602:2: 'include'
            {
             before(grammarAccess.getDataFeederAccess().getIncludeKeyword_4_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getDataFeederAccess().getIncludeKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__0__Impl"


    // $ANTLR start "rule__DataFeeder__Group_4__1"
    // InternalDOF.g:1611:1: rule__DataFeeder__Group_4__1 : rule__DataFeeder__Group_4__1__Impl ;
    public final void rule__DataFeeder__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1615:1: ( rule__DataFeeder__Group_4__1__Impl )
            // InternalDOF.g:1616:2: rule__DataFeeder__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__1"


    // $ANTLR start "rule__DataFeeder__Group_4__1__Impl"
    // InternalDOF.g:1622:1: rule__DataFeeder__Group_4__1__Impl : ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) ) ;
    public final void rule__DataFeeder__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1626:1: ( ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) ) )
            // InternalDOF.g:1627:1: ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) )
            {
            // InternalDOF.g:1627:1: ( ( rule__DataFeeder__IncludedReferencesAssignment_4_1 ) )
            // InternalDOF.g:1628:2: ( rule__DataFeeder__IncludedReferencesAssignment_4_1 )
            {
             before(grammarAccess.getDataFeederAccess().getIncludedReferencesAssignment_4_1()); 
            // InternalDOF.g:1629:2: ( rule__DataFeeder__IncludedReferencesAssignment_4_1 )
            // InternalDOF.g:1629:3: rule__DataFeeder__IncludedReferencesAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__IncludedReferencesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getIncludedReferencesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group_4__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group__0"
    // InternalDOF.g:1638:1: rule__SimpleReference__Group__0 : rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1 ;
    public final void rule__SimpleReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1642:1: ( rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1 )
            // InternalDOF.g:1643:2: rule__SimpleReference__Group__0__Impl rule__SimpleReference__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__0"


    // $ANTLR start "rule__SimpleReference__Group__0__Impl"
    // InternalDOF.g:1650:1: rule__SimpleReference__Group__0__Impl : ( ( rule__SimpleReference__NameAssignment_0 ) ) ;
    public final void rule__SimpleReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1654:1: ( ( ( rule__SimpleReference__NameAssignment_0 ) ) )
            // InternalDOF.g:1655:1: ( ( rule__SimpleReference__NameAssignment_0 ) )
            {
            // InternalDOF.g:1655:1: ( ( rule__SimpleReference__NameAssignment_0 ) )
            // InternalDOF.g:1656:2: ( rule__SimpleReference__NameAssignment_0 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getNameAssignment_0()); 
            // InternalDOF.g:1657:2: ( rule__SimpleReference__NameAssignment_0 )
            // InternalDOF.g:1657:3: rule__SimpleReference__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group__1"
    // InternalDOF.g:1665:1: rule__SimpleReference__Group__1 : rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2 ;
    public final void rule__SimpleReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1669:1: ( rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2 )
            // InternalDOF.g:1670:2: rule__SimpleReference__Group__1__Impl rule__SimpleReference__Group__2
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__1"


    // $ANTLR start "rule__SimpleReference__Group__1__Impl"
    // InternalDOF.g:1677:1: rule__SimpleReference__Group__1__Impl : ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? ) ;
    public final void rule__SimpleReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1681:1: ( ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? ) )
            // InternalDOF.g:1682:1: ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? )
            {
            // InternalDOF.g:1682:1: ( ( rule__SimpleReference__AttributeFilterAssignment_1 )? )
            // InternalDOF.g:1683:2: ( rule__SimpleReference__AttributeFilterAssignment_1 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAssignment_1()); 
            // InternalDOF.g:1684:2: ( rule__SimpleReference__AttributeFilterAssignment_1 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==31) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDOF.g:1684:3: rule__SimpleReference__AttributeFilterAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__AttributeFilterAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group__2"
    // InternalDOF.g:1692:1: rule__SimpleReference__Group__2 : rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3 ;
    public final void rule__SimpleReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1696:1: ( rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3 )
            // InternalDOF.g:1697:2: rule__SimpleReference__Group__2__Impl rule__SimpleReference__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__SimpleReference__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__2"


    // $ANTLR start "rule__SimpleReference__Group__2__Impl"
    // InternalDOF.g:1704:1: rule__SimpleReference__Group__2__Impl : ( ( rule__SimpleReference__Group_2__0 )? ) ;
    public final void rule__SimpleReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1708:1: ( ( ( rule__SimpleReference__Group_2__0 )? ) )
            // InternalDOF.g:1709:1: ( ( rule__SimpleReference__Group_2__0 )? )
            {
            // InternalDOF.g:1709:1: ( ( rule__SimpleReference__Group_2__0 )? )
            // InternalDOF.g:1710:2: ( rule__SimpleReference__Group_2__0 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getGroup_2()); 
            // InternalDOF.g:1711:2: ( rule__SimpleReference__Group_2__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==22) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDOF.g:1711:3: rule__SimpleReference__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group__3"
    // InternalDOF.g:1719:1: rule__SimpleReference__Group__3 : rule__SimpleReference__Group__3__Impl ;
    public final void rule__SimpleReference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1723:1: ( rule__SimpleReference__Group__3__Impl )
            // InternalDOF.g:1724:2: rule__SimpleReference__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__3"


    // $ANTLR start "rule__SimpleReference__Group__3__Impl"
    // InternalDOF.g:1730:1: rule__SimpleReference__Group__3__Impl : ( ( rule__SimpleReference__Alternatives_3 )? ) ;
    public final void rule__SimpleReference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1734:1: ( ( ( rule__SimpleReference__Alternatives_3 )? ) )
            // InternalDOF.g:1735:1: ( ( rule__SimpleReference__Alternatives_3 )? )
            {
            // InternalDOF.g:1735:1: ( ( rule__SimpleReference__Alternatives_3 )? )
            // InternalDOF.g:1736:2: ( rule__SimpleReference__Alternatives_3 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getAlternatives_3()); 
            // InternalDOF.g:1737:2: ( rule__SimpleReference__Alternatives_3 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==23||LA19_0==25) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalDOF.g:1737:3: rule__SimpleReference__Alternatives_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__Alternatives_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getAlternatives_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group__3__Impl"


    // $ANTLR start "rule__SimpleReference__Group_2__0"
    // InternalDOF.g:1746:1: rule__SimpleReference__Group_2__0 : rule__SimpleReference__Group_2__0__Impl rule__SimpleReference__Group_2__1 ;
    public final void rule__SimpleReference__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1750:1: ( rule__SimpleReference__Group_2__0__Impl rule__SimpleReference__Group_2__1 )
            // InternalDOF.g:1751:2: rule__SimpleReference__Group_2__0__Impl rule__SimpleReference__Group_2__1
            {
            pushFollow(FOLLOW_7);
            rule__SimpleReference__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__0"


    // $ANTLR start "rule__SimpleReference__Group_2__0__Impl"
    // InternalDOF.g:1758:1: rule__SimpleReference__Group_2__0__Impl : ( 'by' ) ;
    public final void rule__SimpleReference__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1762:1: ( ( 'by' ) )
            // InternalDOF.g:1763:1: ( 'by' )
            {
            // InternalDOF.g:1763:1: ( 'by' )
            // InternalDOF.g:1764:2: 'by'
            {
             before(grammarAccess.getSimpleReferenceAccess().getByKeyword_2_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getByKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_2__1"
    // InternalDOF.g:1773:1: rule__SimpleReference__Group_2__1 : rule__SimpleReference__Group_2__1__Impl rule__SimpleReference__Group_2__2 ;
    public final void rule__SimpleReference__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1777:1: ( rule__SimpleReference__Group_2__1__Impl rule__SimpleReference__Group_2__2 )
            // InternalDOF.g:1778:2: rule__SimpleReference__Group_2__1__Impl rule__SimpleReference__Group_2__2
            {
            pushFollow(FOLLOW_18);
            rule__SimpleReference__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__1"


    // $ANTLR start "rule__SimpleReference__Group_2__1__Impl"
    // InternalDOF.g:1785:1: rule__SimpleReference__Group_2__1__Impl : ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) ) ;
    public final void rule__SimpleReference__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1789:1: ( ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) ) )
            // InternalDOF.g:1790:1: ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) )
            {
            // InternalDOF.g:1790:1: ( ( rule__SimpleReference__PivotingIdAssignment_2_1 ) )
            // InternalDOF.g:1791:2: ( rule__SimpleReference__PivotingIdAssignment_2_1 )
            {
             before(grammarAccess.getSimpleReferenceAccess().getPivotingIdAssignment_2_1()); 
            // InternalDOF.g:1792:2: ( rule__SimpleReference__PivotingIdAssignment_2_1 )
            // InternalDOF.g:1792:3: rule__SimpleReference__PivotingIdAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__PivotingIdAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSimpleReferenceAccess().getPivotingIdAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_2__2"
    // InternalDOF.g:1800:1: rule__SimpleReference__Group_2__2 : rule__SimpleReference__Group_2__2__Impl ;
    public final void rule__SimpleReference__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1804:1: ( rule__SimpleReference__Group_2__2__Impl )
            // InternalDOF.g:1805:2: rule__SimpleReference__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__2"


    // $ANTLR start "rule__SimpleReference__Group_2__2__Impl"
    // InternalDOF.g:1811:1: rule__SimpleReference__Group_2__2__Impl : ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? ) ;
    public final void rule__SimpleReference__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1815:1: ( ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? ) )
            // InternalDOF.g:1816:1: ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? )
            {
            // InternalDOF.g:1816:1: ( ( rule__SimpleReference__InstancesFilterAssignment_2_2 )? )
            // InternalDOF.g:1817:2: ( rule__SimpleReference__InstancesFilterAssignment_2_2 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getInstancesFilterAssignment_2_2()); 
            // InternalDOF.g:1818:2: ( rule__SimpleReference__InstancesFilterAssignment_2_2 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==34) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalDOF.g:1818:3: rule__SimpleReference__InstancesFilterAssignment_2_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__InstancesFilterAssignment_2_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getInstancesFilterAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_2__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_0__0"
    // InternalDOF.g:1827:1: rule__SimpleReference__Group_3_0__0 : rule__SimpleReference__Group_3_0__0__Impl rule__SimpleReference__Group_3_0__1 ;
    public final void rule__SimpleReference__Group_3_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1831:1: ( rule__SimpleReference__Group_3_0__0__Impl rule__SimpleReference__Group_3_0__1 )
            // InternalDOF.g:1832:2: rule__SimpleReference__Group_3_0__0__Impl rule__SimpleReference__Group_3_0__1
            {
            pushFollow(FOLLOW_19);
            rule__SimpleReference__Group_3_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__0"


    // $ANTLR start "rule__SimpleReference__Group_3_0__0__Impl"
    // InternalDOF.g:1839:1: rule__SimpleReference__Group_3_0__0__Impl : ( '{' ) ;
    public final void rule__SimpleReference__Group_3_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1843:1: ( ( '{' ) )
            // InternalDOF.g:1844:1: ( '{' )
            {
            // InternalDOF.g:1844:1: ( '{' )
            // InternalDOF.g:1845:2: '{'
            {
             before(grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_3_0_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getLeftCurlyBracketKeyword_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_0__1"
    // InternalDOF.g:1854:1: rule__SimpleReference__Group_3_0__1 : rule__SimpleReference__Group_3_0__1__Impl rule__SimpleReference__Group_3_0__2 ;
    public final void rule__SimpleReference__Group_3_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1858:1: ( rule__SimpleReference__Group_3_0__1__Impl rule__SimpleReference__Group_3_0__2 )
            // InternalDOF.g:1859:2: rule__SimpleReference__Group_3_0__1__Impl rule__SimpleReference__Group_3_0__2
            {
            pushFollow(FOLLOW_20);
            rule__SimpleReference__Group_3_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__1"


    // $ANTLR start "rule__SimpleReference__Group_3_0__1__Impl"
    // InternalDOF.g:1866:1: rule__SimpleReference__Group_3_0__1__Impl : ( 'include' ) ;
    public final void rule__SimpleReference__Group_3_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1870:1: ( ( 'include' ) )
            // InternalDOF.g:1871:1: ( 'include' )
            {
            // InternalDOF.g:1871:1: ( 'include' )
            // InternalDOF.g:1872:2: 'include'
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_3_0_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getIncludeKeyword_3_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_0__2"
    // InternalDOF.g:1881:1: rule__SimpleReference__Group_3_0__2 : rule__SimpleReference__Group_3_0__2__Impl rule__SimpleReference__Group_3_0__3 ;
    public final void rule__SimpleReference__Group_3_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1885:1: ( rule__SimpleReference__Group_3_0__2__Impl rule__SimpleReference__Group_3_0__3 )
            // InternalDOF.g:1886:2: rule__SimpleReference__Group_3_0__2__Impl rule__SimpleReference__Group_3_0__3
            {
            pushFollow(FOLLOW_20);
            rule__SimpleReference__Group_3_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__2"


    // $ANTLR start "rule__SimpleReference__Group_3_0__2__Impl"
    // InternalDOF.g:1893:1: rule__SimpleReference__Group_3_0__2__Impl : ( ( rule__SimpleReference__IncludedReferencesAssignment_3_0_2 )* ) ;
    public final void rule__SimpleReference__Group_3_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1897:1: ( ( ( rule__SimpleReference__IncludedReferencesAssignment_3_0_2 )* ) )
            // InternalDOF.g:1898:1: ( ( rule__SimpleReference__IncludedReferencesAssignment_3_0_2 )* )
            {
            // InternalDOF.g:1898:1: ( ( rule__SimpleReference__IncludedReferencesAssignment_3_0_2 )* )
            // InternalDOF.g:1899:2: ( rule__SimpleReference__IncludedReferencesAssignment_3_0_2 )*
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesAssignment_3_0_2()); 
            // InternalDOF.g:1900:2: ( rule__SimpleReference__IncludedReferencesAssignment_3_0_2 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==RULE_ID||LA21_0==26) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalDOF.g:1900:3: rule__SimpleReference__IncludedReferencesAssignment_3_0_2
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__SimpleReference__IncludedReferencesAssignment_3_0_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesAssignment_3_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__2__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_0__3"
    // InternalDOF.g:1908:1: rule__SimpleReference__Group_3_0__3 : rule__SimpleReference__Group_3_0__3__Impl rule__SimpleReference__Group_3_0__4 ;
    public final void rule__SimpleReference__Group_3_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1912:1: ( rule__SimpleReference__Group_3_0__3__Impl rule__SimpleReference__Group_3_0__4 )
            // InternalDOF.g:1913:2: rule__SimpleReference__Group_3_0__3__Impl rule__SimpleReference__Group_3_0__4
            {
            pushFollow(FOLLOW_20);
            rule__SimpleReference__Group_3_0__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_0__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__3"


    // $ANTLR start "rule__SimpleReference__Group_3_0__3__Impl"
    // InternalDOF.g:1920:1: rule__SimpleReference__Group_3_0__3__Impl : ( ( rule__SimpleReference__TypeFilterAssignment_3_0_3 )? ) ;
    public final void rule__SimpleReference__Group_3_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1924:1: ( ( ( rule__SimpleReference__TypeFilterAssignment_3_0_3 )? ) )
            // InternalDOF.g:1925:1: ( ( rule__SimpleReference__TypeFilterAssignment_3_0_3 )? )
            {
            // InternalDOF.g:1925:1: ( ( rule__SimpleReference__TypeFilterAssignment_3_0_3 )? )
            // InternalDOF.g:1926:2: ( rule__SimpleReference__TypeFilterAssignment_3_0_3 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_3_0_3()); 
            // InternalDOF.g:1927:2: ( rule__SimpleReference__TypeFilterAssignment_3_0_3 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==27||LA22_0==30) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalDOF.g:1927:3: rule__SimpleReference__TypeFilterAssignment_3_0_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__TypeFilterAssignment_3_0_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_3_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__3__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_0__4"
    // InternalDOF.g:1935:1: rule__SimpleReference__Group_3_0__4 : rule__SimpleReference__Group_3_0__4__Impl ;
    public final void rule__SimpleReference__Group_3_0__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1939:1: ( rule__SimpleReference__Group_3_0__4__Impl )
            // InternalDOF.g:1940:2: rule__SimpleReference__Group_3_0__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_0__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__4"


    // $ANTLR start "rule__SimpleReference__Group_3_0__4__Impl"
    // InternalDOF.g:1946:1: rule__SimpleReference__Group_3_0__4__Impl : ( '}' ) ;
    public final void rule__SimpleReference__Group_3_0__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1950:1: ( ( '}' ) )
            // InternalDOF.g:1951:1: ( '}' )
            {
            // InternalDOF.g:1951:1: ( '}' )
            // InternalDOF.g:1952:2: '}'
            {
             before(grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_3_0_4()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getRightCurlyBracketKeyword_3_0_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_0__4__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_1__0"
    // InternalDOF.g:1962:1: rule__SimpleReference__Group_3_1__0 : rule__SimpleReference__Group_3_1__0__Impl rule__SimpleReference__Group_3_1__1 ;
    public final void rule__SimpleReference__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1966:1: ( rule__SimpleReference__Group_3_1__0__Impl rule__SimpleReference__Group_3_1__1 )
            // InternalDOF.g:1967:2: rule__SimpleReference__Group_3_1__0__Impl rule__SimpleReference__Group_3_1__1
            {
            pushFollow(FOLLOW_22);
            rule__SimpleReference__Group_3_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_1__0"


    // $ANTLR start "rule__SimpleReference__Group_3_1__0__Impl"
    // InternalDOF.g:1974:1: rule__SimpleReference__Group_3_1__0__Impl : ( '.' ) ;
    public final void rule__SimpleReference__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1978:1: ( ( '.' ) )
            // InternalDOF.g:1979:1: ( '.' )
            {
            // InternalDOF.g:1979:1: ( '.' )
            // InternalDOF.g:1980:2: '.'
            {
             before(grammarAccess.getSimpleReferenceAccess().getFullStopKeyword_3_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getFullStopKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_1__0__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_1__1"
    // InternalDOF.g:1989:1: rule__SimpleReference__Group_3_1__1 : rule__SimpleReference__Group_3_1__1__Impl rule__SimpleReference__Group_3_1__2 ;
    public final void rule__SimpleReference__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1993:1: ( rule__SimpleReference__Group_3_1__1__Impl rule__SimpleReference__Group_3_1__2 )
            // InternalDOF.g:1994:2: rule__SimpleReference__Group_3_1__1__Impl rule__SimpleReference__Group_3_1__2
            {
            pushFollow(FOLLOW_22);
            rule__SimpleReference__Group_3_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_1__1"


    // $ANTLR start "rule__SimpleReference__Group_3_1__1__Impl"
    // InternalDOF.g:2001:1: rule__SimpleReference__Group_3_1__1__Impl : ( ( rule__SimpleReference__IncludedReferencesAssignment_3_1_1 )* ) ;
    public final void rule__SimpleReference__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2005:1: ( ( ( rule__SimpleReference__IncludedReferencesAssignment_3_1_1 )* ) )
            // InternalDOF.g:2006:1: ( ( rule__SimpleReference__IncludedReferencesAssignment_3_1_1 )* )
            {
            // InternalDOF.g:2006:1: ( ( rule__SimpleReference__IncludedReferencesAssignment_3_1_1 )* )
            // InternalDOF.g:2007:2: ( rule__SimpleReference__IncludedReferencesAssignment_3_1_1 )*
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesAssignment_3_1_1()); 
            // InternalDOF.g:2008:2: ( rule__SimpleReference__IncludedReferencesAssignment_3_1_1 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_ID) ) {
                    alt23=1;
                }
                else if ( (LA23_0==26) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalDOF.g:2008:3: rule__SimpleReference__IncludedReferencesAssignment_3_1_1
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__SimpleReference__IncludedReferencesAssignment_3_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesAssignment_3_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_1__1__Impl"


    // $ANTLR start "rule__SimpleReference__Group_3_1__2"
    // InternalDOF.g:2016:1: rule__SimpleReference__Group_3_1__2 : rule__SimpleReference__Group_3_1__2__Impl ;
    public final void rule__SimpleReference__Group_3_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2020:1: ( rule__SimpleReference__Group_3_1__2__Impl )
            // InternalDOF.g:2021:2: rule__SimpleReference__Group_3_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleReference__Group_3_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_1__2"


    // $ANTLR start "rule__SimpleReference__Group_3_1__2__Impl"
    // InternalDOF.g:2027:1: rule__SimpleReference__Group_3_1__2__Impl : ( ( rule__SimpleReference__TypeFilterAssignment_3_1_2 )? ) ;
    public final void rule__SimpleReference__Group_3_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2031:1: ( ( ( rule__SimpleReference__TypeFilterAssignment_3_1_2 )? ) )
            // InternalDOF.g:2032:1: ( ( rule__SimpleReference__TypeFilterAssignment_3_1_2 )? )
            {
            // InternalDOF.g:2032:1: ( ( rule__SimpleReference__TypeFilterAssignment_3_1_2 )? )
            // InternalDOF.g:2033:2: ( rule__SimpleReference__TypeFilterAssignment_3_1_2 )?
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_3_1_2()); 
            // InternalDOF.g:2034:2: ( rule__SimpleReference__TypeFilterAssignment_3_1_2 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==27) ) {
                alt24=1;
            }
            else if ( (LA24_0==30) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalDOF.g:2034:3: rule__SimpleReference__TypeFilterAssignment_3_1_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleReference__TypeFilterAssignment_3_1_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterAssignment_3_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__Group_3_1__2__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__0"
    // InternalDOF.g:2043:1: rule__AggregatedReference__Group__0 : rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1 ;
    public final void rule__AggregatedReference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2047:1: ( rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1 )
            // InternalDOF.g:2048:2: rule__AggregatedReference__Group__0__Impl rule__AggregatedReference__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__AggregatedReference__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__0"


    // $ANTLR start "rule__AggregatedReference__Group__0__Impl"
    // InternalDOF.g:2055:1: rule__AggregatedReference__Group__0__Impl : ( 'calculate' ) ;
    public final void rule__AggregatedReference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2059:1: ( ( 'calculate' ) )
            // InternalDOF.g:2060:1: ( 'calculate' )
            {
            // InternalDOF.g:2060:1: ( 'calculate' )
            // InternalDOF.g:2061:2: 'calculate'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getCalculateKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__0__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__1"
    // InternalDOF.g:2070:1: rule__AggregatedReference__Group__1 : rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2 ;
    public final void rule__AggregatedReference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2074:1: ( rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2 )
            // InternalDOF.g:2075:2: rule__AggregatedReference__Group__1__Impl rule__AggregatedReference__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__AggregatedReference__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__1"


    // $ANTLR start "rule__AggregatedReference__Group__1__Impl"
    // InternalDOF.g:2082:1: rule__AggregatedReference__Group__1__Impl : ( ( rule__AggregatedReference__NameAssignment_1 ) ) ;
    public final void rule__AggregatedReference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2086:1: ( ( ( rule__AggregatedReference__NameAssignment_1 ) ) )
            // InternalDOF.g:2087:1: ( ( rule__AggregatedReference__NameAssignment_1 ) )
            {
            // InternalDOF.g:2087:1: ( ( rule__AggregatedReference__NameAssignment_1 ) )
            // InternalDOF.g:2088:2: ( rule__AggregatedReference__NameAssignment_1 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getNameAssignment_1()); 
            // InternalDOF.g:2089:2: ( rule__AggregatedReference__NameAssignment_1 )
            // InternalDOF.g:2089:3: rule__AggregatedReference__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__1__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__2"
    // InternalDOF.g:2097:1: rule__AggregatedReference__Group__2 : rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3 ;
    public final void rule__AggregatedReference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2101:1: ( rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3 )
            // InternalDOF.g:2102:2: rule__AggregatedReference__Group__2__Impl rule__AggregatedReference__Group__3
            {
            pushFollow(FOLLOW_24);
            rule__AggregatedReference__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__2"


    // $ANTLR start "rule__AggregatedReference__Group__2__Impl"
    // InternalDOF.g:2109:1: rule__AggregatedReference__Group__2__Impl : ( 'as' ) ;
    public final void rule__AggregatedReference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2113:1: ( ( 'as' ) )
            // InternalDOF.g:2114:1: ( 'as' )
            {
            // InternalDOF.g:2114:1: ( 'as' )
            // InternalDOF.g:2115:2: 'as'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAsKeyword_2()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getAsKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__2__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__3"
    // InternalDOF.g:2124:1: rule__AggregatedReference__Group__3 : rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4 ;
    public final void rule__AggregatedReference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2128:1: ( rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4 )
            // InternalDOF.g:2129:2: rule__AggregatedReference__Group__3__Impl rule__AggregatedReference__Group__4
            {
            pushFollow(FOLLOW_25);
            rule__AggregatedReference__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__3"


    // $ANTLR start "rule__AggregatedReference__Group__3__Impl"
    // InternalDOF.g:2136:1: rule__AggregatedReference__Group__3__Impl : ( ( rule__AggregatedReference__FunctionAssignment_3 ) ) ;
    public final void rule__AggregatedReference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2140:1: ( ( ( rule__AggregatedReference__FunctionAssignment_3 ) ) )
            // InternalDOF.g:2141:1: ( ( rule__AggregatedReference__FunctionAssignment_3 ) )
            {
            // InternalDOF.g:2141:1: ( ( rule__AggregatedReference__FunctionAssignment_3 ) )
            // InternalDOF.g:2142:2: ( rule__AggregatedReference__FunctionAssignment_3 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getFunctionAssignment_3()); 
            // InternalDOF.g:2143:2: ( rule__AggregatedReference__FunctionAssignment_3 )
            // InternalDOF.g:2143:3: rule__AggregatedReference__FunctionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__FunctionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getFunctionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__3__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__4"
    // InternalDOF.g:2151:1: rule__AggregatedReference__Group__4 : rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5 ;
    public final void rule__AggregatedReference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2155:1: ( rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5 )
            // InternalDOF.g:2156:2: rule__AggregatedReference__Group__4__Impl rule__AggregatedReference__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__AggregatedReference__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__4"


    // $ANTLR start "rule__AggregatedReference__Group__4__Impl"
    // InternalDOF.g:2163:1: rule__AggregatedReference__Group__4__Impl : ( '(' ) ;
    public final void rule__AggregatedReference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2167:1: ( ( '(' ) )
            // InternalDOF.g:2168:1: ( '(' )
            {
            // InternalDOF.g:2168:1: ( '(' )
            // InternalDOF.g:2169:2: '('
            {
             before(grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_4()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getLeftParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__4__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__5"
    // InternalDOF.g:2178:1: rule__AggregatedReference__Group__5 : rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6 ;
    public final void rule__AggregatedReference__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2182:1: ( rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6 )
            // InternalDOF.g:2183:2: rule__AggregatedReference__Group__5__Impl rule__AggregatedReference__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__AggregatedReference__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__5"


    // $ANTLR start "rule__AggregatedReference__Group__5__Impl"
    // InternalDOF.g:2190:1: rule__AggregatedReference__Group__5__Impl : ( ( rule__AggregatedReference__AggValueAssignment_5 ) ) ;
    public final void rule__AggregatedReference__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2194:1: ( ( ( rule__AggregatedReference__AggValueAssignment_5 ) ) )
            // InternalDOF.g:2195:1: ( ( rule__AggregatedReference__AggValueAssignment_5 ) )
            {
            // InternalDOF.g:2195:1: ( ( rule__AggregatedReference__AggValueAssignment_5 ) )
            // InternalDOF.g:2196:2: ( rule__AggregatedReference__AggValueAssignment_5 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAggValueAssignment_5()); 
            // InternalDOF.g:2197:2: ( rule__AggregatedReference__AggValueAssignment_5 )
            // InternalDOF.g:2197:3: rule__AggregatedReference__AggValueAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__AggValueAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getAggValueAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__5__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__6"
    // InternalDOF.g:2205:1: rule__AggregatedReference__Group__6 : rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7 ;
    public final void rule__AggregatedReference__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2209:1: ( rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7 )
            // InternalDOF.g:2210:2: rule__AggregatedReference__Group__6__Impl rule__AggregatedReference__Group__7
            {
            pushFollow(FOLLOW_27);
            rule__AggregatedReference__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__6"


    // $ANTLR start "rule__AggregatedReference__Group__6__Impl"
    // InternalDOF.g:2217:1: rule__AggregatedReference__Group__6__Impl : ( ')' ) ;
    public final void rule__AggregatedReference__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2221:1: ( ( ')' ) )
            // InternalDOF.g:2222:1: ( ')' )
            {
            // InternalDOF.g:2222:1: ( ')' )
            // InternalDOF.g:2223:2: ')'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_6()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__6__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__7"
    // InternalDOF.g:2232:1: rule__AggregatedReference__Group__7 : rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8 ;
    public final void rule__AggregatedReference__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2236:1: ( rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8 )
            // InternalDOF.g:2237:2: rule__AggregatedReference__Group__7__Impl rule__AggregatedReference__Group__8
            {
            pushFollow(FOLLOW_27);
            rule__AggregatedReference__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__7"


    // $ANTLR start "rule__AggregatedReference__Group__7__Impl"
    // InternalDOF.g:2244:1: rule__AggregatedReference__Group__7__Impl : ( ( rule__AggregatedReference__Group_7__0 )? ) ;
    public final void rule__AggregatedReference__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2248:1: ( ( ( rule__AggregatedReference__Group_7__0 )? ) )
            // InternalDOF.g:2249:1: ( ( rule__AggregatedReference__Group_7__0 )? )
            {
            // InternalDOF.g:2249:1: ( ( rule__AggregatedReference__Group_7__0 )? )
            // InternalDOF.g:2250:2: ( rule__AggregatedReference__Group_7__0 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getGroup_7()); 
            // InternalDOF.g:2251:2: ( rule__AggregatedReference__Group_7__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==22) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalDOF.g:2251:3: rule__AggregatedReference__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__7__Impl"


    // $ANTLR start "rule__AggregatedReference__Group__8"
    // InternalDOF.g:2259:1: rule__AggregatedReference__Group__8 : rule__AggregatedReference__Group__8__Impl ;
    public final void rule__AggregatedReference__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2263:1: ( rule__AggregatedReference__Group__8__Impl )
            // InternalDOF.g:2264:2: rule__AggregatedReference__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__8"


    // $ANTLR start "rule__AggregatedReference__Group__8__Impl"
    // InternalDOF.g:2270:1: rule__AggregatedReference__Group__8__Impl : ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? ) ;
    public final void rule__AggregatedReference__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2274:1: ( ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? ) )
            // InternalDOF.g:2275:1: ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? )
            {
            // InternalDOF.g:2275:1: ( ( rule__AggregatedReference__InstancesFilterAssignment_8 )? )
            // InternalDOF.g:2276:2: ( rule__AggregatedReference__InstancesFilterAssignment_8 )?
            {
             before(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterAssignment_8()); 
            // InternalDOF.g:2277:2: ( rule__AggregatedReference__InstancesFilterAssignment_8 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==34) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalDOF.g:2277:3: rule__AggregatedReference__InstancesFilterAssignment_8
                    {
                    pushFollow(FOLLOW_2);
                    rule__AggregatedReference__InstancesFilterAssignment_8();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group__8__Impl"


    // $ANTLR start "rule__AggregatedReference__Group_7__0"
    // InternalDOF.g:2286:1: rule__AggregatedReference__Group_7__0 : rule__AggregatedReference__Group_7__0__Impl rule__AggregatedReference__Group_7__1 ;
    public final void rule__AggregatedReference__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2290:1: ( rule__AggregatedReference__Group_7__0__Impl rule__AggregatedReference__Group_7__1 )
            // InternalDOF.g:2291:2: rule__AggregatedReference__Group_7__0__Impl rule__AggregatedReference__Group_7__1
            {
            pushFollow(FOLLOW_7);
            rule__AggregatedReference__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__0"


    // $ANTLR start "rule__AggregatedReference__Group_7__0__Impl"
    // InternalDOF.g:2298:1: rule__AggregatedReference__Group_7__0__Impl : ( 'by' ) ;
    public final void rule__AggregatedReference__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2302:1: ( ( 'by' ) )
            // InternalDOF.g:2303:1: ( 'by' )
            {
            // InternalDOF.g:2303:1: ( 'by' )
            // InternalDOF.g:2304:2: 'by'
            {
             before(grammarAccess.getAggregatedReferenceAccess().getByKeyword_7_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getByKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__0__Impl"


    // $ANTLR start "rule__AggregatedReference__Group_7__1"
    // InternalDOF.g:2313:1: rule__AggregatedReference__Group_7__1 : rule__AggregatedReference__Group_7__1__Impl ;
    public final void rule__AggregatedReference__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2317:1: ( rule__AggregatedReference__Group_7__1__Impl )
            // InternalDOF.g:2318:2: rule__AggregatedReference__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__1"


    // $ANTLR start "rule__AggregatedReference__Group_7__1__Impl"
    // InternalDOF.g:2324:1: rule__AggregatedReference__Group_7__1__Impl : ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) ) ;
    public final void rule__AggregatedReference__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2328:1: ( ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) ) )
            // InternalDOF.g:2329:1: ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) )
            {
            // InternalDOF.g:2329:1: ( ( rule__AggregatedReference__PivotingIdAssignment_7_1 ) )
            // InternalDOF.g:2330:2: ( rule__AggregatedReference__PivotingIdAssignment_7_1 )
            {
             before(grammarAccess.getAggregatedReferenceAccess().getPivotingIdAssignment_7_1()); 
            // InternalDOF.g:2331:2: ( rule__AggregatedReference__PivotingIdAssignment_7_1 )
            // InternalDOF.g:2331:3: rule__AggregatedReference__PivotingIdAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__AggregatedReference__PivotingIdAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getAggregatedReferenceAccess().getPivotingIdAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__Group_7__1__Impl"


    // $ANTLR start "rule__TypeCompletion__Group__0"
    // InternalDOF.g:2340:1: rule__TypeCompletion__Group__0 : rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1 ;
    public final void rule__TypeCompletion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2344:1: ( rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1 )
            // InternalDOF.g:2345:2: rule__TypeCompletion__Group__0__Impl rule__TypeCompletion__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__TypeCompletion__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCompletion__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__0"


    // $ANTLR start "rule__TypeCompletion__Group__0__Impl"
    // InternalDOF.g:2352:1: rule__TypeCompletion__Group__0__Impl : ( 'as' ) ;
    public final void rule__TypeCompletion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2356:1: ( ( 'as' ) )
            // InternalDOF.g:2357:1: ( 'as' )
            {
            // InternalDOF.g:2357:1: ( 'as' )
            // InternalDOF.g:2358:2: 'as'
            {
             before(grammarAccess.getTypeCompletionAccess().getAsKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getTypeCompletionAccess().getAsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__0__Impl"


    // $ANTLR start "rule__TypeCompletion__Group__1"
    // InternalDOF.g:2367:1: rule__TypeCompletion__Group__1 : rule__TypeCompletion__Group__1__Impl ;
    public final void rule__TypeCompletion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2371:1: ( rule__TypeCompletion__Group__1__Impl )
            // InternalDOF.g:2372:2: rule__TypeCompletion__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCompletion__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__1"


    // $ANTLR start "rule__TypeCompletion__Group__1__Impl"
    // InternalDOF.g:2378:1: rule__TypeCompletion__Group__1__Impl : ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) ) ;
    public final void rule__TypeCompletion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2382:1: ( ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) ) )
            // InternalDOF.g:2383:1: ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) )
            {
            // InternalDOF.g:2383:1: ( ( rule__TypeCompletion__TypeCustomizationsAssignment_1 ) )
            // InternalDOF.g:2384:2: ( rule__TypeCompletion__TypeCustomizationsAssignment_1 )
            {
             before(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsAssignment_1()); 
            // InternalDOF.g:2385:2: ( rule__TypeCompletion__TypeCustomizationsAssignment_1 )
            // InternalDOF.g:2385:3: rule__TypeCompletion__TypeCustomizationsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeCompletion__TypeCustomizationsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__Group__1__Impl"


    // $ANTLR start "rule__TypeSelection__Group__0"
    // InternalDOF.g:2394:1: rule__TypeSelection__Group__0 : rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1 ;
    public final void rule__TypeSelection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2398:1: ( rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1 )
            // InternalDOF.g:2399:2: rule__TypeSelection__Group__0__Impl rule__TypeSelection__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__TypeSelection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeSelection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__0"


    // $ANTLR start "rule__TypeSelection__Group__0__Impl"
    // InternalDOF.g:2406:1: rule__TypeSelection__Group__0__Impl : ( 'only_as' ) ;
    public final void rule__TypeSelection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2410:1: ( ( 'only_as' ) )
            // InternalDOF.g:2411:1: ( 'only_as' )
            {
            // InternalDOF.g:2411:1: ( 'only_as' )
            // InternalDOF.g:2412:2: 'only_as'
            {
             before(grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getTypeSelectionAccess().getOnly_asKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__0__Impl"


    // $ANTLR start "rule__TypeSelection__Group__1"
    // InternalDOF.g:2421:1: rule__TypeSelection__Group__1 : rule__TypeSelection__Group__1__Impl ;
    public final void rule__TypeSelection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2425:1: ( rule__TypeSelection__Group__1__Impl )
            // InternalDOF.g:2426:2: rule__TypeSelection__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeSelection__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__1"


    // $ANTLR start "rule__TypeSelection__Group__1__Impl"
    // InternalDOF.g:2432:1: rule__TypeSelection__Group__1__Impl : ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) ) ;
    public final void rule__TypeSelection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2436:1: ( ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) ) )
            // InternalDOF.g:2437:1: ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) )
            {
            // InternalDOF.g:2437:1: ( ( rule__TypeSelection__TypeCustomizationsAssignment_1 ) )
            // InternalDOF.g:2438:2: ( rule__TypeSelection__TypeCustomizationsAssignment_1 )
            {
             before(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsAssignment_1()); 
            // InternalDOF.g:2439:2: ( rule__TypeSelection__TypeCustomizationsAssignment_1 )
            // InternalDOF.g:2439:3: rule__TypeSelection__TypeCustomizationsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeSelection__TypeCustomizationsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__Group__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__0"
    // InternalDOF.g:2448:1: rule__TypeCustomization__Group__0 : rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1 ;
    public final void rule__TypeCustomization__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2452:1: ( rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1 )
            // InternalDOF.g:2453:2: rule__TypeCustomization__Group__0__Impl rule__TypeCustomization__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__TypeCustomization__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__0"


    // $ANTLR start "rule__TypeCustomization__Group__0__Impl"
    // InternalDOF.g:2460:1: rule__TypeCustomization__Group__0__Impl : ( ( rule__TypeCustomization__NameAssignment_0 ) ) ;
    public final void rule__TypeCustomization__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2464:1: ( ( ( rule__TypeCustomization__NameAssignment_0 ) ) )
            // InternalDOF.g:2465:1: ( ( rule__TypeCustomization__NameAssignment_0 ) )
            {
            // InternalDOF.g:2465:1: ( ( rule__TypeCustomization__NameAssignment_0 ) )
            // InternalDOF.g:2466:2: ( rule__TypeCustomization__NameAssignment_0 )
            {
             before(grammarAccess.getTypeCustomizationAccess().getNameAssignment_0()); 
            // InternalDOF.g:2467:2: ( rule__TypeCustomization__NameAssignment_0 )
            // InternalDOF.g:2467:3: rule__TypeCustomization__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTypeCustomizationAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__0__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__1"
    // InternalDOF.g:2475:1: rule__TypeCustomization__Group__1 : rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2 ;
    public final void rule__TypeCustomization__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2479:1: ( rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2 )
            // InternalDOF.g:2480:2: rule__TypeCustomization__Group__1__Impl rule__TypeCustomization__Group__2
            {
            pushFollow(FOLLOW_28);
            rule__TypeCustomization__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__1"


    // $ANTLR start "rule__TypeCustomization__Group__1__Impl"
    // InternalDOF.g:2487:1: rule__TypeCustomization__Group__1__Impl : ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? ) ;
    public final void rule__TypeCustomization__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2491:1: ( ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? ) )
            // InternalDOF.g:2492:1: ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? )
            {
            // InternalDOF.g:2492:1: ( ( rule__TypeCustomization__AttributeFilterAssignment_1 )? )
            // InternalDOF.g:2493:2: ( rule__TypeCustomization__AttributeFilterAssignment_1 )?
            {
             before(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAssignment_1()); 
            // InternalDOF.g:2494:2: ( rule__TypeCustomization__AttributeFilterAssignment_1 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==31) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalDOF.g:2494:3: rule__TypeCustomization__AttributeFilterAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeCustomization__AttributeFilterAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group__2"
    // InternalDOF.g:2502:1: rule__TypeCustomization__Group__2 : rule__TypeCustomization__Group__2__Impl ;
    public final void rule__TypeCustomization__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2506:1: ( rule__TypeCustomization__Group__2__Impl )
            // InternalDOF.g:2507:2: rule__TypeCustomization__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__2"


    // $ANTLR start "rule__TypeCustomization__Group__2__Impl"
    // InternalDOF.g:2513:1: rule__TypeCustomization__Group__2__Impl : ( ( rule__TypeCustomization__Group_2__0 )? ) ;
    public final void rule__TypeCustomization__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2517:1: ( ( ( rule__TypeCustomization__Group_2__0 )? ) )
            // InternalDOF.g:2518:1: ( ( rule__TypeCustomization__Group_2__0 )? )
            {
            // InternalDOF.g:2518:1: ( ( rule__TypeCustomization__Group_2__0 )? )
            // InternalDOF.g:2519:2: ( rule__TypeCustomization__Group_2__0 )?
            {
             before(grammarAccess.getTypeCustomizationAccess().getGroup_2()); 
            // InternalDOF.g:2520:2: ( rule__TypeCustomization__Group_2__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==23) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalDOF.g:2520:3: rule__TypeCustomization__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeCustomization__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeCustomizationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group__2__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__0"
    // InternalDOF.g:2529:1: rule__TypeCustomization__Group_2__0 : rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1 ;
    public final void rule__TypeCustomization__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2533:1: ( rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1 )
            // InternalDOF.g:2534:2: rule__TypeCustomization__Group_2__0__Impl rule__TypeCustomization__Group_2__1
            {
            pushFollow(FOLLOW_29);
            rule__TypeCustomization__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__0"


    // $ANTLR start "rule__TypeCustomization__Group_2__0__Impl"
    // InternalDOF.g:2541:1: rule__TypeCustomization__Group_2__0__Impl : ( '{' ) ;
    public final void rule__TypeCustomization__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2545:1: ( ( '{' ) )
            // InternalDOF.g:2546:1: ( '{' )
            {
            // InternalDOF.g:2546:1: ( '{' )
            // InternalDOF.g:2547:2: '{'
            {
             before(grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__0__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__1"
    // InternalDOF.g:2556:1: rule__TypeCustomization__Group_2__1 : rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2 ;
    public final void rule__TypeCustomization__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2560:1: ( rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2 )
            // InternalDOF.g:2561:2: rule__TypeCustomization__Group_2__1__Impl rule__TypeCustomization__Group_2__2
            {
            pushFollow(FOLLOW_29);
            rule__TypeCustomization__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__1"


    // $ANTLR start "rule__TypeCustomization__Group_2__1__Impl"
    // InternalDOF.g:2568:1: rule__TypeCustomization__Group_2__1__Impl : ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* ) ;
    public final void rule__TypeCustomization__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2572:1: ( ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* ) )
            // InternalDOF.g:2573:1: ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* )
            {
            // InternalDOF.g:2573:1: ( ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )* )
            // InternalDOF.g:2574:2: ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )*
            {
             before(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesAssignment_2_1()); 
            // InternalDOF.g:2575:2: ( rule__TypeCustomization__IncludedReferencesAssignment_2_1 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==RULE_ID||LA29_0==26) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalDOF.g:2575:3: rule__TypeCustomization__IncludedReferencesAssignment_2_1
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__TypeCustomization__IncludedReferencesAssignment_2_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__1__Impl"


    // $ANTLR start "rule__TypeCustomization__Group_2__2"
    // InternalDOF.g:2583:1: rule__TypeCustomization__Group_2__2 : rule__TypeCustomization__Group_2__2__Impl ;
    public final void rule__TypeCustomization__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2587:1: ( rule__TypeCustomization__Group_2__2__Impl )
            // InternalDOF.g:2588:2: rule__TypeCustomization__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeCustomization__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__2"


    // $ANTLR start "rule__TypeCustomization__Group_2__2__Impl"
    // InternalDOF.g:2594:1: rule__TypeCustomization__Group_2__2__Impl : ( '}' ) ;
    public final void rule__TypeCustomization__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2598:1: ( ( '}' ) )
            // InternalDOF.g:2599:1: ( '}' )
            {
            // InternalDOF.g:2599:1: ( '}' )
            // InternalDOF.g:2600:2: '}'
            {
             before(grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__Group_2__2__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__0"
    // InternalDOF.g:2610:1: rule__AttributeFilter__Group__0 : rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1 ;
    public final void rule__AttributeFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2614:1: ( rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1 )
            // InternalDOF.g:2615:2: rule__AttributeFilter__Group__0__Impl rule__AttributeFilter__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__AttributeFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__0"


    // $ANTLR start "rule__AttributeFilter__Group__0__Impl"
    // InternalDOF.g:2622:1: rule__AttributeFilter__Group__0__Impl : ( () ) ;
    public final void rule__AttributeFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2626:1: ( ( () ) )
            // InternalDOF.g:2627:1: ( () )
            {
            // InternalDOF.g:2627:1: ( () )
            // InternalDOF.g:2628:2: ()
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributeFilterAction_0()); 
            // InternalDOF.g:2629:2: ()
            // InternalDOF.g:2629:3: 
            {
            }

             after(grammarAccess.getAttributeFilterAccess().getAttributeFilterAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__1"
    // InternalDOF.g:2637:1: rule__AttributeFilter__Group__1 : rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2 ;
    public final void rule__AttributeFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2641:1: ( rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2 )
            // InternalDOF.g:2642:2: rule__AttributeFilter__Group__1__Impl rule__AttributeFilter__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__AttributeFilter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__1"


    // $ANTLR start "rule__AttributeFilter__Group__1__Impl"
    // InternalDOF.g:2649:1: rule__AttributeFilter__Group__1__Impl : ( '[' ) ;
    public final void rule__AttributeFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2653:1: ( ( '[' ) )
            // InternalDOF.g:2654:1: ( '[' )
            {
            // InternalDOF.g:2654:1: ( '[' )
            // InternalDOF.g:2655:2: '['
            {
             before(grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getLeftSquareBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__1__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__2"
    // InternalDOF.g:2664:1: rule__AttributeFilter__Group__2 : rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3 ;
    public final void rule__AttributeFilter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2668:1: ( rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3 )
            // InternalDOF.g:2669:2: rule__AttributeFilter__Group__2__Impl rule__AttributeFilter__Group__3
            {
            pushFollow(FOLLOW_31);
            rule__AttributeFilter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__2"


    // $ANTLR start "rule__AttributeFilter__Group__2__Impl"
    // InternalDOF.g:2676:1: rule__AttributeFilter__Group__2__Impl : ( ( rule__AttributeFilter__Group_2__0 )? ) ;
    public final void rule__AttributeFilter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2680:1: ( ( ( rule__AttributeFilter__Group_2__0 )? ) )
            // InternalDOF.g:2681:1: ( ( rule__AttributeFilter__Group_2__0 )? )
            {
            // InternalDOF.g:2681:1: ( ( rule__AttributeFilter__Group_2__0 )? )
            // InternalDOF.g:2682:2: ( rule__AttributeFilter__Group_2__0 )?
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup_2()); 
            // InternalDOF.g:2683:2: ( rule__AttributeFilter__Group_2__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_ID) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalDOF.g:2683:3: rule__AttributeFilter__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__AttributeFilter__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttributeFilterAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__2__Impl"


    // $ANTLR start "rule__AttributeFilter__Group__3"
    // InternalDOF.g:2691:1: rule__AttributeFilter__Group__3 : rule__AttributeFilter__Group__3__Impl ;
    public final void rule__AttributeFilter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2695:1: ( rule__AttributeFilter__Group__3__Impl )
            // InternalDOF.g:2696:2: rule__AttributeFilter__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__3"


    // $ANTLR start "rule__AttributeFilter__Group__3__Impl"
    // InternalDOF.g:2702:1: rule__AttributeFilter__Group__3__Impl : ( ']' ) ;
    public final void rule__AttributeFilter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2706:1: ( ( ']' ) )
            // InternalDOF.g:2707:1: ( ']' )
            {
            // InternalDOF.g:2707:1: ( ']' )
            // InternalDOF.g:2708:2: ']'
            {
             before(grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group__3__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2__0"
    // InternalDOF.g:2718:1: rule__AttributeFilter__Group_2__0 : rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1 ;
    public final void rule__AttributeFilter__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2722:1: ( rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1 )
            // InternalDOF.g:2723:2: rule__AttributeFilter__Group_2__0__Impl rule__AttributeFilter__Group_2__1
            {
            pushFollow(FOLLOW_32);
            rule__AttributeFilter__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__0"


    // $ANTLR start "rule__AttributeFilter__Group_2__0__Impl"
    // InternalDOF.g:2730:1: rule__AttributeFilter__Group_2__0__Impl : ( ( rule__AttributeFilter__AttributesAssignment_2_0 ) ) ;
    public final void rule__AttributeFilter__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2734:1: ( ( ( rule__AttributeFilter__AttributesAssignment_2_0 ) ) )
            // InternalDOF.g:2735:1: ( ( rule__AttributeFilter__AttributesAssignment_2_0 ) )
            {
            // InternalDOF.g:2735:1: ( ( rule__AttributeFilter__AttributesAssignment_2_0 ) )
            // InternalDOF.g:2736:2: ( rule__AttributeFilter__AttributesAssignment_2_0 )
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_0()); 
            // InternalDOF.g:2737:2: ( rule__AttributeFilter__AttributesAssignment_2_0 )
            // InternalDOF.g:2737:3: rule__AttributeFilter__AttributesAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__AttributesAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2__1"
    // InternalDOF.g:2745:1: rule__AttributeFilter__Group_2__1 : rule__AttributeFilter__Group_2__1__Impl ;
    public final void rule__AttributeFilter__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2749:1: ( rule__AttributeFilter__Group_2__1__Impl )
            // InternalDOF.g:2750:2: rule__AttributeFilter__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__1"


    // $ANTLR start "rule__AttributeFilter__Group_2__1__Impl"
    // InternalDOF.g:2756:1: rule__AttributeFilter__Group_2__1__Impl : ( ( rule__AttributeFilter__Group_2_1__0 )* ) ;
    public final void rule__AttributeFilter__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2760:1: ( ( ( rule__AttributeFilter__Group_2_1__0 )* ) )
            // InternalDOF.g:2761:1: ( ( rule__AttributeFilter__Group_2_1__0 )* )
            {
            // InternalDOF.g:2761:1: ( ( rule__AttributeFilter__Group_2_1__0 )* )
            // InternalDOF.g:2762:2: ( rule__AttributeFilter__Group_2_1__0 )*
            {
             before(grammarAccess.getAttributeFilterAccess().getGroup_2_1()); 
            // InternalDOF.g:2763:2: ( rule__AttributeFilter__Group_2_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==33) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalDOF.g:2763:3: rule__AttributeFilter__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__AttributeFilter__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getAttributeFilterAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2__1__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2_1__0"
    // InternalDOF.g:2772:1: rule__AttributeFilter__Group_2_1__0 : rule__AttributeFilter__Group_2_1__0__Impl rule__AttributeFilter__Group_2_1__1 ;
    public final void rule__AttributeFilter__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2776:1: ( rule__AttributeFilter__Group_2_1__0__Impl rule__AttributeFilter__Group_2_1__1 )
            // InternalDOF.g:2777:2: rule__AttributeFilter__Group_2_1__0__Impl rule__AttributeFilter__Group_2_1__1
            {
            pushFollow(FOLLOW_7);
            rule__AttributeFilter__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2_1__0"


    // $ANTLR start "rule__AttributeFilter__Group_2_1__0__Impl"
    // InternalDOF.g:2784:1: rule__AttributeFilter__Group_2_1__0__Impl : ( ',' ) ;
    public final void rule__AttributeFilter__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2788:1: ( ( ',' ) )
            // InternalDOF.g:2789:1: ( ',' )
            {
            // InternalDOF.g:2789:1: ( ',' )
            // InternalDOF.g:2790:2: ','
            {
             before(grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_1_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getCommaKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2_1__0__Impl"


    // $ANTLR start "rule__AttributeFilter__Group_2_1__1"
    // InternalDOF.g:2799:1: rule__AttributeFilter__Group_2_1__1 : rule__AttributeFilter__Group_2_1__1__Impl ;
    public final void rule__AttributeFilter__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2803:1: ( rule__AttributeFilter__Group_2_1__1__Impl )
            // InternalDOF.g:2804:2: rule__AttributeFilter__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2_1__1"


    // $ANTLR start "rule__AttributeFilter__Group_2_1__1__Impl"
    // InternalDOF.g:2810:1: rule__AttributeFilter__Group_2_1__1__Impl : ( ( rule__AttributeFilter__AttributesAssignment_2_1_1 ) ) ;
    public final void rule__AttributeFilter__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2814:1: ( ( ( rule__AttributeFilter__AttributesAssignment_2_1_1 ) ) )
            // InternalDOF.g:2815:1: ( ( rule__AttributeFilter__AttributesAssignment_2_1_1 ) )
            {
            // InternalDOF.g:2815:1: ( ( rule__AttributeFilter__AttributesAssignment_2_1_1 ) )
            // InternalDOF.g:2816:2: ( rule__AttributeFilter__AttributesAssignment_2_1_1 )
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_1_1()); 
            // InternalDOF.g:2817:2: ( rule__AttributeFilter__AttributesAssignment_2_1_1 )
            // InternalDOF.g:2817:3: rule__AttributeFilter__AttributesAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AttributeFilter__AttributesAssignment_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeFilterAccess().getAttributesAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__Group_2_1__1__Impl"


    // $ANTLR start "rule__InstancesFilter__Group__0"
    // InternalDOF.g:2826:1: rule__InstancesFilter__Group__0 : rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1 ;
    public final void rule__InstancesFilter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2830:1: ( rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1 )
            // InternalDOF.g:2831:2: rule__InstancesFilter__Group__0__Impl rule__InstancesFilter__Group__1
            {
            pushFollow(FOLLOW_34);
            rule__InstancesFilter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__0"


    // $ANTLR start "rule__InstancesFilter__Group__0__Impl"
    // InternalDOF.g:2838:1: rule__InstancesFilter__Group__0__Impl : ( 'where' ) ;
    public final void rule__InstancesFilter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2842:1: ( ( 'where' ) )
            // InternalDOF.g:2843:1: ( 'where' )
            {
            // InternalDOF.g:2843:1: ( 'where' )
            // InternalDOF.g:2844:2: 'where'
            {
             before(grammarAccess.getInstancesFilterAccess().getWhereKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getInstancesFilterAccess().getWhereKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__0__Impl"


    // $ANTLR start "rule__InstancesFilter__Group__1"
    // InternalDOF.g:2853:1: rule__InstancesFilter__Group__1 : rule__InstancesFilter__Group__1__Impl ;
    public final void rule__InstancesFilter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2857:1: ( rule__InstancesFilter__Group__1__Impl )
            // InternalDOF.g:2858:2: rule__InstancesFilter__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InstancesFilter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__1"


    // $ANTLR start "rule__InstancesFilter__Group__1__Impl"
    // InternalDOF.g:2864:1: rule__InstancesFilter__Group__1__Impl : ( ruleAndConjunction ) ;
    public final void rule__InstancesFilter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2868:1: ( ( ruleAndConjunction ) )
            // InternalDOF.g:2869:1: ( ruleAndConjunction )
            {
            // InternalDOF.g:2869:1: ( ruleAndConjunction )
            // InternalDOF.g:2870:2: ruleAndConjunction
            {
             before(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getInstancesFilterAccess().getAndConjunctionParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstancesFilter__Group__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group__0"
    // InternalDOF.g:2880:1: rule__AndConjunction__Group__0 : rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1 ;
    public final void rule__AndConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2884:1: ( rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1 )
            // InternalDOF.g:2885:2: rule__AndConjunction__Group__0__Impl rule__AndConjunction__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__AndConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__0"


    // $ANTLR start "rule__AndConjunction__Group__0__Impl"
    // InternalDOF.g:2892:1: rule__AndConjunction__Group__0__Impl : ( ruleOrConjunction ) ;
    public final void rule__AndConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2896:1: ( ( ruleOrConjunction ) )
            // InternalDOF.g:2897:1: ( ruleOrConjunction )
            {
            // InternalDOF.g:2897:1: ( ruleOrConjunction )
            // InternalDOF.g:2898:2: ruleOrConjunction
            {
             before(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionAccess().getOrConjunctionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__0__Impl"


    // $ANTLR start "rule__AndConjunction__Group__1"
    // InternalDOF.g:2907:1: rule__AndConjunction__Group__1 : rule__AndConjunction__Group__1__Impl ;
    public final void rule__AndConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2911:1: ( rule__AndConjunction__Group__1__Impl )
            // InternalDOF.g:2912:2: rule__AndConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__1"


    // $ANTLR start "rule__AndConjunction__Group__1__Impl"
    // InternalDOF.g:2918:1: rule__AndConjunction__Group__1__Impl : ( ( rule__AndConjunction__Group_1__0 )* ) ;
    public final void rule__AndConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2922:1: ( ( ( rule__AndConjunction__Group_1__0 )* ) )
            // InternalDOF.g:2923:1: ( ( rule__AndConjunction__Group_1__0 )* )
            {
            // InternalDOF.g:2923:1: ( ( rule__AndConjunction__Group_1__0 )* )
            // InternalDOF.g:2924:2: ( rule__AndConjunction__Group_1__0 )*
            {
             before(grammarAccess.getAndConjunctionAccess().getGroup_1()); 
            // InternalDOF.g:2925:2: ( rule__AndConjunction__Group_1__0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==35) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalDOF.g:2925:3: rule__AndConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_36);
            	    rule__AndConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getAndConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__0"
    // InternalDOF.g:2934:1: rule__AndConjunction__Group_1__0 : rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1 ;
    public final void rule__AndConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2938:1: ( rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1 )
            // InternalDOF.g:2939:2: rule__AndConjunction__Group_1__0__Impl rule__AndConjunction__Group_1__1
            {
            pushFollow(FOLLOW_35);
            rule__AndConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__0"


    // $ANTLR start "rule__AndConjunction__Group_1__0__Impl"
    // InternalDOF.g:2946:1: rule__AndConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__AndConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2950:1: ( ( () ) )
            // InternalDOF.g:2951:1: ( () )
            {
            // InternalDOF.g:2951:1: ( () )
            // InternalDOF.g:2952:2: ()
            {
             before(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0()); 
            // InternalDOF.g:2953:2: ()
            // InternalDOF.g:2953:3: 
            {
            }

             after(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__1"
    // InternalDOF.g:2961:1: rule__AndConjunction__Group_1__1 : rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2 ;
    public final void rule__AndConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2965:1: ( rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2 )
            // InternalDOF.g:2966:2: rule__AndConjunction__Group_1__1__Impl rule__AndConjunction__Group_1__2
            {
            pushFollow(FOLLOW_34);
            rule__AndConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__1"


    // $ANTLR start "rule__AndConjunction__Group_1__1__Impl"
    // InternalDOF.g:2973:1: rule__AndConjunction__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__AndConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2977:1: ( ( 'and' ) )
            // InternalDOF.g:2978:1: ( 'and' )
            {
            // InternalDOF.g:2978:1: ( 'and' )
            // InternalDOF.g:2979:2: 'and'
            {
             before(grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getAndConjunctionAccess().getAndKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__AndConjunction__Group_1__2"
    // InternalDOF.g:2988:1: rule__AndConjunction__Group_1__2 : rule__AndConjunction__Group_1__2__Impl ;
    public final void rule__AndConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2992:1: ( rule__AndConjunction__Group_1__2__Impl )
            // InternalDOF.g:2993:2: rule__AndConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__2"


    // $ANTLR start "rule__AndConjunction__Group_1__2__Impl"
    // InternalDOF.g:2999:1: rule__AndConjunction__Group_1__2__Impl : ( ( rule__AndConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__AndConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3003:1: ( ( ( rule__AndConjunction__RightAssignment_1_2 ) ) )
            // InternalDOF.g:3004:1: ( ( rule__AndConjunction__RightAssignment_1_2 ) )
            {
            // InternalDOF.g:3004:1: ( ( rule__AndConjunction__RightAssignment_1_2 ) )
            // InternalDOF.g:3005:2: ( rule__AndConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getAndConjunctionAccess().getRightAssignment_1_2()); 
            // InternalDOF.g:3006:2: ( rule__AndConjunction__RightAssignment_1_2 )
            // InternalDOF.g:3006:3: rule__AndConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__AndConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAndConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__OrConjunction__Group__0"
    // InternalDOF.g:3015:1: rule__OrConjunction__Group__0 : rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1 ;
    public final void rule__OrConjunction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3019:1: ( rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1 )
            // InternalDOF.g:3020:2: rule__OrConjunction__Group__0__Impl rule__OrConjunction__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__OrConjunction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__0"


    // $ANTLR start "rule__OrConjunction__Group__0__Impl"
    // InternalDOF.g:3027:1: rule__OrConjunction__Group__0__Impl : ( rulePrimary ) ;
    public final void rule__OrConjunction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3031:1: ( ( rulePrimary ) )
            // InternalDOF.g:3032:1: ( rulePrimary )
            {
            // InternalDOF.g:3032:1: ( rulePrimary )
            // InternalDOF.g:3033:2: rulePrimary
            {
             before(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getOrConjunctionAccess().getPrimaryParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__0__Impl"


    // $ANTLR start "rule__OrConjunction__Group__1"
    // InternalDOF.g:3042:1: rule__OrConjunction__Group__1 : rule__OrConjunction__Group__1__Impl ;
    public final void rule__OrConjunction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3046:1: ( rule__OrConjunction__Group__1__Impl )
            // InternalDOF.g:3047:2: rule__OrConjunction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__1"


    // $ANTLR start "rule__OrConjunction__Group__1__Impl"
    // InternalDOF.g:3053:1: rule__OrConjunction__Group__1__Impl : ( ( rule__OrConjunction__Group_1__0 )* ) ;
    public final void rule__OrConjunction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3057:1: ( ( ( rule__OrConjunction__Group_1__0 )* ) )
            // InternalDOF.g:3058:1: ( ( rule__OrConjunction__Group_1__0 )* )
            {
            // InternalDOF.g:3058:1: ( ( rule__OrConjunction__Group_1__0 )* )
            // InternalDOF.g:3059:2: ( rule__OrConjunction__Group_1__0 )*
            {
             before(grammarAccess.getOrConjunctionAccess().getGroup_1()); 
            // InternalDOF.g:3060:2: ( rule__OrConjunction__Group_1__0 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==36) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalDOF.g:3060:3: rule__OrConjunction__Group_1__0
            	    {
            	    pushFollow(FOLLOW_38);
            	    rule__OrConjunction__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

             after(grammarAccess.getOrConjunctionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group__1__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__0"
    // InternalDOF.g:3069:1: rule__OrConjunction__Group_1__0 : rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1 ;
    public final void rule__OrConjunction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3073:1: ( rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1 )
            // InternalDOF.g:3074:2: rule__OrConjunction__Group_1__0__Impl rule__OrConjunction__Group_1__1
            {
            pushFollow(FOLLOW_37);
            rule__OrConjunction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__0"


    // $ANTLR start "rule__OrConjunction__Group_1__0__Impl"
    // InternalDOF.g:3081:1: rule__OrConjunction__Group_1__0__Impl : ( () ) ;
    public final void rule__OrConjunction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3085:1: ( ( () ) )
            // InternalDOF.g:3086:1: ( () )
            {
            // InternalDOF.g:3086:1: ( () )
            // InternalDOF.g:3087:2: ()
            {
             before(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0()); 
            // InternalDOF.g:3088:2: ()
            // InternalDOF.g:3088:3: 
            {
            }

             after(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__0__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__1"
    // InternalDOF.g:3096:1: rule__OrConjunction__Group_1__1 : rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2 ;
    public final void rule__OrConjunction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3100:1: ( rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2 )
            // InternalDOF.g:3101:2: rule__OrConjunction__Group_1__1__Impl rule__OrConjunction__Group_1__2
            {
            pushFollow(FOLLOW_34);
            rule__OrConjunction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__1"


    // $ANTLR start "rule__OrConjunction__Group_1__1__Impl"
    // InternalDOF.g:3108:1: rule__OrConjunction__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__OrConjunction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3112:1: ( ( 'or' ) )
            // InternalDOF.g:3113:1: ( 'or' )
            {
            // InternalDOF.g:3113:1: ( 'or' )
            // InternalDOF.g:3114:2: 'or'
            {
             before(grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getOrConjunctionAccess().getOrKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__1__Impl"


    // $ANTLR start "rule__OrConjunction__Group_1__2"
    // InternalDOF.g:3123:1: rule__OrConjunction__Group_1__2 : rule__OrConjunction__Group_1__2__Impl ;
    public final void rule__OrConjunction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3127:1: ( rule__OrConjunction__Group_1__2__Impl )
            // InternalDOF.g:3128:2: rule__OrConjunction__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__2"


    // $ANTLR start "rule__OrConjunction__Group_1__2__Impl"
    // InternalDOF.g:3134:1: rule__OrConjunction__Group_1__2__Impl : ( ( rule__OrConjunction__RightAssignment_1_2 ) ) ;
    public final void rule__OrConjunction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3138:1: ( ( ( rule__OrConjunction__RightAssignment_1_2 ) ) )
            // InternalDOF.g:3139:1: ( ( rule__OrConjunction__RightAssignment_1_2 ) )
            {
            // InternalDOF.g:3139:1: ( ( rule__OrConjunction__RightAssignment_1_2 ) )
            // InternalDOF.g:3140:2: ( rule__OrConjunction__RightAssignment_1_2 )
            {
             before(grammarAccess.getOrConjunctionAccess().getRightAssignment_1_2()); 
            // InternalDOF.g:3141:2: ( rule__OrConjunction__RightAssignment_1_2 )
            // InternalDOF.g:3141:3: rule__OrConjunction__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__OrConjunction__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getOrConjunctionAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__Group_1__2__Impl"


    // $ANTLR start "rule__Primary__Group_1__0"
    // InternalDOF.g:3150:1: rule__Primary__Group_1__0 : rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 ;
    public final void rule__Primary__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3154:1: ( rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1 )
            // InternalDOF.g:3155:2: rule__Primary__Group_1__0__Impl rule__Primary__Group_1__1
            {
            pushFollow(FOLLOW_34);
            rule__Primary__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0"


    // $ANTLR start "rule__Primary__Group_1__0__Impl"
    // InternalDOF.g:3162:1: rule__Primary__Group_1__0__Impl : ( '(' ) ;
    public final void rule__Primary__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3166:1: ( ( '(' ) )
            // InternalDOF.g:3167:1: ( '(' )
            {
            // InternalDOF.g:3167:1: ( '(' )
            // InternalDOF.g:3168:2: '('
            {
             before(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getLeftParenthesisKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__0__Impl"


    // $ANTLR start "rule__Primary__Group_1__1"
    // InternalDOF.g:3177:1: rule__Primary__Group_1__1 : rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 ;
    public final void rule__Primary__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3181:1: ( rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2 )
            // InternalDOF.g:3182:2: rule__Primary__Group_1__1__Impl rule__Primary__Group_1__2
            {
            pushFollow(FOLLOW_26);
            rule__Primary__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1"


    // $ANTLR start "rule__Primary__Group_1__1__Impl"
    // InternalDOF.g:3189:1: rule__Primary__Group_1__1__Impl : ( ruleAndConjunction ) ;
    public final void rule__Primary__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3193:1: ( ( ruleAndConjunction ) )
            // InternalDOF.g:3194:1: ( ruleAndConjunction )
            {
            // InternalDOF.g:3194:1: ( ruleAndConjunction )
            // InternalDOF.g:3195:2: ruleAndConjunction
            {
             before(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1()); 
            pushFollow(FOLLOW_2);
            ruleAndConjunction();

            state._fsp--;

             after(grammarAccess.getPrimaryAccess().getAndConjunctionParserRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__1__Impl"


    // $ANTLR start "rule__Primary__Group_1__2"
    // InternalDOF.g:3204:1: rule__Primary__Group_1__2 : rule__Primary__Group_1__2__Impl ;
    public final void rule__Primary__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3208:1: ( rule__Primary__Group_1__2__Impl )
            // InternalDOF.g:3209:2: rule__Primary__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Primary__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2"


    // $ANTLR start "rule__Primary__Group_1__2__Impl"
    // InternalDOF.g:3215:1: rule__Primary__Group_1__2__Impl : ( ')' ) ;
    public final void rule__Primary__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3219:1: ( ( ')' ) )
            // InternalDOF.g:3220:1: ( ')' )
            {
            // InternalDOF.g:3220:1: ( ')' )
            // InternalDOF.g:3221:2: ')'
            {
             before(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getPrimaryAccess().getRightParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Primary__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group_0__0"
    // InternalDOF.g:3231:1: rule__Comparison__Group_0__0 : rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1 ;
    public final void rule__Comparison__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3235:1: ( rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1 )
            // InternalDOF.g:3236:2: rule__Comparison__Group_0__0__Impl rule__Comparison__Group_0__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__0"


    // $ANTLR start "rule__Comparison__Group_0__0__Impl"
    // InternalDOF.g:3243:1: rule__Comparison__Group_0__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3247:1: ( ( () ) )
            // InternalDOF.g:3248:1: ( () )
            {
            // InternalDOF.g:3248:1: ( () )
            // InternalDOF.g:3249:2: ()
            {
             before(grammarAccess.getComparisonAccess().getEqualityAction_0_0()); 
            // InternalDOF.g:3250:2: ()
            // InternalDOF.g:3250:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getEqualityAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__0__Impl"


    // $ANTLR start "rule__Comparison__Group_0__1"
    // InternalDOF.g:3258:1: rule__Comparison__Group_0__1 : rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2 ;
    public final void rule__Comparison__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3262:1: ( rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2 )
            // InternalDOF.g:3263:2: rule__Comparison__Group_0__1__Impl rule__Comparison__Group_0__2
            {
            pushFollow(FOLLOW_39);
            rule__Comparison__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__1"


    // $ANTLR start "rule__Comparison__Group_0__1__Impl"
    // InternalDOF.g:3270:1: rule__Comparison__Group_0__1__Impl : ( ( rule__Comparison__PathAssignment_0_1 ) ) ;
    public final void rule__Comparison__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3274:1: ( ( ( rule__Comparison__PathAssignment_0_1 ) ) )
            // InternalDOF.g:3275:1: ( ( rule__Comparison__PathAssignment_0_1 ) )
            {
            // InternalDOF.g:3275:1: ( ( rule__Comparison__PathAssignment_0_1 ) )
            // InternalDOF.g:3276:2: ( rule__Comparison__PathAssignment_0_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_0_1()); 
            // InternalDOF.g:3277:2: ( rule__Comparison__PathAssignment_0_1 )
            // InternalDOF.g:3277:3: rule__Comparison__PathAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__1__Impl"


    // $ANTLR start "rule__Comparison__Group_0__2"
    // InternalDOF.g:3285:1: rule__Comparison__Group_0__2 : rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3 ;
    public final void rule__Comparison__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3289:1: ( rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3 )
            // InternalDOF.g:3290:2: rule__Comparison__Group_0__2__Impl rule__Comparison__Group_0__3
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_0__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__2"


    // $ANTLR start "rule__Comparison__Group_0__2__Impl"
    // InternalDOF.g:3297:1: rule__Comparison__Group_0__2__Impl : ( '=' ) ;
    public final void rule__Comparison__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3301:1: ( ( '=' ) )
            // InternalDOF.g:3302:1: ( '=' )
            {
            // InternalDOF.g:3302:1: ( '=' )
            // InternalDOF.g:3303:2: '='
            {
             before(grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getEqualsSignKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__2__Impl"


    // $ANTLR start "rule__Comparison__Group_0__3"
    // InternalDOF.g:3312:1: rule__Comparison__Group_0__3 : rule__Comparison__Group_0__3__Impl ;
    public final void rule__Comparison__Group_0__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3316:1: ( rule__Comparison__Group_0__3__Impl )
            // InternalDOF.g:3317:2: rule__Comparison__Group_0__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_0__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__3"


    // $ANTLR start "rule__Comparison__Group_0__3__Impl"
    // InternalDOF.g:3323:1: rule__Comparison__Group_0__3__Impl : ( ( rule__Comparison__ValueAssignment_0_3 ) ) ;
    public final void rule__Comparison__Group_0__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3327:1: ( ( ( rule__Comparison__ValueAssignment_0_3 ) ) )
            // InternalDOF.g:3328:1: ( ( rule__Comparison__ValueAssignment_0_3 ) )
            {
            // InternalDOF.g:3328:1: ( ( rule__Comparison__ValueAssignment_0_3 ) )
            // InternalDOF.g:3329:2: ( rule__Comparison__ValueAssignment_0_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_0_3()); 
            // InternalDOF.g:3330:2: ( rule__Comparison__ValueAssignment_0_3 )
            // InternalDOF.g:3330:3: rule__Comparison__ValueAssignment_0_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_0_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_0_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_0__3__Impl"


    // $ANTLR start "rule__Comparison__Group_1__0"
    // InternalDOF.g:3339:1: rule__Comparison__Group_1__0 : rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 ;
    public final void rule__Comparison__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3343:1: ( rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1 )
            // InternalDOF.g:3344:2: rule__Comparison__Group_1__0__Impl rule__Comparison__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0"


    // $ANTLR start "rule__Comparison__Group_1__0__Impl"
    // InternalDOF.g:3351:1: rule__Comparison__Group_1__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3355:1: ( ( () ) )
            // InternalDOF.g:3356:1: ( () )
            {
            // InternalDOF.g:3356:1: ( () )
            // InternalDOF.g:3357:2: ()
            {
             before(grammarAccess.getComparisonAccess().getInequalityAction_1_0()); 
            // InternalDOF.g:3358:2: ()
            // InternalDOF.g:3358:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getInequalityAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__0__Impl"


    // $ANTLR start "rule__Comparison__Group_1__1"
    // InternalDOF.g:3366:1: rule__Comparison__Group_1__1 : rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 ;
    public final void rule__Comparison__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3370:1: ( rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2 )
            // InternalDOF.g:3371:2: rule__Comparison__Group_1__1__Impl rule__Comparison__Group_1__2
            {
            pushFollow(FOLLOW_41);
            rule__Comparison__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1"


    // $ANTLR start "rule__Comparison__Group_1__1__Impl"
    // InternalDOF.g:3378:1: rule__Comparison__Group_1__1__Impl : ( ( rule__Comparison__PathAssignment_1_1 ) ) ;
    public final void rule__Comparison__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3382:1: ( ( ( rule__Comparison__PathAssignment_1_1 ) ) )
            // InternalDOF.g:3383:1: ( ( rule__Comparison__PathAssignment_1_1 ) )
            {
            // InternalDOF.g:3383:1: ( ( rule__Comparison__PathAssignment_1_1 ) )
            // InternalDOF.g:3384:2: ( rule__Comparison__PathAssignment_1_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_1_1()); 
            // InternalDOF.g:3385:2: ( rule__Comparison__PathAssignment_1_1 )
            // InternalDOF.g:3385:3: rule__Comparison__PathAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__1__Impl"


    // $ANTLR start "rule__Comparison__Group_1__2"
    // InternalDOF.g:3393:1: rule__Comparison__Group_1__2 : rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3 ;
    public final void rule__Comparison__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3397:1: ( rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3 )
            // InternalDOF.g:3398:2: rule__Comparison__Group_1__2__Impl rule__Comparison__Group_1__3
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2"


    // $ANTLR start "rule__Comparison__Group_1__2__Impl"
    // InternalDOF.g:3405:1: rule__Comparison__Group_1__2__Impl : ( '!=' ) ;
    public final void rule__Comparison__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3409:1: ( ( '!=' ) )
            // InternalDOF.g:3410:1: ( '!=' )
            {
            // InternalDOF.g:3410:1: ( '!=' )
            // InternalDOF.g:3411:2: '!='
            {
             before(grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getExclamationMarkEqualsSignKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__2__Impl"


    // $ANTLR start "rule__Comparison__Group_1__3"
    // InternalDOF.g:3420:1: rule__Comparison__Group_1__3 : rule__Comparison__Group_1__3__Impl ;
    public final void rule__Comparison__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3424:1: ( rule__Comparison__Group_1__3__Impl )
            // InternalDOF.g:3425:2: rule__Comparison__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__3"


    // $ANTLR start "rule__Comparison__Group_1__3__Impl"
    // InternalDOF.g:3431:1: rule__Comparison__Group_1__3__Impl : ( ( rule__Comparison__ValueAssignment_1_3 ) ) ;
    public final void rule__Comparison__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3435:1: ( ( ( rule__Comparison__ValueAssignment_1_3 ) ) )
            // InternalDOF.g:3436:1: ( ( rule__Comparison__ValueAssignment_1_3 ) )
            {
            // InternalDOF.g:3436:1: ( ( rule__Comparison__ValueAssignment_1_3 ) )
            // InternalDOF.g:3437:2: ( rule__Comparison__ValueAssignment_1_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_1_3()); 
            // InternalDOF.g:3438:2: ( rule__Comparison__ValueAssignment_1_3 )
            // InternalDOF.g:3438:3: rule__Comparison__ValueAssignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_1__3__Impl"


    // $ANTLR start "rule__Comparison__Group_2__0"
    // InternalDOF.g:3447:1: rule__Comparison__Group_2__0 : rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1 ;
    public final void rule__Comparison__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3451:1: ( rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1 )
            // InternalDOF.g:3452:2: rule__Comparison__Group_2__0__Impl rule__Comparison__Group_2__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__0"


    // $ANTLR start "rule__Comparison__Group_2__0__Impl"
    // InternalDOF.g:3459:1: rule__Comparison__Group_2__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3463:1: ( ( () ) )
            // InternalDOF.g:3464:1: ( () )
            {
            // InternalDOF.g:3464:1: ( () )
            // InternalDOF.g:3465:2: ()
            {
             before(grammarAccess.getComparisonAccess().getMoreThanAction_2_0()); 
            // InternalDOF.g:3466:2: ()
            // InternalDOF.g:3466:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getMoreThanAction_2_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__0__Impl"


    // $ANTLR start "rule__Comparison__Group_2__1"
    // InternalDOF.g:3474:1: rule__Comparison__Group_2__1 : rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2 ;
    public final void rule__Comparison__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3478:1: ( rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2 )
            // InternalDOF.g:3479:2: rule__Comparison__Group_2__1__Impl rule__Comparison__Group_2__2
            {
            pushFollow(FOLLOW_42);
            rule__Comparison__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__1"


    // $ANTLR start "rule__Comparison__Group_2__1__Impl"
    // InternalDOF.g:3486:1: rule__Comparison__Group_2__1__Impl : ( ( rule__Comparison__PathAssignment_2_1 ) ) ;
    public final void rule__Comparison__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3490:1: ( ( ( rule__Comparison__PathAssignment_2_1 ) ) )
            // InternalDOF.g:3491:1: ( ( rule__Comparison__PathAssignment_2_1 ) )
            {
            // InternalDOF.g:3491:1: ( ( rule__Comparison__PathAssignment_2_1 ) )
            // InternalDOF.g:3492:2: ( rule__Comparison__PathAssignment_2_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_2_1()); 
            // InternalDOF.g:3493:2: ( rule__Comparison__PathAssignment_2_1 )
            // InternalDOF.g:3493:3: rule__Comparison__PathAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__1__Impl"


    // $ANTLR start "rule__Comparison__Group_2__2"
    // InternalDOF.g:3501:1: rule__Comparison__Group_2__2 : rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3 ;
    public final void rule__Comparison__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3505:1: ( rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3 )
            // InternalDOF.g:3506:2: rule__Comparison__Group_2__2__Impl rule__Comparison__Group_2__3
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__2"


    // $ANTLR start "rule__Comparison__Group_2__2__Impl"
    // InternalDOF.g:3513:1: rule__Comparison__Group_2__2__Impl : ( '>' ) ;
    public final void rule__Comparison__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3517:1: ( ( '>' ) )
            // InternalDOF.g:3518:1: ( '>' )
            {
            // InternalDOF.g:3518:1: ( '>' )
            // InternalDOF.g:3519:2: '>'
            {
             before(grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getGreaterThanSignKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__2__Impl"


    // $ANTLR start "rule__Comparison__Group_2__3"
    // InternalDOF.g:3528:1: rule__Comparison__Group_2__3 : rule__Comparison__Group_2__3__Impl ;
    public final void rule__Comparison__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3532:1: ( rule__Comparison__Group_2__3__Impl )
            // InternalDOF.g:3533:2: rule__Comparison__Group_2__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__3"


    // $ANTLR start "rule__Comparison__Group_2__3__Impl"
    // InternalDOF.g:3539:1: rule__Comparison__Group_2__3__Impl : ( ( rule__Comparison__ValueAssignment_2_3 ) ) ;
    public final void rule__Comparison__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3543:1: ( ( ( rule__Comparison__ValueAssignment_2_3 ) ) )
            // InternalDOF.g:3544:1: ( ( rule__Comparison__ValueAssignment_2_3 ) )
            {
            // InternalDOF.g:3544:1: ( ( rule__Comparison__ValueAssignment_2_3 ) )
            // InternalDOF.g:3545:2: ( rule__Comparison__ValueAssignment_2_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_2_3()); 
            // InternalDOF.g:3546:2: ( rule__Comparison__ValueAssignment_2_3 )
            // InternalDOF.g:3546:3: rule__Comparison__ValueAssignment_2_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_2__3__Impl"


    // $ANTLR start "rule__Comparison__Group_3__0"
    // InternalDOF.g:3555:1: rule__Comparison__Group_3__0 : rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1 ;
    public final void rule__Comparison__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3559:1: ( rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1 )
            // InternalDOF.g:3560:2: rule__Comparison__Group_3__0__Impl rule__Comparison__Group_3__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__0"


    // $ANTLR start "rule__Comparison__Group_3__0__Impl"
    // InternalDOF.g:3567:1: rule__Comparison__Group_3__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3571:1: ( ( () ) )
            // InternalDOF.g:3572:1: ( () )
            {
            // InternalDOF.g:3572:1: ( () )
            // InternalDOF.g:3573:2: ()
            {
             before(grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0()); 
            // InternalDOF.g:3574:2: ()
            // InternalDOF.g:3574:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getMoreThanOrEqualAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__0__Impl"


    // $ANTLR start "rule__Comparison__Group_3__1"
    // InternalDOF.g:3582:1: rule__Comparison__Group_3__1 : rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2 ;
    public final void rule__Comparison__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3586:1: ( rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2 )
            // InternalDOF.g:3587:2: rule__Comparison__Group_3__1__Impl rule__Comparison__Group_3__2
            {
            pushFollow(FOLLOW_43);
            rule__Comparison__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__1"


    // $ANTLR start "rule__Comparison__Group_3__1__Impl"
    // InternalDOF.g:3594:1: rule__Comparison__Group_3__1__Impl : ( ( rule__Comparison__PathAssignment_3_1 ) ) ;
    public final void rule__Comparison__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3598:1: ( ( ( rule__Comparison__PathAssignment_3_1 ) ) )
            // InternalDOF.g:3599:1: ( ( rule__Comparison__PathAssignment_3_1 ) )
            {
            // InternalDOF.g:3599:1: ( ( rule__Comparison__PathAssignment_3_1 ) )
            // InternalDOF.g:3600:2: ( rule__Comparison__PathAssignment_3_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_3_1()); 
            // InternalDOF.g:3601:2: ( rule__Comparison__PathAssignment_3_1 )
            // InternalDOF.g:3601:3: rule__Comparison__PathAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__1__Impl"


    // $ANTLR start "rule__Comparison__Group_3__2"
    // InternalDOF.g:3609:1: rule__Comparison__Group_3__2 : rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3 ;
    public final void rule__Comparison__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3613:1: ( rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3 )
            // InternalDOF.g:3614:2: rule__Comparison__Group_3__2__Impl rule__Comparison__Group_3__3
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__2"


    // $ANTLR start "rule__Comparison__Group_3__2__Impl"
    // InternalDOF.g:3621:1: rule__Comparison__Group_3__2__Impl : ( '>=' ) ;
    public final void rule__Comparison__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3625:1: ( ( '>=' ) )
            // InternalDOF.g:3626:1: ( '>=' )
            {
            // InternalDOF.g:3626:1: ( '>=' )
            // InternalDOF.g:3627:2: '>='
            {
             before(grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getGreaterThanSignEqualsSignKeyword_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__2__Impl"


    // $ANTLR start "rule__Comparison__Group_3__3"
    // InternalDOF.g:3636:1: rule__Comparison__Group_3__3 : rule__Comparison__Group_3__3__Impl ;
    public final void rule__Comparison__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3640:1: ( rule__Comparison__Group_3__3__Impl )
            // InternalDOF.g:3641:2: rule__Comparison__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__3"


    // $ANTLR start "rule__Comparison__Group_3__3__Impl"
    // InternalDOF.g:3647:1: rule__Comparison__Group_3__3__Impl : ( ( rule__Comparison__ValueAssignment_3_3 ) ) ;
    public final void rule__Comparison__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3651:1: ( ( ( rule__Comparison__ValueAssignment_3_3 ) ) )
            // InternalDOF.g:3652:1: ( ( rule__Comparison__ValueAssignment_3_3 ) )
            {
            // InternalDOF.g:3652:1: ( ( rule__Comparison__ValueAssignment_3_3 ) )
            // InternalDOF.g:3653:2: ( rule__Comparison__ValueAssignment_3_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_3_3()); 
            // InternalDOF.g:3654:2: ( rule__Comparison__ValueAssignment_3_3 )
            // InternalDOF.g:3654:3: rule__Comparison__ValueAssignment_3_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_3_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_3__3__Impl"


    // $ANTLR start "rule__Comparison__Group_4__0"
    // InternalDOF.g:3663:1: rule__Comparison__Group_4__0 : rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1 ;
    public final void rule__Comparison__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3667:1: ( rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1 )
            // InternalDOF.g:3668:2: rule__Comparison__Group_4__0__Impl rule__Comparison__Group_4__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__0"


    // $ANTLR start "rule__Comparison__Group_4__0__Impl"
    // InternalDOF.g:3675:1: rule__Comparison__Group_4__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3679:1: ( ( () ) )
            // InternalDOF.g:3680:1: ( () )
            {
            // InternalDOF.g:3680:1: ( () )
            // InternalDOF.g:3681:2: ()
            {
             before(grammarAccess.getComparisonAccess().getLessThanAction_4_0()); 
            // InternalDOF.g:3682:2: ()
            // InternalDOF.g:3682:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getLessThanAction_4_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__0__Impl"


    // $ANTLR start "rule__Comparison__Group_4__1"
    // InternalDOF.g:3690:1: rule__Comparison__Group_4__1 : rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2 ;
    public final void rule__Comparison__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3694:1: ( rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2 )
            // InternalDOF.g:3695:2: rule__Comparison__Group_4__1__Impl rule__Comparison__Group_4__2
            {
            pushFollow(FOLLOW_44);
            rule__Comparison__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__1"


    // $ANTLR start "rule__Comparison__Group_4__1__Impl"
    // InternalDOF.g:3702:1: rule__Comparison__Group_4__1__Impl : ( ( rule__Comparison__PathAssignment_4_1 ) ) ;
    public final void rule__Comparison__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3706:1: ( ( ( rule__Comparison__PathAssignment_4_1 ) ) )
            // InternalDOF.g:3707:1: ( ( rule__Comparison__PathAssignment_4_1 ) )
            {
            // InternalDOF.g:3707:1: ( ( rule__Comparison__PathAssignment_4_1 ) )
            // InternalDOF.g:3708:2: ( rule__Comparison__PathAssignment_4_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_4_1()); 
            // InternalDOF.g:3709:2: ( rule__Comparison__PathAssignment_4_1 )
            // InternalDOF.g:3709:3: rule__Comparison__PathAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__1__Impl"


    // $ANTLR start "rule__Comparison__Group_4__2"
    // InternalDOF.g:3717:1: rule__Comparison__Group_4__2 : rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3 ;
    public final void rule__Comparison__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3721:1: ( rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3 )
            // InternalDOF.g:3722:2: rule__Comparison__Group_4__2__Impl rule__Comparison__Group_4__3
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__2"


    // $ANTLR start "rule__Comparison__Group_4__2__Impl"
    // InternalDOF.g:3729:1: rule__Comparison__Group_4__2__Impl : ( '<' ) ;
    public final void rule__Comparison__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3733:1: ( ( '<' ) )
            // InternalDOF.g:3734:1: ( '<' )
            {
            // InternalDOF.g:3734:1: ( '<' )
            // InternalDOF.g:3735:2: '<'
            {
             before(grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getLessThanSignKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__2__Impl"


    // $ANTLR start "rule__Comparison__Group_4__3"
    // InternalDOF.g:3744:1: rule__Comparison__Group_4__3 : rule__Comparison__Group_4__3__Impl ;
    public final void rule__Comparison__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3748:1: ( rule__Comparison__Group_4__3__Impl )
            // InternalDOF.g:3749:2: rule__Comparison__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__3"


    // $ANTLR start "rule__Comparison__Group_4__3__Impl"
    // InternalDOF.g:3755:1: rule__Comparison__Group_4__3__Impl : ( ( rule__Comparison__ValueAssignment_4_3 ) ) ;
    public final void rule__Comparison__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3759:1: ( ( ( rule__Comparison__ValueAssignment_4_3 ) ) )
            // InternalDOF.g:3760:1: ( ( rule__Comparison__ValueAssignment_4_3 ) )
            {
            // InternalDOF.g:3760:1: ( ( rule__Comparison__ValueAssignment_4_3 ) )
            // InternalDOF.g:3761:2: ( rule__Comparison__ValueAssignment_4_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_4_3()); 
            // InternalDOF.g:3762:2: ( rule__Comparison__ValueAssignment_4_3 )
            // InternalDOF.g:3762:3: rule__Comparison__ValueAssignment_4_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_4_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_4__3__Impl"


    // $ANTLR start "rule__Comparison__Group_5__0"
    // InternalDOF.g:3771:1: rule__Comparison__Group_5__0 : rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1 ;
    public final void rule__Comparison__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3775:1: ( rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1 )
            // InternalDOF.g:3776:2: rule__Comparison__Group_5__0__Impl rule__Comparison__Group_5__1
            {
            pushFollow(FOLLOW_7);
            rule__Comparison__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__0"


    // $ANTLR start "rule__Comparison__Group_5__0__Impl"
    // InternalDOF.g:3783:1: rule__Comparison__Group_5__0__Impl : ( () ) ;
    public final void rule__Comparison__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3787:1: ( ( () ) )
            // InternalDOF.g:3788:1: ( () )
            {
            // InternalDOF.g:3788:1: ( () )
            // InternalDOF.g:3789:2: ()
            {
             before(grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0()); 
            // InternalDOF.g:3790:2: ()
            // InternalDOF.g:3790:3: 
            {
            }

             after(grammarAccess.getComparisonAccess().getLessThanOrEqualAction_5_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__0__Impl"


    // $ANTLR start "rule__Comparison__Group_5__1"
    // InternalDOF.g:3798:1: rule__Comparison__Group_5__1 : rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2 ;
    public final void rule__Comparison__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3802:1: ( rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2 )
            // InternalDOF.g:3803:2: rule__Comparison__Group_5__1__Impl rule__Comparison__Group_5__2
            {
            pushFollow(FOLLOW_45);
            rule__Comparison__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__1"


    // $ANTLR start "rule__Comparison__Group_5__1__Impl"
    // InternalDOF.g:3810:1: rule__Comparison__Group_5__1__Impl : ( ( rule__Comparison__PathAssignment_5_1 ) ) ;
    public final void rule__Comparison__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3814:1: ( ( ( rule__Comparison__PathAssignment_5_1 ) ) )
            // InternalDOF.g:3815:1: ( ( rule__Comparison__PathAssignment_5_1 ) )
            {
            // InternalDOF.g:3815:1: ( ( rule__Comparison__PathAssignment_5_1 ) )
            // InternalDOF.g:3816:2: ( rule__Comparison__PathAssignment_5_1 )
            {
             before(grammarAccess.getComparisonAccess().getPathAssignment_5_1()); 
            // InternalDOF.g:3817:2: ( rule__Comparison__PathAssignment_5_1 )
            // InternalDOF.g:3817:3: rule__Comparison__PathAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__PathAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getPathAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__1__Impl"


    // $ANTLR start "rule__Comparison__Group_5__2"
    // InternalDOF.g:3825:1: rule__Comparison__Group_5__2 : rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3 ;
    public final void rule__Comparison__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3829:1: ( rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3 )
            // InternalDOF.g:3830:2: rule__Comparison__Group_5__2__Impl rule__Comparison__Group_5__3
            {
            pushFollow(FOLLOW_40);
            rule__Comparison__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__2"


    // $ANTLR start "rule__Comparison__Group_5__2__Impl"
    // InternalDOF.g:3837:1: rule__Comparison__Group_5__2__Impl : ( '<=' ) ;
    public final void rule__Comparison__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3841:1: ( ( '<=' ) )
            // InternalDOF.g:3842:1: ( '<=' )
            {
            // InternalDOF.g:3842:1: ( '<=' )
            // InternalDOF.g:3843:2: '<='
            {
             before(grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getLessThanSignEqualsSignKeyword_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__2__Impl"


    // $ANTLR start "rule__Comparison__Group_5__3"
    // InternalDOF.g:3852:1: rule__Comparison__Group_5__3 : rule__Comparison__Group_5__3__Impl ;
    public final void rule__Comparison__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3856:1: ( rule__Comparison__Group_5__3__Impl )
            // InternalDOF.g:3857:2: rule__Comparison__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__3"


    // $ANTLR start "rule__Comparison__Group_5__3__Impl"
    // InternalDOF.g:3863:1: rule__Comparison__Group_5__3__Impl : ( ( rule__Comparison__ValueAssignment_5_3 ) ) ;
    public final void rule__Comparison__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3867:1: ( ( ( rule__Comparison__ValueAssignment_5_3 ) ) )
            // InternalDOF.g:3868:1: ( ( rule__Comparison__ValueAssignment_5_3 ) )
            {
            // InternalDOF.g:3868:1: ( ( rule__Comparison__ValueAssignment_5_3 ) )
            // InternalDOF.g:3869:2: ( rule__Comparison__ValueAssignment_5_3 )
            {
             before(grammarAccess.getComparisonAccess().getValueAssignment_5_3()); 
            // InternalDOF.g:3870:2: ( rule__Comparison__ValueAssignment_5_3 )
            // InternalDOF.g:3870:3: rule__Comparison__ValueAssignment_5_3
            {
            pushFollow(FOLLOW_2);
            rule__Comparison__ValueAssignment_5_3();

            state._fsp--;


            }

             after(grammarAccess.getComparisonAccess().getValueAssignment_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__Group_5__3__Impl"


    // $ANTLR start "rule__CompoundCause__Group__0"
    // InternalDOF.g:3879:1: rule__CompoundCause__Group__0 : rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1 ;
    public final void rule__CompoundCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3883:1: ( rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1 )
            // InternalDOF.g:3884:2: rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__CompoundCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__0"


    // $ANTLR start "rule__CompoundCause__Group__0__Impl"
    // InternalDOF.g:3891:1: rule__CompoundCause__Group__0__Impl : ( () ) ;
    public final void rule__CompoundCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3895:1: ( ( () ) )
            // InternalDOF.g:3896:1: ( () )
            {
            // InternalDOF.g:3896:1: ( () )
            // InternalDOF.g:3897:2: ()
            {
             before(grammarAccess.getCompoundCauseAccess().getCompoundCauseAction_0()); 
            // InternalDOF.g:3898:2: ()
            // InternalDOF.g:3898:3: 
            {
            }

             after(grammarAccess.getCompoundCauseAccess().getCompoundCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__0__Impl"


    // $ANTLR start "rule__CompoundCause__Group__1"
    // InternalDOF.g:3906:1: rule__CompoundCause__Group__1 : rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2 ;
    public final void rule__CompoundCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3910:1: ( rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2 )
            // InternalDOF.g:3911:2: rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__CompoundCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__1"


    // $ANTLR start "rule__CompoundCause__Group__1__Impl"
    // InternalDOF.g:3918:1: rule__CompoundCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__CompoundCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3922:1: ( ( 'cause' ) )
            // InternalDOF.g:3923:1: ( 'cause' )
            {
            // InternalDOF.g:3923:1: ( 'cause' )
            // InternalDOF.g:3924:2: 'cause'
            {
             before(grammarAccess.getCompoundCauseAccess().getCauseKeyword_1()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__1__Impl"


    // $ANTLR start "rule__CompoundCause__Group__2"
    // InternalDOF.g:3933:1: rule__CompoundCause__Group__2 : rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3 ;
    public final void rule__CompoundCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3937:1: ( rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3 )
            // InternalDOF.g:3938:2: rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3
            {
            pushFollow(FOLLOW_46);
            rule__CompoundCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__2"


    // $ANTLR start "rule__CompoundCause__Group__2__Impl"
    // InternalDOF.g:3945:1: rule__CompoundCause__Group__2__Impl : ( ( rule__CompoundCause__NameAssignment_2 ) ) ;
    public final void rule__CompoundCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3949:1: ( ( ( rule__CompoundCause__NameAssignment_2 ) ) )
            // InternalDOF.g:3950:1: ( ( rule__CompoundCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:3950:1: ( ( rule__CompoundCause__NameAssignment_2 ) )
            // InternalDOF.g:3951:2: ( rule__CompoundCause__NameAssignment_2 )
            {
             before(grammarAccess.getCompoundCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:3952:2: ( rule__CompoundCause__NameAssignment_2 )
            // InternalDOF.g:3952:3: rule__CompoundCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__2__Impl"


    // $ANTLR start "rule__CompoundCause__Group__3"
    // InternalDOF.g:3960:1: rule__CompoundCause__Group__3 : rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4 ;
    public final void rule__CompoundCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3964:1: ( rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4 )
            // InternalDOF.g:3965:2: rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4
            {
            pushFollow(FOLLOW_46);
            rule__CompoundCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__3"


    // $ANTLR start "rule__CompoundCause__Group__3__Impl"
    // InternalDOF.g:3972:1: rule__CompoundCause__Group__3__Impl : ( ( rule__CompoundCause__Group_3__0 )? ) ;
    public final void rule__CompoundCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3976:1: ( ( ( rule__CompoundCause__Group_3__0 )? ) )
            // InternalDOF.g:3977:1: ( ( rule__CompoundCause__Group_3__0 )? )
            {
            // InternalDOF.g:3977:1: ( ( rule__CompoundCause__Group_3__0 )? )
            // InternalDOF.g:3978:2: ( rule__CompoundCause__Group_3__0 )?
            {
             before(grammarAccess.getCompoundCauseAccess().getGroup_3()); 
            // InternalDOF.g:3979:2: ( rule__CompoundCause__Group_3__0 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==45) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalDOF.g:3979:3: rule__CompoundCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CompoundCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCompoundCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__3__Impl"


    // $ANTLR start "rule__CompoundCause__Group__4"
    // InternalDOF.g:3987:1: rule__CompoundCause__Group__4 : rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5 ;
    public final void rule__CompoundCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:3991:1: ( rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5 )
            // InternalDOF.g:3992:2: rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5
            {
            pushFollow(FOLLOW_47);
            rule__CompoundCause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__4"


    // $ANTLR start "rule__CompoundCause__Group__4__Impl"
    // InternalDOF.g:3999:1: rule__CompoundCause__Group__4__Impl : ( 'contains' ) ;
    public final void rule__CompoundCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4003:1: ( ( 'contains' ) )
            // InternalDOF.g:4004:1: ( 'contains' )
            {
            // InternalDOF.g:4004:1: ( 'contains' )
            // InternalDOF.g:4005:2: 'contains'
            {
             before(grammarAccess.getCompoundCauseAccess().getContainsKeyword_4()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getContainsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__4__Impl"


    // $ANTLR start "rule__CompoundCause__Group__5"
    // InternalDOF.g:4014:1: rule__CompoundCause__Group__5 : rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6 ;
    public final void rule__CompoundCause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4018:1: ( rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6 )
            // InternalDOF.g:4019:2: rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6
            {
            pushFollow(FOLLOW_12);
            rule__CompoundCause__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__5"


    // $ANTLR start "rule__CompoundCause__Group__5__Impl"
    // InternalDOF.g:4026:1: rule__CompoundCause__Group__5__Impl : ( '{' ) ;
    public final void rule__CompoundCause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4030:1: ( ( '{' ) )
            // InternalDOF.g:4031:1: ( '{' )
            {
            // InternalDOF.g:4031:1: ( '{' )
            // InternalDOF.g:4032:2: '{'
            {
             before(grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__5__Impl"


    // $ANTLR start "rule__CompoundCause__Group__6"
    // InternalDOF.g:4041:1: rule__CompoundCause__Group__6 : rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7 ;
    public final void rule__CompoundCause__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4045:1: ( rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7 )
            // InternalDOF.g:4046:2: rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7
            {
            pushFollow(FOLLOW_48);
            rule__CompoundCause__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__6"


    // $ANTLR start "rule__CompoundCause__Group__6__Impl"
    // InternalDOF.g:4053:1: rule__CompoundCause__Group__6__Impl : ( ( rule__CompoundCause__SubCausesAssignment_6 ) ) ;
    public final void rule__CompoundCause__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4057:1: ( ( ( rule__CompoundCause__SubCausesAssignment_6 ) ) )
            // InternalDOF.g:4058:1: ( ( rule__CompoundCause__SubCausesAssignment_6 ) )
            {
            // InternalDOF.g:4058:1: ( ( rule__CompoundCause__SubCausesAssignment_6 ) )
            // InternalDOF.g:4059:2: ( rule__CompoundCause__SubCausesAssignment_6 )
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_6()); 
            // InternalDOF.g:4060:2: ( rule__CompoundCause__SubCausesAssignment_6 )
            // InternalDOF.g:4060:3: rule__CompoundCause__SubCausesAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__SubCausesAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__6__Impl"


    // $ANTLR start "rule__CompoundCause__Group__7"
    // InternalDOF.g:4068:1: rule__CompoundCause__Group__7 : rule__CompoundCause__Group__7__Impl rule__CompoundCause__Group__8 ;
    public final void rule__CompoundCause__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4072:1: ( rule__CompoundCause__Group__7__Impl rule__CompoundCause__Group__8 )
            // InternalDOF.g:4073:2: rule__CompoundCause__Group__7__Impl rule__CompoundCause__Group__8
            {
            pushFollow(FOLLOW_48);
            rule__CompoundCause__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__7"


    // $ANTLR start "rule__CompoundCause__Group__7__Impl"
    // InternalDOF.g:4080:1: rule__CompoundCause__Group__7__Impl : ( ( rule__CompoundCause__SubCausesAssignment_7 )* ) ;
    public final void rule__CompoundCause__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4084:1: ( ( ( rule__CompoundCause__SubCausesAssignment_7 )* ) )
            // InternalDOF.g:4085:1: ( ( rule__CompoundCause__SubCausesAssignment_7 )* )
            {
            // InternalDOF.g:4085:1: ( ( rule__CompoundCause__SubCausesAssignment_7 )* )
            // InternalDOF.g:4086:2: ( rule__CompoundCause__SubCausesAssignment_7 )*
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_7()); 
            // InternalDOF.g:4087:2: ( rule__CompoundCause__SubCausesAssignment_7 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==43) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalDOF.g:4087:3: rule__CompoundCause__SubCausesAssignment_7
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__CompoundCause__SubCausesAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__7__Impl"


    // $ANTLR start "rule__CompoundCause__Group__8"
    // InternalDOF.g:4095:1: rule__CompoundCause__Group__8 : rule__CompoundCause__Group__8__Impl ;
    public final void rule__CompoundCause__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4099:1: ( rule__CompoundCause__Group__8__Impl )
            // InternalDOF.g:4100:2: rule__CompoundCause__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__8"


    // $ANTLR start "rule__CompoundCause__Group__8__Impl"
    // InternalDOF.g:4106:1: rule__CompoundCause__Group__8__Impl : ( '}' ) ;
    public final void rule__CompoundCause__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4110:1: ( ( '}' ) )
            // InternalDOF.g:4111:1: ( '}' )
            {
            // InternalDOF.g:4111:1: ( '}' )
            // InternalDOF.g:4112:2: '}'
            {
             before(grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_8()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__8__Impl"


    // $ANTLR start "rule__CompoundCause__Group_3__0"
    // InternalDOF.g:4122:1: rule__CompoundCause__Group_3__0 : rule__CompoundCause__Group_3__0__Impl rule__CompoundCause__Group_3__1 ;
    public final void rule__CompoundCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4126:1: ( rule__CompoundCause__Group_3__0__Impl rule__CompoundCause__Group_3__1 )
            // InternalDOF.g:4127:2: rule__CompoundCause__Group_3__0__Impl rule__CompoundCause__Group_3__1
            {
            pushFollow(FOLLOW_40);
            rule__CompoundCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__0"


    // $ANTLR start "rule__CompoundCause__Group_3__0__Impl"
    // InternalDOF.g:4134:1: rule__CompoundCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__CompoundCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4138:1: ( ( 'realizes' ) )
            // InternalDOF.g:4139:1: ( 'realizes' )
            {
            // InternalDOF.g:4139:1: ( 'realizes' )
            // InternalDOF.g:4140:2: 'realizes'
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesKeyword_3_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__0__Impl"


    // $ANTLR start "rule__CompoundCause__Group_3__1"
    // InternalDOF.g:4149:1: rule__CompoundCause__Group_3__1 : rule__CompoundCause__Group_3__1__Impl ;
    public final void rule__CompoundCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4153:1: ( rule__CompoundCause__Group_3__1__Impl )
            // InternalDOF.g:4154:2: rule__CompoundCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__1"


    // $ANTLR start "rule__CompoundCause__Group_3__1__Impl"
    // InternalDOF.g:4160:1: rule__CompoundCause__Group_3__1__Impl : ( ( rule__CompoundCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__CompoundCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4164:1: ( ( ( rule__CompoundCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:4165:1: ( ( rule__CompoundCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:4165:1: ( ( rule__CompoundCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:4166:2: ( rule__CompoundCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:4167:2: ( rule__CompoundCause__RealizesAssignment_3_1 )
            // InternalDOF.g:4167:3: rule__CompoundCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_3__1__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__0"
    // InternalDOF.g:4176:1: rule__DataLinkedCause__Group__0 : rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1 ;
    public final void rule__DataLinkedCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4180:1: ( rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1 )
            // InternalDOF.g:4181:2: rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__DataLinkedCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__0"


    // $ANTLR start "rule__DataLinkedCause__Group__0__Impl"
    // InternalDOF.g:4188:1: rule__DataLinkedCause__Group__0__Impl : ( () ) ;
    public final void rule__DataLinkedCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4192:1: ( ( () ) )
            // InternalDOF.g:4193:1: ( () )
            {
            // InternalDOF.g:4193:1: ( () )
            // InternalDOF.g:4194:2: ()
            {
             before(grammarAccess.getDataLinkedCauseAccess().getDataLinkedCauseAction_0()); 
            // InternalDOF.g:4195:2: ()
            // InternalDOF.g:4195:3: 
            {
            }

             after(grammarAccess.getDataLinkedCauseAccess().getDataLinkedCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__0__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__1"
    // InternalDOF.g:4203:1: rule__DataLinkedCause__Group__1 : rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2 ;
    public final void rule__DataLinkedCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4207:1: ( rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2 )
            // InternalDOF.g:4208:2: rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__DataLinkedCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__1"


    // $ANTLR start "rule__DataLinkedCause__Group__1__Impl"
    // InternalDOF.g:4215:1: rule__DataLinkedCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__DataLinkedCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4219:1: ( ( 'cause' ) )
            // InternalDOF.g:4220:1: ( 'cause' )
            {
            // InternalDOF.g:4220:1: ( 'cause' )
            // InternalDOF.g:4221:2: 'cause'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_1()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__1__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__2"
    // InternalDOF.g:4230:1: rule__DataLinkedCause__Group__2 : rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3 ;
    public final void rule__DataLinkedCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4234:1: ( rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3 )
            // InternalDOF.g:4235:2: rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3
            {
            pushFollow(FOLLOW_49);
            rule__DataLinkedCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__2"


    // $ANTLR start "rule__DataLinkedCause__Group__2__Impl"
    // InternalDOF.g:4242:1: rule__DataLinkedCause__Group__2__Impl : ( ( rule__DataLinkedCause__NameAssignment_2 ) ) ;
    public final void rule__DataLinkedCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4246:1: ( ( ( rule__DataLinkedCause__NameAssignment_2 ) ) )
            // InternalDOF.g:4247:1: ( ( rule__DataLinkedCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:4247:1: ( ( rule__DataLinkedCause__NameAssignment_2 ) )
            // InternalDOF.g:4248:2: ( rule__DataLinkedCause__NameAssignment_2 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:4249:2: ( rule__DataLinkedCause__NameAssignment_2 )
            // InternalDOF.g:4249:3: rule__DataLinkedCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__2__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__3"
    // InternalDOF.g:4257:1: rule__DataLinkedCause__Group__3 : rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4 ;
    public final void rule__DataLinkedCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4261:1: ( rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4 )
            // InternalDOF.g:4262:2: rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4
            {
            pushFollow(FOLLOW_49);
            rule__DataLinkedCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__3"


    // $ANTLR start "rule__DataLinkedCause__Group__3__Impl"
    // InternalDOF.g:4269:1: rule__DataLinkedCause__Group__3__Impl : ( ( rule__DataLinkedCause__Group_3__0 )? ) ;
    public final void rule__DataLinkedCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4273:1: ( ( ( rule__DataLinkedCause__Group_3__0 )? ) )
            // InternalDOF.g:4274:1: ( ( rule__DataLinkedCause__Group_3__0 )? )
            {
            // InternalDOF.g:4274:1: ( ( rule__DataLinkedCause__Group_3__0 )? )
            // InternalDOF.g:4275:2: ( rule__DataLinkedCause__Group_3__0 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getGroup_3()); 
            // InternalDOF.g:4276:2: ( rule__DataLinkedCause__Group_3__0 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==45) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalDOF.g:4276:3: rule__DataLinkedCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__3__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__4"
    // InternalDOF.g:4284:1: rule__DataLinkedCause__Group__4 : rule__DataLinkedCause__Group__4__Impl rule__DataLinkedCause__Group__5 ;
    public final void rule__DataLinkedCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4288:1: ( rule__DataLinkedCause__Group__4__Impl rule__DataLinkedCause__Group__5 )
            // InternalDOF.g:4289:2: rule__DataLinkedCause__Group__4__Impl rule__DataLinkedCause__Group__5
            {
            pushFollow(FOLLOW_50);
            rule__DataLinkedCause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__4"


    // $ANTLR start "rule__DataLinkedCause__Group__4__Impl"
    // InternalDOF.g:4296:1: rule__DataLinkedCause__Group__4__Impl : ( 'is' ) ;
    public final void rule__DataLinkedCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4300:1: ( ( 'is' ) )
            // InternalDOF.g:4301:1: ( 'is' )
            {
            // InternalDOF.g:4301:1: ( 'is' )
            // InternalDOF.g:4302:2: 'is'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIsKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getIsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__4__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__5"
    // InternalDOF.g:4311:1: rule__DataLinkedCause__Group__5 : rule__DataLinkedCause__Group__5__Impl rule__DataLinkedCause__Group__6 ;
    public final void rule__DataLinkedCause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4315:1: ( rule__DataLinkedCause__Group__5__Impl rule__DataLinkedCause__Group__6 )
            // InternalDOF.g:4316:2: rule__DataLinkedCause__Group__5__Impl rule__DataLinkedCause__Group__6
            {
            pushFollow(FOLLOW_50);
            rule__DataLinkedCause__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__5"


    // $ANTLR start "rule__DataLinkedCause__Group__5__Impl"
    // InternalDOF.g:4323:1: rule__DataLinkedCause__Group__5__Impl : ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? ) ;
    public final void rule__DataLinkedCause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4327:1: ( ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? ) )
            // InternalDOF.g:4328:1: ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? )
            {
            // InternalDOF.g:4328:1: ( ( rule__DataLinkedCause__AttributeFilterAssignment_5 )? )
            // InternalDOF.g:4329:2: ( rule__DataLinkedCause__AttributeFilterAssignment_5 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAssignment_5()); 
            // InternalDOF.g:4330:2: ( rule__DataLinkedCause__AttributeFilterAssignment_5 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==31) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalDOF.g:4330:3: rule__DataLinkedCause__AttributeFilterAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__AttributeFilterAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__5__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__6"
    // InternalDOF.g:4338:1: rule__DataLinkedCause__Group__6 : rule__DataLinkedCause__Group__6__Impl rule__DataLinkedCause__Group__7 ;
    public final void rule__DataLinkedCause__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4342:1: ( rule__DataLinkedCause__Group__6__Impl rule__DataLinkedCause__Group__7 )
            // InternalDOF.g:4343:2: rule__DataLinkedCause__Group__6__Impl rule__DataLinkedCause__Group__7
            {
            pushFollow(FOLLOW_50);
            rule__DataLinkedCause__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__6"


    // $ANTLR start "rule__DataLinkedCause__Group__6__Impl"
    // InternalDOF.g:4350:1: rule__DataLinkedCause__Group__6__Impl : ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? ) ;
    public final void rule__DataLinkedCause__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4354:1: ( ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? ) )
            // InternalDOF.g:4355:1: ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? )
            {
            // InternalDOF.g:4355:1: ( ( rule__DataLinkedCause__InstancesFilterAssignment_6 )? )
            // InternalDOF.g:4356:2: ( rule__DataLinkedCause__InstancesFilterAssignment_6 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterAssignment_6()); 
            // InternalDOF.g:4357:2: ( rule__DataLinkedCause__InstancesFilterAssignment_6 )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==34) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalDOF.g:4357:3: rule__DataLinkedCause__InstancesFilterAssignment_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__InstancesFilterAssignment_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__6__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__7"
    // InternalDOF.g:4365:1: rule__DataLinkedCause__Group__7 : rule__DataLinkedCause__Group__7__Impl rule__DataLinkedCause__Group__8 ;
    public final void rule__DataLinkedCause__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4369:1: ( rule__DataLinkedCause__Group__7__Impl rule__DataLinkedCause__Group__8 )
            // InternalDOF.g:4370:2: rule__DataLinkedCause__Group__7__Impl rule__DataLinkedCause__Group__8
            {
            pushFollow(FOLLOW_50);
            rule__DataLinkedCause__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__7"


    // $ANTLR start "rule__DataLinkedCause__Group__7__Impl"
    // InternalDOF.g:4377:1: rule__DataLinkedCause__Group__7__Impl : ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* ) ;
    public final void rule__DataLinkedCause__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4381:1: ( ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* ) )
            // InternalDOF.g:4382:1: ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* )
            {
            // InternalDOF.g:4382:1: ( ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )* )
            // InternalDOF.g:4383:2: ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )*
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesAssignment_7()); 
            // InternalDOF.g:4384:2: ( rule__DataLinkedCause__IncludedReferencesAssignment_7 )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==RULE_ID||LA39_0==26) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalDOF.g:4384:3: rule__DataLinkedCause__IncludedReferencesAssignment_7
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__DataLinkedCause__IncludedReferencesAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

             after(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__7__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__8"
    // InternalDOF.g:4392:1: rule__DataLinkedCause__Group__8 : rule__DataLinkedCause__Group__8__Impl ;
    public final void rule__DataLinkedCause__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4396:1: ( rule__DataLinkedCause__Group__8__Impl )
            // InternalDOF.g:4397:2: rule__DataLinkedCause__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__8"


    // $ANTLR start "rule__DataLinkedCause__Group__8__Impl"
    // InternalDOF.g:4403:1: rule__DataLinkedCause__Group__8__Impl : ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? ) ;
    public final void rule__DataLinkedCause__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4407:1: ( ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? ) )
            // InternalDOF.g:4408:1: ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? )
            {
            // InternalDOF.g:4408:1: ( ( rule__DataLinkedCause__TypeFilterAssignment_8 )? )
            // InternalDOF.g:4409:2: ( rule__DataLinkedCause__TypeFilterAssignment_8 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getTypeFilterAssignment_8()); 
            // InternalDOF.g:4410:2: ( rule__DataLinkedCause__TypeFilterAssignment_8 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==27||LA40_0==30) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalDOF.g:4410:3: rule__DataLinkedCause__TypeFilterAssignment_8
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__TypeFilterAssignment_8();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getTypeFilterAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__8__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group_3__0"
    // InternalDOF.g:4419:1: rule__DataLinkedCause__Group_3__0 : rule__DataLinkedCause__Group_3__0__Impl rule__DataLinkedCause__Group_3__1 ;
    public final void rule__DataLinkedCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4423:1: ( rule__DataLinkedCause__Group_3__0__Impl rule__DataLinkedCause__Group_3__1 )
            // InternalDOF.g:4424:2: rule__DataLinkedCause__Group_3__0__Impl rule__DataLinkedCause__Group_3__1
            {
            pushFollow(FOLLOW_40);
            rule__DataLinkedCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__0"


    // $ANTLR start "rule__DataLinkedCause__Group_3__0__Impl"
    // InternalDOF.g:4431:1: rule__DataLinkedCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__DataLinkedCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4435:1: ( ( 'realizes' ) )
            // InternalDOF.g:4436:1: ( 'realizes' )
            {
            // InternalDOF.g:4436:1: ( 'realizes' )
            // InternalDOF.g:4437:2: 'realizes'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_3_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__0__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group_3__1"
    // InternalDOF.g:4446:1: rule__DataLinkedCause__Group_3__1 : rule__DataLinkedCause__Group_3__1__Impl ;
    public final void rule__DataLinkedCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4450:1: ( rule__DataLinkedCause__Group_3__1__Impl )
            // InternalDOF.g:4451:2: rule__DataLinkedCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__1"


    // $ANTLR start "rule__DataLinkedCause__Group_3__1__Impl"
    // InternalDOF.g:4457:1: rule__DataLinkedCause__Group_3__1__Impl : ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__DataLinkedCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4461:1: ( ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:4462:1: ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:4462:1: ( ( rule__DataLinkedCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:4463:2: ( rule__DataLinkedCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:4464:2: ( rule__DataLinkedCause__RealizesAssignment_3_1 )
            // InternalDOF.g:4464:3: rule__DataLinkedCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_3__1__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__0"
    // InternalDOF.g:4473:1: rule__NotMappedCause__Group__0 : rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1 ;
    public final void rule__NotMappedCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4477:1: ( rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1 )
            // InternalDOF.g:4478:2: rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__NotMappedCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__0"


    // $ANTLR start "rule__NotMappedCause__Group__0__Impl"
    // InternalDOF.g:4485:1: rule__NotMappedCause__Group__0__Impl : ( () ) ;
    public final void rule__NotMappedCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4489:1: ( ( () ) )
            // InternalDOF.g:4490:1: ( () )
            {
            // InternalDOF.g:4490:1: ( () )
            // InternalDOF.g:4491:2: ()
            {
             before(grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0()); 
            // InternalDOF.g:4492:2: ()
            // InternalDOF.g:4492:3: 
            {
            }

             after(grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__0__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__1"
    // InternalDOF.g:4500:1: rule__NotMappedCause__Group__1 : rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2 ;
    public final void rule__NotMappedCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4504:1: ( rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2 )
            // InternalDOF.g:4505:2: rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__NotMappedCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__1"


    // $ANTLR start "rule__NotMappedCause__Group__1__Impl"
    // InternalDOF.g:4512:1: rule__NotMappedCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__NotMappedCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4516:1: ( ( 'cause' ) )
            // InternalDOF.g:4517:1: ( 'cause' )
            {
            // InternalDOF.g:4517:1: ( 'cause' )
            // InternalDOF.g:4518:2: 'cause'
            {
             before(grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__1__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__2"
    // InternalDOF.g:4527:1: rule__NotMappedCause__Group__2 : rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3 ;
    public final void rule__NotMappedCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4531:1: ( rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3 )
            // InternalDOF.g:4532:2: rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3
            {
            pushFollow(FOLLOW_51);
            rule__NotMappedCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__2"


    // $ANTLR start "rule__NotMappedCause__Group__2__Impl"
    // InternalDOF.g:4539:1: rule__NotMappedCause__Group__2__Impl : ( ( rule__NotMappedCause__NameAssignment_2 ) ) ;
    public final void rule__NotMappedCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4543:1: ( ( ( rule__NotMappedCause__NameAssignment_2 ) ) )
            // InternalDOF.g:4544:1: ( ( rule__NotMappedCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:4544:1: ( ( rule__NotMappedCause__NameAssignment_2 ) )
            // InternalDOF.g:4545:2: ( rule__NotMappedCause__NameAssignment_2 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:4546:2: ( rule__NotMappedCause__NameAssignment_2 )
            // InternalDOF.g:4546:3: rule__NotMappedCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__2__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__3"
    // InternalDOF.g:4554:1: rule__NotMappedCause__Group__3 : rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4 ;
    public final void rule__NotMappedCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4558:1: ( rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4 )
            // InternalDOF.g:4559:2: rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4
            {
            pushFollow(FOLLOW_51);
            rule__NotMappedCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__3"


    // $ANTLR start "rule__NotMappedCause__Group__3__Impl"
    // InternalDOF.g:4566:1: rule__NotMappedCause__Group__3__Impl : ( ( rule__NotMappedCause__Group_3__0 )? ) ;
    public final void rule__NotMappedCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4570:1: ( ( ( rule__NotMappedCause__Group_3__0 )? ) )
            // InternalDOF.g:4571:1: ( ( rule__NotMappedCause__Group_3__0 )? )
            {
            // InternalDOF.g:4571:1: ( ( rule__NotMappedCause__Group_3__0 )? )
            // InternalDOF.g:4572:2: ( rule__NotMappedCause__Group_3__0 )?
            {
             before(grammarAccess.getNotMappedCauseAccess().getGroup_3()); 
            // InternalDOF.g:4573:2: ( rule__NotMappedCause__Group_3__0 )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==45) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalDOF.g:4573:3: rule__NotMappedCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NotMappedCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNotMappedCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__3__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__4"
    // InternalDOF.g:4581:1: rule__NotMappedCause__Group__4 : rule__NotMappedCause__Group__4__Impl ;
    public final void rule__NotMappedCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4585:1: ( rule__NotMappedCause__Group__4__Impl )
            // InternalDOF.g:4586:2: rule__NotMappedCause__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__4"


    // $ANTLR start "rule__NotMappedCause__Group__4__Impl"
    // InternalDOF.g:4592:1: rule__NotMappedCause__Group__4__Impl : ( 'notMapped' ) ;
    public final void rule__NotMappedCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4596:1: ( ( 'notMapped' ) )
            // InternalDOF.g:4597:1: ( 'notMapped' )
            {
            // InternalDOF.g:4597:1: ( 'notMapped' )
            // InternalDOF.g:4598:2: 'notMapped'
            {
             before(grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__4__Impl"


    // $ANTLR start "rule__NotMappedCause__Group_3__0"
    // InternalDOF.g:4608:1: rule__NotMappedCause__Group_3__0 : rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1 ;
    public final void rule__NotMappedCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4612:1: ( rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1 )
            // InternalDOF.g:4613:2: rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1
            {
            pushFollow(FOLLOW_40);
            rule__NotMappedCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__0"


    // $ANTLR start "rule__NotMappedCause__Group_3__0__Impl"
    // InternalDOF.g:4620:1: rule__NotMappedCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__NotMappedCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4624:1: ( ( 'realizes' ) )
            // InternalDOF.g:4625:1: ( 'realizes' )
            {
            // InternalDOF.g:4625:1: ( 'realizes' )
            // InternalDOF.g:4626:2: 'realizes'
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__0__Impl"


    // $ANTLR start "rule__NotMappedCause__Group_3__1"
    // InternalDOF.g:4635:1: rule__NotMappedCause__Group_3__1 : rule__NotMappedCause__Group_3__1__Impl ;
    public final void rule__NotMappedCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4639:1: ( rule__NotMappedCause__Group_3__1__Impl )
            // InternalDOF.g:4640:2: rule__NotMappedCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__1"


    // $ANTLR start "rule__NotMappedCause__Group_3__1__Impl"
    // InternalDOF.g:4646:1: rule__NotMappedCause__Group_3__1__Impl : ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__NotMappedCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4650:1: ( ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:4651:1: ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:4651:1: ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:4652:2: ( rule__NotMappedCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:4653:2: ( rule__NotMappedCause__RealizesAssignment_3_1 )
            // InternalDOF.g:4653:3: rule__NotMappedCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // InternalDOF.g:4662:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4666:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // InternalDOF.g:4667:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_52);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // InternalDOF.g:4674:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4678:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:4679:1: ( ruleQualifiedName )
            {
            // InternalDOF.g:4679:1: ( ruleQualifiedName )
            // InternalDOF.g:4680:2: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // InternalDOF.g:4689:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4693:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // InternalDOF.g:4694:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // InternalDOF.g:4700:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4704:1: ( ( ( '.*' )? ) )
            // InternalDOF.g:4705:1: ( ( '.*' )? )
            {
            // InternalDOF.g:4705:1: ( ( '.*' )? )
            // InternalDOF.g:4706:2: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // InternalDOF.g:4707:2: ( '.*' )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==47) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalDOF.g:4707:3: '.*'
                    {
                    match(input,47,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalDOF.g:4716:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4720:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalDOF.g:4721:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_53);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalDOF.g:4728:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4732:1: ( ( RULE_ID ) )
            // InternalDOF.g:4733:1: ( RULE_ID )
            {
            // InternalDOF.g:4733:1: ( RULE_ID )
            // InternalDOF.g:4734:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalDOF.g:4743:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4747:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalDOF.g:4748:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalDOF.g:4754:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4758:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalDOF.g:4759:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalDOF.g:4759:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalDOF.g:4760:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalDOF.g:4761:2: ( rule__QualifiedName__Group_1__0 )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==25) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // InternalDOF.g:4761:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_54);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalDOF.g:4770:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4774:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalDOF.g:4775:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalDOF.g:4782:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4786:1: ( ( '.' ) )
            // InternalDOF.g:4787:1: ( '.' )
            {
            // InternalDOF.g:4787:1: ( '.' )
            // InternalDOF.g:4788:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalDOF.g:4797:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4801:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalDOF.g:4802:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalDOF.g:4808:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4812:1: ( ( RULE_ID ) )
            // InternalDOF.g:4813:1: ( RULE_ID )
            {
            // InternalDOF.g:4813:1: ( RULE_ID )
            // InternalDOF.g:4814:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Path__Group__0"
    // InternalDOF.g:4824:1: rule__Path__Group__0 : rule__Path__Group__0__Impl rule__Path__Group__1 ;
    public final void rule__Path__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4828:1: ( rule__Path__Group__0__Impl rule__Path__Group__1 )
            // InternalDOF.g:4829:2: rule__Path__Group__0__Impl rule__Path__Group__1
            {
            pushFollow(FOLLOW_53);
            rule__Path__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Path__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__0"


    // $ANTLR start "rule__Path__Group__0__Impl"
    // InternalDOF.g:4836:1: rule__Path__Group__0__Impl : ( ( rule__Path__JumpsAssignment_0 ) ) ;
    public final void rule__Path__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4840:1: ( ( ( rule__Path__JumpsAssignment_0 ) ) )
            // InternalDOF.g:4841:1: ( ( rule__Path__JumpsAssignment_0 ) )
            {
            // InternalDOF.g:4841:1: ( ( rule__Path__JumpsAssignment_0 ) )
            // InternalDOF.g:4842:2: ( rule__Path__JumpsAssignment_0 )
            {
             before(grammarAccess.getPathAccess().getJumpsAssignment_0()); 
            // InternalDOF.g:4843:2: ( rule__Path__JumpsAssignment_0 )
            // InternalDOF.g:4843:3: rule__Path__JumpsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Path__JumpsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getJumpsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__0__Impl"


    // $ANTLR start "rule__Path__Group__1"
    // InternalDOF.g:4851:1: rule__Path__Group__1 : rule__Path__Group__1__Impl ;
    public final void rule__Path__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4855:1: ( rule__Path__Group__1__Impl )
            // InternalDOF.g:4856:2: rule__Path__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__1"


    // $ANTLR start "rule__Path__Group__1__Impl"
    // InternalDOF.g:4862:1: rule__Path__Group__1__Impl : ( ( rule__Path__Group_1__0 )* ) ;
    public final void rule__Path__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4866:1: ( ( ( rule__Path__Group_1__0 )* ) )
            // InternalDOF.g:4867:1: ( ( rule__Path__Group_1__0 )* )
            {
            // InternalDOF.g:4867:1: ( ( rule__Path__Group_1__0 )* )
            // InternalDOF.g:4868:2: ( rule__Path__Group_1__0 )*
            {
             before(grammarAccess.getPathAccess().getGroup_1()); 
            // InternalDOF.g:4869:2: ( rule__Path__Group_1__0 )*
            loop44:
            do {
                int alt44=2;
                int LA44_0 = input.LA(1);

                if ( (LA44_0==25) ) {
                    int LA44_2 = input.LA(2);

                    if ( (LA44_2==RULE_ID) ) {
                        alt44=1;
                    }


                }


                switch (alt44) {
            	case 1 :
            	    // InternalDOF.g:4869:3: rule__Path__Group_1__0
            	    {
            	    pushFollow(FOLLOW_54);
            	    rule__Path__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop44;
                }
            } while (true);

             after(grammarAccess.getPathAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group__1__Impl"


    // $ANTLR start "rule__Path__Group_1__0"
    // InternalDOF.g:4878:1: rule__Path__Group_1__0 : rule__Path__Group_1__0__Impl rule__Path__Group_1__1 ;
    public final void rule__Path__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4882:1: ( rule__Path__Group_1__0__Impl rule__Path__Group_1__1 )
            // InternalDOF.g:4883:2: rule__Path__Group_1__0__Impl rule__Path__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Path__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Path__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__0"


    // $ANTLR start "rule__Path__Group_1__0__Impl"
    // InternalDOF.g:4890:1: rule__Path__Group_1__0__Impl : ( '.' ) ;
    public final void rule__Path__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4894:1: ( ( '.' ) )
            // InternalDOF.g:4895:1: ( '.' )
            {
            // InternalDOF.g:4895:1: ( '.' )
            // InternalDOF.g:4896:2: '.'
            {
             before(grammarAccess.getPathAccess().getFullStopKeyword_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__0__Impl"


    // $ANTLR start "rule__Path__Group_1__1"
    // InternalDOF.g:4905:1: rule__Path__Group_1__1 : rule__Path__Group_1__1__Impl ;
    public final void rule__Path__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4909:1: ( rule__Path__Group_1__1__Impl )
            // InternalDOF.g:4910:2: rule__Path__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Path__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__1"


    // $ANTLR start "rule__Path__Group_1__1__Impl"
    // InternalDOF.g:4916:1: rule__Path__Group_1__1__Impl : ( ( rule__Path__JumpsAssignment_1_1 ) ) ;
    public final void rule__Path__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4920:1: ( ( ( rule__Path__JumpsAssignment_1_1 ) ) )
            // InternalDOF.g:4921:1: ( ( rule__Path__JumpsAssignment_1_1 ) )
            {
            // InternalDOF.g:4921:1: ( ( rule__Path__JumpsAssignment_1_1 ) )
            // InternalDOF.g:4922:2: ( rule__Path__JumpsAssignment_1_1 )
            {
             before(grammarAccess.getPathAccess().getJumpsAssignment_1_1()); 
            // InternalDOF.g:4923:2: ( rule__Path__JumpsAssignment_1_1 )
            // InternalDOF.g:4923:3: rule__Path__JumpsAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Path__JumpsAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPathAccess().getJumpsAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__Group_1__1__Impl"


    // $ANTLR start "rule__DOF__ImportsAssignment_0"
    // InternalDOF.g:4932:1: rule__DOF__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__DOF__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4936:1: ( ( ruleImport ) )
            // InternalDOF.g:4937:2: ( ruleImport )
            {
            // InternalDOF.g:4937:2: ( ruleImport )
            // InternalDOF.g:4938:3: ruleImport
            {
             before(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__ImportsAssignment_0"


    // $ANTLR start "rule__DOF__NameAssignment_2"
    // InternalDOF.g:4947:1: rule__DOF__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__DOF__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4951:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:4952:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:4952:2: ( ruleQualifiedName )
            // InternalDOF.g:4953:3: ruleQualifiedName
            {
             before(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__NameAssignment_2"


    // $ANTLR start "rule__DOF__EffectAssignment_3"
    // InternalDOF.g:4962:1: rule__DOF__EffectAssignment_3 : ( ruleEffect ) ;
    public final void rule__DOF__EffectAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4966:1: ( ( ruleEffect ) )
            // InternalDOF.g:4967:2: ( ruleEffect )
            {
            // InternalDOF.g:4967:2: ( ruleEffect )
            // InternalDOF.g:4968:3: ruleEffect
            {
             before(grammarAccess.getDOFAccess().getEffectEffectParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getEffectEffectParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__EffectAssignment_3"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // InternalDOF.g:4977:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4981:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:4982:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:4982:2: ( ruleQualifiedNameWithWildcard )
            // InternalDOF.g:4983:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__Effect__NameAssignment_2"
    // InternalDOF.g:4992:1: rule__Effect__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Effect__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:4996:1: ( ( RULE_ID ) )
            // InternalDOF.g:4997:2: ( RULE_ID )
            {
            // InternalDOF.g:4997:2: ( RULE_ID )
            // InternalDOF.g:4998:3: RULE_ID
            {
             before(grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__NameAssignment_2"


    // $ANTLR start "rule__Effect__DataFeederAssignment_4"
    // InternalDOF.g:5007:1: rule__Effect__DataFeederAssignment_4 : ( ruleDataFeeder ) ;
    public final void rule__Effect__DataFeederAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5011:1: ( ( ruleDataFeeder ) )
            // InternalDOF.g:5012:2: ( ruleDataFeeder )
            {
            // InternalDOF.g:5012:2: ( ruleDataFeeder )
            // InternalDOF.g:5013:3: ruleDataFeeder
            {
             before(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__DataFeederAssignment_4"


    // $ANTLR start "rule__Effect__CategoriesAssignment_5"
    // InternalDOF.g:5022:1: rule__Effect__CategoriesAssignment_5 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5026:1: ( ( ruleCategory ) )
            // InternalDOF.g:5027:2: ( ruleCategory )
            {
            // InternalDOF.g:5027:2: ( ruleCategory )
            // InternalDOF.g:5028:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_5"


    // $ANTLR start "rule__Effect__CategoriesAssignment_6"
    // InternalDOF.g:5037:1: rule__Effect__CategoriesAssignment_6 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5041:1: ( ( ruleCategory ) )
            // InternalDOF.g:5042:2: ( ruleCategory )
            {
            // InternalDOF.g:5042:2: ( ruleCategory )
            // InternalDOF.g:5043:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_6"


    // $ANTLR start "rule__Category__NameAssignment_2"
    // InternalDOF.g:5052:1: rule__Category__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Category__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5056:1: ( ( RULE_ID ) )
            // InternalDOF.g:5057:2: ( RULE_ID )
            {
            // InternalDOF.g:5057:2: ( RULE_ID )
            // InternalDOF.g:5058:3: RULE_ID
            {
             before(grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__NameAssignment_2"


    // $ANTLR start "rule__Category__CausesAssignment_3"
    // InternalDOF.g:5067:1: rule__Category__CausesAssignment_3 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5071:1: ( ( ruleCause ) )
            // InternalDOF.g:5072:2: ( ruleCause )
            {
            // InternalDOF.g:5072:2: ( ruleCause )
            // InternalDOF.g:5073:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_3"


    // $ANTLR start "rule__Category__CausesAssignment_4"
    // InternalDOF.g:5082:1: rule__Category__CausesAssignment_4 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5086:1: ( ( ruleCause ) )
            // InternalDOF.g:5087:2: ( ruleCause )
            {
            // InternalDOF.g:5087:2: ( ruleCause )
            // InternalDOF.g:5088:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_4"


    // $ANTLR start "rule__DataFeeder__NameAssignment_1"
    // InternalDOF.g:5097:1: rule__DataFeeder__NameAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__DataFeeder__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5101:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:5102:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:5102:2: ( ruleQualifiedNameWithWildcard )
            // InternalDOF.g:5103:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getDataFeederAccess().getNameQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getNameQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__NameAssignment_1"


    // $ANTLR start "rule__DataFeeder__AttributeFilterAssignment_2"
    // InternalDOF.g:5112:1: rule__DataFeeder__AttributeFilterAssignment_2 : ( ruleAttributeFilter ) ;
    public final void rule__DataFeeder__AttributeFilterAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5116:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5117:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5117:2: ( ruleAttributeFilter )
            // InternalDOF.g:5118:3: ruleAttributeFilter
            {
             before(grammarAccess.getDataFeederAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getAttributeFilterAttributeFilterParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__AttributeFilterAssignment_2"


    // $ANTLR start "rule__DataFeeder__InstancesFilterAssignment_3"
    // InternalDOF.g:5127:1: rule__DataFeeder__InstancesFilterAssignment_3 : ( ruleInstancesFilter ) ;
    public final void rule__DataFeeder__InstancesFilterAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5131:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5132:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5132:2: ( ruleInstancesFilter )
            // InternalDOF.g:5133:3: ruleInstancesFilter
            {
             before(grammarAccess.getDataFeederAccess().getInstancesFilterInstancesFilterParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getInstancesFilterInstancesFilterParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__InstancesFilterAssignment_3"


    // $ANTLR start "rule__DataFeeder__IncludedReferencesAssignment_4_1"
    // InternalDOF.g:5142:1: rule__DataFeeder__IncludedReferencesAssignment_4_1 : ( ruleIncludedReference ) ;
    public final void rule__DataFeeder__IncludedReferencesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5146:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5147:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5147:2: ( ruleIncludedReference )
            // InternalDOF.g:5148:3: ruleIncludedReference
            {
             before(grammarAccess.getDataFeederAccess().getIncludedReferencesIncludedReferenceParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getIncludedReferencesIncludedReferenceParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__IncludedReferencesAssignment_4_1"


    // $ANTLR start "rule__DataFeeder__TypeFilterAssignment_5"
    // InternalDOF.g:5157:1: rule__DataFeeder__TypeFilterAssignment_5 : ( ruleTypeFilter ) ;
    public final void rule__DataFeeder__TypeFilterAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5161:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5162:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5162:2: ( ruleTypeFilter )
            // InternalDOF.g:5163:3: ruleTypeFilter
            {
             before(grammarAccess.getDataFeederAccess().getTypeFilterTypeFilterParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getDataFeederAccess().getTypeFilterTypeFilterParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__TypeFilterAssignment_5"


    // $ANTLR start "rule__SimpleReference__NameAssignment_0"
    // InternalDOF.g:5172:1: rule__SimpleReference__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__SimpleReference__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5176:1: ( ( RULE_ID ) )
            // InternalDOF.g:5177:2: ( RULE_ID )
            {
            // InternalDOF.g:5177:2: ( RULE_ID )
            // InternalDOF.g:5178:3: RULE_ID
            {
             before(grammarAccess.getSimpleReferenceAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getSimpleReferenceAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__NameAssignment_0"


    // $ANTLR start "rule__SimpleReference__AttributeFilterAssignment_1"
    // InternalDOF.g:5187:1: rule__SimpleReference__AttributeFilterAssignment_1 : ( ruleAttributeFilter ) ;
    public final void rule__SimpleReference__AttributeFilterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5191:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5192:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5192:2: ( ruleAttributeFilter )
            // InternalDOF.g:5193:3: ruleAttributeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__AttributeFilterAssignment_1"


    // $ANTLR start "rule__SimpleReference__PivotingIdAssignment_2_1"
    // InternalDOF.g:5202:1: rule__SimpleReference__PivotingIdAssignment_2_1 : ( rulePath ) ;
    public final void rule__SimpleReference__PivotingIdAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5206:1: ( ( rulePath ) )
            // InternalDOF.g:5207:2: ( rulePath )
            {
            // InternalDOF.g:5207:2: ( rulePath )
            // InternalDOF.g:5208:3: rulePath
            {
             before(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getPivotingIdPathParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__PivotingIdAssignment_2_1"


    // $ANTLR start "rule__SimpleReference__InstancesFilterAssignment_2_2"
    // InternalDOF.g:5217:1: rule__SimpleReference__InstancesFilterAssignment_2_2 : ( ruleInstancesFilter ) ;
    public final void rule__SimpleReference__InstancesFilterAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5221:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5222:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5222:2: ( ruleInstancesFilter )
            // InternalDOF.g:5223:3: ruleInstancesFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__InstancesFilterAssignment_2_2"


    // $ANTLR start "rule__SimpleReference__IncludedReferencesAssignment_3_0_2"
    // InternalDOF.g:5232:1: rule__SimpleReference__IncludedReferencesAssignment_3_0_2 : ( ruleIncludedReference ) ;
    public final void rule__SimpleReference__IncludedReferencesAssignment_3_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5236:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5237:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5237:2: ( ruleIncludedReference )
            // InternalDOF.g:5238:3: ruleIncludedReference
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__IncludedReferencesAssignment_3_0_2"


    // $ANTLR start "rule__SimpleReference__TypeFilterAssignment_3_0_3"
    // InternalDOF.g:5247:1: rule__SimpleReference__TypeFilterAssignment_3_0_3 : ( ruleTypeFilter ) ;
    public final void rule__SimpleReference__TypeFilterAssignment_3_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5251:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5252:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5252:2: ( ruleTypeFilter )
            // InternalDOF.g:5253:3: ruleTypeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_0_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__TypeFilterAssignment_3_0_3"


    // $ANTLR start "rule__SimpleReference__IncludedReferencesAssignment_3_1_1"
    // InternalDOF.g:5262:1: rule__SimpleReference__IncludedReferencesAssignment_3_1_1 : ( ruleIncludedReference ) ;
    public final void rule__SimpleReference__IncludedReferencesAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5266:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5267:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5267:2: ( ruleIncludedReference )
            // InternalDOF.g:5268:3: ruleIncludedReference
            {
             before(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getIncludedReferencesIncludedReferenceParserRuleCall_3_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__IncludedReferencesAssignment_3_1_1"


    // $ANTLR start "rule__SimpleReference__TypeFilterAssignment_3_1_2"
    // InternalDOF.g:5277:1: rule__SimpleReference__TypeFilterAssignment_3_1_2 : ( ruleTypeFilter ) ;
    public final void rule__SimpleReference__TypeFilterAssignment_3_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5281:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5282:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5282:2: ( ruleTypeFilter )
            // InternalDOF.g:5283:3: ruleTypeFilter
            {
             before(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getSimpleReferenceAccess().getTypeFilterTypeFilterParserRuleCall_3_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleReference__TypeFilterAssignment_3_1_2"


    // $ANTLR start "rule__AggregatedReference__NameAssignment_1"
    // InternalDOF.g:5292:1: rule__AggregatedReference__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__AggregatedReference__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5296:1: ( ( RULE_ID ) )
            // InternalDOF.g:5297:2: ( RULE_ID )
            {
            // InternalDOF.g:5297:2: ( RULE_ID )
            // InternalDOF.g:5298:3: RULE_ID
            {
             before(grammarAccess.getAggregatedReferenceAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAggregatedReferenceAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__NameAssignment_1"


    // $ANTLR start "rule__AggregatedReference__FunctionAssignment_3"
    // InternalDOF.g:5307:1: rule__AggregatedReference__FunctionAssignment_3 : ( ruleAggFunction ) ;
    public final void rule__AggregatedReference__FunctionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5311:1: ( ( ruleAggFunction ) )
            // InternalDOF.g:5312:2: ( ruleAggFunction )
            {
            // InternalDOF.g:5312:2: ( ruleAggFunction )
            // InternalDOF.g:5313:3: ruleAggFunction
            {
             before(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAggFunction();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getFunctionAggFunctionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__FunctionAssignment_3"


    // $ANTLR start "rule__AggregatedReference__AggValueAssignment_5"
    // InternalDOF.g:5322:1: rule__AggregatedReference__AggValueAssignment_5 : ( rulePath ) ;
    public final void rule__AggregatedReference__AggValueAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5326:1: ( ( rulePath ) )
            // InternalDOF.g:5327:2: ( rulePath )
            {
            // InternalDOF.g:5327:2: ( rulePath )
            // InternalDOF.g:5328:3: rulePath
            {
             before(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getAggValuePathParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__AggValueAssignment_5"


    // $ANTLR start "rule__AggregatedReference__PivotingIdAssignment_7_1"
    // InternalDOF.g:5337:1: rule__AggregatedReference__PivotingIdAssignment_7_1 : ( rulePath ) ;
    public final void rule__AggregatedReference__PivotingIdAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5341:1: ( ( rulePath ) )
            // InternalDOF.g:5342:2: ( rulePath )
            {
            // InternalDOF.g:5342:2: ( rulePath )
            // InternalDOF.g:5343:3: rulePath
            {
             before(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getPivotingIdPathParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__PivotingIdAssignment_7_1"


    // $ANTLR start "rule__AggregatedReference__InstancesFilterAssignment_8"
    // InternalDOF.g:5352:1: rule__AggregatedReference__InstancesFilterAssignment_8 : ( ruleInstancesFilter ) ;
    public final void rule__AggregatedReference__InstancesFilterAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5356:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5357:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5357:2: ( ruleInstancesFilter )
            // InternalDOF.g:5358:3: ruleInstancesFilter
            {
             before(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getAggregatedReferenceAccess().getInstancesFilterInstancesFilterParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregatedReference__InstancesFilterAssignment_8"


    // $ANTLR start "rule__TypeCompletion__TypeCustomizationsAssignment_1"
    // InternalDOF.g:5367:1: rule__TypeCompletion__TypeCustomizationsAssignment_1 : ( ruleTypeCustomization ) ;
    public final void rule__TypeCompletion__TypeCustomizationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5371:1: ( ( ruleTypeCustomization ) )
            // InternalDOF.g:5372:2: ( ruleTypeCustomization )
            {
            // InternalDOF.g:5372:2: ( ruleTypeCustomization )
            // InternalDOF.g:5373:3: ruleTypeCustomization
            {
             before(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeCompletionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCompletion__TypeCustomizationsAssignment_1"


    // $ANTLR start "rule__TypeSelection__TypeCustomizationsAssignment_1"
    // InternalDOF.g:5382:1: rule__TypeSelection__TypeCustomizationsAssignment_1 : ( ruleTypeCustomization ) ;
    public final void rule__TypeSelection__TypeCustomizationsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5386:1: ( ( ruleTypeCustomization ) )
            // InternalDOF.g:5387:2: ( ruleTypeCustomization )
            {
            // InternalDOF.g:5387:2: ( ruleTypeCustomization )
            // InternalDOF.g:5388:3: ruleTypeCustomization
            {
             before(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeCustomization();

            state._fsp--;

             after(grammarAccess.getTypeSelectionAccess().getTypeCustomizationsTypeCustomizationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeSelection__TypeCustomizationsAssignment_1"


    // $ANTLR start "rule__TypeCustomization__NameAssignment_0"
    // InternalDOF.g:5397:1: rule__TypeCustomization__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__TypeCustomization__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5401:1: ( ( RULE_ID ) )
            // InternalDOF.g:5402:2: ( RULE_ID )
            {
            // InternalDOF.g:5402:2: ( RULE_ID )
            // InternalDOF.g:5403:3: RULE_ID
            {
             before(grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTypeCustomizationAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__NameAssignment_0"


    // $ANTLR start "rule__TypeCustomization__AttributeFilterAssignment_1"
    // InternalDOF.g:5412:1: rule__TypeCustomization__AttributeFilterAssignment_1 : ( ruleAttributeFilter ) ;
    public final void rule__TypeCustomization__AttributeFilterAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5416:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5417:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5417:2: ( ruleAttributeFilter )
            // InternalDOF.g:5418:3: ruleAttributeFilter
            {
             before(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationAccess().getAttributeFilterAttributeFilterParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__AttributeFilterAssignment_1"


    // $ANTLR start "rule__TypeCustomization__IncludedReferencesAssignment_2_1"
    // InternalDOF.g:5427:1: rule__TypeCustomization__IncludedReferencesAssignment_2_1 : ( ruleIncludedReference ) ;
    public final void rule__TypeCustomization__IncludedReferencesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5431:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5432:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5432:2: ( ruleIncludedReference )
            // InternalDOF.g:5433:3: ruleIncludedReference
            {
             before(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getTypeCustomizationAccess().getIncludedReferencesIncludedReferenceParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeCustomization__IncludedReferencesAssignment_2_1"


    // $ANTLR start "rule__AttributeFilter__AttributesAssignment_2_0"
    // InternalDOF.g:5442:1: rule__AttributeFilter__AttributesAssignment_2_0 : ( RULE_ID ) ;
    public final void rule__AttributeFilter__AttributesAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5446:1: ( ( RULE_ID ) )
            // InternalDOF.g:5447:2: ( RULE_ID )
            {
            // InternalDOF.g:5447:2: ( RULE_ID )
            // InternalDOF.g:5448:3: RULE_ID
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__AttributesAssignment_2_0"


    // $ANTLR start "rule__AttributeFilter__AttributesAssignment_2_1_1"
    // InternalDOF.g:5457:1: rule__AttributeFilter__AttributesAssignment_2_1_1 : ( RULE_ID ) ;
    public final void rule__AttributeFilter__AttributesAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5461:1: ( ( RULE_ID ) )
            // InternalDOF.g:5462:2: ( RULE_ID )
            {
            // InternalDOF.g:5462:2: ( RULE_ID )
            // InternalDOF.g:5463:3: RULE_ID
            {
             before(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAttributeFilterAccess().getAttributesIDTerminalRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeFilter__AttributesAssignment_2_1_1"


    // $ANTLR start "rule__AndConjunction__RightAssignment_1_2"
    // InternalDOF.g:5472:1: rule__AndConjunction__RightAssignment_1_2 : ( ruleOrConjunction ) ;
    public final void rule__AndConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5476:1: ( ( ruleOrConjunction ) )
            // InternalDOF.g:5477:2: ( ruleOrConjunction )
            {
            // InternalDOF.g:5477:2: ( ruleOrConjunction )
            // InternalDOF.g:5478:3: ruleOrConjunction
            {
             before(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOrConjunction();

            state._fsp--;

             after(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__OrConjunction__RightAssignment_1_2"
    // InternalDOF.g:5487:1: rule__OrConjunction__RightAssignment_1_2 : ( rulePrimary ) ;
    public final void rule__OrConjunction__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5491:1: ( ( rulePrimary ) )
            // InternalDOF.g:5492:2: ( rulePrimary )
            {
            // InternalDOF.g:5492:2: ( rulePrimary )
            // InternalDOF.g:5493:3: rulePrimary
            {
             before(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            rulePrimary();

            state._fsp--;

             after(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrConjunction__RightAssignment_1_2"


    // $ANTLR start "rule__Comparison__PathAssignment_0_1"
    // InternalDOF.g:5502:1: rule__Comparison__PathAssignment_0_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5506:1: ( ( rulePath ) )
            // InternalDOF.g:5507:2: ( rulePath )
            {
            // InternalDOF.g:5507:2: ( rulePath )
            // InternalDOF.g:5508:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_0_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_0_3"
    // InternalDOF.g:5517:1: rule__Comparison__ValueAssignment_0_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_0_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5521:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5522:2: ( RULE_STRING )
            {
            // InternalDOF.g:5522:2: ( RULE_STRING )
            // InternalDOF.g:5523:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_0_3"


    // $ANTLR start "rule__Comparison__PathAssignment_1_1"
    // InternalDOF.g:5532:1: rule__Comparison__PathAssignment_1_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5536:1: ( ( rulePath ) )
            // InternalDOF.g:5537:2: ( rulePath )
            {
            // InternalDOF.g:5537:2: ( rulePath )
            // InternalDOF.g:5538:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_1_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_1_3"
    // InternalDOF.g:5547:1: rule__Comparison__ValueAssignment_1_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5551:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5552:2: ( RULE_STRING )
            {
            // InternalDOF.g:5552:2: ( RULE_STRING )
            // InternalDOF.g:5553:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_1_3"


    // $ANTLR start "rule__Comparison__PathAssignment_2_1"
    // InternalDOF.g:5562:1: rule__Comparison__PathAssignment_2_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5566:1: ( ( rulePath ) )
            // InternalDOF.g:5567:2: ( rulePath )
            {
            // InternalDOF.g:5567:2: ( rulePath )
            // InternalDOF.g:5568:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_2_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_2_3"
    // InternalDOF.g:5577:1: rule__Comparison__ValueAssignment_2_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5581:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5582:2: ( RULE_STRING )
            {
            // InternalDOF.g:5582:2: ( RULE_STRING )
            // InternalDOF.g:5583:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_2_3"


    // $ANTLR start "rule__Comparison__PathAssignment_3_1"
    // InternalDOF.g:5592:1: rule__Comparison__PathAssignment_3_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5596:1: ( ( rulePath ) )
            // InternalDOF.g:5597:2: ( rulePath )
            {
            // InternalDOF.g:5597:2: ( rulePath )
            // InternalDOF.g:5598:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_3_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_3_3"
    // InternalDOF.g:5607:1: rule__Comparison__ValueAssignment_3_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_3_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5611:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5612:2: ( RULE_STRING )
            {
            // InternalDOF.g:5612:2: ( RULE_STRING )
            // InternalDOF.g:5613:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_3_3"


    // $ANTLR start "rule__Comparison__PathAssignment_4_1"
    // InternalDOF.g:5622:1: rule__Comparison__PathAssignment_4_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5626:1: ( ( rulePath ) )
            // InternalDOF.g:5627:2: ( rulePath )
            {
            // InternalDOF.g:5627:2: ( rulePath )
            // InternalDOF.g:5628:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_4_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_4_3"
    // InternalDOF.g:5637:1: rule__Comparison__ValueAssignment_4_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5641:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5642:2: ( RULE_STRING )
            {
            // InternalDOF.g:5642:2: ( RULE_STRING )
            // InternalDOF.g:5643:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_4_3"


    // $ANTLR start "rule__Comparison__PathAssignment_5_1"
    // InternalDOF.g:5652:1: rule__Comparison__PathAssignment_5_1 : ( rulePath ) ;
    public final void rule__Comparison__PathAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5656:1: ( ( rulePath ) )
            // InternalDOF.g:5657:2: ( rulePath )
            {
            // InternalDOF.g:5657:2: ( rulePath )
            // InternalDOF.g:5658:3: rulePath
            {
             before(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            rulePath();

            state._fsp--;

             after(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__PathAssignment_5_1"


    // $ANTLR start "rule__Comparison__ValueAssignment_5_3"
    // InternalDOF.g:5667:1: rule__Comparison__ValueAssignment_5_3 : ( RULE_STRING ) ;
    public final void rule__Comparison__ValueAssignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5671:1: ( ( RULE_STRING ) )
            // InternalDOF.g:5672:2: ( RULE_STRING )
            {
            // InternalDOF.g:5672:2: ( RULE_STRING )
            // InternalDOF.g:5673:3: RULE_STRING
            {
             before(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comparison__ValueAssignment_5_3"


    // $ANTLR start "rule__CompoundCause__NameAssignment_2"
    // InternalDOF.g:5682:1: rule__CompoundCause__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__CompoundCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5686:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:5687:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:5687:2: ( ruleQualifiedName )
            // InternalDOF.g:5688:3: ruleQualifiedName
            {
             before(grammarAccess.getCompoundCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__NameAssignment_2"


    // $ANTLR start "rule__CompoundCause__RealizesAssignment_3_1"
    // InternalDOF.g:5697:1: rule__CompoundCause__RealizesAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__CompoundCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5701:1: ( ( ( ruleEString ) ) )
            // InternalDOF.g:5702:2: ( ( ruleEString ) )
            {
            // InternalDOF.g:5702:2: ( ( ruleEString ) )
            // InternalDOF.g:5703:3: ( ruleEString )
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesCauseCrossReference_3_1_0()); 
            // InternalDOF.g:5704:3: ( ruleEString )
            // InternalDOF.g:5705:4: ruleEString
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getCompoundCauseAccess().getRealizesCauseCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__RealizesAssignment_3_1"


    // $ANTLR start "rule__CompoundCause__SubCausesAssignment_6"
    // InternalDOF.g:5716:1: rule__CompoundCause__SubCausesAssignment_6 : ( ruleCause ) ;
    public final void rule__CompoundCause__SubCausesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5720:1: ( ( ruleCause ) )
            // InternalDOF.g:5721:2: ( ruleCause )
            {
            // InternalDOF.g:5721:2: ( ruleCause )
            // InternalDOF.g:5722:3: ruleCause
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__SubCausesAssignment_6"


    // $ANTLR start "rule__CompoundCause__SubCausesAssignment_7"
    // InternalDOF.g:5731:1: rule__CompoundCause__SubCausesAssignment_7 : ( ruleCause ) ;
    public final void rule__CompoundCause__SubCausesAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5735:1: ( ( ruleCause ) )
            // InternalDOF.g:5736:2: ( ruleCause )
            {
            // InternalDOF.g:5736:2: ( ruleCause )
            // InternalDOF.g:5737:3: ruleCause
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__SubCausesAssignment_7"


    // $ANTLR start "rule__DataLinkedCause__NameAssignment_2"
    // InternalDOF.g:5746:1: rule__DataLinkedCause__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__DataLinkedCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5750:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:5751:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:5751:2: ( ruleQualifiedName )
            // InternalDOF.g:5752:3: ruleQualifiedName
            {
             before(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__NameAssignment_2"


    // $ANTLR start "rule__DataLinkedCause__RealizesAssignment_3_1"
    // InternalDOF.g:5761:1: rule__DataLinkedCause__RealizesAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__DataLinkedCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5765:1: ( ( ( ruleEString ) ) )
            // InternalDOF.g:5766:2: ( ( ruleEString ) )
            {
            // InternalDOF.g:5766:2: ( ( ruleEString ) )
            // InternalDOF.g:5767:3: ( ruleEString )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 
            // InternalDOF.g:5768:3: ( ruleEString )
            // InternalDOF.g:5769:4: ruleEString
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__RealizesAssignment_3_1"


    // $ANTLR start "rule__DataLinkedCause__AttributeFilterAssignment_5"
    // InternalDOF.g:5780:1: rule__DataLinkedCause__AttributeFilterAssignment_5 : ( ruleAttributeFilter ) ;
    public final void rule__DataLinkedCause__AttributeFilterAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5784:1: ( ( ruleAttributeFilter ) )
            // InternalDOF.g:5785:2: ( ruleAttributeFilter )
            {
            // InternalDOF.g:5785:2: ( ruleAttributeFilter )
            // InternalDOF.g:5786:3: ruleAttributeFilter
            {
             before(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeFilter();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getAttributeFilterAttributeFilterParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__AttributeFilterAssignment_5"


    // $ANTLR start "rule__DataLinkedCause__InstancesFilterAssignment_6"
    // InternalDOF.g:5795:1: rule__DataLinkedCause__InstancesFilterAssignment_6 : ( ruleInstancesFilter ) ;
    public final void rule__DataLinkedCause__InstancesFilterAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5799:1: ( ( ruleInstancesFilter ) )
            // InternalDOF.g:5800:2: ( ruleInstancesFilter )
            {
            // InternalDOF.g:5800:2: ( ruleInstancesFilter )
            // InternalDOF.g:5801:3: ruleInstancesFilter
            {
             before(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleInstancesFilter();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getInstancesFilterInstancesFilterParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__InstancesFilterAssignment_6"


    // $ANTLR start "rule__DataLinkedCause__IncludedReferencesAssignment_7"
    // InternalDOF.g:5810:1: rule__DataLinkedCause__IncludedReferencesAssignment_7 : ( ruleIncludedReference ) ;
    public final void rule__DataLinkedCause__IncludedReferencesAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5814:1: ( ( ruleIncludedReference ) )
            // InternalDOF.g:5815:2: ( ruleIncludedReference )
            {
            // InternalDOF.g:5815:2: ( ruleIncludedReference )
            // InternalDOF.g:5816:3: ruleIncludedReference
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleIncludedReference();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getIncludedReferencesIncludedReferenceParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__IncludedReferencesAssignment_7"


    // $ANTLR start "rule__DataLinkedCause__TypeFilterAssignment_8"
    // InternalDOF.g:5825:1: rule__DataLinkedCause__TypeFilterAssignment_8 : ( ruleTypeFilter ) ;
    public final void rule__DataLinkedCause__TypeFilterAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5829:1: ( ( ruleTypeFilter ) )
            // InternalDOF.g:5830:2: ( ruleTypeFilter )
            {
            // InternalDOF.g:5830:2: ( ruleTypeFilter )
            // InternalDOF.g:5831:3: ruleTypeFilter
            {
             before(grammarAccess.getDataLinkedCauseAccess().getTypeFilterTypeFilterParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeFilter();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getTypeFilterTypeFilterParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__TypeFilterAssignment_8"


    // $ANTLR start "rule__NotMappedCause__NameAssignment_2"
    // InternalDOF.g:5840:1: rule__NotMappedCause__NameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__NotMappedCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5844:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:5845:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:5845:2: ( ruleQualifiedName )
            // InternalDOF.g:5846:3: ruleQualifiedName
            {
             before(grammarAccess.getNotMappedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseAccess().getNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__NameAssignment_2"


    // $ANTLR start "rule__NotMappedCause__RealizesAssignment_3_1"
    // InternalDOF.g:5855:1: rule__NotMappedCause__RealizesAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__NotMappedCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5859:1: ( ( ( ruleEString ) ) )
            // InternalDOF.g:5860:2: ( ( ruleEString ) )
            {
            // InternalDOF.g:5860:2: ( ( ruleEString ) )
            // InternalDOF.g:5861:3: ( ruleEString )
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 
            // InternalDOF.g:5862:3: ( ruleEString )
            // InternalDOF.g:5863:4: ruleEString
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseAccess().getRealizesCauseEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getNotMappedCauseAccess().getRealizesCauseCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__RealizesAssignment_3_1"


    // $ANTLR start "rule__Path__JumpsAssignment_0"
    // InternalDOF.g:5874:1: rule__Path__JumpsAssignment_0 : ( RULE_ID ) ;
    public final void rule__Path__JumpsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5878:1: ( ( RULE_ID ) )
            // InternalDOF.g:5879:2: ( RULE_ID )
            {
            // InternalDOF.g:5879:2: ( RULE_ID )
            // InternalDOF.g:5880:3: RULE_ID
            {
             before(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__JumpsAssignment_0"


    // $ANTLR start "rule__Path__JumpsAssignment_1_1"
    // InternalDOF.g:5889:1: rule__Path__JumpsAssignment_1_1 : ( RULE_ID ) ;
    public final void rule__Path__JumpsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:5893:1: ( ( RULE_ID ) )
            // InternalDOF.g:5894:2: ( RULE_ID )
            {
            // InternalDOF.g:5894:2: ( RULE_ID )
            // InternalDOF.g:5895:3: RULE_ID
            {
             before(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPathAccess().getJumpsIDTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Path__JumpsAssignment_1_1"

    // Delegated rules


    protected DFA8 dfa8 = new DFA8(this);
    protected DFA9 dfa9 = new DFA9(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\1\5\1\31\1\5\6\uffff\1\31";
    static final String dfa_3s = "\1\5\1\52\1\5\6\uffff\1\52";
    static final String dfa_4s = "\3\uffff\1\2\1\4\1\6\1\1\1\3\1\5\1\uffff";
    static final String dfa_5s = "\12\uffff}>";
    static final String[] dfa_6s = {
            "\1\1",
            "\1\2\13\uffff\1\6\1\3\1\7\1\4\1\10\1\5",
            "\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\2\13\uffff\1\6\1\3\1\7\1\4\1\10\1\5"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "864:1: rule__Comparison__Alternatives : ( ( ( rule__Comparison__Group_0__0 ) ) | ( ( rule__Comparison__Group_1__0 ) ) | ( ( rule__Comparison__Group_2__0 ) ) | ( ( rule__Comparison__Group_3__0 ) ) | ( ( rule__Comparison__Group_4__0 ) ) | ( ( rule__Comparison__Group_5__0 ) ) );";
        }
    }
    static final String dfa_7s = "\1\53\1\5\1\23\1\5\1\4\3\uffff\2\23";
    static final String dfa_8s = "\1\53\1\5\1\56\1\5\1\4\3\uffff\2\56";
    static final String dfa_9s = "\5\uffff\1\3\1\2\1\1\2\uffff";
    static final String[] dfa_10s = {
            "\1\1",
            "\1\2",
            "\1\6\5\uffff\1\3\22\uffff\1\7\1\4\1\5",
            "\1\10",
            "\1\11",
            "",
            "",
            "",
            "\1\6\5\uffff\1\3\22\uffff\1\7\1\4\1\5",
            "\1\6\30\uffff\1\7\1\uffff\1\5"
    };
    static final char[] dfa_7 = DFA.unpackEncodedStringToUnsignedChars(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final short[][] dfa_10 = unpackEncodedStringArray(dfa_10s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_7;
            this.max = dfa_8;
            this.accept = dfa_9;
            this.special = dfa_5;
            this.transition = dfa_10;
        }
        public String getDescription() {
            return "909:1: rule__Cause__Alternatives : ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000004C8200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000004000020L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000082C00000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x000000004D000020L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000022L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000000004C000020L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000400400000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000080800000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000005000020L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000100000020L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000010000020L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000300000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000080001000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000200000080000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x00000004CC000020L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000600000000000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000002000002L});

}