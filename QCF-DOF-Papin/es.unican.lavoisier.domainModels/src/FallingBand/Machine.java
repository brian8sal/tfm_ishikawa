/**
 */
package FallingBand;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.Machine#getLastCalibration <em>Last Calibration</em>}</li>
 *   <li>{@link FallingBand.Machine#getPrograms <em>Programs</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getMachine()
 * @model
 * @generated
 */
public interface Machine extends EObject {
	/**
	 * Returns the value of the '<em><b>Last Calibration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Calibration</em>' attribute.
	 * @see #setLastCalibration(Date)
	 * @see FallingBand.FallingPackage#getMachine_LastCalibration()
	 * @model
	 * @generated
	 */
	Date getLastCalibration();

	/**
	 * Sets the value of the '{@link FallingBand.Machine#getLastCalibration <em>Last Calibration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Calibration</em>' attribute.
	 * @see #getLastCalibration()
	 * @generated
	 */
	void setLastCalibration(Date value);

	/**
	 * Returns the value of the '<em><b>Programs</b></em>' containment reference list.
	 * The list contents are of type {@link FallingBand.Program}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programs</em>' containment reference list.
	 * @see FallingBand.FallingPackage#getMachine_Programs()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Program> getPrograms();

} // Machine
