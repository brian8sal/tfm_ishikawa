driveHalfShafts[["id", "idBandWheel", "idComplianceReport"]].merge(bands[['id', 'model']],
        how="left", left_on="idBandWheel", right_on="id", suffixes=("", "wheelBand")).merge(reports[["id", "fallingWheelBand"]],
        how="left", left_on="idComplianceReport", right_on="id", suffixes=("", "report"))).drop(columns=["idBandWheel",
        "idComplianceReport", "idwheelBand", "idreport"])