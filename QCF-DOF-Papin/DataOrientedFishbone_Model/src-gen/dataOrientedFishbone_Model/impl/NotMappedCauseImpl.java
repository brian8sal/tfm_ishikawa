/**
 */
package dataOrientedFishbone_Model.impl;

import dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage;
import dataOrientedFishbone_Model.NotMappedCause;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Not Mapped Cause</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NotMappedCauseImpl extends CauseImpl implements NotMappedCause {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NotMappedCauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataOrientedFishbone_ModelPackage.Literals.NOT_MAPPED_CAUSE;
	}

} //NotMappedCauseImpl
