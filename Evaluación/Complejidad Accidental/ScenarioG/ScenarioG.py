driveHalfShafts[["id", "idBandEngine", "idComplianceReport", "idShaft" ]].merge(bands, how="left", left_on="idBandEngine", right_on="id",
    suffixes=( "", "engineBand")).merge(reports[['id', 'fallingWheelBand']], how="left", left_on="idComplianceReport",
    right_on="id", suffixes=("", "report")).merge(shafts, how="left", left_on="idShaft", right_on="id",
    suffixes=( "", "shaft")).merge(shaftBatchParameters, how="left", left_on="batchId", right_on="batchID",suffixes=("",
    "parameters")).merge((powderCoatingChecks.pivot(index="id", columns=["zone"],values=["thickness"])), how="left",
    left_on="idPowderCoatingInspections", right_on="id", suffixes=("", "parameters")).merge(providers, how="left",
    left_on="idProvider", right_on="id", suffixes=("","provider")).drop(columns=["idBandEngine", "idComplianceReport",
    "idengineBand", "idreport", "batchID", "idProvider", "idprovider", "idShaft", "idshaft","idPowderCoatingInspections"])