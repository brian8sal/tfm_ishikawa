/*
 * generated by Xtext 2.25.0
 */
package es.unican.serializer;

import com.google.inject.Inject;
import es.unican.papin.AggregatedReference;
import es.unican.papin.AndConjunction;
import es.unican.papin.AttributeFilter;
import es.unican.papin.Dataset;
import es.unican.papin.Datasets;
import es.unican.papin.Equality;
import es.unican.papin.Inequality;
import es.unican.papin.LessThan;
import es.unican.papin.LessThanOrEqual;
import es.unican.papin.MoreThan;
import es.unican.papin.MoreThanOrEqual;
import es.unican.papin.OrConjunction;
import es.unican.papin.PapinPackage;
import es.unican.papin.Path;
import es.unican.papin.SimpleReference;
import es.unican.papin.TypeCompletion;
import es.unican.papin.TypeCustomization;
import es.unican.papin.TypeSelection;
import es.unican.services.PapinGrammarAccess;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class PapinSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private PapinGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == PapinPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case PapinPackage.AGGREGATED_REFERENCE:
				sequence_AggregatedReference(context, (AggregatedReference) semanticObject); 
				return; 
			case PapinPackage.AND_CONJUNCTION:
				sequence_AndConjunction(context, (AndConjunction) semanticObject); 
				return; 
			case PapinPackage.ATTRIBUTE_FILTER:
				sequence_AttributeFilter(context, (AttributeFilter) semanticObject); 
				return; 
			case PapinPackage.DATASET:
				sequence_Dataset(context, (Dataset) semanticObject); 
				return; 
			case PapinPackage.DATASETS:
				sequence_Datasets(context, (Datasets) semanticObject); 
				return; 
			case PapinPackage.EQUALITY:
				sequence_Comparison(context, (Equality) semanticObject); 
				return; 
			case PapinPackage.INEQUALITY:
				sequence_Comparison(context, (Inequality) semanticObject); 
				return; 
			case PapinPackage.LESS_THAN:
				sequence_Comparison(context, (LessThan) semanticObject); 
				return; 
			case PapinPackage.LESS_THAN_OR_EQUAL:
				sequence_Comparison(context, (LessThanOrEqual) semanticObject); 
				return; 
			case PapinPackage.MORE_THAN:
				sequence_Comparison(context, (MoreThan) semanticObject); 
				return; 
			case PapinPackage.MORE_THAN_OR_EQUAL:
				sequence_Comparison(context, (MoreThanOrEqual) semanticObject); 
				return; 
			case PapinPackage.OR_CONJUNCTION:
				sequence_OrConjunction(context, (OrConjunction) semanticObject); 
				return; 
			case PapinPackage.PATH:
				sequence_Path(context, (Path) semanticObject); 
				return; 
			case PapinPackage.SIMPLE_REFERENCE:
				sequence_SimpleReference(context, (SimpleReference) semanticObject); 
				return; 
			case PapinPackage.TYPE_COMPLETION:
				sequence_TypeCompletion(context, (TypeCompletion) semanticObject); 
				return; 
			case PapinPackage.TYPE_CUSTOMIZATION:
				sequence_TypeCustomization(context, (TypeCustomization) semanticObject); 
				return; 
			case PapinPackage.TYPE_SELECTION:
				sequence_TypeSelection(context, (TypeSelection) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     IncludedReference returns AggregatedReference
	 *     AggregatedReference returns AggregatedReference
	 *
	 * Constraint:
	 *     (
	 *         cat=[Category|QualifiedName] 
	 *         causes=[DataLinkedCause|QualifiedName]? 
	 *         function=AggFunction 
	 *         aggValue=Path 
	 *         pivotingId=Path? 
	 *         instancesFilter=InstancesFilter?
	 *     )
	 */
	protected void sequence_AggregatedReference(ISerializationContext context, AggregatedReference semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns AndConjunction
	 *     AndConjunction returns AndConjunction
	 *     AndConjunction.AndConjunction_1_0 returns AndConjunction
	 *     OrConjunction returns AndConjunction
	 *     OrConjunction.OrConjunction_1_0 returns AndConjunction
	 *     Primary returns AndConjunction
	 *
	 * Constraint:
	 *     (left=AndConjunction_AndConjunction_1_0 right=OrConjunction)
	 */
	protected void sequence_AndConjunction(ISerializationContext context, AndConjunction semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.AND_CONJUNCTION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.AND_CONJUNCTION__LEFT));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.AND_CONJUNCTION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.AND_CONJUNCTION__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAndConjunctionAccess().getAndConjunctionLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAndConjunctionAccess().getRightOrConjunctionParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     AttributeFilter returns AttributeFilter
	 *
	 * Constraint:
	 *     (attributes+=ID attributes+=ID*)
	 */
	protected void sequence_AttributeFilter(ISerializationContext context, AttributeFilter semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns Equality
	 *     AndConjunction returns Equality
	 *     AndConjunction.AndConjunction_1_0 returns Equality
	 *     OrConjunction returns Equality
	 *     OrConjunction.OrConjunction_1_0 returns Equality
	 *     Primary returns Equality
	 *     Comparison returns Equality
	 *
	 * Constraint:
	 *     (path=Path value=STRING)
	 */
	protected void sequence_Comparison(ISerializationContext context, Equality semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__PATH));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_0_1_0(), semanticObject.getPath());
		feeder.accept(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_0_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns Inequality
	 *     AndConjunction returns Inequality
	 *     AndConjunction.AndConjunction_1_0 returns Inequality
	 *     OrConjunction returns Inequality
	 *     OrConjunction.OrConjunction_1_0 returns Inequality
	 *     Primary returns Inequality
	 *     Comparison returns Inequality
	 *
	 * Constraint:
	 *     (path=Path value=STRING)
	 */
	protected void sequence_Comparison(ISerializationContext context, Inequality semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__PATH));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_1_1_0(), semanticObject.getPath());
		feeder.accept(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_1_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns LessThan
	 *     AndConjunction returns LessThan
	 *     AndConjunction.AndConjunction_1_0 returns LessThan
	 *     OrConjunction returns LessThan
	 *     OrConjunction.OrConjunction_1_0 returns LessThan
	 *     Primary returns LessThan
	 *     Comparison returns LessThan
	 *
	 * Constraint:
	 *     (path=Path value=STRING)
	 */
	protected void sequence_Comparison(ISerializationContext context, LessThan semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__PATH));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_4_1_0(), semanticObject.getPath());
		feeder.accept(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_4_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns LessThanOrEqual
	 *     AndConjunction returns LessThanOrEqual
	 *     AndConjunction.AndConjunction_1_0 returns LessThanOrEqual
	 *     OrConjunction returns LessThanOrEqual
	 *     OrConjunction.OrConjunction_1_0 returns LessThanOrEqual
	 *     Primary returns LessThanOrEqual
	 *     Comparison returns LessThanOrEqual
	 *
	 * Constraint:
	 *     (path=Path value=STRING)
	 */
	protected void sequence_Comparison(ISerializationContext context, LessThanOrEqual semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__PATH));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_5_1_0(), semanticObject.getPath());
		feeder.accept(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_5_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns MoreThan
	 *     AndConjunction returns MoreThan
	 *     AndConjunction.AndConjunction_1_0 returns MoreThan
	 *     OrConjunction returns MoreThan
	 *     OrConjunction.OrConjunction_1_0 returns MoreThan
	 *     Primary returns MoreThan
	 *     Comparison returns MoreThan
	 *
	 * Constraint:
	 *     (path=Path value=STRING)
	 */
	protected void sequence_Comparison(ISerializationContext context, MoreThan semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__PATH));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_2_1_0(), semanticObject.getPath());
		feeder.accept(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_2_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns MoreThanOrEqual
	 *     AndConjunction returns MoreThanOrEqual
	 *     AndConjunction.AndConjunction_1_0 returns MoreThanOrEqual
	 *     OrConjunction returns MoreThanOrEqual
	 *     OrConjunction.OrConjunction_1_0 returns MoreThanOrEqual
	 *     Primary returns MoreThanOrEqual
	 *     Comparison returns MoreThanOrEqual
	 *
	 * Constraint:
	 *     (path=Path value=STRING)
	 */
	protected void sequence_Comparison(ISerializationContext context, MoreThanOrEqual semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__PATH) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__PATH));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.COMPARISON__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.COMPARISON__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getComparisonAccess().getPathPathParserRuleCall_3_1_0(), semanticObject.getPath());
		feeder.accept(grammarAccess.getComparisonAccess().getValueSTRINGTerminalRuleCall_3_3_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Dataset returns Dataset
	 *
	 * Constraint:
	 *     (
	 *         name=ID 
	 *         using=[Effect|QualifiedName] 
	 *         attributeFilter=AttributeFilter? 
	 *         instancesFilter=InstancesFilter? 
	 *         includedReferences+=IncludedReference* 
	 *         typeFilter=TypeFilter?
	 *     )
	 */
	protected void sequence_Dataset(ISerializationContext context, Dataset semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Datasets returns Datasets
	 *
	 * Constraint:
	 *     (domainModelNSURI=STRING domainModelInstance=STRING datasets+=Dataset*)
	 */
	protected void sequence_Datasets(ISerializationContext context, Datasets semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     InstancesFilter returns OrConjunction
	 *     AndConjunction returns OrConjunction
	 *     AndConjunction.AndConjunction_1_0 returns OrConjunction
	 *     OrConjunction returns OrConjunction
	 *     OrConjunction.OrConjunction_1_0 returns OrConjunction
	 *     Primary returns OrConjunction
	 *
	 * Constraint:
	 *     (left=OrConjunction_OrConjunction_1_0 right=Primary)
	 */
	protected void sequence_OrConjunction(ISerializationContext context, OrConjunction semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.OR_CONJUNCTION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.OR_CONJUNCTION__LEFT));
			if (transientValues.isValueTransient(semanticObject, PapinPackage.Literals.OR_CONJUNCTION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PapinPackage.Literals.OR_CONJUNCTION__RIGHT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOrConjunctionAccess().getOrConjunctionLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getOrConjunctionAccess().getRightPrimaryParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Path returns Path
	 *
	 * Constraint:
	 *     (jumps+=ID jumps+=ID*)
	 */
	protected void sequence_Path(ISerializationContext context, Path semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     IncludedReference returns SimpleReference
	 *     SimpleReference returns SimpleReference
	 *
	 * Constraint:
	 *     (
	 *         cat=[Category|QualifiedName] 
	 *         attributeFilter=AttributeFilter? 
	 *         (pivotingId=Path instancesFilter=InstancesFilter?)? 
	 *         causes+=[DataLinkedCause|QualifiedName]* 
	 *         typeFilter=TypeFilter?
	 *     )
	 */
	protected void sequence_SimpleReference(ISerializationContext context, SimpleReference semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     TypeFilter returns TypeCompletion
	 *     TypeCompletion returns TypeCompletion
	 *
	 * Constraint:
	 *     typeCustomizations+=TypeCustomization+
	 */
	protected void sequence_TypeCompletion(ISerializationContext context, TypeCompletion semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     TypeCustomization returns TypeCustomization
	 *
	 * Constraint:
	 *     (name=ID attributeFilter=AttributeFilter? includedReferences+=IncludedReference*)
	 */
	protected void sequence_TypeCustomization(ISerializationContext context, TypeCustomization semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     TypeFilter returns TypeSelection
	 *     TypeSelection returns TypeSelection
	 *
	 * Constraint:
	 *     typeCustomizations+=TypeCustomization+
	 */
	protected void sequence_TypeSelection(ISerializationContext context, TypeSelection semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
