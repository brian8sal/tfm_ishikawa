/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see es.unican.dof.DofPackage
 * @generated
 */
public interface DofFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DofFactory eINSTANCE = es.unican.dof.impl.DofFactoryImpl.init();

  /**
   * Returns a new object of class '<em>DOF</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>DOF</em>'.
   * @generated
   */
  DOF createDOF();

  /**
   * Returns a new object of class '<em>Import</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Import</em>'.
   * @generated
   */
  Import createImport();

  /**
   * Returns a new object of class '<em>Effect</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Effect</em>'.
   * @generated
   */
  Effect createEffect();

  /**
   * Returns a new object of class '<em>Category</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Category</em>'.
   * @generated
   */
  Category createCategory();

  /**
   * Returns a new object of class '<em>Data Feeder</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Data Feeder</em>'.
   * @generated
   */
  DataFeeder createDataFeeder();

  /**
   * Returns a new object of class '<em>Included Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Included Reference</em>'.
   * @generated
   */
  IncludedReference createIncludedReference();

  /**
   * Returns a new object of class '<em>Simple Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Reference</em>'.
   * @generated
   */
  SimpleReference createSimpleReference();

  /**
   * Returns a new object of class '<em>Aggregated Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Aggregated Reference</em>'.
   * @generated
   */
  AggregatedReference createAggregatedReference();

  /**
   * Returns a new object of class '<em>Type Filter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Filter</em>'.
   * @generated
   */
  TypeFilter createTypeFilter();

  /**
   * Returns a new object of class '<em>Type Completion</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Completion</em>'.
   * @generated
   */
  TypeCompletion createTypeCompletion();

  /**
   * Returns a new object of class '<em>Type Selection</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Selection</em>'.
   * @generated
   */
  TypeSelection createTypeSelection();

  /**
   * Returns a new object of class '<em>Type Customization</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Type Customization</em>'.
   * @generated
   */
  TypeCustomization createTypeCustomization();

  /**
   * Returns a new object of class '<em>Attribute Filter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Attribute Filter</em>'.
   * @generated
   */
  AttributeFilter createAttributeFilter();

  /**
   * Returns a new object of class '<em>Boolean Expression</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Expression</em>'.
   * @generated
   */
  BooleanExpression createBooleanExpression();

  /**
   * Returns a new object of class '<em>Comparison</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comparison</em>'.
   * @generated
   */
  Comparison createComparison();

  /**
   * Returns a new object of class '<em>Cause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cause</em>'.
   * @generated
   */
  Cause createCause();

  /**
   * Returns a new object of class '<em>Compound Cause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Compound Cause</em>'.
   * @generated
   */
  CompoundCause createCompoundCause();

  /**
   * Returns a new object of class '<em>Data Linked Cause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Data Linked Cause</em>'.
   * @generated
   */
  DataLinkedCause createDataLinkedCause();

  /**
   * Returns a new object of class '<em>Not Mapped Cause</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Not Mapped Cause</em>'.
   * @generated
   */
  NotMappedCause createNotMappedCause();

  /**
   * Returns a new object of class '<em>Path</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path</em>'.
   * @generated
   */
  Path createPath();

  /**
   * Returns a new object of class '<em>And Conjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>And Conjunction</em>'.
   * @generated
   */
  AndConjunction createAndConjunction();

  /**
   * Returns a new object of class '<em>Or Conjunction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Or Conjunction</em>'.
   * @generated
   */
  OrConjunction createOrConjunction();

  /**
   * Returns a new object of class '<em>Equality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Equality</em>'.
   * @generated
   */
  Equality createEquality();

  /**
   * Returns a new object of class '<em>Inequality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Inequality</em>'.
   * @generated
   */
  Inequality createInequality();

  /**
   * Returns a new object of class '<em>More Than</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>More Than</em>'.
   * @generated
   */
  MoreThan createMoreThan();

  /**
   * Returns a new object of class '<em>More Than Or Equal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>More Than Or Equal</em>'.
   * @generated
   */
  MoreThanOrEqual createMoreThanOrEqual();

  /**
   * Returns a new object of class '<em>Less Than</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Less Than</em>'.
   * @generated
   */
  LessThan createLessThan();

  /**
   * Returns a new object of class '<em>Less Than Or Equal</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Less Than Or Equal</em>'.
   * @generated
   */
  LessThanOrEqual createLessThanOrEqual();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  DofPackage getDofPackage();

} //DofFactory
