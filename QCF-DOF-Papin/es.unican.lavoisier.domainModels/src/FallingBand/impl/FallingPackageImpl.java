/**
 */
package FallingBand.impl;

import FallingBand.AssemblySession;
import FallingBand.Band;
import FallingBand.BandBatchParameters;
import FallingBand.ComplianceReport;
import FallingBand.Coordinate;
import FallingBand.DriveHalfShaft;
import FallingBand.FallingFactory;
import FallingBand.FallingPackage;
import FallingBand.Housing;
import FallingBand.HousingParameters;
import FallingBand.Machine;
import FallingBand.MonitoredParameters;
import FallingBand.Parameters;
import FallingBand.PliersData;
import FallingBand.PowderCoatingChecks;
import FallingBand.Program;
import FallingBand.Provider;

import FallingBand.Shaft;
import FallingBand.ShaftBatchParameters;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FallingPackageImpl extends EPackageImpl implements FallingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass driveHalfShaftEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bandBatchParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assemblySessionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitoredParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pliersDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complianceReportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass shaftEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass shaftBatchParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass powderCoatingChecksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass machineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass programEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coordinateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass housingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass housingParametersEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see FallingBand.FallingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FallingPackageImpl() {
		super(eNS_URI, FallingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FallingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FallingPackage init() {
		if (isInited) return (FallingPackage)EPackage.Registry.INSTANCE.getEPackage(FallingPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFallingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FallingPackageImpl theFallingPackage = registeredFallingPackage instanceof FallingPackageImpl ? (FallingPackageImpl)registeredFallingPackage : new FallingPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theFallingPackage.createPackageContents();

		// Initialize created meta-data
		theFallingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFallingPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FallingPackage.eNS_URI, theFallingPackage);
		return theFallingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDriveHalfShaft() {
		return driveHalfShaftEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDriveHalfShaft_Id() {
		return (EAttribute)driveHalfShaftEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDriveHalfShaft_ManufacturedTime() {
		return (EAttribute)driveHalfShaftEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_EngineBand() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_WheelBand() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_AssemblySession() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_Report() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_Shaft() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_EngineHousing() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDriveHalfShaft_WheelHousing() {
		return (EReference)driveHalfShaftEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBand() {
		return bandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBand_Model() {
		return (EAttribute)bandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBand_Parameters() {
		return (EReference)bandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBandBatchParameters() {
		return bandBatchParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_MaxThickness() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_MinThickness() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_AvgThickness() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBandBatchParameters_BatchId() {
		return (EAttribute)bandBatchParametersEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBandBatchParameters_Provider() {
		return (EReference)bandBatchParametersEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssemblySession() {
		return assemblySessionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssemblySession_Start() {
		return (EAttribute)assemblySessionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssemblySession_Stop() {
		return (EAttribute)assemblySessionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssemblySession_Parameters() {
		return (EReference)assemblySessionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssemblySession_Machine() {
		return (EReference)assemblySessionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssemblySession_Program() {
		return (EReference)assemblySessionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitoredParameters() {
		return monitoredParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoredParameters_Temperature() {
		return (EAttribute)monitoredParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonitoredParameters_Humidity() {
		return (EAttribute)monitoredParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMonitoredParameters_PliersTightenings() {
		return (EReference)monitoredParametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPliersData() {
		return pliersDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPliersData_Pressure() {
		return (EAttribute)pliersDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPliersData_Number() {
		return (EAttribute)pliersDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplianceReport() {
		return complianceReportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplianceReport_FallingWheelBand() {
		return (EAttribute)complianceReportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplianceReport_FallingEngineBand() {
		return (EAttribute)complianceReportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvider() {
		return providerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProvider_Provider() {
		return (EAttribute)providerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShaft() {
		return shaftEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaft_Model() {
		return (EAttribute)shaftEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShaft_Parameters() {
		return (EReference)shaftEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShaftBatchParameters() {
		return shaftBatchParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_MaxLength() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_MinLength() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_AvgLength() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_MaxDiameter() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_MinDiameter() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_AvgDiameter() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShaftBatchParameters_BatchId() {
		return (EAttribute)shaftBatchParametersEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShaftBatchParameters_Provider() {
		return (EReference)shaftBatchParametersEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShaftBatchParameters_PowderCoatingInspections() {
		return (EReference)shaftBatchParametersEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPowderCoatingChecks() {
		return powderCoatingChecksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPowderCoatingChecks_Zone() {
		return (EAttribute)powderCoatingChecksEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPowderCoatingChecks_Thickness() {
		return (EAttribute)powderCoatingChecksEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMachine() {
		return machineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMachine_LastCalibration() {
		return (EAttribute)machineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMachine_Programs() {
		return (EReference)machineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProgram() {
		return programEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProgram_Number() {
		return (EAttribute)programEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProgram_Parameters() {
		return (EReference)programEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameters() {
		return parametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameters_TighteningForce() {
		return (EAttribute)parametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameters_TighteningTime() {
		return (EAttribute)parametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameters_WheelBandPosition() {
		return (EReference)parametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameters_EngineBandPosition() {
		return (EReference)parametersEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameters_WheelHousingPosition() {
		return (EReference)parametersEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameters_EngineHousingPosition() {
		return (EReference)parametersEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoordinate() {
		return coordinateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCoordinate_X() {
		return (EAttribute)coordinateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCoordinate_Y() {
		return (EAttribute)coordinateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCoordinate_Z() {
		return (EAttribute)coordinateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHousing() {
		return housingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousing_Model() {
		return (EAttribute)housingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHousing_Parameters() {
		return (EReference)housingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHousingParameters() {
		return housingParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_MaxLength() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_MinLength() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_AvgLength() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_MaxDiameter() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_MinDiameter() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_AvgDiameter() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHousingParameters_BatchId() {
		return (EAttribute)housingParametersEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHousingParameters_Provider() {
		return (EReference)housingParametersEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FallingFactory getFallingFactory() {
		return (FallingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		driveHalfShaftEClass = createEClass(DRIVE_HALF_SHAFT);
		createEAttribute(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ID);
		createEAttribute(driveHalfShaftEClass, DRIVE_HALF_SHAFT__MANUFACTURED_TIME);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ENGINE_BAND);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__WHEEL_BAND);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ASSEMBLY_SESSION);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__REPORT);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__SHAFT);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__ENGINE_HOUSING);
		createEReference(driveHalfShaftEClass, DRIVE_HALF_SHAFT__WHEEL_HOUSING);

		bandEClass = createEClass(BAND);
		createEAttribute(bandEClass, BAND__MODEL);
		createEReference(bandEClass, BAND__PARAMETERS);

		bandBatchParametersEClass = createEClass(BAND_BATCH_PARAMETERS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__MAX_THICKNESS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__MIN_THICKNESS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__AVG_THICKNESS);
		createEAttribute(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__BATCH_ID);
		createEReference(bandBatchParametersEClass, BAND_BATCH_PARAMETERS__PROVIDER);

		assemblySessionEClass = createEClass(ASSEMBLY_SESSION);
		createEAttribute(assemblySessionEClass, ASSEMBLY_SESSION__START);
		createEAttribute(assemblySessionEClass, ASSEMBLY_SESSION__STOP);
		createEReference(assemblySessionEClass, ASSEMBLY_SESSION__PARAMETERS);
		createEReference(assemblySessionEClass, ASSEMBLY_SESSION__MACHINE);
		createEReference(assemblySessionEClass, ASSEMBLY_SESSION__PROGRAM);

		monitoredParametersEClass = createEClass(MONITORED_PARAMETERS);
		createEAttribute(monitoredParametersEClass, MONITORED_PARAMETERS__TEMPERATURE);
		createEAttribute(monitoredParametersEClass, MONITORED_PARAMETERS__HUMIDITY);
		createEReference(monitoredParametersEClass, MONITORED_PARAMETERS__PLIERS_TIGHTENINGS);

		pliersDataEClass = createEClass(PLIERS_DATA);
		createEAttribute(pliersDataEClass, PLIERS_DATA__PRESSURE);
		createEAttribute(pliersDataEClass, PLIERS_DATA__NUMBER);

		complianceReportEClass = createEClass(COMPLIANCE_REPORT);
		createEAttribute(complianceReportEClass, COMPLIANCE_REPORT__FALLING_WHEEL_BAND);
		createEAttribute(complianceReportEClass, COMPLIANCE_REPORT__FALLING_ENGINE_BAND);

		providerEClass = createEClass(PROVIDER);
		createEAttribute(providerEClass, PROVIDER__PROVIDER);

		shaftEClass = createEClass(SHAFT);
		createEAttribute(shaftEClass, SHAFT__MODEL);
		createEReference(shaftEClass, SHAFT__PARAMETERS);

		shaftBatchParametersEClass = createEClass(SHAFT_BATCH_PARAMETERS);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__MAX_LENGTH);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__MIN_LENGTH);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__AVG_LENGTH);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__MAX_DIAMETER);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__MIN_DIAMETER);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__AVG_DIAMETER);
		createEAttribute(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__BATCH_ID);
		createEReference(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__PROVIDER);
		createEReference(shaftBatchParametersEClass, SHAFT_BATCH_PARAMETERS__POWDER_COATING_INSPECTIONS);

		powderCoatingChecksEClass = createEClass(POWDER_COATING_CHECKS);
		createEAttribute(powderCoatingChecksEClass, POWDER_COATING_CHECKS__ZONE);
		createEAttribute(powderCoatingChecksEClass, POWDER_COATING_CHECKS__THICKNESS);

		machineEClass = createEClass(MACHINE);
		createEAttribute(machineEClass, MACHINE__LAST_CALIBRATION);
		createEReference(machineEClass, MACHINE__PROGRAMS);

		programEClass = createEClass(PROGRAM);
		createEAttribute(programEClass, PROGRAM__NUMBER);
		createEReference(programEClass, PROGRAM__PARAMETERS);

		parametersEClass = createEClass(PARAMETERS);
		createEAttribute(parametersEClass, PARAMETERS__TIGHTENING_FORCE);
		createEAttribute(parametersEClass, PARAMETERS__TIGHTENING_TIME);
		createEReference(parametersEClass, PARAMETERS__WHEEL_BAND_POSITION);
		createEReference(parametersEClass, PARAMETERS__ENGINE_BAND_POSITION);
		createEReference(parametersEClass, PARAMETERS__WHEEL_HOUSING_POSITION);
		createEReference(parametersEClass, PARAMETERS__ENGINE_HOUSING_POSITION);

		coordinateEClass = createEClass(COORDINATE);
		createEAttribute(coordinateEClass, COORDINATE__X);
		createEAttribute(coordinateEClass, COORDINATE__Y);
		createEAttribute(coordinateEClass, COORDINATE__Z);

		housingEClass = createEClass(HOUSING);
		createEAttribute(housingEClass, HOUSING__MODEL);
		createEReference(housingEClass, HOUSING__PARAMETERS);

		housingParametersEClass = createEClass(HOUSING_PARAMETERS);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__MAX_LENGTH);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__MIN_LENGTH);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__AVG_LENGTH);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__MAX_DIAMETER);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__MIN_DIAMETER);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__AVG_DIAMETER);
		createEAttribute(housingParametersEClass, HOUSING_PARAMETERS__BATCH_ID);
		createEReference(housingParametersEClass, HOUSING_PARAMETERS__PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(driveHalfShaftEClass, DriveHalfShaft.class, "DriveHalfShaft", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDriveHalfShaft_Id(), ecorePackage.getEString(), "id", null, 0, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDriveHalfShaft_ManufacturedTime(), ecorePackage.getEDate(), "manufacturedTime", null, 0, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_EngineBand(), this.getBand(), null, "engineBand", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_WheelBand(), this.getBand(), null, "wheelBand", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_AssemblySession(), this.getAssemblySession(), null, "assemblySession", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_Report(), this.getComplianceReport(), null, "report", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_Shaft(), this.getShaft(), null, "shaft", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_EngineHousing(), this.getHousing(), null, "engineHousing", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDriveHalfShaft_WheelHousing(), this.getHousing(), null, "wheelHousing", null, 1, 1, DriveHalfShaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bandEClass, Band.class, "Band", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBand_Model(), ecorePackage.getEString(), "model", null, 0, 1, Band.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBand_Parameters(), this.getBandBatchParameters(), null, "parameters", null, 1, 1, Band.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bandBatchParametersEClass, BandBatchParameters.class, "BandBatchParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBandBatchParameters_MaxThickness(), ecorePackage.getEDoubleObject(), "maxThickness", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBandBatchParameters_MinThickness(), ecorePackage.getEDoubleObject(), "minThickness", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBandBatchParameters_AvgThickness(), ecorePackage.getEDoubleObject(), "avgThickness", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBandBatchParameters_BatchId(), ecorePackage.getEDoubleObject(), "batchId", null, 0, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBandBatchParameters_Provider(), this.getProvider(), null, "provider", null, 1, 1, BandBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assemblySessionEClass, AssemblySession.class, "AssemblySession", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAssemblySession_Start(), ecorePackage.getEDate(), "start", null, 0, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAssemblySession_Stop(), ecorePackage.getEDate(), "stop", null, 0, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssemblySession_Parameters(), this.getMonitoredParameters(), null, "parameters", null, 1, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssemblySession_Machine(), this.getMachine(), null, "machine", null, 1, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssemblySession_Program(), this.getProgram(), null, "program", null, 1, 1, AssemblySession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(monitoredParametersEClass, MonitoredParameters.class, "MonitoredParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonitoredParameters_Temperature(), ecorePackage.getEDoubleObject(), "temperature", null, 0, 1, MonitoredParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonitoredParameters_Humidity(), ecorePackage.getEDoubleObject(), "humidity", null, 0, 1, MonitoredParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMonitoredParameters_PliersTightenings(), this.getPliersData(), null, "pliersTightenings", null, 4, 4, MonitoredParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pliersDataEClass, PliersData.class, "PliersData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPliersData_Pressure(), ecorePackage.getEDoubleObject(), "pressure", null, 0, 1, PliersData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPliersData_Number(), ecorePackage.getEIntegerObject(), "number", null, 0, 1, PliersData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complianceReportEClass, ComplianceReport.class, "ComplianceReport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplianceReport_FallingWheelBand(), ecorePackage.getEBooleanObject(), "fallingWheelBand", null, 0, 1, ComplianceReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplianceReport_FallingEngineBand(), ecorePackage.getEBooleanObject(), "fallingEngineBand", null, 0, 1, ComplianceReport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providerEClass, Provider.class, "Provider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProvider_Provider(), ecorePackage.getEString(), "provider", null, 0, 1, Provider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(shaftEClass, Shaft.class, "Shaft", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getShaft_Model(), ecorePackage.getEString(), "model", null, 0, 1, Shaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getShaft_Parameters(), this.getShaftBatchParameters(), null, "parameters", null, 1, 1, Shaft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(shaftBatchParametersEClass, ShaftBatchParameters.class, "ShaftBatchParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getShaftBatchParameters_MaxLength(), ecorePackage.getEDoubleObject(), "maxLength", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShaftBatchParameters_MinLength(), ecorePackage.getEDoubleObject(), "minLength", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShaftBatchParameters_AvgLength(), ecorePackage.getEDoubleObject(), "avgLength", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShaftBatchParameters_MaxDiameter(), ecorePackage.getEDoubleObject(), "maxDiameter", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShaftBatchParameters_MinDiameter(), ecorePackage.getEDoubleObject(), "minDiameter", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShaftBatchParameters_AvgDiameter(), ecorePackage.getEDoubleObject(), "avgDiameter", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShaftBatchParameters_BatchId(), ecorePackage.getEString(), "batchId", null, 0, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getShaftBatchParameters_Provider(), this.getProvider(), null, "provider", null, 1, 1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getShaftBatchParameters_PowderCoatingInspections(), this.getPowderCoatingChecks(), null, "powderCoatingInspections", null, 0, -1, ShaftBatchParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(powderCoatingChecksEClass, PowderCoatingChecks.class, "PowderCoatingChecks", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPowderCoatingChecks_Zone(), ecorePackage.getEString(), "zone", null, 0, 1, PowderCoatingChecks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPowderCoatingChecks_Thickness(), ecorePackage.getEDoubleObject(), "thickness", null, 0, 1, PowderCoatingChecks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(machineEClass, Machine.class, "Machine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMachine_LastCalibration(), ecorePackage.getEDate(), "lastCalibration", null, 0, 1, Machine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMachine_Programs(), this.getProgram(), null, "programs", null, 1, -1, Machine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(programEClass, Program.class, "Program", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProgram_Number(), ecorePackage.getEIntegerObject(), "number", null, 0, 1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProgram_Parameters(), this.getParameters(), null, "parameters", null, 1, 1, Program.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parametersEClass, Parameters.class, "Parameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getParameters_TighteningForce(), ecorePackage.getEDoubleObject(), "tighteningForce", null, 0, 1, Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameters_TighteningTime(), ecorePackage.getEDoubleObject(), "tighteningTime", null, 0, 1, Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getParameters_WheelBandPosition(), this.getCoordinate(), null, "wheelBandPosition", null, 1, 1, Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getParameters_EngineBandPosition(), this.getCoordinate(), null, "engineBandPosition", null, 1, 1, Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getParameters_WheelHousingPosition(), this.getCoordinate(), null, "wheelHousingPosition", null, 1, 1, Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getParameters_EngineHousingPosition(), this.getCoordinate(), null, "engineHousingPosition", null, 1, 1, Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(coordinateEClass, Coordinate.class, "Coordinate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCoordinate_X(), ecorePackage.getEDoubleObject(), "x", null, 0, 1, Coordinate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCoordinate_Y(), ecorePackage.getEDoubleObject(), "y", null, 0, 1, Coordinate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCoordinate_Z(), ecorePackage.getEDoubleObject(), "z", null, 0, 1, Coordinate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(housingEClass, Housing.class, "Housing", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHousing_Model(), ecorePackage.getEString(), "model", null, 0, 1, Housing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHousing_Parameters(), this.getHousingParameters(), null, "parameters", null, 1, 1, Housing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(housingParametersEClass, HousingParameters.class, "HousingParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHousingParameters_MaxLength(), ecorePackage.getEDoubleObject(), "maxLength", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHousingParameters_MinLength(), ecorePackage.getEDoubleObject(), "minLength", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHousingParameters_AvgLength(), ecorePackage.getEDoubleObject(), "avgLength", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHousingParameters_MaxDiameter(), ecorePackage.getEDoubleObject(), "maxDiameter", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHousingParameters_MinDiameter(), ecorePackage.getEDoubleObject(), "minDiameter", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHousingParameters_AvgDiameter(), ecorePackage.getEDoubleObject(), "avgDiameter", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHousingParameters_BatchId(), ecorePackage.getEDoubleObject(), "batchId", null, 0, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHousingParameters_Provider(), this.getProvider(), null, "provider", null, 1, 1, HousingParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //FallingPackageImpl
