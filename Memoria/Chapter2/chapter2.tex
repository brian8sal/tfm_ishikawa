% !TeX spellcheck = en_US
%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Second Chapter *********************************
%*******************************************************************************

\chapter{Background and Motivation}
\label{chap:background}

\ifpdf
\graphicspath{{Chapter2/Figs/Raster/}{Chapter2/Figs/PDF/}{Chapter2/Figs/}}
\else
\graphicspath{{Chapter2/Figs/Vector/}{Chapter2/Figs/}}
\fi

\section{Running Example: Falling Band} 
\label{sec:fallingBand}

As running example throughout this work, we will use the production of \emph{drive half-shafts} in a supplier company for the automotive sector. This example is taken from the literature~\citep{Dziuba2013} and based on a real case study. Figure~\ref{fig:driveHalfShaftSketch} provides a rough sketch of the components of a drive half-shaft. A drive half-shaft is a piece of the car transmission system that joins the engine with a wheel. It is composed of a rotating bar of metal, the \emph{shaft}, with two articulated ends, that allow the wheel to move freely as required by the steering and suspension systems at the same time it continues receiving the engine force.

The articulated ends are protected by a flexible piece of rubber known as \emph{housing}. The housings are fixed to the shaft by means of two metal bands. To produce these drive half-shafts, an operator inserts a shaft in an assembly machine, mounts each housing on the shaft and tights the bands around the housings using a pneumatic pliers. Once finished, these pieces are sent to car manufacturers, who check they are compliant with their requirements. During these checks, car manufacturers detected that, sometimes, bands on the side of the wheel unfastened, originating different problems. To solve this issue, the company wished to analyze data gathered during the drive half-shafts production process in order to find the causes of this problem. To analyze these data, the company may use data mining techniques, for which it would need to execute a data mining process. These processes are described in the next section.

\begin{figure}[!tb]
	\centering
	% Requires \usepackage{graphicx}
	\includegraphics[width=0.60\linewidth]{Chapter2/Figs/driveHalfShaft.eps} \\
	\caption{Elements of a drive half-shaft.}
	\label{fig:driveHalfShaftSketch}
\end{figure}


\section{Data Mining Processes}
\label{sec:motivation:dmProcesses}

To describe the steps that comprise a data mining process, we use as reference the \emph{KDD (Knowledge Discovery in Databases)} process~\citep{Fayyad1996}.  Some steps of this process have been split into several sub-stages, in order to clarify some important issues related to the work described here.

Any data mining process is created to answer business questions (Figure~\ref{fig:kdd}, Step 1). These questions are derived from the business or domain experts' needs. For instance, in our running example, industrial engineers in the supplier company wish to find causes of falling bands.

\begin{figure}[!tb]
	\centering
	\includegraphics[width=0.55\linewidth]{Chapter2/Figs/dataMiningProcessExpanded.eps}
	\caption{Detailed data mining process.}
	\label{fig:kdd}
\end{figure}

Once these questions are identified, we need to decide what data sources will be used to answer them (Figure~\ref{fig:kdd}, Step 2). In an Industry 4.0 context, data can come from very heterogeneous sources, such as production machine sensors, quality assurance reports, technical specifications of components used in the manufacturing process, or management data hosted in Manufacturing Execution Systems (MES), among other options. These sources must be selected with the help of domain experts, who know what data might be helpful to find answers to the business questions.

Data provided by these sources is initially unstructured and described in different notations despite having relationships among them. For instance, data of manufactured products might be initially retrieved from the MES relational database, which might be visualized using Entity-Relationship (ER) diagrams~\citep{Chen1976}. These data might refer to raw materials used during the manufacturing process. Data about these raw materials might be available in technical data sheets, stored as PDF files. Additionally, some properties of these raw materials might have been checked during quality control processes. Results of these quality inspections might be registered in spreadsheets. In cases like this, it is worth constructing a \emph{domain model} that unifies these data bundles and helps to visualize them as a whole in a common format~\citep{Hartmann2019} (Figure~\ref{fig:kdd}, Step 3).

Subsequently, data scientists must select the data mining technique that is most adequate for answering each business question (Figure~\ref{fig:kdd}, Step 4). For instance, in our running example, \emph{association rules} techniques~\citep{Witten2016} might be employed to find patterns in data that are most likely to lead to a falling band. For each specific kind of data mining technique, such as \emph{clustering}, \emph{association rules} or \emph{classification}, there is a plethora of algorithms available in the literature. Each one of these algorithms is designed to perform better than the others depending on certain characteristics of the input data. Therefore, data scientists are in charge of selecting the algorithm that best fits with the nature of our input data. For instance, in our case study, the well-known \emph{Apriori algorithm}~\citep{Agrawal1994} could be used to discover common patterns that are most likely to lead to a falling band. Complementary, we may use other algorithms to get rare patterns~\citep{Koh2010}, this is, patterns that appear in very few cases but that lead to a falling band with a high probability degree.

Once a data mining algorithm has been selected, we need to feed it with input data. Most data mining algorithms can only accept as input data arranged in a very specific tabular format. Data scientists often refer to bundles of data arranged in this format as \emph{datasets}.  Therefore, as next step of a data mining process, we need to reshape the available data to create \emph{datasets} that can be digested by data mining algorithms (Figure~\ref{fig:kdd}, Step 5). The work presented in this project focuses on this specific step, which will be explained more in detail in the next section.

In addition to this reshaping, each algorithm might impose other extra constraints to their input data. For instance, some distance-based algorithms require data to be normalized into the range [0, 1]. Therefore, we would need to perform some extra data transformations in order to ensure these constraints are satisfied. These transformations can take place after a dataset has been produced or at the same time it is generated (Figure~\ref{fig:kdd}, Step 6).

Finally, we execute the selected data mining algorithms with the generated datasets (Figure~\ref{fig:kdd}, Step 7) and we obtain the output results. These results must be analyzed to assess their quality and reliability. Then, curated results can be passed to the domain experts (Figure~\ref{fig:kdd}, Step 8), who would interpret them to extract some conclusions and make some decisions. As an example, in our running example, it might be reported that most drive half-shafts with falling bands contain a specific combination of bands from a concrete provider and housings from another particular provider.

Next section details the stage of this data mining process in which this work focuses: the creation of tabular datasets from non-tabular data, such as linked or hierarchical data. 

\section{The Dataset Formatting Problem} \label{sec:dataFormatting}

\begin{figure}[!bt]
	\centering
	% Requires \usepackage{graphicx}
	\includegraphics[width=\linewidth]{Chapter2/Figs/driveHalfShaftDomainModel.pdf} \\
	\caption{A fragment of the domain model for the production of drive half-shafts.}
	\label{fig:domainModel}
\end{figure}

As previously commented, most data analysis algorithms require their input data to be arranged in a specific tabular format, where all the information related to an instance of the entities being analyzed must be placed in a single table row. In the following, we will refer to this requirement as the \emph{one entity, one row} constraint.

Nevertheless, domain data is typically available as a graph of linked and nested data. As an example, Figure~\ref{fig:domainModel} shows a fragment of an object-oriented data model for our running example\footnote{The complete domain model for this case study can be found in the supplementary material to this work} and Listing~\ref{lst:domainData} provides an example of data, in JSON format, for a concrete half-shaft, conforming to that model. This data model is in Ecore notation~\citep{Steinberg2008}, which, roughly speaking, can be considered as a subset of UML 2.0.

\begin{minipage}{\linewidth}
	\begin{center}
		\lstinputlisting[style=json, caption={Data for a drive half-shaft instance},label=lst:domainData,captionpos=b,basicstyle=\scriptsize]{listings/domainData.txt}
	\end{center}
\end{minipage}

According to Figure~\ref{fig:domainModel}, each \emph{DriveHalfShaft} has a \emph{ComplianceReport} associated, created by the company customers, that indicates whether the piece fulfills the customer requirements. Each \emph{DriveHalfShaft} is comprised of two \emph{bands}, among other elements. Each band belongs to a \emph{batch}. Band batches are acquired from different providers and, for each batch, some \emph{parameters} are measured to verify its quality. On the other hand, each \emph{DriveHalfShaft} is mounted in a \emph{session} of an assembling machine. This machine has several sensors that monitor different parameters such as \emph{temperature}, \emph{humidity}, or the \emph{pressure} in the pneumatic pliers system before tightening each band.

As it can be seen in Listing~\ref{lst:domainData}, these data are linked and nested. For example, the drive half-shaft with id \emph{5209} (Listing~\ref{lst:domainData}, line 1) is linked to some \emph{wheelBand} data (Listing~\ref{lst:domainData}, lines 2-6), as well as some \emph{assembly session} data, among others. Moreover, each assembly session data contains four nested measures of the pressure in pneumatic pliers system. Therefore, before executing any data analysis algorithm, we would need to convert this linked and nested data into a single vector of information that can be placed in just one row of a dataset, such as depicted in Figure~\ref{fig:finalTabularResult}.

\begin{figure}[!tb]
	\centering
	\renewcommand{\arraystretch}{0.85}
	\begin{scriptsize}
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\toprule
			id &  \dots & humidity & temperature & 1\_pressure & 2\_pressure & 3\_pressure & 4\_pressure \\
			\midrule
			5209 & \dots  & 56.7 &20.4 & 7.9 & 7.9 & 7.8 & 7.9 \\
			6556 & \dots  & 58.0 & 19.8 & 7.2 & 7.1 & 7.1 & 7.1 \\
			\dots & \dots  & \dots  & \dots  & \dots  & \dots & \dots & \dots \\
			\bottomrule
		\end{tabular} \\
	\end{scriptsize}
	\caption{A tabular dataset for drive half-shaft analysis.}
	\label{fig:finalTabularResult}
\end{figure}

To rearrange domain data, data scientists rely on data management languages or libraries, such as SQL~\citep{Beighley2007} or Pandas~\citep{McKinney2010}. Using these technologies, data scientists concatenate several low-level operations, such as \emph{filters}, \emph{joins}, \emph{pivots} or \emph{aggregations}, until reaching the desired tabular format. As an example, Listing~\ref{lst:sqlExample} shows an SQL script for combining data from the \emph{AssemblySession}, \emph{MonitoredParameters} and \emph{PliersData} classes to create a tabular representation like depicted in Figure~\ref{fig:finalTabularResult}. This script needs to place each measure of the pressure in the pliers pneumatic system in a separated column, which is achieved by using a nested query for each specific tightening (Listing~\ref{lst:sqlExample}, lines 2-9).

\begin{minipage}{\linewidth}
	\begin{center}
		\lstinputlisting[style=sql, caption={SQL script to rearrange \emph{Assembly} information as tabular data.},label=lst:sqlExample,captionpos=b,basicstyle=\scriptsize]{listings/sqlExample.sql}
	\end{center}
\end{minipage}

As it can be seen, this script is quite complex as compared to the example size and it has obvious scalability problems if the number of tightenings grows. Similar problems are found when other data management languages, such as Pandas~\citep{McKinney2010} or R~\citep{R2020}, are used. As a consequence, dataset creation becomes a labor intensive and prone to errors process. To overcome this problem, researchers of the group  developed a language called \emph{Lavoisier}~\citep{DelaVega2019,DelaVega2020}, which is described in the next section. 

\section{Lavoisier}\label{sec:lavoisier}

\emph{Lavoisier}~\citep{DelaVega2019,DelaVega2020} is a declarative language for data selection and formatting that provides a set of high-level primitives that allows focusing on what data must be selected and skips the details of how these data need to be transformed to be formatted as a dataset. We illustrate how Lavoisier works using the example of Listing~\ref{lst:lavoisier}, which serves to create a dataset containing data from just the compliance reports and the assembly session for each drive half-shaft.

In Lavoisier, we must first specify the name of the dataset that will be generated, which is \emph{driveHalfShaft\_AssemblyParameters} in our example (Listing~\ref{lst:lavoisier}, line 1). Then, we must specify what are the main entities that will be analyzed, for which we use the \emph{mainclass} keyword. In our case, the \emph{DriveHalfShaft} class will represent these main entities (Listing~\ref{lst:lavoisier}, line 2). It is worth remembering that, according to the \emph{one entity, one row} constraint, all the information about each instance of these entities must be placed in a single row of the output dataset. 

\noindent
\begin{minipage}{\linewidth}
	\lstinputlisting[style=lv,caption={An example of Lavoisier specification.},label=lst:lavoisier,captionpos=b,basicstyle=\scriptsize]{listings/lavoisierScript.txt}
\end{minipage}

When a class is selected to be included in a dataset, Lavoisier adds all attributes of that class to the output dataset, whereas references to other classes are excluded by default. This behavior can be modified according to our needs. A concrete subset of attributes of a class can be selected by listing them between square brackets after the class name. For example, Listing~\ref{lst:lavoisier}, line 2 specifies that just the \emph{id} of each half-shaft has to be included in the output dataset.

Regarding references, we can add them using the \emph{include} keyword. This primitive works differently depending on the reference multiplicity. For single-bounded references, i.e., references with upper bound lower than or equal to one, we just need to specify the name of the reference to be added. For example, Listing~\ref{lst:lavoisier}, line 3 incorporates the compliance report of each half-shaft, retrieved through the \emph{report} reference, to the output dataset.

For each included reference, the default Lavoisier behavior is executed again. This is, attributes are automatically included and references are excluded. As before, this default behavior can be modified by specifying a list of concrete attributes to be included between square brackets and new \emph{include} statements that add references of the reference. For instance, Listing~\ref{lst:lavoisier}, line 5 adds the \emph{parameters} reference of the \emph{assemblySession} reference to the resulting dataset. This recursive inclusion can continue as far as we need.

To process reference inclusion, the Lavoisier interpreter relies on a set of transformation patterns~\citep{DelaVega2017,DelaVega2019} that automatically reduce a set of interconnected classes into a single class from which a tabular dataset is directly generated. In the case of single-bounded references, the Lavoisier interpreter simply adds the attributes of the referenced class to the class holding the reference. For example, in Listing~\ref{lst:lavoisier}, line 3, the attributes of the \emph{ComplianceReport} class are added to the \emph{DriveHalfShaft} class, which would be equivalent to performing a \emph{left outer join} between these classes. To handle nested references, the Lavoisier interpreter processes the deepest reference first and repeats the process until reaching the main class.

The case of multibounded references, i.e., references with upper bound greater than or equal to one, is more challenging, since each instance of the referencing class is related to several instances of the referenced class. So, to satisfy the \emph{one entity, one row} constraint, we need to find a mechanism to spread data of each one of these instances into a single row. To achieve this goal, Lavoisier creates a specific set of columns for each instance that might appear in a multibounded reference. This way, each instance is able to find a well-defined set of columns where to place its data.

However, this strategy requires that Lavoisier can distinguish between different referenced instances. For this purpose, we must use one or more attributes of the referenced class as identifiers for the instances. In Lavoisier, these identifier attributes are specified after a \emph{by} keyword, which is added to \emph{include} statements involving multibounded references. As an example, Listing~\ref{lst:lavoisier}, line 6 specifies that the \emph{pliersTightenings} reference must be added to the output dataset. This reference contains four measures of the pressure in the pliers pneumatic system (see Listing~\ref{lst:domainData}, lines 11-14). Each measure is identified by the number of the tightening in which it was taken.

To process these multibounded references, the Lavoisier interpreter carries out the following actions: first, all distinct values for the identifier attributes are calculated. In our example, these values are \emph{\{1, 2, 3, 4\}}. They represent all potential kinds of instances in the multibounded reference. Then, the set of non-identifier attributes of the referenced class is calculated. In our case, this set is \emph{\{pressure\}}. This set represents the information to be specified for each kind of instance. Next, to place the information of each instance kind, for each identifier value and each element in the set of non-identifier attributes, a column is added to the output dataset. Each one of these columns will have as name the name of the respective non-identifier attribute prefixed with its corresponding identifier value. For example, in our case, these generated columns are \emph{\{\{1\_pressure\},\{2\_pressure\},\{3\_pressure\},\{ 4\_pressure\}\}}. This structure is illustrated in Figure~\ref{fig:finalTabularResult}. For the sake of clarity, we would like to mention that, if the referenced class had contained an additional non-identifier attribute, such as \emph{tighteningTime}, the resulting set of columns would have been \emph{\{\{1\_pressure\}, \{1\_tighteningTime\}, \{2\_pressure\}, \{2\_tighteningTime\}, \{3\_pressure\}, \{3\_tighteningTime\}, \{ 4\_pressure\}, \{4\_tighteningTime\}\}}.

Formally speaking, the processing of a multibounded references would be equivalent to joining both classes first and then pivoting the result using the identifier attributes as pivoting elements. The processing of multibounded references contains a lot of picky details that we are omitting here for the sake of simplicity. We refer the interested reader to work where Lavoisier is described in more detail~\citep{DelaVega2017,DelaVega2019,DelaVega2020}.

Multibounded references can also be processed using \emph{aggregation functions}. These functions calculate values that summarize the set of the instances of the multibounded references. Thus, we can use these values as representatives of the collection instead of including its elements individually in the output dataset. For instance, in the case of the \emph{pliersTightenings}, we might use the \emph{average pressure} in the pliers pneumatic system instead of the values of the pressure for each individual tightening.

Finally, Lavoisier provides some primitives to deal with inheritance hierarchies. These primitives are not described here for the sake of brevity. Again, we refer the interested reader to work describing Lavoisier in detail~\citep{DelaVega2019,DelaVega2017,DelaVega2020}.

By using Lavoisier, the total number of operations required to generate a dataset decreases by $\sim$40\%, and script size reduces by $\sim$60\% as compared to other languages used for this purpose, such as SQL or Pandas. Therefore, it was considered that Lavoisier might be helpful for data selection in Industry 4.0. However, when their authors started to explore this idea, they soon realized that object-oriented data models are not commonly used in manufacturing settings, but they discovered that industrial engineers were quite familiar with a kind of diagram, called \emph{fishbone diagrams}, that might be used to represent relationships between domain data. These diagrams are described in next section.

\section{Fishbone Diagrams}\label{sec:back:fishbone}

\emph{Fishbone diagrams}~\citep{Ishikawa1976}, also known as \emph{Cause-Effect}, \emph{Ishikawa} or \emph{Fishikawa diagrams}, aim to identify causes that might lead to a certain effect. They were formally proposed by Kaoru Ishikawa and they are acknowledged nowadays as one of the seven basic tools for quality control and process improvement in manufacturing settings~\citep{Tague2005}.

\begin{figure}[!b]
	\centering
	% Requires \usepackage{graphicx}
	\includegraphics[width=\linewidth]{Chapter2/Figs/fallingBandIshikawa.pdf} \\
	\caption{A fishbone diagram for the drive half-shaft running example (taken from~\citep{Dziuba2013}).}
	\label{fig:fishboneDiagram}
\end{figure}

Figure~\ref{fig:fishboneDiagram} shows a fishbone diagram, taken from the literature~\citep{Dziuba2013}, for our running example. As it can be observed, these diagrams are called fishbone diagrams because of its shape, which resembles a fish skeleton. They are elaborated as follows. First, the effect to be analyzed is placed on the right, commonly inside a rectangle or a circle, representing the fish head. In our case, \emph{falling bands on the wheel side} would be the problem, or effect, to be studied. Then, a horizontal line from right to left, representing the fish main bone, is added. This line is used to connect causes with the effect.

Then, we start to identify causes that might lead to the effect. For each identified cause, we would try to find subcauses that might lead to that cause, repeating recursively the process until reaching a decomposition level that can be considered as adequate. To help to start the process, a set of predefined main causes, known as \emph{categories}, are used in each domain. These categories are attached as ribs to the main fishbone. In the case of the manufacturing domain, these predefined categories, known as the \emph{5 Ms model}~\citep{Tague2005}, are \emph{Material}, \emph{Man}, \emph{Machine}, \emph{Method} and \emph{Management}, as illustrated in Figure~\ref{fig:fishboneDiagram}.

\emph{Material} specifies causes related to the source materials used during the manufacturing process. For instance, a wheeling band might fall because an unsuitable band has been used (Figure~\ref{fig:fishboneDiagram}, cause 1.1). A band might be unsuitable either because a wrong model is used or because its parameters are not adequate. For instance, the band might be thicker than required. \emph{Man} collects causes related to people participating in the process, such as the operator selecting a wrong assembly program (Figure~\ref{fig:fishboneDiagram}, cause 2.1). \emph{Machine} analyses elements of the machines and tools used in the process, such as, for instance, the pressure in the pliers pneumatic system (Figure~\ref{fig:fishboneDiagram}, cause 3.2). Finally, \emph{Method} refers to the procedure used to manufacture an item, such as machine configurations; whereas \emph{Management} considers general aspects of the process, as adequate working conditions or workers' motivation. It should be noted that more categories might be added if it is considered helpful. Indeed, in some manufacturing companies, these categories are complemented with an extra E that represents the \emph{Environment}, which is known as the 5M + 1E model. 

\section{A New Language for Dataset Generation in Industry 4.0}
\label{sec:problemStatement}

As previously stated, object-oriented data models are rarely found in manufacturing settings, which may hamper the adoption of Lavoisier in the development of Industry 4.0 applications. On the other hand, fishbone diagrams are a common and well-known tool for quality control in these contexts. These diagrams establish cause-effect relationships between domain elements, but they can also be used to describe influence relationships between domain data, helping to decide what concrete data might be useful to include in a dataset for analyzing a specific phenomenon. Hence, it was decided that it would be desirable that Lavoisier worked with fishbone diagrams instead of with object-oriented data models. This decision lead to a new research work, in which the author of this work participated actively, and whose results are described in the next chapter.     