/**
 */
package FallingBand;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Housing Parameters</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.HousingParameters#getMaxLength <em>Max Length</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getMinLength <em>Min Length</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getAvgLength <em>Avg Length</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getMaxDiameter <em>Max Diameter</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getMinDiameter <em>Min Diameter</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getAvgDiameter <em>Avg Diameter</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getBatchId <em>Batch Id</em>}</li>
 *   <li>{@link FallingBand.HousingParameters#getProvider <em>Provider</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getHousingParameters()
 * @model
 * @generated
 */
public interface HousingParameters extends EObject {
	/**
	 * Returns the value of the '<em><b>Max Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Length</em>' attribute.
	 * @see #setMaxLength(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_MaxLength()
	 * @model
	 * @generated
	 */
	Double getMaxLength();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getMaxLength <em>Max Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Length</em>' attribute.
	 * @see #getMaxLength()
	 * @generated
	 */
	void setMaxLength(Double value);

	/**
	 * Returns the value of the '<em><b>Min Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Length</em>' attribute.
	 * @see #setMinLength(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_MinLength()
	 * @model
	 * @generated
	 */
	Double getMinLength();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getMinLength <em>Min Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Length</em>' attribute.
	 * @see #getMinLength()
	 * @generated
	 */
	void setMinLength(Double value);

	/**
	 * Returns the value of the '<em><b>Avg Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Length</em>' attribute.
	 * @see #setAvgLength(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_AvgLength()
	 * @model
	 * @generated
	 */
	Double getAvgLength();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getAvgLength <em>Avg Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Length</em>' attribute.
	 * @see #getAvgLength()
	 * @generated
	 */
	void setAvgLength(Double value);

	/**
	 * Returns the value of the '<em><b>Max Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Diameter</em>' attribute.
	 * @see #setMaxDiameter(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_MaxDiameter()
	 * @model
	 * @generated
	 */
	Double getMaxDiameter();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getMaxDiameter <em>Max Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Diameter</em>' attribute.
	 * @see #getMaxDiameter()
	 * @generated
	 */
	void setMaxDiameter(Double value);

	/**
	 * Returns the value of the '<em><b>Min Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Diameter</em>' attribute.
	 * @see #setMinDiameter(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_MinDiameter()
	 * @model
	 * @generated
	 */
	Double getMinDiameter();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getMinDiameter <em>Min Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Diameter</em>' attribute.
	 * @see #getMinDiameter()
	 * @generated
	 */
	void setMinDiameter(Double value);

	/**
	 * Returns the value of the '<em><b>Avg Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Diameter</em>' attribute.
	 * @see #setAvgDiameter(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_AvgDiameter()
	 * @model
	 * @generated
	 */
	Double getAvgDiameter();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getAvgDiameter <em>Avg Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Diameter</em>' attribute.
	 * @see #getAvgDiameter()
	 * @generated
	 */
	void setAvgDiameter(Double value);

	/**
	 * Returns the value of the '<em><b>Batch Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Batch Id</em>' attribute.
	 * @see #setBatchId(Double)
	 * @see FallingBand.FallingPackage#getHousingParameters_BatchId()
	 * @model
	 * @generated
	 */
	Double getBatchId();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getBatchId <em>Batch Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Batch Id</em>' attribute.
	 * @see #getBatchId()
	 * @generated
	 */
	void setBatchId(Double value);

	/**
	 * Returns the value of the '<em><b>Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider</em>' containment reference.
	 * @see #setProvider(Provider)
	 * @see FallingBand.FallingPackage#getHousingParameters_Provider()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Provider getProvider();

	/**
	 * Sets the value of the '{@link FallingBand.HousingParameters#getProvider <em>Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provider</em>' containment reference.
	 * @see #getProvider()
	 * @generated
	 */
	void setProvider(Provider value);

} // HousingParameters
