effect FallingBand is DriveHalfShaft
    include report
category Material
  cause Bands realizes `Unsuitable band' contains {
    cause WheelBand contains {
      cause Model realizes `Wrong band model' is
        wheelBand.model
      cause Parameters realizes `Inadequate parameters' is
        wheelBand.parameters { include provider }
    } -- WheelBand
    cause EngineBand contains { ... }
  }  -- Bands
  cause Shaft realizes `Unsuitable shaft' contains {...}
category Machine
  cause PneumaticPliersPressure realizes 
    `Pressure in the pneumatic system too low' is
    assemblySession.parameters.plierFastenings by number
...
category Management
  cause WorkingConditions 
    realizes `Unsuitable Work Conditions' notMapped 