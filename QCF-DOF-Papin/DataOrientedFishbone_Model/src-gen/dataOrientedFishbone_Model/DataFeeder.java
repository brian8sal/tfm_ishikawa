/**
 */
package dataOrientedFishbone_Model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Feeder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dataOrientedFishbone_Model.DataFeeder#getDomainElementSelector <em>Domain Element Selector</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.DataFeeder#getAttributeFilter <em>Attribute Filter</em>}</li>
 *   <li>{@link dataOrientedFishbone_Model.DataFeeder#getIncludedReferences <em>Included References</em>}</li>
 * </ul>
 *
 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getDataFeeder()
 * @model
 * @generated
 */
public interface DataFeeder extends EObject {
	/**
	 * Returns the value of the '<em><b>Domain Element Selector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Element Selector</em>' attribute.
	 * @see #setDomainElementSelector(String)
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getDataFeeder_DomainElementSelector()
	 * @model
	 * @generated
	 */
	String getDomainElementSelector();

	/**
	 * Sets the value of the '{@link dataOrientedFishbone_Model.DataFeeder#getDomainElementSelector <em>Domain Element Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Element Selector</em>' attribute.
	 * @see #getDomainElementSelector()
	 * @generated
	 */
	void setDomainElementSelector(String value);

	/**
	 * Returns the value of the '<em><b>Attribute Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Filter</em>' containment reference.
	 * @see #setAttributeFilter(AttributeFilter)
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getDataFeeder_AttributeFilter()
	 * @model containment="true"
	 * @generated
	 */
	AttributeFilter getAttributeFilter();

	/**
	 * Sets the value of the '{@link dataOrientedFishbone_Model.DataFeeder#getAttributeFilter <em>Attribute Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Filter</em>' containment reference.
	 * @see #getAttributeFilter()
	 * @generated
	 */
	void setAttributeFilter(AttributeFilter value);

	/**
	 * Returns the value of the '<em><b>Included References</b></em>' containment reference list.
	 * The list contents are of type {@link dataOrientedFishbone_Model.IncludedReference}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Included References</em>' containment reference list.
	 * @see dataOrientedFishbone_Model.DataOrientedFishbone_ModelPackage#getDataFeeder_IncludedReferences()
	 * @model containment="true"
	 * @generated
	 */
	EList<IncludedReference> getIncludedReferences();

} // DataFeeder
