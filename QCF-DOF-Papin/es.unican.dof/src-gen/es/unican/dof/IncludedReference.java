/**
 * generated by Xtext 2.25.0
 */
package es.unican.dof;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Included Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link es.unican.dof.IncludedReference#getName <em>Name</em>}</li>
 *   <li>{@link es.unican.dof.IncludedReference#getPivotingId <em>Pivoting Id</em>}</li>
 *   <li>{@link es.unican.dof.IncludedReference#getInstancesFilter <em>Instances Filter</em>}</li>
 * </ul>
 *
 * @see es.unican.dof.DofPackage#getIncludedReference()
 * @model
 * @generated
 */
public interface IncludedReference extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see es.unican.dof.DofPackage#getIncludedReference_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link es.unican.dof.IncludedReference#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Pivoting Id</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pivoting Id</em>' containment reference.
   * @see #setPivotingId(Path)
   * @see es.unican.dof.DofPackage#getIncludedReference_PivotingId()
   * @model containment="true"
   * @generated
   */
  Path getPivotingId();

  /**
   * Sets the value of the '{@link es.unican.dof.IncludedReference#getPivotingId <em>Pivoting Id</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pivoting Id</em>' containment reference.
   * @see #getPivotingId()
   * @generated
   */
  void setPivotingId(Path value);

  /**
   * Returns the value of the '<em><b>Instances Filter</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Instances Filter</em>' containment reference.
   * @see #setInstancesFilter(BooleanExpression)
   * @see es.unican.dof.DofPackage#getIncludedReference_InstancesFilter()
   * @model containment="true"
   * @generated
   */
  BooleanExpression getInstancesFilter();

  /**
   * Sets the value of the '{@link es.unican.dof.IncludedReference#getInstancesFilter <em>Instances Filter</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Instances Filter</em>' containment reference.
   * @see #getInstancesFilter()
   * @generated
   */
  void setInstancesFilter(BooleanExpression value);

} // IncludedReference
