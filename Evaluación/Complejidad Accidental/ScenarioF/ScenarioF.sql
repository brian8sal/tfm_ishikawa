SELECT d.id,
   c.fallingWheelBand,
   bw.model as modelwheelEngine,
   be.model,
   bbw.maxThickness as maxThicknesswheelEngine,
   bbw.minThickness as wMinThicknesswheelEngine,
   bbw.avgThickness as wAvgThicknesswheelEngine,
   bbe.maxThickness,
   bbe.minThickness,
   bbe.avgThickness,
   pw.name as namewheelEngine,
   pe.name
   FROM DriveHalfShaft d JOIN ComplianceReport c
      ON d.idComplianceReport = c.id
   JOIN
   (Band bw JOIN BandBatchParameters bbw
         ON bw.batchId = bbw.batchId
         JOIN Provider pw
         ON pw.id = bbw.idProvider)
   ON d.idBandWheel = bw.id
   JOIN
   (Band be JOIN BandBatchParameters bbe
         ON be.batchId = bbe.batchId
         JOIN Provider pe
         ON pe.id = bbe.idProvider)
   ON d.idBandEngine = be.id;
