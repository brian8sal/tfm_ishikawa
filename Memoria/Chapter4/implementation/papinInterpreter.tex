% !TeX spellcheck = en_US

\section{Papin Interpreter}
\label{sec:papinInterpreter}

Finally, once the editors for the three DSLs had been created, we developed an interpreter, or language processor, that accepted a Papin specification as input and generated a CSV file containing the required dataset as output.
Although this interpreter needed to access the object-oriented model and the DOF specification, it was associated to Papin since it is executed from this language. 

To process the Lavoisier expressions of the data-linked causes, the Papin interpreter delegates on the Lavoisier one, which is who actually evaluates these expressions. 
As a result of these evaluations, the Lavoisier interpreter returns \emph{ColumnSets}, which are a special kind of data structures, inspired on Pandas dataframes, that can be considered as an in-memory representation of a dataset that, in addition, provides  a set of methods to manage it. 

With these preliminary considerations in mind, the process executed by the Papin interpreter can be described as follows: first of all, the interpreter loads the domain model and the domain data. 
Then, for each dataset definition, it executes the following procedure (see Algorithm~\ref{alg:dataset}):

\begin{enumerate}
	\item First, an empty \emph{ColumnSet}, representing the output result, is created.
	\item Then, for each included category, its leaves causes are calculated using the Algorithm~\ref{alg:extractLeaves}, which basically traverses the hierarchy of \emph{IncludeCategory} and \emph{IncludeCauses} blocks (see Figure~\ref{fig:papinMetamodel}) to calculate its leaves elements, this is, the categories and causes explicitly listed to be included in the output dataset.
	\item Next, each leaf cause is evaluated according to the Algorithm~\ref{alg:evaluateCause}. Each one of these evaluations generates a \emph{ColumnSet} with a dataset including the information required to just analyze the evaluated cause. Thus, this dataset is joined to the output dataset, 
	where all these partial datasets are aggregated to create the global dataset.
	\item Finally, the \emph{ColumnSet} representing the global dataset is exported to a file in the CSV format.
\end{enumerate}

\begin{algorithm}[!tb]
	\SetKwProg{GeneratesDataset}{GeneratesDataset}{}{}	
	\SetKwInOut{Input}{In}
	\SetKwInOut{Output}{Out}

	\GeneratesDataset{$(d)$}{
		\Input{An instance $d$ of the Dataset metaclass}
		\Output{A CSV file}
		$outputColumnSet \gets new ColumnSet()$\;
		\ForEach{$c \in d.includedCategories$}{
			$leavesCauses \gets extractLeavesCauses(c)$\;
			\ForEach{$c \in leavesCauses$}{
				$causeDataset \gets evaluate(c)$\;
				$outputColumnSet \gets leftOuterJoin(outputColumnSet,cuaseDataset)$\;
			}
		}
		$columnSetToCSV(outputColumnSet)$\;
	}
	\caption{Algorithm for processing dataset definitions}
	\label{alg:dataset}
\end{algorithm}

\begin{algorithm}[!tb]
	\SetKwProg{extractLeafCauses}{extractLeafCauses}{}{}	
	\SetKwInOut{Input}{In}
	\SetKwInOut{Output}{Out}
	
	\extractLeafCauses{$(e)$}{
		\Input{An category or cause $e$}
		\Output{A set of causes/categories}
		$outputCauses \gets \emptyset$\;
		\If{$containsSublements(e)$}{
			\ForEach{$se \in subelements(e)$}{
				$outputCauses \gets outputCauses \bigcup extractLeaveCauses(se)$\;
			}
		\Else{
			$outputCauses \gets \{d\}$\;
		}
		}
		\Return $outputCauses$;
	}
	\caption{Algorithm for extracting leaves elements of a Papin specification}
	\label{alg:extractLeaves}
\end{algorithm}

To evaluate each cause, the Papin interpreter checks first which kind of cause is (see  Algorithm~\ref{alg:evaluateCause}). 
If it is a data-linked cause, The Lavoisier interpreter is invoked to process the corresponding data feeder. 
the Lavoisier interpreter returns a \emph{ColumnSet}, which will be the result of the cause evaluation. 
For compound causes, the Papin interpreter extracts all their subcauses and evaluates them. 
Each subcause evaluation produces a \emph{ColumnSet}. 
All these \emph{ColumnSet} are properly merged into a single one, which is returned as the result of the cause evaluation. 
Finally, in the case of not mapped causes, they are simply ignored, generating an empty \emph{ColumnSet} as output.

\begin{algorithm}[!tb]
	\SetKwProg{evaluateCause}{evaluateCause}{}{}	
	\SetKwInOut{Input}{In}
	\SetKwInOut{Output}{Out}
	
	\evaluateCause{$(c)$}{
		\Input{An cause $c$}
		\Output{A ColumnSet}
		$outputColumnSet \gets \emptyset$\;
		\If{isDataLinkedCause(c)}{
			$outputColumnSet \gets lavoisier.evaluates(e.dataFeeder) $\;
		}
		\ElseIf{isCompoundCause(c)}{
			\ForEach{$sc \in subcauses(c)$}{
				$outputCauses \gets leftOuterJoin(outputCauses,evaluateCause(sc))$
			}
		}
		\Return $outputColumnSet$;
	}
	\caption{Algorithm for evaluating causes}
	\label{alg:evaluateCause}
\end{algorithm}


%% NOTA: This might be nicely expressed using the algorithm package

%% In this case, the domain model and its instance should not be accessed directly, since Papin itself does not deal explicitly with its elements, but rather with a DOF, which is in charge of linking causes with the domain elements. Thus,  it takes advantage of its link with the DOF, provided by the specification of the DOF's effect, to extract the data of interest. At this point, the Papin specification has already been linked to the domain model and the DOF, giving the interpreter direct access to both.

%% After this, the Papin interpreter may encounter several situations. The first one consists in finding that the specification, besides containing a domain model, an instance of it, the dataset's name and the DOF's effect, it only contains the name of one of the DOF categories. In this case, the interpreter recognizes that all the causes belonging to that category are relevant, so it runs through them all and extracts the data from the \emph{DataLinkedCauses} by means of its Lavoisier-like statements. With these statements, the instances of the main class to be included in the output dataset are gathered, taking into account the specified filters. Then, the attribute and reference inclusions constructs are processed to determine the different sets of columns that must be generated, in the same order that they were defined. Lastly, these columns are calculated for each gathered instance of the main class, and placed as rows in the output dataset. This dataset is provided as a \emph{ColumnSet}, a data structure created for the Lavoisier implementation that is similar to a Pandas \emph{DataFrame}, and that will be exported in CSV format.

%% In addition to the previous situation, the Papin interpreter may encounter others, such as one that only specifies a \emph{Category} and a \emph{DataLinkedCause} belonging to it, proceeding to the construction of the \emph{ColumnSet} directly with the data indicated by the cause. Another would be the case of finding a \emph{CompoundCause} in the specification, applying the aforementioned process recursively. Finally, it could also encounter the specification of a \emph{NotMappedCause}, a scenario in which the cause would be ignored because it has no linked data to include in the dataset.
