/**
 * generated by Xtext 2.25.0
 */
package es.unican.papin;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Less Than</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see es.unican.papin.PapinPackage#getLessThan()
 * @model
 * @generated
 */
public interface LessThan extends Comparison
{
} // LessThan
