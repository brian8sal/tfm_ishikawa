/**
 */
package FallingBand;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assembly Session</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FallingBand.AssemblySession#getStart <em>Start</em>}</li>
 *   <li>{@link FallingBand.AssemblySession#getStop <em>Stop</em>}</li>
 *   <li>{@link FallingBand.AssemblySession#getParameters <em>Parameters</em>}</li>
 *   <li>{@link FallingBand.AssemblySession#getMachine <em>Machine</em>}</li>
 *   <li>{@link FallingBand.AssemblySession#getProgram <em>Program</em>}</li>
 * </ul>
 *
 * @see FallingBand.FallingPackage#getAssemblySession()
 * @model
 * @generated
 */
public interface AssemblySession extends EObject {
	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(Date)
	 * @see FallingBand.FallingPackage#getAssemblySession_Start()
	 * @model
	 * @generated
	 */
	Date getStart();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(Date value);

	/**
	 * Returns the value of the '<em><b>Stop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stop</em>' attribute.
	 * @see #setStop(Date)
	 * @see FallingBand.FallingPackage#getAssemblySession_Stop()
	 * @model
	 * @generated
	 */
	Date getStop();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getStop <em>Stop</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stop</em>' attribute.
	 * @see #getStop()
	 * @generated
	 */
	void setStop(Date value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference.
	 * @see #setParameters(MonitoredParameters)
	 * @see FallingBand.FallingPackage#getAssemblySession_Parameters()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MonitoredParameters getParameters();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getParameters <em>Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameters</em>' containment reference.
	 * @see #getParameters()
	 * @generated
	 */
	void setParameters(MonitoredParameters value);

	/**
	 * Returns the value of the '<em><b>Machine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine</em>' containment reference.
	 * @see #setMachine(Machine)
	 * @see FallingBand.FallingPackage#getAssemblySession_Machine()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Machine getMachine();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getMachine <em>Machine</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine</em>' containment reference.
	 * @see #getMachine()
	 * @generated
	 */
	void setMachine(Machine value);

	/**
	 * Returns the value of the '<em><b>Program</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' containment reference.
	 * @see #setProgram(Program)
	 * @see FallingBand.FallingPackage#getAssemblySession_Program()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Program getProgram();

	/**
	 * Sets the value of the '{@link FallingBand.AssemblySession#getProgram <em>Program</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' containment reference.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(Program value);

} // AssemblySession
